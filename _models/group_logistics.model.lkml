connection: "chb-prod-supplychain-data"
include: "../views/group_log_views/*"
include: "../views/group_supply_chain_views/*"
include: "../views/_common/*.view"
include: "../views/_common/dim_suggest/*.view"

week_start_day: sunday
label: "Group Logistics"


explore: logitude_summary {

  label: "Logitude Summary"
  join: tp95_logitude {
    view_label: "TP 95 table"
    type: left_outer
    relationship: one_to_one
    sql_on: ${logitude_summary.shipment_id} = ${tp95_logitude.shipment_id} AND ${logitude_summary.direction} = ${tp95_logitude.direction} AND ${logitude_summary.main_carriage_transport_mode} = ${tp95_logitude.main_carriage_transport_mode} AND ${logitude_summary.country_of_departure} = ${tp95_logitude.country_of_departure} AND ${logitude_summary.country_of_destination} = ${tp95_logitude.country_of_destination} AND ${logitude_summary.is_courier} = ${tp95_logitude.is_courier} AND ${logitude_summary.dgr} = ${tp95_logitude.dgr}  ;;
  }

}

explore: logitude_summary_MC {
  from:  logitude_summary
  label: "Logitude Summary - Managed Companies"
  sql_always_where: ${vertical_shipper} ="MANAGED COMPANIES" OR ${vertical_consignee} = "MANAGED COMPANIES";;
  join: tp95_logitude {
    view_label: "TP 95 table"
    type: left_outer
    relationship: one_to_one
    sql_on: ${logitude_summary_MC.shipment_id} = ${tp95_logitude.shipment_id} AND ${logitude_summary_MC.direction} = ${tp95_logitude.direction} AND ${logitude_summary_MC.main_carriage_transport_mode} = ${tp95_logitude.main_carriage_transport_mode} AND ${logitude_summary_MC.country_of_departure} = ${tp95_logitude.country_of_departure} AND ${logitude_summary_MC.country_of_destination} = ${tp95_logitude.country_of_destination} AND ${logitude_summary_MC.is_courier} = ${tp95_logitude.is_courier} AND ${logitude_summary_MC.dgr} = ${tp95_logitude.dgr}  ;;
  }
  access_filter: {
    field: vertical_managed
    user_attribute: vertical
  }
}

explore: logitude_summary_JV {
  from:  logitude_summary
  label: "Logitude Summary - Joined Ventures"
  sql_always_where: (${vertical_shipper} = "JOINT VENTURES" OR ${vertical_consignee} = "JOINT VENTURES") AND (${brand_shipper_accessfilter} LIKE ('{{ _user_attributes['brand'] }}') OR ${brand_consignee_accessfilter} LIKE ('{{ _user_attributes['brand'] }}')) ;;
  join: tp95_logitude {
    view_label: "TP 95 table"
    type: left_outer
    relationship: one_to_one
    sql_on: ${logitude_summary_JV.shipment_id} = ${tp95_logitude.shipment_id} AND ${logitude_summary_JV.direction} = ${tp95_logitude.direction} AND ${logitude_summary_JV.main_carriage_transport_mode} = ${tp95_logitude.main_carriage_transport_mode} AND ${logitude_summary_JV.country_of_departure} = ${tp95_logitude.country_of_departure} AND ${logitude_summary_JV.country_of_destination} = ${tp95_logitude.country_of_destination} AND ${logitude_summary_JV.is_courier} = ${tp95_logitude.is_courier} AND ${logitude_summary_JV.dgr} = ${tp95_logitude.dgr}  ;;
  }
  access_filter: {
    field: vertical_joint
    user_attribute: vertical
  }
}

# explore: oms_x_carriyo {
#   label: "Store Fulfillment"

#   access_filter: {
#     # field: brand_vertical_mapping.bu_brand
#     field: bu_brand_for_access_filter
#     user_attribute: brand
#   }
#   access_filter: {
#     # field: brand_vertical_mapping.vertical
#     field: vertical_for_access_filter
#     user_attribute: vertical
#   }
#   join:brand_vertical_mapping_log {
#     type: left_outer
#     relationship: many_to_one
#     sql_on:
#       ${oms_x_carriyo.merchant}=${brand_vertical_mapping_log.bu_brand};;
#   }

# }

# explore: store_collection_schedule {
#   label: "Store Collection Schedule"
#   from: store_collection_schedule
# }

# map_layer: saudi_map_layer {
#   file: "/maps/saudi_map.topojson"
#   property_key: "ecom_pickup_region"
# }

# explore: carriyo_unified {
#   always_filter: {
#     filters: {
#       field: carrier
#       value: "-NULL"
#     }
#   }
#   #

#   label: "Last Mile Analysis"


#   access_filter: {
#     # field: brand_vertical_mapping.bu_brand
#     field: bu_brand_for_access_filter
#     user_attribute: brand
#   }
#   access_filter: {
#     # field: brand_vertical_mapping.vertical
#     field: vertical_for_access_filter
#     user_attribute: vertical
#   }
#   join:brand_vertical_mapping_log {
#     type: left_outer
#     relationship: many_to_one
#     sql_on:
#       ${carriyo_unified.merchant}=${brand_vertical_mapping_log.bu_brand};;
#   }


#   # join:carriyo_unified_previous_day {
#   #   type: left_outer
#   #   relationship: many_to_one
#   #   sql_on:
#   #     ${carriyo_unified.update_date_date}  = ${carriyo_unified_previous_day.update_date_date}
#   #       ;;

#   #   }

# }



# #   explore: carriyo_date_master {
# #       label: "Backlog Tracker (BETA)"

# #     join: carriyo_unified {
# #       sql_where: latest_status =1  ;;

# #       sql_on: 1=1;;
# #       }

# #       access_filter: {
# #         # field: brand_vertical_mapping.bu_brand
# #         field: carriyo_unified.bu_brand_for_access_filter
# #         user_attribute: brand
# #       }
# #       access_filter: {
# #         # field: brand_vertical_mapping.vertical
# #         field: carriyo_unified.vertical_for_access_filter
# #         user_attribute: vertical
# #       }

# #       join:brand_vertical_mapping_log {
# #         type: left_outer
# #         relationship: many_to_one
# #         sql_on:
# #         ${carriyo_unified.merchant}=${brand_vertical_mapping_log.bu_brand};;
# #         }

# # }




# # explore: backlog_calc_forecast {
# #   label: "Backlog Tracker (BETA)"
# #   hidden: yes
# #   join: carriyo_unified {
# #     # sql_where: latest_status =1  ;;
# #     type: full_outer
# #     relationship: one_to_many
# #     sql_on: ${backlog_calc_forecast.date_date} = ${carriyo_unified.collected_at_latest_date}
# #     and ${backlog_calc_forecast.tbd_date} = ${carriyo_unified.to_be_next_attempted_date}
# #     and ${backlog_calc_forecast.merchant} = ${carriyo_unified.merchant}
# #     and ${backlog_calc_forecast.country} =${carriyo_unified.pickup_country}
# #     and ${backlog_calc_forecast.service_type} = ${carriyo_unified.service_type}
# #     and ${carriyo_unified.latest_status} =1
# #     and ${carriyo_unified.SLA_REQ} <>'MISSING_SLA';;
# #   }

# #   join: carriyo_backlog {
# #     relationship: many_to_many
# #     # sql_on: ${carriyo_backlog.core_date_date} = ${carriyo_unified.collected_at_latest_date} ;;
# #     sql_on: ${carriyo_backlog.core_date_date} = ${backlog_calc_forecast.core_date_date} ;;

# #   }

# #     # and ${carriyo_backlog.merchant} = ${carriyo_unified.merchant}
# #     # and ${carriyo_backlog.country} =${carriyo_unified.pickup_country}
# #     # and ${carriyo_backlog.service_type} = ${carriyo_unified.service_type};;

# #   access_filter: {
# #     # field: brand_vertical_mapping.bu_brand
# #     field: carriyo_unified.bu_brand_for_access_filter
# #     user_attribute: brand
# #   }
# #   access_filter: {
# #     # field: brand_vertical_mapping.vertical
# #     field: carriyo_unified.vertical_for_access_filter
# #     user_attribute: vertical
# #   }

# #   join:brand_vertical_mapping_log {
# #     type: left_outer
# #     relationship: many_to_one
# #     sql_on:
# #         ${carriyo_unified.merchant}=${brand_vertical_mapping_log.bu_brand};;
# #   }

# # }

# # explore: backlog_calc_forecast_new {
# #   from: backlog_calc_forecast
# #   label: "Backlog Tracker New (BETA)"

# #   join: carriyo_unified {
# #     # sql_where: latest_status =1  ;;
# #     type: full_outer
# #     relationship: one_to_many
# #     sql_on: ${backlog_calc_forecast_new.date_date} = ${carriyo_unified.collected_at_date}
# #           and ${backlog_calc_forecast_new.tbd_date} = ${carriyo_unified.to_be_next_attempted_date}
# #           and ${backlog_calc_forecast_new.merchant} = ${carriyo_unified.merchant}
# #           and ${backlog_calc_forecast_new.country} =${carriyo_unified.pickup_country}
# #           and ${backlog_calc_forecast_new.service_type} = ${carriyo_unified.service_type}
# #           and ${carriyo_unified.latest_status} =1
# #           and ${carriyo_unified.SLA_REQ} <>'MISSING_SLA';;
# #   }

# #   join: carriyo_backlog_new {
# #     relationship: many_to_many
# #     # sql_on: ${carriyo_backlog.core_date_date} = ${carriyo_unified.collected_at_latest_date} ;;
# #     sql_on: ${carriyo_backlog_new.core_date_new_date} = ${backlog_calc_forecast_new.core_date_new_date} ;;

# #   }

# #   # and ${carriyo_backlog.merchant} = ${carriyo_unified.merchant}
# #   # and ${carriyo_backlog.country} =${carriyo_unified.pickup_country}
# #   # and ${carriyo_backlog.service_type} = ${carriyo_unified.service_type};;

# #   access_filter: {
# #     # field: brand_vertical_mapping.bu_brand
# #     field: carriyo_unified.bu_brand_for_access_filter
# #     user_attribute: brand
# #   }
# #   access_filter: {
# #     # field: brand_vertical_mapping.vertical
# #     field: carriyo_unified.vertical_for_access_filter
# #     user_attribute: vertical
# #   }

# #   join:brand_vertical_mapping_log {
# #     type: left_outer
# #     relationship: many_to_one
# #     sql_on:
# #         ${carriyo_unified.merchant}=${brand_vertical_mapping_log.bu_brand};;
# #   }

# # }

# # explore: backlog_calc_forecast_new {
# #   from: backlog_calc_forecast
# #   label: "Backlog Tracker New (BETA)"
# #   hidden: yes

# #   join: carriyo_unified {
# #     # sql_where: latest_status =1  ;;
# #     type: full_outer
# #     relationship: one_to_many
# #     sql_on: ${backlog_calc_forecast_new.date_date} = ${carriyo_unified.order_creation_date}
# #           and ${backlog_calc_forecast_new.tbd_date} = ${carriyo_unified.to_be_next_attempted_date}
# #           and ${backlog_calc_forecast_new.merchant} = ${carriyo_unified.merchant}
# #           and ${backlog_calc_forecast_new.country} =${carriyo_unified.pickup_country}
# #           and ${backlog_calc_forecast_new.service_type} = ${carriyo_unified.service_type}
# #           and ${carriyo_unified.latest_status} =1
# #           and ${carriyo_unified.SLA_REQ} <>'MISSING_SLA';;
# #   }

# #   join: backlog_agg_dims {
# #     relationship: many_to_many
# #     # sql_on: ${carriyo_backlog.core_date_date} = ${carriyo_unified.collected_at_latest_date} ;;
# #     sql_on: ${backlog_agg_dims.core_date_new_date} = ${backlog_calc_forecast_new.core_date_new_date} ;;

# #   }

# #   # and ${carriyo_backlog.merchant} = ${carriyo_unified.merchant}
# #   # and ${carriyo_backlog.country} =${carriyo_unified.pickup_country}
# #   # and ${carriyo_backlog.service_type} = ${carriyo_unified.service_type};;

# #   access_filter: {
# #     # field: brand_vertical_mapping.bu_brand
# #     field: carriyo_unified.bu_brand_for_access_filter
# #     user_attribute: brand
# #   }
# #   access_filter: {
# #     # field: brand_vertical_mapping.vertical
# #     field: carriyo_unified.vertical_for_access_filter
# #     user_attribute: vertical
# #   }

# #   join:brand_vertical_mapping_log {
# #     type: left_outer
# #     relationship: many_to_one
# #     sql_on:
# #         ${carriyo_unified.merchant}=${brand_vertical_mapping_log.bu_brand};;
# #   }

# # }



# explore: dm_logistics_unified_by_shipment {
#   label: "Logistics by Shipment (BETA)"
#   view_label: "Logistics by Shipment (BETA)"
#   hidden: yes

#   join: dm_wh_ecom_order_forecast_data {
#     type: left_outer
#     relationship: many_to_one
#     sql_on: ${dm_logistics_unified_by_shipment.warehouse_id}=${dm_wh_ecom_order_forecast_data.schema}
#               and
#               ${dm_logistics_unified_by_shipment.order_creation_date} = ${dm_wh_ecom_order_forecast_data.date_date};;
#   }
#   join: dm_fulfillment_wm9_orders_city_sla_uat {
#     type: left_outer
#     relationship: many_to_one
#     sql_on: ${dm_logistics_unified_by_shipment.tracking_no} = ${dm_fulfillment_wm9_orders_city_sla_uat.tracking_no} ;;
#   }

#   join: dm_fulfillment_wm9_orders_details {
#     type: left_outer
#     relationship: many_to_one
#     sql_on: ${dm_fulfillment_wm9_orders_details.order_key}=${dm_logistics_unified_by_shipment.wm9_order_ID} ;;
#   }

#   join:date_master {
#     type: left_outer
#     relationship: many_to_one
#     sql_on: 1=1
#       ;;
#   }


# }

# explore: backlog_calc_forecast_2{
#   label: "Backlog Calculator"
#   hidden: yes

#   join: backlog_agg_dims {
#     type: left_outer
#     relationship: many_to_one
#     sql_on: ${backlog_agg_dims.order_creation_date} = ${backlog_calc_forecast_2.order_creation_date}
#           and ${backlog_agg_dims.flag} =  ${backlog_calc_forecast_2.flag}
#           ;;

#     }

#     access_filter: {
#       # field: brand_vertical_mapping.bu_brand
#       field: backlog_calc_forecast_2.bu_brand_for_access_filter
#       user_attribute: brand
#     }
#     access_filter: {
#       # field: brand_vertical_mapping.vertical
#       field: backlog_calc_forecast_2.vertical_for_access_filter
#       user_attribute: vertical
#     }

#     join:brand_vertical_mapping_log {
#       type: left_outer
#       relationship: many_to_one
#       sql_where: ${backlog_calc_forecast_2.vertical_for_access_filter} != 'COUNTRY MANAGEMENT' ;;
#       sql_on:
#         ${backlog_calc_forecast_2.merchant}=${brand_vertical_mapping_log.bu_brand}
#         ;;
#     }

#     sql_always_where:
#       NOT(${flag} ="Forecast" and date(${order_creation_date}) < current_date())
#       and  NOT(${flag} ="Actual" and date(${order_creation_date}) >= current_date())
#       and
#       date(${backlog_calc_forecast_2.order_creation_date}) >= '2020-11-01'
#       ;;




#       # sql_always_where:  date(${backlog_calc_forecast_2.order_creation_date}) >= '2020-11-01' ;;

#     }




#     explore: dm_fulfillment_wm9_orders_b2b {
#       label: "Fulfillment B2B"
#       view_label: "Orders B2B"
#       sql_always_where: ${order_ohtype} <> "CO"
#                     and ${facility_name} <> 'AM MALKI'
#                     and coalesce(${external_factor},'abc') <> 'DUM';;
#                     #and coalesce(${transportation_mode},'abc') <> 'DUMMY-TRN';;

#         join: dm_fulfillment_wm9_orders_details_b2b {
#           view_label: "Order Details B2B"
#           type: left_outer
#           relationship: one_to_many
#           sql_on: ${dm_fulfillment_wm9_orders_b2b.order_id} = ${dm_fulfillment_wm9_orders_details_b2b.order_key} ;;
#         }

#         join: b2b_order_fulfillment {
#           view_label: "Order Fulfillment"
#           type: left_outer
#           relationship: one_to_one
#           sql_on: ${dm_fulfillment_wm9_orders_b2b.order_id} = ${b2b_order_fulfillment.order_id} ;;
#         }

#         join: dim_wh_pack_on_time_sla {
#           view_label: "Pack On Time SLA"
#           type: left_outer
#           sql_on: ${dm_fulfillment_wm9_orders_b2b.country} =
#               ${dim_wh_pack_on_time_sla.country}
#             and ${dm_fulfillment_wm9_orders_b2b.warehouse_id} =
#               ${dim_wh_pack_on_time_sla.wh_id}
#             and ${dm_fulfillment_wm9_orders_b2b.order_custom_type} =
#               cast(${dim_wh_pack_on_time_sla.custom_type} as string)
#             --and format_datetime('%A',${dm_fulfillment_wm9_orders_b2b.order_creation_date}) = ${dim_wh_pack_on_time_sla.weekday}
#             and format_datetime('%A',${dm_fulfillment_wm9_orders_b2b.new_order_creation_date}) = ${dim_wh_pack_on_time_sla.weekday}
#     ;;
#         }

#         join: dim_logistics_bu_mapping {
#           view_label: "BU Mapping"
#           type: left_outer
#           relationship: many_to_one
#           sql_on:
#               ${dm_fulfillment_wm9_orders_details_b2b.storerkey} = ${dim_logistics_bu_mapping.storer_key}
#               and CASE
#                 when ${dim_logistics_bu_mapping.mapped_criteria} ='STORER'
#                   then 'X'
#                 when ${dim_logistics_bu_mapping.mapped_criteria} ='BRAND'
#                   then ${dm_fulfillment_wm9_orders_details_b2b.brand_name}
#                 when ${dim_logistics_bu_mapping.mapped_criteria} ='BRAND_ITEM_CATEGORY'
#                   then ${dm_fulfillment_wm9_orders_details_b2b.brand_item_category}
#               end =
#               CASE
#                 when ${dim_logistics_bu_mapping.mapped_criteria} ='STORER'
#                   then 'X'
#                 when ${dim_logistics_bu_mapping.mapped_criteria} ='BRAND'
#                   then ${dim_logistics_bu_mapping.brand_name}
#                 when ${dim_logistics_bu_mapping.mapped_criteria} ='BRAND_ITEM_CATEGORY'
#                   then ${dim_logistics_bu_mapping.brand_name}
#               end
#               ;;
#         }

#         join: dim_logistics_claims {
#           view_label: "Claims"
#           type: left_outer
#           relationship: one_to_one
#           sql_on: ${dm_fulfillment_wm9_orders_b2b.warehouse_id} =
#               ${dim_logistics_claims.facility}
#             and cast(${dm_fulfillment_wm9_orders_b2b.order_id} as int64) =
#               ${dim_logistics_claims.order_number}
#     ;;
#         }

#         join: dim_order_qty_exclusion {
#           view_label: "Qty Exclusion"
#           type: left_outer
#           sql_on: ${dm_fulfillment_wm9_orders_b2b.warehouse_id} =
#               ${dim_order_qty_exclusion.wh_id}
#             and cast(${dm_fulfillment_wm9_orders_b2b.order_id} as int64) =
#               ${dim_order_qty_exclusion.order_key}
#     ;;
#         }

#         join: dim_logistics_public_holiday {
#           view_label: "Public Holidays(O)"
#           type: left_outer
#           sql_on:  ${dm_fulfillment_wm9_orders_b2b.warehouse_id} =
#               ${dim_logistics_public_holiday.wh_id}
#             and ${dm_fulfillment_wm9_orders_b2b.order_creation_date} =
#                   ${dim_logistics_public_holiday.holiday_start_date}
#     ;;
#         }

#         join: dim_logistics_public_holiday2 {
#           from: dim_logistics_public_holiday
#           view_label: "Public Holidays(E)"
#           type: left_outer
#           sql_on:  ${dm_fulfillment_wm9_orders_b2b.warehouse_id} =
#               ${dim_logistics_public_holiday2.wh_id}
#             and date(${dm_fulfillment_wm9_orders_b2b.expected_pack_time_1}) =
#             ${dim_logistics_public_holiday2.holiday_start_date}
#     ;;
#         }

#       }

#       explore: dm_fulfillment_wm9_orders {
#         from: dm_fulfillment_wm9_orders_b2c
#         label: "Fulfillment E-Com"
#         view_label: "Order"
#         hidden: no
#         sql_always_where: ${order_ohtype} ="CO" ;;
#         #view_name: dm_fulfillment_wm9_orders_b2c
#         # always_filter: {
#         #   filters: {
#         #     field: order_ohtype
#         #     value: "CO"
#         #   }
#         #   # filters: {
#         #   #   field: dim_wh_storer_vertical_brand_mapping.brand_access_filter
#         #   #   value: "{{ _user_attributes['brand'] }}"
#         #   # }
#         #   # filters: {
#         #   #   field: dim_wh_storer_vertical_brand_mapping.vertical
#         #   #   value: "{{ _user_attributes['vertical'] }}"
#         #   # }
#         # }

#         # access_filter: {
#         #   field: dim_wh_storer_vertical_brand_mapping.brand_access_filter
#         #   user_attribute: brand
#         # }
#         # access_filter: {
#         #   field: dm_fulfillment_wm9_orders.country_access_filter
#         #   user_attribute: country
#         # }

#         # access_filter: {
#         #   field: dim_wh_storer_vertical_brand_mapping.vertical
#         #   user_attribute: vertical
#         # }

#         # join: dm_fulfillment_wm9_orders_details {
#         #   view_label: "Order Details"
#         #   type: left_outer
#         #   relationship: one_to_many
#         #   sql_on: ${dm_fulfillment_wm9_orders.order_id} = ${dm_fulfillment_wm9_orders_details.order_key} ;;
#         # }

#         join: dm_fulfillment_wm9_orders_details_b2c {
#           view_label: "Order Details"
#           type: left_outer
#           relationship: one_to_many
#           sql_on: ${dm_fulfillment_wm9_orders.order_id} = ${dm_fulfillment_wm9_orders_details_b2c.order_key} ;;
#         }

#         join: dim_wh_storer_vertical_brand_mapping {
#           view_label: "Brands"
#           type: left_outer
#           relationship: many_to_one
#           sql_on: ${dm_fulfillment_wm9_orders.storerkey} = ${dim_wh_storer_vertical_brand_mapping.storer_key} ;;
#         }
#         # join: dm_wh_ecom_order_forecast_data {
#         #   view_label: "Forecasts"
#         #   type: left_outer
#         #   relationship: many_to_one
#         #   sql_on: ${dm_fulfillment_wm9_orders.warehouse_id}=${dm_wh_ecom_order_forecast_data.schema}
#         #             and
#         #             ${dm_fulfillment_wm9_orders.order_creation_date} = ${dm_wh_ecom_order_forecast_data.date_date}
#         #             and
#         #             --${dim_wh_storer_vertical_brand_mapping.brand} = ${dm_wh_ecom_order_forecast_data.brand}
#         #             ${dm_fulfillment_wm9_orders_details.brand_name} = ${dm_wh_ecom_order_forecast_data.brand}
#         #             ;;
#         # }

#         join: dim_wh_collection_time {
#           view_label: "Collections"
#           type: left_outer
#           relationship: many_to_one
#           sql_on: ${dm_fulfillment_wm9_orders.country}=${dim_wh_collection_time.country}
#               and
#               ${dm_fulfillment_wm9_orders.warehouse_id} = ${dim_wh_collection_time.wh_id}
#               and
#               cast(${dm_fulfillment_wm9_orders.priority} as int64)=${dim_wh_collection_time.priority}
#               and
#               format_datetime('%A',${dm_fulfillment_wm9_orders.order_creation_date}) = ${dim_wh_collection_time.weekday}
#               and
#               case
#                 when ${dm_fulfillment_wm9_orders.carrier_code} is null
#                   then 'NA'
#                   else ${dm_fulfillment_wm9_orders.carrier_code}
#               end =   ${dim_wh_collection_time.dsp}
#               and
#               case
#                 when ${dm_fulfillment_wm9_orders.priority} = '1'
#                   then ${dm_fulfillment_wm9_orders.city_p1_cleaned} else 'X'
#               end =
#               case
#                 when ${dm_fulfillment_wm9_orders.priority} = '1'
#                   then ${dim_wh_collection_time.city} else 'X'
#               end
#               and cast(${dm_fulfillment_wm9_orders.order_creation_raw} as date)
#                 BETWEEN ${dim_wh_collection_time.rule_start_date} and ${dim_wh_collection_time.rule_end_date}
#       ;;
#         }

#         join: dm_fulfillment_percentile {
#           view_label: "Percentile"
#           type: left_outer
#           relationship: one_to_one
#           sql_on: ${dm_fulfillment_wm9_orders.warehouse_id}=${dm_fulfillment_percentile.warehouse_id}
#               and
#               ${dm_fulfillment_wm9_orders.order_id}=${dm_fulfillment_percentile.order_id}
#       ;;
#         }

#         join:date_master {
#           view_label: "Date Master"
#           type: left_outer
#           relationship: many_to_one
#           sql_on: 1=1
#             ;;
#         }

#         join: dm_wh_ecom_order_forecast_data {
#           view_label: "Forecasts"
#           type: full_outer
#           relationship: many_to_one
#           sql_on: ${dm_fulfillment_wm9_orders.warehouse_id}=${dm_wh_ecom_order_forecast_data.schema}
#               and
#               ${dm_fulfillment_wm9_orders.order_creation_date} = ${dm_wh_ecom_order_forecast_data.date_date}
#               and
#               --${dim_wh_storer_vertical_brand_mapping.brand} = ${dm_wh_ecom_order_forecast_data.brand}
#               ${dm_fulfillment_wm9_orders_details_b2c.brand_name} = ${dm_wh_ecom_order_forecast_data.brand}
#               ;;
#         }

#       }

#       explore: dm_logistics_bu {
#         label: "Logistics Book(BETA)"
#         view_label: "BU's"

#         join:dm_lb_period_from_to {
#           view_label: "LB Period"
#           type: full_outer
#           relationship: one_to_many
#           sql_on: 1=1 ;;
#         }

#         join:  dm_lb_distribution_b2b{
#           view_label: "Distribution B2B"
#           type: inner
#           relationship: one_to_many
#           sql_on: ${dm_logistics_bu.facility_name} = ${dm_lb_distribution_b2b.facility_name}
#             and
#             ${dm_logistics_bu.bu_code} = ${dm_lb_distribution_b2b.bu_code}
#             and
#             ${dm_logistics_bu.legal_entity} = ${dm_lb_distribution_b2b.legal_entity}
#             and
#             ${dm_lb_period_from_to.period_from} = ${dm_lb_distribution_b2b.period_from}
#             and
#             ${dm_lb_period_from_to.period_to} = ${dm_lb_distribution_b2b.period_to}
#             ;;
#         }

#         join:  dm_lb_distribution_b2c{
#           view_label: "Distribution B2C"
#           type: inner
#           relationship: one_to_many
#           sql_on: ${dm_logistics_bu.facility_name} = ${dm_lb_distribution_b2c.facility_name}
#             and
#             ${dm_logistics_bu.bu_code} = ${dm_lb_distribution_b2c.bu_code}
#             and
#             ${dm_logistics_bu.legal_entity} = ${dm_lb_distribution_b2c.legal_entity}
#             and
#             ${dm_lb_period_from_to.period_from} = ${dm_lb_distribution_b2c.period_from}
#             and
#             ${dm_lb_period_from_to.period_to} = ${dm_lb_distribution_b2c.period_to}
#             ;;
#         }

#         # join:  dm_lb_warehouse_sh01{
#         #   view_label: "Warehousing SH01"
#         #   type: inner
#         #   relationship: one_to_many
#         #   sql_on: ${dm_logistics_bu.facility_name} = ${dm_lb_warehouse_sh01.facility_name}
#         #           and
#         #           ${dm_logistics_bu.bu_code} = ${dm_lb_warehouse_sh01.bu_code}
#         #           and
#         #           ${dm_logistics_bu.legal_entity} = ${dm_lb_warehouse_sh01.legal_entity}
#         #           and
#         #           ${dm_lb_period_from_to.period_from} = ${dm_lb_warehouse_sh01.period_from}
#         #           and
#         #           ${dm_lb_period_from_to.period_to} = ${dm_lb_warehouse_sh01.period_to}
#         #           ;;
#         # }

#         # join:  dm_lb_warehouse_rc01{
#         #   view_label: "Warehousing RC01"
#         #   type: inner
#         #   relationship: one_to_many
#         #   sql_on: ${dm_logistics_bu.facility_name} = ${dm_lb_warehouse_rc01.facility_name}
#         #           and
#         #           ${dm_logistics_bu.bu_code} = ${dm_lb_warehouse_rc01.bu_code}
#         #           and
#         #           ${dm_logistics_bu.legal_entity} = ${dm_lb_warehouse_rc01.legal_entity}
#         #           and
#         #           ${dm_lb_period_from_to.period_from} = ${dm_lb_warehouse_rc01.period_from}
#         #           and
#         #           ${dm_lb_period_from_to.period_to} = ${dm_lb_warehouse_rc01.period_to}
#         #           ;;
#         # }

#         join:  dm_lb_consolidated_oblig{
#           view_label: "Consolidated OBLIG"
#           type: left_outer
#           relationship: one_to_many
#           sql_on: ${dm_logistics_bu.facility_name} = ${dm_lb_consolidated_oblig.facility_name}
#             and
#             ${dm_logistics_bu.bu_code} = ${dm_lb_consolidated_oblig.bu_code}
#             and
#             ${dm_logistics_bu.legal_entity} = ${dm_lb_consolidated_oblig.legal_entity}
#             and
#             ${dm_lb_period_from_to.period_from} = ${dm_lb_consolidated_oblig.period_from}
#             and
#             ${dm_lb_period_from_to.period_to} = ${dm_lb_consolidated_oblig.period_to}
#             ;;
#         }

#         join:  dm_lb_shipping_rc01{
#           view_label: "Shipping RC01"
#           type: left_outer
#           relationship: one_to_many
#           sql_on: ${dm_logistics_bu.facility_name} = ${dm_lb_shipping_rc01.facility_name}
#             and
#             ${dm_logistics_bu.bu_code} = ${dm_lb_shipping_rc01.bu_code}
#             ;;
#         }

#         join:  dm_lb_shipping_sh01{
#           view_label: "Shipping SH01"
#           type: left_outer
#           relationship: one_to_many
#           sql_on: ${dm_logistics_bu.facility_name} = ${dm_lb_shipping_sh01.facility_name}
#             and
#             ${dm_logistics_bu.bu_code} = ${dm_lb_shipping_sh01.bu_code}
#             ;;
#         }

#         join:  dm_lb_shipping_logitude{
#           view_label: "Shipping Logitude"
#           type: left_outer
#           relationship: one_to_many
#           sql_on: ${dm_logistics_bu.facility_name} = ${dm_lb_shipping_logitude.facility_name}
#             and
#             ${dm_logistics_bu.bu_code} = ${dm_lb_shipping_logitude.bu_code}
#             ;;
#         }

#         join:  dm_lb_shipping_manual{
#           view_label: "Shipping Manual"
#           type: left_outer
#           relationship: one_to_many
#           sql_on: ${dm_logistics_bu.facility_name} = ${dm_lb_shipping_manual.facility_name}
#             and
#             ${dm_logistics_bu.bu_code} = ${dm_lb_shipping_manual.bu_code}
#             ;;
#         }

#       }
