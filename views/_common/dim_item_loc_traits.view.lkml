view: dim_item_loc_traits {
  sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_item_loc_traits`
    ;;

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql: CONCAT(${item}, "-", ${loc}) ;;
  }

  dimension: _fivetran_deleted {
    type: yesno
    hidden: yes
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: alt_storage_location {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Season (Location)"
    description: "Season value specific for the location"
    sql: UPPER(${TABLE}.alt_storage_location) ;;
  }


  dimension: consignment_flag
  {
    type: string
    hidden: yes

    sql: ${TABLE}.food_stamp_ind ;;
  }

  dimension: consignment
  {
    type: yesno
    view_label: "Location Traits"
    label: "Is Consignment?"
    sql: ${consignment_flag} = "Y" ;;
  }

  dimension: alt_storeage_location_bucket {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Level Season Bucket"
    description: "Season bucket based on location trait"
    sql: CASE
          WHEN  REPLACE(${alt_storage_location}, ' ', '') IN ("SS21P", "SS21M", "SS21B") THEN "SS21"
          WHEN  REPLACE(${alt_storage_location}, ' ', '') IN ("FW21P", "FW21M", "FW21B") THEN "FW21"
          WHEN  REPLACE(${alt_storage_location}, ' ', '') IN ("SS22P", "SS22M", "SS22B") THEN "SS22"
          WHEN  REPLACE(${alt_storage_location}, ' ', '') IN ("FW22P", "FW22M", "FW22B") THEN "FW22"
          WHEN  ${alt_storage_location} LIKE "BASIC" THEN "Basic"
          WHEN  ${alt_storage_location} LIKE "REGULAR" THEN "Regular"
          WHEN  ${alt_storage_location} LIKE "%C" OR  ${alt_storage_location}  LIKE "%CO" THEN "Carry-over"
          WHEN  ${alt_storage_location} IS NULL THEN "No Season Info"
          ELSE "Previous Seasons"
          END;;
  }

  dimension: alt_storeage_location_bucket_2 {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Level Season Bucket 2"
    description: "Season bucket based on location trait"
    sql: CASE WHEN  ${alt_storage_location} LIKE "BASIC" THEN "Basic"
          ELSE "Seasonal"
          END;;
  }

  dimension: back_order_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.back_order_ind ;;
  }

  dimension_group: create_datetime {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.create_datetime ;;
  }

  dimension: deposit_code {
    type: string
    hidden: yes
    sql: ${TABLE}.deposit_code ;;
  }

  dimension: elect_mtk_clubs {
    type: string
    view_label: "Location Traits"
    label: "Tryano Area Description"
    sql: ${TABLE}.elect_mtk_clubs ;;
  }

  dimension: fixed_tare_uom {
    type: string
    hidden: yes
    sql: ${TABLE}.fixed_tare_uom ;;
  }

  dimension: fixed_tare_value {
    type: number
    hidden: yes
    sql: ${TABLE}.fixed_tare_value ;;
  }

  dimension: food_stamp_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.food_stamp_ind ;;
  }

  dimension: full_pallet_item {
    type: string
    hidden: yes
    sql: ${TABLE}.full_pallet_item ;;
  }

  dimension: ib_shelf_life {
    type: number
    hidden: yes
    sql: ${TABLE}.ib_shelf_life ;;
  }

  dimension: in_store_market_basket {
    type: string
    hidden: yes
    sql: ${TABLE}.in_store_market_basket ;;
  }

  dimension: item {
    type: string
    hidden: yes
    sql: ${TABLE}.item ;;
  }

  dimension_group: last_update_datetime {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_update_datetime ;;
  }

  dimension: last_update_id {
    type: string
    hidden: yes
    sql: ${TABLE}.last_update_id ;;
  }

  dimension_group: launch {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.launch_date ;;
  }

  dimension: loc {
    type: number
    hidden: yes
    sql: ${TABLE}.loc ;;
  }

  dimension: manual_price_entry {
    type: string
    hidden: yes
    sql: ${TABLE}.manual_price_entry ;;
  }

  dimension: natl_brand_comp_item {
    type: string
    hidden: yes
    sql: ${TABLE}.natl_brand_comp_item ;;
  }

  dimension: proportional_tare_pct {
    type: number
    hidden: yes
    sql: ${TABLE}.proportional_tare_pct ;;
  }

  dimension: qty_key_options {
    type: string
    hidden: yes
    sql: ${TABLE}.qty_key_options ;;
  }

  dimension: rack_size {
    type: string
    hidden: yes
    sql: ${TABLE}.rack_size ;;
  }

  dimension: refundable_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.refundable_ind ;;
  }

  dimension: report_code {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Zone"
    description: "Zone is a field sourced from RMS and refers to a zone in a department brand store"
    sql: ${TABLE}.report_code ;;
  }

  dimension: zone_3 {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Zone 3"
    description: "Zone 3 for Level shoes"
    sql: CASE WHEN ${report_code} LIKE "%ACC" Then "Accesories"
    WHEN ${report_code}="GIAROS" AND ${dim_retail_product.gender}="WOMEN" THEN "Direct Concession"
    ELSE "Multibrand"
    END;;
  }

  dimension: zone_category_sort {
    type: string
    hidden: yes
    view_label: "Location Traits"
    label: "Zone Category Sort"
    description: "Zone Category Sort for Level shoes"
    sql: CASE WHEN ${report_code} in ("CONTMP", "DESIGM", "DESIGW", "KIDS", "MENACC", "TRENDM", "TRENDW", "WMNACC", "KDACC") Then "1"
              WHEN ${report_code} in ("AXEARI", "CREPRO", "GIAROS", "SOLLOU", "STRFAR", "TORBUR", "VERSAC", "DOLCE") Then "2"
              WHEN ${report_code} in ("INTCON", "EXTCON") Then "3"
          ELSE "4"
          END;;
  }

  dimension: zone_category {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Zone Category"
    description: "Zone Category for Level shoes"
    order_by_field: zone_category_sort
    sql: CASE WHEN ${report_code} in ("CONTMP", "DESIGM", "DESIGW", "KIDS", "MENACC", "TRENDM", "TRENDW", "WMNACC", "KDACC") Then "Multibrand"
              WHEN ${report_code} in ("AXEARI", "CREPRO", "GIAROS", "SOLLOU", "STRFAR", "TORBUR", "VERSAC", "DOLCE") Then "Shop in Shop"
              WHEN ${report_code} in ("INTCON", "EXTCON") Then "Concessions"
          ELSE null
          END;;
  }

  dimension: req_shelf_life_on_receipt {
    type: number
    hidden: yes
    sql: ${TABLE}.req_shelf_life_on_receipt ;;
  }

  dimension: req_shelf_life_on_selection {
    type: number
    hidden: yes
    sql: ${TABLE}.req_shelf_life_on_selection ;;
  }

  dimension: return_policy {
    type: string
    hidden: yes
    sql: ${TABLE}.return_policy ;;
  }

  dimension: returnable_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.returnable_ind ;;
  }

  dimension: reward_eligible_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.reward_eligible_ind ;;
  }

  dimension: stop_sale_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.stop_sale_ind ;;
  }

  dimension: storage_location {
    type: string
    hidden: yes
    sql: ${TABLE}.storage_location ;;
  }

  dimension: store_reorderable_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.store_reorderable_ind ;;
  }

  dimension: wic_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.wic_ind ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: []
  }

  measure: count_of_seasons {
    type: count_distinct
    hidden: yes
    drill_fields: []
    sql:${alt_storage_location}  ;;
  }

}
