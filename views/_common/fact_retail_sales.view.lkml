include: "period_over_period.view"
include: "../group_supply_chain_views/ch_commission_valid_data.view"

view: fact_retail_sales {
# sql_table_name: `chb-prod-data-comm.prod_unified_sales.factretailsales_unified` ;;
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.factretailsales` ;;

  extends: [period_over_period]

  drill_fields: [id]

#   filter: customer_purchase_date {
#     description: "Choose the date range that defines the period for the date filtered measures"
#     type: date
#   }

  parameter: customer_granularity_parameter {
    label: "Brand / Group"
    view_label: "Muse Customer Information"
    description: "Choose the granularity you need the customer table to be"
    type: unquoted
    default_value: "overall"
    allowed_value: {
      label: "Group"
      value: "overall"
    }
    allowed_value: {
      label: "Brand"
      value: "brand"
    }
  }



  parameter: budget_selection {
    label: "Budget selection (USD)"
    view_label: "Parameters"
    type: string
    default_value: "R2"
    allowed_value: {
      label: "R0"
      value: "R0"
    }
    allowed_value: {
      label: "R1"
      value: "R1"
    }
    allowed_value: {
      label: "R2"
      value: "R2"
    }
  }

  dimension: customer_granularity {
    hidden: yes
    type: string
    sql:
    {% if customer_granularity_parameter._parameter_value == 'overall' %}
      true
    {% elsif customer_granularity_parameter._parameter_value == 'brand' %}
      ${dim_alternate_bu_hierarchy.brand}
    {% else %}
      true
    {% endif %}
    ;;
#     label: "@{granularity_dimension_label}"
  }

  dimension: id {
    primary_key: yes
    type: number
    hidden: yes
    sql: ${TABLE}.id ;;
  }

#   dimension: customer_fact_key {
#     type: string
#     sql: CONCAT(CAST(${atr_muse_id} AS STRING), CAST(${customer_granularity} AS STRING)) ;;
#   }

  dimension: muse_customer_fact_key {
    type: string
    sql: CONCAT(CAST(${atr_muse_id} AS STRING), CAST(${customer_granularity} AS STRING)) ;;
  }

  dimension_group: pop_no_tz {
    sql: ${business_raw} ;;
  }

  dimension: transaction_id {
    type: string
    hidden: no
    sql: CONCAT(${atr_cginvoiceno}, ${bk_storeid}) ;;
  }

  dimension: month_no {
    type: string
    hidden:yes
    sql: EXTRACT(MONTH FROM DATE ${business_raw}) ;;
  }

  dimension: month_name {
      description: "Name of the Month in the analysis, can be used for month over month comparison"
      case: {
        when: {
          sql: ${month_no}= 1 ;;
          label: "January"
        }
        when: {
          sql: ${month_no}= 2 ;;
          label: "February"
        }
        when: {
          sql: ${month_no}= 3 ;;
          label: "March"
        }
        when: {
          sql: ${month_no}= 4 ;;
          label: "April"
        }
        when: {
          sql: ${month_no}= 5 ;;
          label: "May"
        }
        when: {
          sql: ${month_no}= 6 ;;
          label: "June"
        }
        when: {
          sql: ${month_no}= 7 ;;
          label: "July"
        }
        when: {
          sql: ${month_no}= 8 ;;
          label: "August"
        }
        when: {
          sql: ${month_no}= 9 ;;
          label: "September"
        }
        when: {
          sql: ${month_no}= 10 ;;
          label: "October"
        }
        when: {
          sql: ${month_no}= 11 ;;
          label: "November"
        }
        when: {
          sql: ${month_no}= 12 ;;
          label: "December"
        }
      }
  }

  dimension: like_for_like {
    type: yesno
    view_label: "-- Period over Period"
    label: "Is like-for-like?"
    description: "Checks if the store was opened during the whole timeframe of analysis"
    sql: ${period_2_start} >= ${dim_retail_loc.store_open_date} AND (${period_1_end} <= ${dim_retail_loc.store_close_date} OR ${dim_retail_loc.store_close_date} IS NULL)  ;;

  }

  dimension: like_for_like_swarovski {
    type: yesno
    view_label: "-- Period over Period"
    label: "Is like-for-like?(Swarovski)"
    description: "Checks if the store was opened prior to comparison time frames"
    sql:  (EXTRACT(YEAR FROM ${period_1_start})-EXTRACT(YEAR FROM ${dim_retail_loc.store_open_date})) >1 AND ${dim_retail_loc.store_close_date} IS NULL  ;;

  }

  dimension_group: business{
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_month,
      week,
      day_of_week,
      month,
      month_name,
      quarter,
      year
    ]
    sql: TIMESTAMP(${TABLE}.bk_businessdate) ;;
  }

  dimension: ltl_till_date {
    type: string
    view_label: "— Like to Like Comparison"
    label: "LTL Timeframe"
    description: "For Like to like timeframe comparison"
    sql: CASE WHEN EXTRACT(MONTH FROM CAST((TIMESTAMP(fact_retail_sales.bk_businessdate))  AS DATE)) > EXTRACT(MONTH FROM CURRENT_TIMESTAMP) THEN "NO"
              WHEN EXTRACT(MONTH FROM CAST((TIMESTAMP(fact_retail_sales.bk_businessdate))  AS DATE)) < EXTRACT(MONTH FROM CURRENT_TIMESTAMP) THEN "YES"
              WHEN EXTRACT(MONTH FROM CAST((TIMESTAMP(fact_retail_sales.bk_businessdate))  AS DATE)) = EXTRACT(MONTH FROM CURRENT_TIMESTAMP) AND
              EXTRACT(DAY FROM CAST((TIMESTAMP(fact_retail_sales.bk_businessdate))  AS DATE)) >= EXTRACT(DAY FROM CURRENT_TIMESTAMP) THEN "NO"
              ELSE "YES" END;;
  }

  dimension: business_week_no{
    type: number
    description: "Week No. extracted from the business date"
    sql: EXTRACT( WEEK FROM(${TABLE}.bk_businessdate))+1 ;;
  }

  dimension: business_month_no{
    type: number
    description: "Month No. extracted from the business date"
    sql: EXTRACT( MONTH FROM(${TABLE}.bk_businessdate)) ;;
  }

# dimension: Category_basedon_division_TB {
#     group_label: "Products"
#     description: "Product categories for Tory Burch"
#     label: "Category"
#     type: string
#     sql:     CASE WHEN dim_retail_product.division ="APPAREL" AND  dim_retail_product.sub_line LIKE "%Basic%" Then "BASIC"
#               WHEN dim_retail_product.division ="APPAREL" AND  dim_retail_product.sub_line LIKE "%BASIC%" Then "BASIC"
#               WHEN dim_retail_product.division ="APPAREL" AND  dim_retail_product.sub_line LIKE "%FASHION%" Then "FASHION"
#               WHEN dim_retail_product.division ="APPAREL" Then "RTW"
#               WHEN dim_retail_product.division ="APPAREL COMPLEMENTS" AND  dim_retail_product.sub_line LIKE "%RC%" Then "BELTS"
#               WHEN dim_retail_product.division = "APPAREL COMPLEMENTS" THEN "ACC"
#               WHEN dim_retail_product.division = "COSTUME JEWELRY" THEN "ACC"
#               WHEN dim_retail_product.division = "JEWELRY" THEN "ACC"
#               WHEN dim_retail_product.division = "DECORATION" THEN "ACC"
#               WHEN dim_retail_product.division = "TOYS & GAMES" THEN "ACC"
#               WHEN dim_retail_product.division = "BATH & BEACHWEAR" Then "RTW"
#               WHEN dim_retail_product.division = "EYE WEAR" Then "EYE WEAR"
#               WHEN dim_retail_product.division = "FOOTWEAR" Then "SHOES"
#               WHEN dim_retail_product.division = "FRAGRANCES" Then "FRAGRANCES"
#               WHEN dim_retail_product.division = "AMBIENT SCENTS" Then "FRAGRANCES"
#               WHEN dim_retail_product.division = "GWP" Then "GWP"
#               WHEN dim_retail_product.division = "WATCHES" Then "WATCHES"
#               WHEN dim_retail_product.division = "LIGHT ELECTRONICS" Then "WATCHES"
#               WHEN dim_retail_product.division = "SERVICE ITEMS" Then "SERVICE ITEMS"
#               WHEN dim_retail_product.division = "PLV" Then "PLV"
#               WHEN dim_retail_product.division = "LEATHER GOODS & LUGGAGE" AND  dim_retail_product.taxo_class LIKE "%BAG%" Then "HANDBAGS"
#               WHEN dim_retail_product.division = "LEATHER GOODS & LUGGAGE" AND  dim_retail_product.taxo_class LIKE "%SMALL LEATHER GOODS%" Then "SLG"
#               WHEN dim_retail_product.division = "LEATHER GOODS & LUGGAGE" Then "HANDBAGS"
#               WHEN dim_retail_product.division = "MEDICARE" Then "MEDICARE"
#               WHEN dim_retail_product.division = "INTIMATE" Then "INTIMATE"
#               WHEN dim_retail_product.division = "CHARITY" Then "CHARITY"
#               WHEN dim_retail_product.division = "SERVICE MATERIAL" Then "SERVICE MATERIAL"
#               END;; }


  dimension: _fivetran_deleted {
    type: yesno
    hidden: yes
    sql: ${TABLE}._fivetran_deleted ;;
  }



  dimension_group: _fivetran_synced {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: amountlocal_beforetax {
    type: number
    hidden: yes
    sql: ${TABLE}.amountlocal_beforetax ;;
  }

  dimension: amountusd_beforetax {
    type: number
    hidden: yes
    sql: ${TABLE}.amountusd_beforetax ;;
  }

  dimension: atr_action {
    type: string
    hidden: yes
    sql: ${TABLE}.atr_action ;;
  }

  dimension: atr_activitytype {
    type: string
    hidden: yes
    sql: ${TABLE}.atr_activitytype ;;
  }

  dimension: atr_batchid {
    type: string
    hidden: yes
    sql: ${TABLE}.atr_batchid ;;
  }

  dimension: atr_cginvoiceno {
    type: string
    hidden: no
    label: "Invoice Number"
    sql: ${TABLE}.atr_cginvoiceno ;;
  }

  dimension: atr_cginvoicenoline {
    type: string
    hidden: yes
    sql: ${TABLE}.atr_cginvoicenoline ;;
  }

  dimension_group: atr_createdate {
    type: time
    hidden: yes
    label: "CreateDate Timestamp"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_createdate ;;
  }

  dimension: atr_day {
    type: number
    hidden: yes
    sql: ${TABLE}.atr_day ;;
  }

  dimension_group: atr_documentdate {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_documentdate ;;
  }

  dimension: atr_errordesc {
    type: string
    hidden: yes
    sql: ${TABLE}.atr_errordesc ;;
  }

  dimension: atr_homecurrency {
    type: string
    hidden: yes
    sql: ${TABLE}.atr_homecurrency ;;
  }

  dimension: atr_itemseqno {
    type: number
    hidden: yes
    sql: ${TABLE}.atr_itemseqno ;;
  }

  dimension: atr_itemstatus {
    type: string
    hidden: yes
    sql: ${TABLE}.atr_itemstatus ;;
  }

  dimension: atr_membershipid {
    type: string
    hidden: yes
    sql: ${TABLE}.atr_membershipid ;;
  }

  dimension: atr_muse_id {
    view_label: "Muse Customer Information"
#     group_label: "ID Information"
    label: "-- Muse Customer ID"
    type: string
#     hidden: yes
    sql: ${TABLE}.atr_muse_id ;;
  }

  dimension: atr_promotionid {
    type: number
    hidden: yes
    value_format_name: id
    sql: ${TABLE}.atr_promotionid ;;
  }

  dimension: atr_promotionid2 {
    type: number
    hidden: yes
    sql: ${TABLE}.atr_promotionid2 ;;
  }

  dimension: atr_salesperson {
    type: string
    hidden: no
    sql: ${TABLE}.atr_salesperson ;;
  }

  dimension: atr_stagestatus {
    type: string
    hidden: yes
    sql: ${TABLE}.atr_stagestatus ;;
  }

  dimension: atr_status {
    type: string
    hidden: yes
    sql: ${TABLE}.atr_status ;;
  }

  dimension: atr_storedayseqno {
    type: number
    hidden: yes
    sql: ${TABLE}.atr_storedayseqno ;;
  }

  dimension: atr_time {
    type: number
    hidden: yes
    sql: ${TABLE}.atr_time ;;
  }

  dimension: atr_timezone {
    type: string
    hidden: yes
    sql: ${TABLE}.atr_timezone ;;
  }

  dimension_group: atr_trandate {
    type: time
    description: "Transaction Date as posted in RESA, there are cases where transaction date does not equal business date of purchase"
    label: "Transaction Timestamp"
    # hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.atr_trandate AS TIMESTAMP) ;;
  }
  dimension_group: atr_trandatetime {
    type: time
    label: "Transaction Timestamp 2"
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.atr_trandatetime AS TIMESTAMP) ;;
  }
  measure: last_updated_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    label: "Last refresh date"
    sql: MAX(${atr_trandatetime_raw}) ;;
    convert_tz: no
  }


  measure: first_atr_trandate {
    hidden: no
    type: date
    label: "First transaction date"
    sql: min(CAST(fact_retail_sales.atr_trandate  AS DATE)) ;;
  }

  measure: last_atr_trandate {
    hidden: yes
    type: date
    sql: max(CAST(fact_retail_sales.atr_trandate  AS DATE)) ;;
  }

  dimension: atr_tranno {
    type: number
    hidden: yes
    sql: ${TABLE}.atr_tranno ;;
  }


  dimension: atr_transeqno {
    type: number
    hidden: yes
    sql: ${TABLE}.atr_transeqno ;;
  }

  dimension: atr_trantype {
    type: string
    hidden: yes
    sql: ${TABLE}.atr_trantype ;;
  }

  dimension: is_refund {
    type: yesno
    hidden: no
    sql: ${TABLE}.mea_amountusd < 0 ;;
  }

  dimension_group: atr_updatedate {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_updatedate ;;
  }

  dimension: av_cost_local {
    type: number
    hidden: yes
    sql: ${TABLE}.av_cost_local ;;
  }

  dimension: av_cost_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.av_cost_usd ;;
  }

  dimension: bk_brand {
    type: number
    hidden: yes
    sql: ${TABLE}.bk_brand ;;
  }

  dimension: bk_businessdate {
    type: number
    hidden: yes
    sql: ${TABLE}.bk_businessdate;;
  }

  dimension: bk_businessmonth {
    type: string
    hidden: yes
    sql: REGEXP_EXTRACT(CAST(${bk_businessdate} AS STRING), r"(.{6})") as bk_month  ;;
  }

  dimension: md_tran_id {
    type: string
    hidden:  yes
    sql: CASE WHEN ${discountamt_usd} > 0 THEN CONCAT(${atr_cginvoiceno}, ${bk_storeid})
    ELSE null END;;
  }

    dimension: fp_tran_id {
    type: string
    hidden:  yes
    sql: CASE WHEN ${discountamt_usd} IS NULL THEN CONCAT(${atr_cginvoiceno}, ${bk_storeid})
    WHEN ${discountamt_usd} = 0  THEN CONCAT(${atr_cginvoiceno}, ${bk_storeid})
    ELSE null END;;
  }

  dimension: bk_customer {
    group_label: "ID Information"
    label: "CRM Customer ID"
    type: string
    hidden: yes
    sql: ${TABLE}.bk_customer ;;
  }

  dimension: bk_loyalty {
    type: string
    hidden: yes

    sql: ${TABLE}.bk_loyalty ;;
  }

  dimension: bk_loyaltyprogram {
    type: string
    hidden: yes
    sql: ${TABLE}.bk_loyaltyprogram ;;
  }

  dimension: bk_productid {
    type: string
    label: "Product ID"
    view_label: "Products"
    sql: ${TABLE}.bk_productid ;;
  }

  dimension: bk_salestype {
    type: number
    hidden: yes
    sql: ${TABLE}.bk_salestype ;;
  }

  dimension: bk_storeid {
    type: number
    hidden: yes
    value_format_name: id
    sql: ${TABLE}.bk_storeid ;;
  }

  dimension: location_number {
    type: string
    hidden: no
    label: "Location Number"
    value_format_name: id
    sql: CAST(${TABLE}.bk_storeid as STRING) ;;
  }

  dimension: bk_top20 {
    type: string
    hidden: yes
    sql: ${TABLE}.bk_top20 ;;
  }

  dimension_group: creationdatetime {
    type: time
    hidden: yes
    label: "Creation Timestamp"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.creationdatetime ;;
  }

  dimension: customer_id {
#     view_label: "Customer Information"
#     group_label: "ID Information"
    type: string
    sql: CONCAT(ifnull(${atr_muse_id},'no-muse-id'), '-', ifnull(${bk_customer},'no-crm-id')) ;;
  }

  dimension: customer_type {
    hidden: yes
#     view_label: "Customer Information"
    group_label: "Customer Type Information"
    type: string
    sql:
    CASE
      WHEN ${atr_muse_id} IS NOT NULL AND ${bk_customer} IS NOT NULL AND ${bk_customer} NOT LIKE 'UNK%'
        THEN 'Muse and CRM'
      WHEN ${atr_muse_id} IS NOT NULL AND ${bk_customer} IS NOT NULL AND ${bk_customer} LIKE 'UNK%'
        THEN 'Muse'
      WHEN ${bk_customer} IS NOT NULL AND ${bk_customer} NOT LIKE 'UNK%'
        THEN 'CRM'
      WHEN ${atr_muse_id} IS NOT NULL
        THEN 'Muse'
      ELSE
        'Unkown'
      END
    ;;
  }

  dimension: sales_channel {
    label: "Sales Channel"
    type: string
    hidden: no
    sql: ${TABLE}.sales_channel ;;
  }

  dimension: sales_channel_group {
    label: "Sales Channel Group"
    type: string
    hidden: no
    sql: case
    when ${sales_channel} LIKE "%Clienteling - Distant Sale%"
    or ${sales_channel} LIKE "%Catalogue Emailers%"
    or ${sales_channel} LIKE "%Customer Service Sales%"
    or ${sales_channel} LIKE "%Digital Concierge%"
    or ${sales_channel} LIKE "%Referral%"
    or ${sales_channel} LIKE "%Instagram%"
    or ${sales_channel} LIKE "%SnapChat%"
    or ${sales_channel} LIKE "%Social Channels%"
    or ${sales_channel} LIKE "%MUSE%"
    or ${sales_channel} LIKE "%THAHAB MARKETPLACE%"
    or ${sales_channel} LIKE "%The Luxury Circle%"
    or ${sales_channel} LIKE "%Tiktok%"
    or ${sales_channel} LIKE "%Facebook%"
    or ${sales_channel} LIKE "%Whatsapp Sale%"
    or ${sales_channel} LIKE "%Virtual Appointment%"
    or ${sales_channel} LIKE "%Whatsapp/Phone Call Store Sales%"
    or ${sales_channel} LIKE "%Whatsapp/Phone Home Delivery%"
    or ${sales_channel} LIKE "%Ecommerce Pop Up%"
    or ${sales_channel} LIKE "%Concierge Upsell%" then "Distant Clienteling"
    when ${sales_channel} LIKE "%Clienteling - Store Sale%"
    or ${sales_channel} LIKE "%Store Appointment%" then "In Store Clienteling"
    when ${sales_channel} LIKE "%E-Com Sale%"
    or ${sales_channel} LIKE "%E-commerce%" then "E-com Sales"
    when ${sales_channel} LIKE "%Home Appointment%" then "Home Appointment Clienteling"
    when ${sales_channel} LIKE "%Store Sale%"
    or ${sales_channel} LIKE "%Stores Sales%" THEN "In Store Sales"
    when ${sales_channel} is NULL then "In Store Sales"
    else "To be defined"
    end
    ;;
  }


  dimension: sales_channel_clienteling_group {
    label: "Sales Channel Clienteling"
    type: string
    hidden: no
    sql: case
          when ${sales_channel_group} LIKE "%In Store Sales%" then "Total In Store Sales"
          when ${sales_channel_group} LIKE "%Distant Clienteling%"
          or ${sales_channel_group} LIKE "%Home Appointment Clienteling%"
          or ${sales_channel_group} LIKE "%In Store Clienteling%" then "Total Clienteling Sales"
          when ${sales_channel_group} LIKE "%E-com Sales%" then "Total E-com Sales"
    else "To be defined"
    end
          ;;
  }


  dimension: is_muse {
#     group_label: "Customer Type Information"
    type: yesno
    sql: ${atr_muse_id} IS NOT NULL ;;
  }

  dimension: is_crm {
#     view_label: "Customer Information"
    group_label: "Customer Type Information"
    type: yesno
    hidden: yes
    sql: ${bk_customer} IS NOT NULL AND ${bk_customer} NOT LIKE 'UNK%' ;;
  }

#   dimension: crm_customer_id {
#     hidden: yes
#     view_label: "Customer Information"
#     type: string
#     label: "CRM Customer ID"
#     sql: ${TABLE}.crm_customer_id ;;
#   }

  dimension: customer_golden_id {
    type: string
    hidden: yes
    sql: ${TABLE}.customer_golden_id ;;
  }


  dimension: discountamt_local {
    type: number
    hidden: yes
    sql: ${TABLE}.discountamt_local ;;
  }

  dimension: discountamt_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.discountamt_usd ;;
  }

  dimension: sales_tran {
    type: yesno
    label: "Sales Transaction"
    sql: ${TABLE}.discountamt_usd IS NOT NULL and ${TABLE}.discountamt_usd <> 0;;
  }

  dimension: mea_amountlocal {
    type: number
    hidden: yes
    sql: ${TABLE}.mea_amountlocal ;;
  }

  dimension: mea_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.mea_amountusd ;;
  }

  dimension: mea_quantity {
    type: number
    hidden: yes
    sql: ${TABLE}.mea_quantity ;;
  }

  dimension: return_transaction {
    type: yesno
    hidden: no
    label: "Return Transaction?"
    sql: ${TABLE}.mea_quantity<0 ;;
  }

  dimension: tran_type {
    type: string
    hidden: no
    label: "Transaction Type"
    sql: ${retail_transaction_classification.transaction_type} ;;
  }

  dimension: pos_customer_id {
    type: string
    hidden: yes
    sql: ${TABLE}.pos_customer_id ;;
  }

  dimension: taxamt_local {
    type: number
    hidden: yes
    sql: ${TABLE}.taxamt_local ;;
  }

  dimension: taxamt_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.taxamt_usd ;;
  }

  dimension: gender_brand_specific {
    group_label: "Products"
    description: "Gender with Kids, Newborn, Infant, Toddler, Layette under Kids"
    label: "Gender"
    type: string
    sql: ${dim_retail_product.gender} ;;
  }

  dimension: unit_retail_usd {
    type: number
    value_format_name: usd
    group_label: "Products"
    label: "Retail Selling Price USD"
    description: "Unit Retail price in USD. Only exists in the datamart as of April' 2020."
    hidden: no
    sql: ${TABLE}.unit_retail_usd;;
  }
  dimension: unit_retail_lc {
    type: number
    value_format_name: decimal_2
    group_label: "Products"
    label: "Retail Selling Price Local Currency"
    description: "Unit Retail price in Local Currency. Only exists in the datamart as of April' 2020."
    hidden: no
    sql: ${TABLE}.unit_retail_local;;
  }
  measure: average_retail_selling_price_local {
    group_label: "Sales Metrics"
    description: "The average of Unit Retail price in local currency. Only exists in the datamart as of April' 2020."
    type: average
    hidden:no
    sql: ${unit_retail_lc} ;;
    value_format_name: decimal_2
  }
#   dimension: max_discount_amount_usd {
#     type: number
#     value_format_name: usd
#     sql: (${discountamt_usd}/NULLIF(${mea_amountusd}+${discountamt_usd},0)*${mea_quantity})*${max_prices.max_price_usd_by_country} ;;
#   }

  measure: markdown_investment {
    type: sum
    label: "Markdown Investment USD"
    description: "Total discount given in USD"
    group_label: "Markdown metrics"
    value_format_name: usd
    sql: ${discountamt_usd};;
  }

  measure: markdown_investment_lc {
    type: sum
    label: "Markdown Investment Local"
    description: "Total discount given in Local currency"
    group_label: "Markdown metrics"
    value_format_name: decimal_2
    sql: ${discountamt_local};;
  }


  measure: average_retail_selling_price_usd {
    group_label: "Sales Metrics"
    description: "Average of Unit Retail price in USD - this can change on a daily basis and is not affected by manual price adjustments or coupons"
    type: average
    hidden:no
    sql: ${unit_retail_usd} ;;
    value_format_name: usd
  }

  dimension: rsp_buckets {
    group_label: "Products"
    description: "Segregation of Retail Selling Price into buckets. Only exists in the datamart as of April' 2020."
    label: "RSP Buckets"
    type: string
    sql: CASE WHEN ${unit_retail_usd} >= 0 AND ${unit_retail_usd} < 100 THEN '0 to 99'
              WHEN ${unit_retail_usd} >= 100 AND ${unit_retail_usd} < 200 THEN '100 to 199'
              WHEN ${unit_retail_usd}>= 200 AND ${unit_retail_usd}< 300 THEN '200 to 299'
              WHEN ${unit_retail_usd}>= 300 AND ${unit_retail_usd} < 400 THEN '300 to 399'
              WHEN ${unit_retail_usd} >= 400 AND ${unit_retail_usd} < 500 THEN '400 to 499'
              WHEN ${unit_retail_usd} >= 500 AND ${unit_retail_usd} < 600 THEN '500 to 599'
              WHEN ${unit_retail_usd} >= 600 AND ${unit_retail_usd} < 700 THEN '600 to 699'
              WHEN ${unit_retail_usd} >= 700 AND ${unit_retail_usd} < 800 THEN '700 to 799'
              WHEN ${unit_retail_usd} >= 800 AND ${unit_retail_usd} < 900 THEN '800 to 899'
              WHEN ${unit_retail_usd} >= 900 AND ${unit_retail_usd} < 1000 THEN '900 to 999'
              WHEN ${unit_retail_usd} >= 1000 AND ${unit_retail_usd} < 1500 THEN '1000 to 1499'
              WHEN ${unit_retail_usd} >= 1500 THEN '>= 1500'
              ELSE 'Undefined'
END;;
  }
  dimension: Store1_name_segregation {
    group_label: "Locations"
    description: "Segregation of Offline-Mkt place - Ecom for FF1"
    label: "Channel FF1"
    view_label: "Locations FF1"
    type: string
     sql: CASE  WHEN ${dim_retail_loc.store_name} LIKE "%FARFETCH%" THEN "MKT PLACE"
    WHEN ${dim_retail_loc.store_name} LIKE "%ECOMMERCE%"  THEN "ECOM"
    WHEN ${dim_retail_loc.store_name} LIKE "%OUTLET%"  THEN "OUTLET"
    else "STORE" END;;
  }
#   measure: max_markdown_investment {
#     type: sum
#     label: "Max Markdown Investment USD"
#     value_format_name: usd
#     sql: ${max_discount_amount_usd};;
#   }



  dimension: count_markdown_items {
    type: number
    hidden: yes
    sql:  CASE WHEN ${discountamt_usd} <> 0 THEN ${mea_quantity} ELSE 0 END;;
  }

  dimension: markdown_transaction {
    type: yesno
    hidden: no
    sql:  ${discountamt_usd} <> 0 ;;
  }


  measure: markdown_depth {
    type: number
    hidden: no
    label: "Markdown Depth %"
    group_label: "Markdown metrics"
    description: "Total discount given divided by the original selling price (before the discount)"
    value_format_name: percent_2
    sql: SUM(${discountamt_usd})/NULLIF(SUM(${mea_amountusd}+${discountamt_usd}),0) ;;
  }

  measure: markdown_transactions {
    group_label: "Transaction metrics"
    description: "Distinct number of transactions with a discount"
    type: count_distinct
    value_format_name: decimal_0
    sql: ${md_tran_id} ;;
  }

  measure: full_price_transactions {
    group_label: "Transaction metrics"
    description: "Distinct number of transactions at full price(no discount)"
    type: count_distinct
    value_format_name: decimal_0
    sql: ${fp_tran_id};;
  }


  measure: share_of_markdown_transactions {
    group_label: "Transaction metrics"
    description: "Percentage of transactions with a discount vs total number of transactions"
    type: number
    value_format_name: percent_2
    sql: COUNT(DISTINCT ${md_tran_id})/COUNT (DISTINCT CONCAT(${atr_cginvoiceno}, ${bk_storeid}));;
  }

  measure: share_of_markdown_items {
    group_label: "Markdown metrics"
    description: "Percentage of total number of items sold a discount vs total number of items sold"
    type: number
    value_format_name: percent_2
    sql: SUM(${count_markdown_items})/ NULLIF(SUM(${TABLE}.mea_quantity),0) ;;
  }



  measure: cross_brand_adoption {
    hidden: yes
    description: "The number of brands that single customer has shopped in."
    type: count_distinct
    sql: ${dim_retail_loc.district_name} ;;
  }

  measure: transaction_count {
    type: count_distinct
    group_label: "Transaction metrics"
    description: "Total number of transactions"
    value_format_name: decimal_0
    sql: ${transaction_id};;
    drill_fields: [dim_retail_product.vpn, transaction_count]
  }


  measure: total_muse_mall_visits {
    label: "Visits per Mall"
    type: count_distinct
    hidden: yes
    sql: CONCAT(${atr_muse_id}, CAST(${atr_trandate_time} AS STRING), ${dim_retail_loc.mall_name}) ;;
  }

  measure: total_muse_brand_visits {
    label: "Visits per Brand"
    type: count_distinct
    hidden: yes
    sql: CONCAT(${atr_muse_id}, CAST(${atr_trandate_time} AS STRING), ${dim_retail_loc.mall_name}, ${dim_alternate_bu_hierarchy.brand}) ;;
  }

#   measure: current_period_transaction_count {
#     type: count_distinct
#     sql: ${transaction_id} ;;
#     filters: [muse_customer_fact_table.is_purchase_in_range: "yes"]
#     value_format_name: decimal_0
#   }
#
#   measure: previous_period_transaction_count {
#     type: count_distinct
#     sql: ${transaction_id} ;;
#     filters: [muse_customer_fact_table.is_purchase_in_previous_range: "yes"]
#     value_format_name: decimal_0
#   }

  measure: muse_transaction_count {
    type: count_distinct
    group_label: "Transaction metrics"
    value_format_name: decimal_0
    sql: ${transaction_id};;
    filters: [is_muse: "yes"]
  }




  measure: cogs_usd {
    type: sum
    hidden: no
    group_label: "Cost of Goods Sold"
    label: "Cost of Goods Sold USD"
    description: "Average unit cost (in USD) multiplied by quantity sold"
    sql:ABS(${av_cost_usd})*${mea_quantity} ;;
    value_format_name: usd
  }

  measure: cogs_local {
    type: sum
    hidden: no
    group_label: "Cost of Goods Sold"
    label: "Cost of Goods Sold Local"
    description: "Average unit cost (in local currency) multiplied by quantity sold"
    sql:ABS(${av_cost_local})*${mea_quantity} ;;
    value_format_name: decimal_2
  }

  #----------------------------GROSS MARGIN CALCULATIONS -------------------------------
#   measure: gross_margin {
#     type: sum
#     hidden: no
#     label: "Gross Margin USD"
#     sql:${amountusd_beforetax}-(${av_cost_usd}*ABS(${mea_quantity})) ;;
#     value_format_name: usd
#   }
#
#   measure: gross_margin_share {
#     type: number
#     hidden: no
#     label: "Gross Margin %"
#     value_format_name: percent_2
#     sql: SAFE_DIVIDE(SUM(${mea_amountusd}-${av_cost_usd}*${mea_quantity}),SUM(${mea_amountusd})) ;;
#   }
#
#   measure: full_price_gross_margin {
#     type: sum
#     hidden: no
#     label: "FP Gross Margin USD"
#     filters: {
#       field: markdown_transaction
#       value: "no"
#     }
#     sql:${amountusd_beforetax}-(${av_cost_usd}*ABS(${mea_quantity})) ;;
#     value_format_name: usd
#   }
#
#   measure: md_price_gross_margin {
#     type: sum
#     hidden: no
#     label: "MD Gross Margin USD"
#     filters: {
#       field: markdown_transaction
#       value: "yes"
#     }
#     sql:${amountusd_beforetax}-(${av_cost_usd}*ABS(${mea_quantity})) ;;
#     value_format_name: usd
#   }
#
#   measure: gross_margin_local {
#     type: sum
#     hidden: no
#     label: "Gross Margin Local"
#     sql:${amountlocal_beforetax}-(${av_cost_local}*ABS(${mea_quantity})) ;;
#     value_format_name: decimal_0
#   }

#  SPD-257 changes starts
  measure: fp_net_sales_usd {
    type :  sum
    hidden : yes
    filters: {
      field: markdown_transaction
      value: "no"
    }
    sql: ${amountusd_beforetax} ;;
    value_format_name: usd
  }

  measure: md_net_sales_usd {
    type :  sum
    hidden : yes
    filters: {
      field: markdown_transaction
      value: "yes"
    }
    sql: ${amountusd_beforetax} ;;
    value_format_name: usd
  }



  measure: fp_cogs_usd {
    type :  sum
    hidden : yes
    filters: {
      field: markdown_transaction
      value: "no"
    }
    sql: ${av_cost_usd}*${mea_quantity} ;;
    value_format_name: usd
  }

  measure: md_cogs_usd {
    type :  sum
    hidden : yes
    filters: {
      field: markdown_transaction
      value: "yes"
    }
    sql: ${av_cost_usd}*${mea_quantity} ;;
    value_format_name: usd
  }

  measure: gross_margin {
    type: number
    hidden: no
    label: "Gross Margin USD"
    group_label: "Gross Margin"
    description: "Net Sales (USD) minus Cost of Goods Sold (USD)"
    sql: ${net_sales_amount_usd} - ${cogs_usd} ;;
    value_format_name: usd
  }

  measure: gross_margin_share {
    type: number
    hidden: no
    label: "Gross Margin %"
    group_label: "Gross Margin"
    description: "Percentage of Gross Margin (USD) vs Net Sales (USD)"
    value_format_name: percent_2
    sql: SAFE_DIVIDE(${gross_margin},${net_sales_amount_usd}) ;;
  }



  measure: full_price_gross_margin {
    type: number
    hidden: yes
    label: "FP Gross Margin USD"
    sql: ${fp_net_sales_usd} - ${fp_cogs_usd} ;;
    value_format_name: usd
  }

  measure: md_price_gross_margin {
    type: number
    hidden: yes
    label: "MD Gross Margin USD"
    sql: ${md_net_sales_usd} -${md_cogs_usd} ;;
    value_format_name: usd
  }

  measure: gross_margin_local {
    type: number
    hidden: no
    label: "Gross Margin Local"
    description: "Net Sales (local currency) minus Cost of Goods Sold (local currency)"
    group_label: "Gross Margin"
    sql: ${net_sales_amount_local} -${cogs_local} ;;
    value_format_name: decimal_0
  }

  measure: total_net_sales_local {
    type: sum
    hidden: yes
    sql: ${amountlocal_beforetax} ;;
    value_format_name: decimal_2
  }


#  SPD-257 changes ends
#----------------------------GROSS MARGIN CALCULATIONS -------------------------------
  measure: net_sales_amount_usd {
    description: "Net Sales USD is calculation is Gross Sales - Returns - Discounts"
    type: sum
    group_label: "Net Sales"
    value_format_name: usd
    sql: ${amountusd_beforetax} ;;
  }

  measure: net_sales_amount_local {
    description: "Gross Sales - Returns - Discounts, use with Customer ID for LTV"
    type: sum
    group_label: "Net Sales"
    value_format_name: decimal_0
    sql: ${amountlocal_beforetax} ;;
  }

  measure: running_net_sales_amount_usd {
    description: "Gross Sales - Returns - Discounts, use with Customer ID for LTV"
    group_label: "Net Sales"
    type: running_total
    value_format_name: usd
    sql: SUM(${amountusd_beforetax}) ;;
  }

  measure: customer_count {
    hidden: yes
#     view_label: "Customer Information"
    description: "The number of customers that have bought in filters applied"
    type: count_distinct
    sql: ${customer_id} ;;
  }

  measure: muse_customer_count {
    view_label: "Muse Customer Information"
    description: "The number of muse customers that have bought in filters applied"
    type: count_distinct
    sql: ${atr_muse_id} ;;
  }

  measure: frequency {
    hidden: yes
    view_label: "Customer Information"
    description: "Number of transactions / number of customers that purchased in applied filters"
    type: number
    value_format_name: decimal_2
    sql: 1.0 * ${transaction_count} / NULLIF(${customer_count},0) ;;
  }

  measure: muse_frequency {
    view_label: "Muse Customer Information"
    description: "Number of transactions / number of customers that purchased in applied filters"
    type: number
    value_format_name: decimal_2
    sql: 1.0 * ${muse_transaction_count} / NULLIF(${muse_customer_count},0) ;;
  }

  measure: average_order_value_usd {
    description: "Net Sales / Transactions"
    type: number
#     maybe should be unique transaction count??
    sql: 1.0 * ${net_sales_amount_usd} / NULLIF(${transaction_count},0) ;;
    value_format_name: usd
  }

  measure: average_order_value_local {
    description: "Net Sales / Transactions in local currency"
    type: number
#     maybe should be unique transaction count??
    sql: 1.0 * ${net_sales_amount_local} / NULLIF(${transaction_count},0) ;;

    value_format_name: decimal_2
  }


  measure: quantity_sold {
    group_label: "Quantity metrics"
    description: "Aggregate Quantity of Units Sold"
    type: sum
    value_format_name: decimal_0
    sql: ${TABLE}.mea_quantity ;;
  }
  measure: UPT {
    label: "UPT"
    description: "Units Per Transaction the calculation is Net Quantity sold divided by Count of Transactions"
    group_label: "Transaction metrics"
    type: number
    value_format_name: decimal_2
    sql: coalesce(${quantity_sold}/nullif(${transaction_count},0),0) ;;
  }
  measure: VPT {
    label: "VPT"
    description: "Value Per Transaction the calculation is Net Sales divided by Count of Transactions"
    group_label: "Transaction metrics"
    type: number
    value_format_name: usd
    sql: coalesce(${net_sales_amount_usd}/nullif(${transaction_count},0),0) ;;
  }

  measure: VPTLocal {
    label: "VPT Local"
    description: "Sales (Local) made per transaction"
    group_label: "Transaction metrics"
    type: number
    value_format_name: decimal_2
    sql:  coalesce(${net_sales_amount_local}/nullif(${transaction_count},0),0) ;;
  }

  measure: markdown_quantity_sold {
    group_label: "Quantity metrics"
    description: "Aggregate Quantity of Units Sold at Discount"
    type: sum
    value_format_name: decimal_0
    sql: ${count_markdown_items} ;;
  }

  measure: NetSalesUSD_onFP{
    label: "Net Sales Amount USD on Full Price"
    description: "Net Sales (USD) made on items sold at full price (no discount)"
    group_label: "Net Sales"
    type: sum
    value_format_name: usd
    sql: CASE WHEN ${discountamt_usd} = 0 or  ${discountamt_usd} is NULL THEN (${amountusd_beforetax})
      ELSE null END ;;
  }
  measure: NetSalesUSD_onMD{
    label: "Net Sales Amount USD on Markdown"
    description: "Net Sales (USD) made on items sold at discount"
    group_label: "Net Sales"
    type: sum
    value_format_name: usd
    sql: CASE WHEN ${discountamt_usd} <> 0 THEN (${amountusd_beforetax})
      ELSE null END ;;
  }

##------------MTD Net Sales Local Calculations

  measure: current_year_mtd_net_sales_local {
    description: "MTD Net Sales (Local Currency)"
    group_label: "Net Sales"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(CURRENT_DATE(), MONTH) THEN (${amountlocal_beforetax})
      ELSE null END ;;
  }

  measure: current_year_mtd_net_sales_usd {
    description: "MTD Net Sales (USD)"
    group_label: "Net Sales"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(CURRENT_DATE(), MONTH) THEN (${amountusd_beforetax})
      ELSE null END ;;
  }

  measure: last_year_mtd_net_sales_local {
    description: "LYMTD Net Sales (Local Currency)"
    group_label: "Net Sales"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(DATE_SUB(current_date(), INTERVAL 1 YEAR), MONTH) and ${business_date} <= DATE_SUB(current_date(), INTERVAL 1 YEAR) THEN (${amountlocal_beforetax})
      ELSE null END ;;
  }

  measure: two_years_ago_mtd_net_sales_local {
    description: "LYMTD-1 Net Sales (Local Currency)"
    group_label: "Net Sales"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(DATE_SUB(current_date(), INTERVAL 2 YEAR), MONTH) and ${business_date} <= DATE_SUB(current_date(), INTERVAL 2 YEAR) THEN (${amountlocal_beforetax})
      ELSE null END ;;
  }

  ##------------YTD Net Sales Local Calculations
  measure: current_year_ytd_net_sales_local {
    description: "YTD Net Sales (Local Currency)"
    group_label: "Net Sales"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(CURRENT_DATE(), YEAR) THEN (${amountlocal_beforetax})
      ELSE null END ;;
  }

  measure: last_year_ytd_net_sales_local {
    description: "LYTD Net Sales (Local Currency)"
    group_label: "Net Sales"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(DATE_SUB(current_date(), INTERVAL 1 YEAR), YEAR) and ${business_date} <= DATE_SUB(current_date(), INTERVAL 1 YEAR) THEN (${amountlocal_beforetax})
      ELSE null END ;;
  }

  measure: two_years_ago_ytd_net_sales_local {
    description: "LYTD-1 Net Sales (Local Currency)"
    group_label: "Net Sales"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(DATE_SUB(current_date(), INTERVAL 2 YEAR), YEAR) and ${business_date} <= DATE_SUB(current_date(), INTERVAL 2 YEAR) THEN (${amountlocal_beforetax})
      ELSE null END ;;
  }

  measure: current_year_ytd_net_sales_usd {
    description: "YTD Net Sales (USD)"
    group_label: "Net Sales"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(CURRENT_DATE(), YEAR) THEN (${amountusd_beforetax})
      ELSE null END ;;
  }

##------------MTD Gross Margin Local Calculations

  measure: current_year_mtd_gross_margin_local {
    description: "MTD Gross Margin (Local Currency)"
    group_label: "Gross Margin"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(CURRENT_DATE(), MONTH) THEN (${amountlocal_beforetax} - (ABS(${av_cost_local})*${mea_quantity}))
      ELSE null END ;;
  }

  measure: last_year_mtd_gross_margin_local {
    description: "LYMTD Gross Margin (Local Currency)"
    group_label: "Gross Margin"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(DATE_SUB(current_date(), INTERVAL 1 YEAR), MONTH) and ${business_date} <= DATE_SUB(current_date(), INTERVAL 1 YEAR) THEN (${amountlocal_beforetax} - (ABS(${av_cost_local})*${mea_quantity}))
      ELSE null END ;;
  }

  measure: two_years_ago_mtd_gross_margin_local {
    description: "LYMTD-1 Gross Margin (Local Currency)"
    group_label: "Gross Margin"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(DATE_SUB(current_date(), INTERVAL 2 YEAR), MONTH) and ${business_date} <= DATE_SUB(current_date(), INTERVAL 2 YEAR) THEN (${amountlocal_beforetax} - (ABS(${av_cost_local})*${mea_quantity}))
      ELSE null END ;;
  }

  ##------------YTD Gross Margin Local Calculations
  measure: current_year_ytd_gross_margin_local {
    description: "YTD Gross Margin (Local Currency)"
    group_label: "Gross Margin"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(CURRENT_DATE(), YEAR) THEN (${amountlocal_beforetax} - (ABS(${av_cost_local})*${mea_quantity}))
      ELSE null END ;;
  }

  measure: last_year_ytd_gross_margin_local {
    description: "LYTD Gross Margin (Local Currency)"
    group_label: "Gross Margin"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(DATE_SUB(current_date(), INTERVAL 1 YEAR), YEAR) and ${business_date} <= DATE_SUB(current_date(), INTERVAL 1 YEAR) THEN (${amountlocal_beforetax} - (ABS(${av_cost_local})*${mea_quantity}))
      ELSE null END ;;
  }

  measure: two_years_ago_ytd_gross_margin_local {
    description: "LYTD-1 Gross Margin (Local Currency)"
    group_label: "Gross Margin"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${business_date} >= DATE_TRUNC(DATE_SUB(current_date(), INTERVAL 2 YEAR), YEAR) and ${business_date} <= DATE_SUB(current_date(), INTERVAL 2 YEAR) THEN (${amountlocal_beforetax} - (ABS(${av_cost_local})*${mea_quantity}))
      ELSE null END ;;
  }

  ##------------Adding Net Sales on FP and MD Local

  measure: NetSalesLocal_onFP{
    label: "Net Sales Amount Local on Full Price"
    description: "Net Sales (Local) made on items sold at full price (no discount)"
    group_label: "Net Sales"
    type: sum
    value_format_name: decimal_2
    sql: CASE WHEN ${discountamt_local} = 0 or  ${discountamt_local} is NULL THEN (${amountlocal_beforetax})
      ELSE null END ;;
  }
  measure: NetSalesLocal_onMD{
    label: "Net Sales Amount Local on Markdown"
    description: "Net Sales (Local) made on items sold at discount"
    group_label: "Net Sales"
    type: sum
    value_format_name: decimal_2
    sql: CASE WHEN ${discountamt_local} <> 0 THEN (${amountlocal_beforetax})
      ELSE null END ;;
  }

  ##------------

  measure: COGSUSD_onFP{
    group_label: "Cost of Goods Sold"
    description: "Cost (USD) of items sold at full price (no discount)"
    label: "COGS USD on Full Price"
    type: sum
    value_format_name: usd
    sql: CASE WHEN ${discountamt_usd} = 0 or  ${discountamt_usd} is NULL THEN ABS(${av_cost_usd})*${mea_quantity}
      ELSE null END ;;
  }
  measure: COGSUSD_onMD{
    group_label: "Cost of Goods Sold"
    description: "Cost (USD) of items sold at discount"
    label: "COGS USD on Markdown"
    type: sum
    value_format_name: usd
    sql: CASE WHEN ${discountamt_usd} <> 0 THEN ABS(${av_cost_usd})*${mea_quantity}
      ELSE null END ;;
  }

  ##---------Adding COGS on MD and FP Local

  measure: COGSLocal_onFP{
    group_label: "Cost of Goods Sold"
    description: "Cost (Local) of items sold at full price (no discount)"
    label: "COGS Local on Full Price"
    type: sum
    value_format_name: decimal_2
    sql: CASE WHEN ${discountamt_local} = 0 or  ${discountamt_local} is NULL THEN ABS(${av_cost_local})*${mea_quantity}
      ELSE null END ;;
  }
  measure: COGSLocal_onMD{
    group_label: "Cost of Goods Sold"
    description: "Cost (Local) of items sold at discount"
    label: "COGS Local on Markdown"
    type: sum
    value_format_name: decimal_2
    sql: CASE WHEN ${discountamt_local} <> 0 THEN ABS(${av_cost_local})*${mea_quantity}
      ELSE null END ;;
  }

  ##----------

  #SPD-286
  measure: quantity_sold_onFP {
    group_label: "Quantity metrics"
    label: "Full Price Quantity Sold"
    description: "Aggregate Quantity of Units Sold on FP"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${discountamt_usd} = 0 THEN (${mea_quantity})
      ELSE null END ;;
  }

  measure: GrossMarginUSD_onFP{
    label: "Gross Margin USD on Full Price"
    description: "Net Sales (USD) minus Cost of Goods Sold (USD) of items sold at full price"
    group_label: "Gross Margin"
    type: number
    value_format_name: usd
    sql: ${NetSalesUSD_onFP}-${COGSUSD_onFP}
      ;;
  }

  #SPD-287
  measure: GrossMarginUSD_onMD{
    label: "Gross Margin USD on Markdown"
    description: "Net Sales (USD) minus Cost of Goods Sold (USD) of items sold at a discount"
    group_label: "Gross Margin"
    type: number
    value_format_name: usd
    sql: ${NetSalesUSD_onMD}-${COGSUSD_onMD}
      ;;
  }

  ##------------Adding Gross Margin on FP and MD Local

  measure: GrossMarginLocal_onFP{
    label: "Gross Margin Local on Full Price"
    description: "Net Sales (Local) minus Cost of Goods Sold (Local) of items sold at full price"
    group_label: "Gross Margin"
    type: number
    value_format_name: decimal_2
    sql: ${NetSalesLocal_onFP}-${COGSLocal_onFP}
      ;;
  }

  #SPD-287
  measure: GrossMarginLocal_onMD{
    label: "Gross Margin Local on Markdown"
    description: "Net Sales (Local) minus Cost of Goods Sold (Local) of items sold at a discount"
    group_label: "Gross Margin"
    type: number
    value_format_name: decimal_2
    sql: ${NetSalesLocal_onMD}-${COGSLocal_onMD}
      ;;
  }

  ##------------



#SPD-28 Start

measure: ReturnQauntity {
  group_label: "Quantity metrics"
  description: "Aggregate Quantity of Units Returned"
  type: sum
  label: "Returned Quantity"
  hidden: no
  sql:case when ${mea_quantity} <0 then ABS(${mea_quantity}) else null end;;
}

measure: TotalQuantity_beforeReturn{
  type: sum
  hidden: yes
  sql:case when ${mea_quantity} >=0 then ${mea_quantity} else null end  ;;
}

measure:ReturnRate  {
  label: "Return Rate"
  description: "Percentage of total number of items returned vs total number of items sold"
  type: number
  value_format_name: percent_2
  sql: SAFE_DIVIDE(cast(${ReturnQauntity} as INT64),cast(${TotalQuantity_beforeReturn} as INT64));;
}

#SPD-28 End



#   measure: sell_through_rate {
#     type: number
#     value_format_name: percent_2
#     sql: SAFE_DIVIDE(SUM(${mea_quantity}),MAX(${order_sums.qty_received})) ;;
#   }
#
#   measure: sell_through_rate_purchased_value {
#     type: number
#     value_format_name: percent_2
#     sql: SAFE_DIVIDE(SUM(${mea_quantity}),MAX(${order_sums.qty_received})) ;;
#   }
#
#   measure: sell_through_rate_md {
#     type: number
#     label: "Sell Through Rate % MD"
#     value_format_name: percent_2
#     sql: SAFE_DIVIDE(SUM(CASE WHEN ${discountamt_usd} > 0 THEN ${mea_quantity} ELSE 0 END),MAX(${order_sums.qty_received}));;
#   }
#
#   measure: sell_through_rate_fp {
#     type: number
#     label: "Sell Through Rate % FP"
#     value_format_name: percent_2
#     sql: SAFE_DIVIDE(SUM(CASE WHEN ${discountamt_usd} = 0 OR ${discountamt_usd} IS NULL THEN ${mea_quantity} ELSE 0 END),SUM(${order_sums.qty_received})) ;;
#   }

  measure: item_count {
    type: count_distinct
    label: "Count of Distinct Items"
    value_format_name: decimal_0
    sql: ${bk_productid} ;;
  }
#   measure: total_visits {
#     type: count_distinct
#     sql: CONCAT(${dim_retail_loc.mall_name}, CAST(${atr_trandate_date} AS STRING), ${atr_muse_id}) ;;
#   }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [id]
  }
  measure: average_retail_value_usd {
    description: "Net Sales / Net Sale Qty"
    type: number
    sql:${net_sales_amount_usd} / ${quantity_sold} ;;
    value_format_name: usd
  }

# ---------------------SPD-275  Changes to include WAC for consignment starts -----------------------





# ---------------------SPD-275  Changes to include WAC for consignment ends -----------------------


#-------Adding net transaction count which excludes returns. And  UPT, VPT to follow it------------

  measure: sales_transaction_count {
    type: count_distinct
    group_label: "Transaction metrics (Sales only)"
    description: "Total number of sales transactions"
    filters: {
      field: tran_type
      value: "SALES"
    }
    value_format_name: decimal_0
    sql: ${transaction_id};;
    drill_fields: [dim_retail_product.vpn, transaction_count]
  }
  measure: sales_qty_count {
    type: count_distinct
    group_label: "Transaction metrics (Sales only)"
    description: "Total number of qty sold"
    filters: {
      field: tran_type
      value: "SALES"
    }
    value_format_name: decimal_0
    sql: ${mea_quantity};;
    drill_fields: [dim_retail_product.vpn, transaction_count]
  }
  measure: UPT_sales_only {
    label: "UPT (Sales)"
    description: "Units Sold per transaction"
    group_label: "Transaction metrics (Sales only)"
    type: number
    value_format_name: decimal_2
   # sql: ${quantity_sold}/${sales_transaction_count} ;;
    sql: coalesce(${quantity_sold}/nullif(${sales_transaction_count},0),0) ;;
  }
  measure: VPT_sales_only {
    label: "VPT USD (Sales)"
    description: "Sales made per transaction"
    group_label: "Transaction metrics (Sales only)"
    type: number
    value_format_name: usd
    sql: coalesce( ${net_sales_amount_usd}/nullif(${sales_transaction_count},0),0) ;;
  }

  measure: VPTLocal_sales_only {
    label: "VPT Local (Sales)"
    description: "Sales (Local) made per transaction"
    group_label: "Transaction metrics (Sales only)"
    type: number
    value_format_name: decimal_2
    sql: coalesce(${net_sales_amount_local}/nullif(${sales_transaction_count},0),0) ;;
  }

  measure: distinct_zone {
    label: "Distinct Zones"
    description: "Count of Distinct Zones Sold"
    group_label: "Transaction metrics (Sales only)"
    type: count_distinct
    value_format_name: decimal_0
    sql: CASE WHEN ${ch_trait_attribute.code_desc} IS NULL THEN "OTHERS" ELSE ${ch_trait_attribute.code_desc} END ;;
  }

  measure: distinct_dept_zone {
    label: "Distinct Dept Zones"
    description: "Count of Distinct Dept Zones Sold"
    group_label: "Transaction metrics (Sales only)"
    type: count_distinct
    value_format_name: decimal_0
    sql: CASE WHEN ${ch_trait_attribute.dept_zone} IS NULL THEN "OTHERS" ELSE ${ch_trait_attribute.dept_zone} END ;;
  }

}
