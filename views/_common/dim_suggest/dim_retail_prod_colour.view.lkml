view: dim_retail_prod_colour {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT attrb_color AS colour FROM `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` ;;
  }
  dimension: colour {}
}
