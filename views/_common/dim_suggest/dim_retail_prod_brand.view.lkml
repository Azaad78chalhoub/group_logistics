view: dim_retail_prod_brand {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT brand FROM `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product`  ;;
  }
  dimension: brand {}
}
