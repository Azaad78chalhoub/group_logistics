view: dim_retail_prod_gender {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT gender FROM `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` ;;
  }
  dimension: gender {}
}
