view: dim_retail_loc_country {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT country_id  FROM dim_retail_loc ;;
  }
  dimension: country_id {}
}
