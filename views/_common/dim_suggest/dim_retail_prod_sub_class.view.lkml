view: dim_retail_prod_sub_class {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT subclass_name FROM `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` ;;
  }
  dimension: subclass_name {}
}
