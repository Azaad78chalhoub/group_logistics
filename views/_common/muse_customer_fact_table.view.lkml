include: "customer_fact_table.view"

view: muse_customer_fact_table {
  extends: [customer_fact_table]

  view_label: "Muse Customer Information"

  derived_table: {
    explore_source: fact_retail_sales {
      column: customer_id { field: fact_retail_sales.atr_muse_id }
    }
  }

  dimension: customer_id {
    label: "Sales Information Muse Customer ID"
  }

}

view: muse_active_years {
  extends: [customer_active_years]

  view_label: "Muse Customer Information"

  derived_table: {
    explore_source: fact_retail_sales {
      column: customer_id { field: fact_retail_sales.atr_muse_id }
    }
  }

}
