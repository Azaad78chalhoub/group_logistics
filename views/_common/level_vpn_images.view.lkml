view: level_vpn_images {
  derived_table: {
    sql: select vpn, url from `chb-prod-data-comm.prod_commercial.level_vpn_images`
      ;;
  }

  dimension: vpn {
    type: string
    description: "VPN"
    label: "VPN"
    hidden: yes
    sql: ${TABLE}.vpn ;;
  }

  dimension: web_image {
    type: string
    description: "Level Shoes Website Image"
    label: "Website Image"
    hidden: yes
    sql: ${TABLE}.url ;;
    html: <img src={{value}}> ;;
  }

  dimension: web_url {
    type: string
    description: "Level Shoes Website Image URL"
    label: "Website Image URL"
    hidden: yes
    sql: ${TABLE}.url ;;
  }

}
