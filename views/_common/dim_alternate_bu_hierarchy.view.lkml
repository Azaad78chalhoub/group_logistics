include: "../_common/dim_retail_loc.view"

view: dim_alternate_bu_hierarchy {
  sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` ;;

  dimension: brand {

    type: string
    view_label: "BU Alternative Hierarchy"
    label: "Business Unit"
    description: "Brand Name from alternative hierarchy"
    sql: ${TABLE}.brand ;;
  }

  dimension: bu_code {

    type: string
    label: "BU Code"
    view_label: "BU Alternative Hierarchy"
    description: "BU code from alternative hierarchy"
    sql: ${TABLE}.bu_code ;;
  }

  dimension: bu_dep_code {

    type: string
    view_label: "BU Alternative Hierarchy"
    label: "BU Department Code"
    description: "BU Department Code from alternative hierarchy"
    sql: ${TABLE}.bu_dep_code ;;
  }

  dimension: bu_dep_desc {

    type: string
    view_label: "BU Alternative Hierarchy"
    label: "BU Department Description"
    description: "BU Departmetn Description from alternative hierarchy"
    sql: ${TABLE}.bu_dep_desc ;;
  }

  dimension: bu_desc {

    type: string
    view_label: "BU Alternative Hierarchy"
    description: "BU description from alternative hierarchy"
    label: "BU Description"
    sql: ${TABLE}.bu_desc ;;
  }

  dimension: country {

    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Country that the transaction happened is attributed to in RMS value examples are UAE, KSA"
    label: "Country"
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: country_revised {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Country value from alternative hierarchy. all the countries not KSA and UAE to be grouped in Other"
    label: "Country Revised"
    sql: CASE WHEN ${TABLE}.country = "KSA" then "2-KSA"
              WHEN ${TABLE}.country = "UAE" then "1-UAE"
              ELSE "3-OTHER"
              END;;
  }

  dimension: ownership {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Ownership from alternative hierarchy"
    sql: ${TABLE}.ownership ;;
  }

  dimension: store_code {

    type: string
    view_label: "BU Alternative Hierarchy"

    description: "Store code from alternative hierarchy"
    sql: ${TABLE}.store_code ;;
  }

  dimension: sub_vertical {
    type: string
    view_label: "BU Alternative Hierarchy"


    sql: ${TABLE}.sub_vertical ;;
  }


  dimension: boat{
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Boat from alternative hierarchy"
    label: "Boat"
    sql: ${TABLE}.boat ;;
  }

  dimension: vertical {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Vertical from alternative hierarchy"
    label: "Vertical"
    sql: ${TABLE}.vertical ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: []
  }


  measure: store_count {
    type: count_distinct
    hidden: no
    view_label: "BU Alternative Hierarchy"
    description: "Number of Stores"
    label: "Number of Stores"
    sql: ${store_code} ;;

  }

  # dimension: city {
  #   type: string
  #   hidden: yes
  #   view_label: "BU Alternative Hierarchy"
  #   sql: ${TABLE}.city ;;
  # }

  dimension: country_name {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Country ID used for Map Visualizations"
    label: "Country ID"
    map_layer_name: countries
    sql: CASE WHEN ${TABLE}.country ="UAE" then "AE"
    WHEN ${TABLE}.country ="KSA" then "SA"
    WHEN ${TABLE}.country ="BAH" then "BH"
    ELSE ${TABLE}.country END  ;;
    #drill_fields: [city, store_code]
    drill_fields: [store_code]
  }

}
