view: dim_date {
  sql_table_name: `chb-prod-data-cust.CX_ControlTower.dim_date` ;;

  ###########################################
  #####        DATE DIMENSIONS          #####
  ###########################################

  dimension_group: calendar {
    type: time
    label: "Date"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.calendar_date AS TIMESTAMP) ;;
  }

  dimension: is_last_twelve_months {
    type: yesno
    sql: date_diff(current_date, ${calendar_date}, month) < 12 ;;
  }

  dimension: is_ytd {
    type: yesno
    sql: EXTRACT(DAYOFYEAR FROM ${calendar_date}) <= EXTRACT(DAYOFYEAR FROM CURRENT_DATE) ;;
  }

  dimension: is_mtd {
    type: yesno
    sql: EXTRACT(DAY FROM ${calendar_date}) <= EXTRACT(DAY FROM CURRENT_DATE) ;;
  }

  ###############################################
  #####         PERIOD OVER PERIOD          #####
  ###############################################

  filter: current_date_range {
    type: date
#     view_label: "_POP"
    label: "1. Date Range"
    description: "Select the date range you are interested in using this filter, can be used by itself. Make sure any filter on Event Date covers this period, or is removed."
    sql: ${period} IS NOT NULL ;;
  }

  parameter: compare_to {
#     view_label: "_POP"
    description: "Choose the period you would like to compare to. Must be used with Current Date Range filter"
    label: "2. Compare To:"
    type: unquoted
    allowed_value: {
      label: "Previous Period"
      value: "Period"
    }
    allowed_value: {
      label: "Previous Week"
      value: "Week"
    }
    allowed_value: {
      label: "Previous Month"
      value: "Month"
    }
    allowed_value: {
      label: "Previous Quarter"
      value: "Quarter"
    }
    allowed_value: {
      label: "Previous Year"
      value: "Year"
    }
    default_value: "Period"
  }

## -------------------------------------------------- ##

## ------------------ HIDDEN HELPER DIMENSIONS  ------------------ ##

  dimension: days_in_period {
    hidden:  yes
#     view_label: "_POP"
    description: "Gives the number of days in the current period date range"
    type: number
    sql: abs(date_diff(DATE({% date_start current_date_range %}), DATE({% date_end current_date_range %}), day)) ;;
  }

  dimension: period_2_start {
    hidden:  yes
#     view_label: "_POP"
    description: "Calculates the start of the previous period"
    type: date
    sql:
      {% if compare_to._parameter_value == "Period" %}
        date_add(DATE({% date_start current_date_range %}), INTERVAL  -${days_in_period} DAY)
      {% else %}
        date_add(DATE({% date_start current_date_range %}), INTERVAL  -1 {% parameter compare_to %})
      {% endif %};;
  }

  dimension: period_2_end {
    hidden:  yes
#     view_label: "_POP"
    description: "Calculates the end of the previous period"
    type: date
    sql:
      {% if compare_to._parameter_value == "Period" %}
        date_add(DATE({% date_start current_date_range %}), INTERVAL -1 DAY)
      {% else %}
        date_add(date_add(DATE({% date_end current_date_range %}), INTERVAL -1 DAY), INTERVAL -1 {% parameter compare_to %})
      {% endif %};;
  }

  dimension: day_in_period {
    hidden: yes
#     view_label: "_POP"
    description: "Gives the number of days since the start of each periods. Use this to align the event dates onto the same axis, the axes will read 1,2,3, etc."
    type: number
    sql:
    {% if current_date_range._is_filtered %}
      CASE
        WHEN {% condition current_date_range %} ${calendar_raw} {% endcondition %}
        THEN abs(date_diff(DATE({% date_start current_date_range %}), ${calendar_date}, DAY)) + 1
        WHEN ${calendar_date} between ${period_2_start} and ${period_2_end}
        THEN abs(date_diff(${period_2_start}, ${calendar_date}, DAY)) + 1
      END
    {% else %} NULL
    {% endif %}
    ;;
  }

  dimension: order_for_period {
    hidden: yes
#     view_label: "_POP"
    type: number
    sql:
       {% if current_date_range._is_filtered %}
         CASE
           WHEN {% condition current_date_range %} ${calendar_raw} {% endcondition %}
           THEN 1
           WHEN ${calendar_date} between ${period_2_start} and ${period_2_end}
           THEN 2
         END
       {% else %}
         NULL
       {% endif %}
       ;;
  }

## -------------------------------------------------- ##

## ------------------ DIMENSIONS TO PLOT ------------------ ##

  dimension: period {
#     view_label: "_POP"
    label: "Period"
    description: "Pivot me! Returns the period the metric covers, i.e. either the 'This Period' or 'Previous Period'"
    type: string
    order_by_field: order_for_period
    sql:
       {% if current_date_range._is_filtered %}
         CASE
           WHEN {% condition current_date_range %} ${calendar_raw} {% endcondition %}
           THEN 'This {% parameter compare_to %}'
           WHEN ${calendar_date} between ${period_2_start} and ${period_2_end}
           THEN 'Last {% parameter compare_to %}'
         END
       {% else %}
         NULL
       {% endif %}
       ;;
  }

  dimension_group: date_in_period {
    description: "Use this as your date dimension when comparing periods. Aligns the previous periods onto the current period"
    label: "Current Period"
    type: time
    sql: cast(date_add(DATE({% date_start current_date_range %}), INTERVAL ${day_in_period} - 1 DAY) as timestamp) ;;
#     view_label: "_POP"
    timeframes: [date, week, month, quarter, year]
  }

  dimension: _fivetran_deleted {
    hidden: yes
    type: yesno
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

dimension: cal_day_in_month {
  hidden: yes
  type: number
  sql: ${TABLE}.cal_day_in_month ;;
}

dimension: cal_day_in_week {
  hidden: yes
  type: string
  sql: ${TABLE}.cal_day_in_week ;;
}

dimension: cal_day_in_week_no {
  hidden: yes
  type: number
  sql: ${TABLE}.cal_day_in_week_no ;;
}

dimension: cal_day_in_year {
  hidden: yes
  type: number
  sql: ${TABLE}.cal_day_in_year ;;
}

dimension: cal_month {
  hidden: yes
  type: number
  sql: ${TABLE}.cal_month ;;
}


  dimension: Yearmonth {
    type: string
    sql: format_date("%Y-%m", ${calendar_date});;
  }


  dimension: MonthNameDay {
    type: string
    sql: format_date("%b-%d", ${calendar_date});;
  }

  dimension: DayMonth {
    type: number
    sql: cast(format_date("%m%d", ${calendar_date}) as INT64);;
  }



dimension: cal_month_name {
  #hidden: yes
  label: "Month Name"
  type: string
  sql: ${TABLE}.cal_month_name ;;
}

dimension: cal_month_no {
  #hidden: yes
  label: "Month Nb"
  type: number
  sql: ${TABLE}.cal_month_no ;;
}

dimension: cal_month_year {
  hidden: yes
  type: string
  sql: ${TABLE}.cal_month_year ;;
}

dimension: cal_quarter {
  #hidden: yes
  label: "Yearquarter"
  type: number
  sql: ${TABLE}.cal_quarter ;;
}

dimension: cal_quarter_name {
  #hidden: yes
  label: "Quarter Name"
  type: string
  sql: ${TABLE}.cal_quarter_name ;;
}

dimension: cal_quarter_no {
  #hidden: yes
  label: "Quarter Nb"
  type: number
  sql: ${TABLE}.cal_quarter_no ;;
}

dimension: cal_quarter_year {
  hidden: yes
  type: string
  sql: ${TABLE}.cal_quarter_year ;;
}

dimension: cal_week {
  hidden: yes
  type: number
  sql: ${TABLE}.cal_week ;;
}

dimension: cal_week_in_year {
  #hidden: yes
  label: "Week Nb"
  type: number
  sql: ${TABLE}.cal_week_in_year ;;
}

dimension: cal_week_name {
  #hidden: yes
  label: "Week Name"
  type: string
  sql: ${TABLE}.cal_week_name ;;
}

dimension: cal_week_year {
  hidden: yes
  type: string
  sql: ${TABLE}.cal_week_year ;;
}

dimension: cal_year {
  #hidden: yes
  label: "Year"
  type: number
  sql: ${TABLE}.cal_year ;;
}

dimension: dim_date_key {
  primary_key: yes
  hidden: yes
  type: number
  sql: ${TABLE}.dim_date_key ;;
}

dimension: filterdate {
  hidden: yes
  type: string
  sql: ${TABLE}.filterdate ;;
}

dimension: filterdatesort {
  hidden: yes
  type: number
  sql: ${TABLE}.filterdatesort ;;
}

dimension: holiday_desc {
  hidden: yes
  type: string
  sql: ${TABLE}.holiday_desc ;;
}

dimension: holiday_flag {
  hidden: yes
  type: string
  sql: ${TABLE}.holiday_flag ;;
}

dimension_group: update {
  hidden: yes
  type: time
  timeframes: [
    raw,
    time,
    date,
    week,
    month,
    quarter,
    year
  ]
  sql: CAST(${TABLE}.update_time AS TIMESTAMP) ;;
}

dimension: week_day_flag {
  hidden: yes
  type: string
  sql: ${TABLE}.week_day_flag ;;
}

dimension: week_end_flag {
  hidden: yes
  type: string
  sql: ${TABLE}.week_end_flag ;;
}

  parameter: date_granularity {
    type: string
    allowed_value: { value: "Day" }
    allowed_value: { value: "Month" }
    allowed_value: { value: "Quarter" }
    allowed_value: { value: "Week" }
    allowed_value: { value: "Year" }
  }


  dimension: date {
    label_from_parameter: date_granularity
    sql:
            CASE
             WHEN {% parameter date_granularity %} = 'Day' THEN ${MonthNameDay}
             WHEN {% parameter date_granularity %} = 'Month' THEN ${cal_month_name}
             WHEN {% parameter date_granularity %} = 'Week' THEN ${cal_week_name}
             WHEN {% parameter date_granularity %} = 'Quarter' THEN ${cal_quarter_name}
             WHEN {% parameter date_granularity %} = 'Year' THEN cast(${calendar_year} as string)
             ELSE ${MonthNameDay}
            END ;;
  }


  dimension: date_sort {
    type: number
    label_from_parameter: date_granularity
    sql:
            CASE
             WHEN {% parameter date_granularity %} = 'Day' THEN ${DayMonth}
             WHEN {% parameter date_granularity %} = 'Month' THEN ${cal_month_no}
             WHEN {% parameter date_granularity %} = 'Week' THEN ${cal_week_in_year}
             WHEN {% parameter date_granularity %} = 'Quarter' THEN ${cal_quarter_no}
             WHEN {% parameter date_granularity %} = 'Year' THEN ${cal_year}
             ELSE ${DayMonth}
            END ;;
  }

}
