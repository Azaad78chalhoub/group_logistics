view: dim_conversion_daily_rates {
  sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_conversion_daily_rates`
    ;;

dimension: pk {
  primary_key: yes
  type: string
  hidden: yes
  sql: CONCAT(${conversion_date} " - " ${from_currency} " - " ${to_currency} " - " ${conversion_type} ;;
}

  dimension: _fivetran_deleted {
    type: yesno
    hidden: yes
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension: _fivetran_id {
    type: string
    hidden: yes
    sql: ${TABLE}._fivetran_id ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: attribute1 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute1 ;;
  }

  dimension: attribute10 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute10 ;;
  }

  dimension: attribute11 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute11 ;;
  }

  dimension: attribute12 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute12 ;;
  }

  dimension: attribute13 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute13 ;;
  }

  dimension: attribute14 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute14 ;;
  }

  dimension: attribute15 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute15 ;;
  }

  dimension: attribute2 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute2 ;;
  }

  dimension: attribute3 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute3 ;;
  }

  dimension: attribute4 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute4 ;;
  }

  dimension: attribute5 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute5 ;;
  }

  dimension: attribute6 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute6 ;;
  }

  dimension: attribute7 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute7 ;;
  }

  dimension: attribute8 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute8 ;;
  }

  dimension: attribute9 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute9 ;;
  }

  dimension: context {
    type: string
    hidden: yes
    sql: ${TABLE}.context ;;
  }

  dimension_group: conversion {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.conversion_date ;;
  }

  dimension: conversion_rate {
    type: number
    hidden: yes
    sql: ${TABLE}.conversion_rate ;;
  }

  dimension: conversion_type {
    type: string
    hidden: yes
    sql: ${TABLE}.conversion_type ;;
  }

  dimension: created_by {
    type: number
    hidden: yes
    sql: ${TABLE}.created_by ;;
  }

  dimension_group: creation {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.creation_date AS TIMESTAMP) ;;
  }

  dimension: from_currency {
    type: string
    hidden: yes
    sql: ${TABLE}.from_currency ;;
  }

  dimension_group: last_update {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.last_update_date AS TIMESTAMP) ;;
  }

  dimension: last_update_login {
    type: number
    hidden: yes
    sql: ${TABLE}.last_update_login ;;
  }

  dimension: last_updated_by {
    type: number
    hidden: yes
    sql: ${TABLE}.last_updated_by ;;
  }

  dimension: rate_source_code {
    type: string
    hidden: yes
    sql: ${TABLE}.rate_source_code ;;
  }

  dimension: status_code {
    type: string
    hidden: yes
    sql: ${TABLE}.status_code ;;
  }

  dimension: to_currency {
    type: string
    hidden: yes
    sql: ${TABLE}.to_currency ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: []
  }
}
