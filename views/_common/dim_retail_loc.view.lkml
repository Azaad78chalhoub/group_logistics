view: dim_retail_loc {
  view_label: "Locations"
  sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` ;;


  dimension: add_1 {
    type: string
    hidden: yes
    sql: ${TABLE}.add_1 ;;
  }

  dimension: add_2 {
    type: string
    hidden: yes
    sql: ${TABLE}.add_2 ;;
  }

  dimension: addr_key {
    type: number
    hidden: yes
    sql: ${TABLE}.addr_key ;;
  }

  dimension: addr_type {
    type: string
    hidden: yes
    sql: ${TABLE}.addr_type ;;
  }

  dimension: area {
    type: number
    hidden: yes
    sql: ${TABLE}.area ;;
  }

  dimension: area_name {
    type: string
    hidden: yes
    sql: ${TABLE}.area_name ;;
  }

  dimension: chain {
    type: number
    hidden: yes
    sql: ${TABLE}.chain ;;
  }

  dimension: chain_name {
    type: string
    hidden: no
    sql: ${TABLE}.chain_name ;;
  }

  dimension: channel_id {
    type: number
    hidden: yes
    sql: ${TABLE}.channel_id ;;
  }

  dimension: channel_name {
    type: string
    hidden: yes
    sql: ${TABLE}.channel_name ;;
  }

  dimension: channel_type {
    type: string
    hidden: yes
    sql: ${TABLE}.channel_type ;;
  }

  dimension: city {
    view_label: "Locations"
    label: "City"
    description: "City coming from Oracle RMS"
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: country_desc {
    view_label: "Locations"
    label: "Country Name"
    description: "Country chosen is the same as attributed to in RMS, country names are spelled out example of Bahrain"
    type: string
    drill_fields: [store_name]
    suggest_explore: dim_retail_loc_store
    suggest_dimension: dim_retail_loc_store.store
    sql: ${TABLE}.country_desc ;;
  }

  dimension: country_desc_swa {
    view_label: "Locations"
    label: "Country Name(Swarovski)"
    description: "Country Name coming from Oracle RMS- For Swarovski, Ecom seperated"
    type: string
    drill_fields: [store_name]
    suggest_explore: dim_retail_loc_store
    suggest_dimension: dim_retail_loc_store.store
    sql: CASE WHEN ${TABLE}.store_name LIKE '%ECOMMERCE%' THEN 'ECOM'
          ELSE ${TABLE}.country_desc END ;;
  }

  dimension: country_id {
    type: string
    hidden: yes
    sql: ${TABLE}.country_id ;;
  }

  dimension: currency_code {
    type: string
    hidden: yes
    sql: ${TABLE}.currency_code ;;
  }

  dimension: description {
    type: string
    hidden: yes
    sql: ${TABLE}.description ;;
  }

  dimension: district {
    type: number
    hidden: yes
    sql: ${TABLE}.district ;;
  }

  dimension: district_name {
    view_label: "Locations"
    description: "District Name from Oracle RMS - similar to the brand name"
    type: string
    suggest_explore: dim_retail_loc_district_name
    suggest_dimension: dim_retail_loc_district_name.district_name
    sql: ${TABLE}.district_name ;;
  }


  dimension: store_name_segregation {
    group_label: "Locations"
    description: "Segregation of retail stores for Lacoste"
    label: "Channel"
    view_label: "Locations"
    type: string
    suggest_explore: dim_retail_loc_store_name
    suggest_dimension: dim_retail_loc_store_name.store_name
    sql: CASE WHEN ${TABLE}.store_name LIKE "LACOSTE - TRYANO%" THEN "TRYANO"
              WHEN ${TABLE}.store_name LIKE "LACOSTE - ECOMMERCE%"  THEN "ECOMMERCE"
              WHEN ${TABLE}.store_name LIKE "%LACOSTE - MERAAS OUTLET VILLAGE%" THEN "OUTLET"
              WHEN ${TABLE}.store_name LIKE "LACOSTE - NORTH COAST - ALEXANDRIA" THEN "OUTLET"
              WHEN ${TABLE}.store_name LIKE "LACOSTE -%" THEN "STORE"
              WHEN ${TABLE}.store_name LIKE "FILA -%" THEN "STORE"
              WHEN ${TABLE}.store_name LIKE "FILA - ECOMMERCE%"  THEN "ECOMMERCE"
              WHEN ${TABLE}.store_name LIKE "FILA - FARFETCH%" THEN "FARFETCH"
              END;;
  }

  dimension: key_value_1 {
    type: string
    hidden: yes
    sql: ${TABLE}.key_value_1 ;;
  }

  dimension: key_value_2 {
    type: string
    hidden: yes
    sql: ${TABLE}.key_value_2 ;;
  }

  dimension: mall_name {
    type: string
    hidden: no
    sql: ${TABLE}.mall_name ;;
  }

  dimension: module {
    type: string
    hidden: yes
    sql: ${TABLE}.module ;;
  }

  dimension: post {
    type: string
    hidden: yes
    sql: ${TABLE}.post ;;
  }

  dimension: primary_addr_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.primary_addr_ind ;;
  }

  dimension: region {
    type: number
    hidden: no
    sql: ${TABLE}.region ;;
  }

  dimension: region_name {
    type: string
    hidden: yes
    sql: ${TABLE}.region_name ;;
  }

  dimension: selling_square_ft {
    type: number
    label: "Selling Square feet"
    hidden: no
    sql: ${TABLE}.selling_square_ft ;;
  }

  dimension: total_square_ft {
    type: number
    label: "Total Square feet"
    hidden: no
    sql: ${TABLE}.total_square_ft ;;
  }

  dimension: state {
    type: string
    hidden: yes
    sql: ${TABLE}.state ;;
  }

  dimension: store {
    primary_key: yes
    type: number
    hidden: yes
    sql: ${TABLE}.store ;;
  }

  dimension_group: store_close {
    type: time
    hidden: no
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_close_date ;;
  }

  dimension: store_format {
    type: number
    view_label: "Locations"
    description: "Store Format of location in Oracle RMS"
    sql: ${TABLE}.store_format ;;
  }

  dimension: store_name {
    view_label: "Locations"
    label: "Location Name"
    description: "Name of location in Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_store_name
    suggest_dimension: dim_retail_loc_store_name.store_name
    sql: ${TABLE}.store_name ;;
  }

  dimension: is_ecommerce {
    view_label: "Locations"
    label: "Is Ecommerce?"
    type: yesno
    sql: ${store_name} LIKE "%ECOMMERCE%" ;;
  }

  dimension: chaneel_ecom_retail {
    view_label: "Locations"
    description: "Ecom or Retail Sales Channel"
    label: "Sales Channel"
    type: string
    sql: case when ${store_name} LIKE "%ECOMMERCE%" then "Ecom" else "Retail" end ;;
  }


  dimension: store_name10 {
    type: string
    hidden: yes
    sql: ${TABLE}.store_name10 ;;
  }

  dimension: store_name3 {
    type: string
    hidden: yes
    sql: ${TABLE}.store_name3 ;;
  }

  dimension_group: store_open {
    type: time
    view_label: "Locations"
    description: "Opening date of location in Oracle RMS"
#     hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_open_date ;;
  }

  dimension: vat_region {
    type: number
    hidden: yes
    sql: ${TABLE}.vat_region ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [detail*]
  }

  measure: count_brands {
    type:  count_distinct
    sql: ${TABLE}.district_name ;;
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      area_name,
      district_name,
      store_name,
      mall_name,
      region_name,
      chain_name,
      channel_name
    ]
  }
}
