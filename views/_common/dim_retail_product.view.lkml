view: dim_retail_product {
  view_label: "Products"
  sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` ;;

  dimension: activity {
    type: string
    hidden: yes
    sql: ${TABLE}.activity ;;
  }

  dimension: age_of_skin {
    type: string
    hidden: yes
    sql: ${TABLE}.age_of_skin ;;
  }
  dimension: short_desc {
    type: string
    label: "Style Description"
    hidden: no
    sql: ${TABLE}.short_desc ;;
  }

  dimension: short_desc_color {
    type: string
    label: "VPN Description"
    hidden: no
    sql: concat (${TABLE}.short_desc,"-",${TABLE}.attrb_color) ;;
  }

  dimension: alcohol_content_gross {
    type: string
    hidden: yes
    sql: ${TABLE}.alcohol_content_gross ;;
  }

  dimension: alcohol_content_net {
    type: string
    hidden: yes
    sql: ${TABLE}.alcohol_content_net ;;
  }

  dimension: arabic_desc {
    type: string
    hidden: yes
    sql: ${TABLE}.arabic_desc ;;
  }

  dimension: atrb_boy_girl {
    type: string
    hidden: yes
    sql: ${TABLE}.atrb_boy_girl ;;
  }

  dimension: attrb_color {
    view_label: "Products"
    label: "Colour"
    description: "Colour attribute from Oracle RMS"
    type: string
    suggest_explore: dim_retail_prod_colour
    suggest_dimension: dim_retail_prod_colour.colour
    sql: ${TABLE}.attrb_color ;;
  }

#SPD-295
  dimension: attrb_made_of {
    view_label: "Products"
    label: "Material"
    description: "'Made of' attribute from Oracle RMS"
    type: string
    hidden: no
    suggest_explore: dim_retail_prod_material
    suggest_dimension: dim_retail_prod_material.material
    sql: ${TABLE}.attrb_made_of ;;
  }

  dimension: attrb_theme {
    type: string
    hidden: yes
    sql: ${TABLE}.attrb_theme ;;
  }

  dimension: barcode {
    type: string
    hidden: yes
    sql: ${TABLE}.barcode ;;
  }



  dimension: batch_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.batch_ind ;;
  }

  dimension: brand {
    hidden: no
    view_label: "Products"
    description: "Product Brand Name this is sourced from Oracle"
    type: string
    suggest_explore: dim_retail_prod_brand
    suggest_dimension: dim_retail_prod_brand.brand
    sql: ${TABLE}.brand ;;
  }

  dimension: class {
    type: number
    hidden: no
    sql: ${TABLE}.class ;;
  }

  dimension: class_name {
    type: string
    hidden: yes
    drill_fields: [subclass_name]
    sql: ${TABLE}.class_name ;;
  }

  dimension: is_concession {
    type: yesno
    hidden: yes
    view_label: "Products"
    sql: UPPER(${class_name}) LIKE '%CONCESSION%' ;;
  }

  dimension: country_of_manu {
    type: string
    label: "Country of Manufacture"
    view_label: "Products"
    sql: ${TABLE}.country_of_manu ;;
  }

  dimension: dept_name {
    type: string
    hidden: no
    label: "Department Name"
    view_label: "Products"
    description: "Department attribute from Oracle RMS"
    sql: ${TABLE}.dept_name ;;
  }

  dimension: dept_no {
    type: number
    hidden: no
    sql: ${TABLE}.dept_no ;;
  }

  dimension: dgr_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.dgr_ind ;;
  }

  dimension: diff_1 {
    type: string
    hidden: yes
    sql: ${TABLE}.diff_1 ;;
  }

  dimension: diff_2 {
    type: string
    view_label: "Products"
    description: "Size (Diff 2) from Oracle RMS"
    label: "Size (Diff 2)"
    hidden: no
    sql: ${TABLE}.diff_2 ;;
  }

  dimension: division {
    description: "Division is a high level category that a product is attributed to (ex. Beauty, Fashion) from Oracle RMS"
    view_label: "Products"
    type: string
    drill_fields: [taxo_class,taxo_subclass ]
    suggest_explore: dim_retail_prod_division
    suggest_dimension: dim_retail_prod_division.division
    sql: ${TABLE}.division ;;
  }

  dimension: division_no {
    type: number
    hidden: yes
    sql: ${TABLE}.division_no ;;
  }

  dimension: entity_type {
    type: string
    view_label: "Products"
    hidden: no
    sql: ${TABLE}.entity_type ;;
  }

  dimension: format {
    type: string
    hidden: yes
    sql: ${TABLE}.format ;;
  }

  dimension: gender {
    view_label: "Products"
    description: "Gender is an attribute of an item examples are Women, Men, Unisex"
    type: string
    suggest_explore: dim_retail_prod_gender
    suggest_dimension: dim_retail_prod_gender.gender
    sql: ${TABLE}.gender ;;
  }

  dimension: grey_mkt_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.grey_mkt_ind ;;
  }

  dimension: group_name {
    type: string
    hidden: no
    sql: ${TABLE}.group_name ;;
  }

  dimension: group_no {
    type: number
    hidden: yes
    sql: ${TABLE}.group_no ;;
  }

  dimension: item {
    view_label: "Products"
    description: "Item ID that was sold in the transaction otherwise known as SKU, Product ID or Item Number"
    type: string
    primary_key: yes
    sql: ${TABLE}.item ;;
  }




  dimension: item_desc {
    label:"Product Description"
    view_label: "Products"
    description: "Product description from Oracle RMS"
    type: string
    hidden: no
    sql: ${TABLE}.item_desc ;;
  }

  dimension: item_desc_wo_vpn {
    label:"Product Description w/o vpn"
    view_label: "Products"
    description: "Description from Oracle RMS"
    type: string
    hidden: no
    sql: ${TABLE}.item_desc_wo_vpn;;
  }


  dimension: item_style {
    view_label: "Products"
    label: "Style"
    description: "Style ID from Oracle RMS"
    type: string
    suggest_explore: fact_soh_sales_poc
    suggest_dimension: dim_retail_prod.item_style
    sql: ${TABLE}.item_style ;;
  }

  dimension: line {
    type: string
    view_label: "Products"
    description: "Line is a trait for products that is at the lowest granularity such as as a name of a newly launched collection; more prevalant for beauty brands. "
    hidden: no
    sql: ${TABLE}.line ;;
  }

  dimension: isconcession {
    type: yesno
    view_label: "Products"
    hidden: no
    sql: ${TABLE}.line like '%CONCESSION%';;
  }
  dimension: perishable_ind {
    type: string
    view_label: "Products"
    hidden: no
    sql: ${TABLE}.perishable_ind ;;
  }

  dimension: recurrence {
    view_label: "Products"
    type: string
    hidden: no
    description: "Recurrence UDA for Season attribute"
    sql: ${TABLE}.recurrence ;;
  }

  dimension: sap_item_code {
    type: string
    hidden: yes
    sql: ${TABLE}.sap_item_code ;;
  }

  dimension: saso_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.saso_ind ;;
  }

  dimension: season_desc {
    view_label: "Products"
    description: "Season is a product attribute field sourced from Oracle RMS and is referred to in business as the Global Season trait"
    label: "Season"
    type: string
    suggest_explore: dim_retail_prod_season
    suggest_dimension: dim_retail_prod_season.season
    sql: ${TABLE}.season_desc ;;
  }

  dimension: sep_axis {
    type: string
    hidden: yes
    sql: ${TABLE}.sep_axis ;;
  }

  # dimension: sep_category {
  #   hidden: yes
  #   sql: ${TABLE}.sep_category ;;
  # }

  dimension: sep_market {
    type: string
    view_label: "Products"
    hidden: no
    label: "Market"
    sql: ${TABLE}.sep_market ;;
  }

  dimension: sep_nature {
    type: string
    hidden: yes
    sql: ${TABLE}.sep_nature ;;
  }

  dimension: sep_range {
    type: string
    hidden: yes
    sql: ${TABLE}.sep_range ;;
  }

  dimension: size_uda {
    view_label: "Products"
    description: "Size (user defined attribute) from Oracle RMS"
    type: string
    label: "Size (UDA)"
    suggest_explore: dim_retail_prod_size
    suggest_dimension: dim_retail_prod_size.size
    sql: ${TABLE}.size_uda ;;
  }

  dimension: standard_uom {
    type: string
    hidden: yes
    sql: ${TABLE}.standard_uom ;;
  }

  dimension: sub_line {
    type: string
    hidden: no
    view_label: "Products"
    sql: ${TABLE}.sub_line ;;
  }

  dimension: subclass {
    type: number
    hidden: no
    sql: ${TABLE}.subclass ;;
  }

  dimension: subclass_name {
    view_label: "Products"
    label: "Sub Class"
    description: "Product subclass from Oracle RMS - product categorization"
    type: string
    suggest_explore: dim_retail_prod_sub_class
    suggest_dimension: dim_retail_prod_sub_class.subclass_name
    sql: ${TABLE}.subclass_name ;;
  }

  dimension: sup_class {
    type: string
    hidden: yes
    sql: ${TABLE}.sup_class ;;
  }

  dimension: sup_subclass {
    type: string
    hidden: yes
    sql: ${TABLE}.sup_subclass ;;
  }

  dimension: taxo_class {
    view_label: "Products"
    label: "Taxonomy Class"
    description: "Taxonomy class is sourced from Oracle and refers to a product categorization at a lower level such as blouse or cleansers."
    type: string
    drill_fields: [taxo_subclass,item]
    suggest_explore: dim_retail_prod_taxo_class
    suggest_dimension: dim_retail_prod_taxo_class.taxo_class
    sql: ${TABLE}.taxo_class ;;
  }

  dimension: taxo_subclass {
    view_label: "Products"
    label: "Taxonomy Subclass"
    description: "Taxonomy subclass attribute from RMS"
    type: string
    suggest_explore: dim_retail_prod_taxo_subclass
    suggest_dimension: dim_retail_prod_taxo_subclass.taxo_subclass
    sql: ${TABLE}.taxo_subclass ;;
  }

  dimension: uc_code {
    type: string
    hidden: yes
    sql: ${TABLE}.uc_code ;;
  }

  dimension: uda_zone {
    type: string
    hidden: yes
    sql: ${TABLE}.uda_zone ;;
  }

  dimension: usage_specificity {
    type: string
    hidden: yes
    sql: ${TABLE}.usage_specificity ;;
  }

dimension: Category_basedon_division_TB {
    description: "Product categories for Tory Burch"
    label: "Category"
    type: string
    sql:  ${TABLE}.Category_basedon_division_TB;;
  }

  dimension: vpn {
    view_label: "Products"
    description: "Item VPN attribute from Oracle RMS"
    type: string
    label: "VPN"
    suggest_dimension: dim_retail_prod.item_vpn
    sql: ${TABLE}.vpn ;;
  }




  measure: count {
    type: count
    hidden: yes
    drill_fields: [group_name, class_name, dept_name, subclass_name]
  }

}

view: dim_retail_product__sep_category {

  dimension: item {
    type: string
    hidden: yes
    sql: ${TABLE}.ITEM ;;
  }

  dimension: sep_category {
    type: string
    hidden: yes
    sql: ${TABLE}.SEP_CATEGORY ;;
  }

  dimension: uda_value {
    type: number
    hidden: yes
    sql: ${TABLE}.UDA_VALUE ;;
  }

  dimension: group_name_FF1 {
    description: "Product categories for FF1"
    label: "Category FF1"
    type: string
    sql: ${TABLE}.group_name_FF1 ;;
  }

  dimension: merchandise_ind {
    description: "Merchandise Indicator"
    label: "Merchandise Indicator"
    type: string
    sql: ${TABLE}.merchandise_ind ;;
  }


  dimension: the_deal_division {
    description: "Deal division"
    label: "Deal division"
    type: string
    sql: ${TABLE}.the_deal_division ;;
  }

}
