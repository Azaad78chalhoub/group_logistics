view: ch_trait_attribute {
  label: "Location Attributes"
  sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_ch_trait_attribute`
    ;;

  dimension: _fivetran_deleted {
    hidden: yes
    type: yesno
    sql: ${TABLE}._fivetran_deleted ;;
  }
  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql: CONCAT(${code}, " - ", ${org_unit}) ;;
  }
  dimension_group: _fivetran_synced {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: code {
    type: string
    hidden: yes
    sql: ${TABLE}.code ;;
  }

  dimension: code_desc {
    type: string
    label: "Zone Description"
    description: "Zone description is the full name of the zone that is not abbreviated"
    sql: ${TABLE}.code_desc ;;
  }

  dimension: dept_zone {
    type: string
    label: "Department Zone Description"
    description: "Updated Department Zone Description for Tryano"
    sql: case when ${code_desc} = "EXTERNAL CONCESSION" then "CONCESSION"
    when ${code_desc} = "WOMEN SHOES" then "SHOES"
    when ${code_desc} = "WOMEN CONTEMPORARY RTW" then "RTW"
    when ${code_desc} = "WOMEN HANDBAGS" then "BAGS"
    when ${code_desc} = "INTERNAL CONCESSION" then "CONCESSION"
    when ${code_desc} = "WOMEN DESIGNER RTW" then "RTW"
    when ${code_desc} = "WOMEN CONTEMPORARY EVENINGWEAR" then "RTW"
    when ${code_desc} = "WOMEN ACCESSORIES" then "ACCESSORIES"
    when ${code_desc} = "WOMEN DESIGNER EVENINGWEAR" then "RTW"
    when ${code_desc} = "WOMENS BAGS" then "BAGS"
    when ${code_desc} = "JEWELRY" then "ACCESSORIES"
    when ${code_desc} = "SALON" then "BEAUTY"
    when ${code_desc} = "GIFTS" then "MEN"
    when ${code_desc} = "WOMEN CASUAL RTW" then "RTW"
    when ${code_desc} = "JUNIOR" then "KIDS"
    when ${code_desc} = "MENS BAG" then "MEN"
    when ${code_desc} = "WOMEN RTW" then "RTW"
    when ${code_desc} = "BABY" then "KIDS" ELSE ${code_desc} END
    ;;
  }

  dimension: code_type {
    type: string
    hidden: yes
    sql: ${TABLE}.code_type ;;
  }

  dimension_group: create {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.create_date ;;
  }

  dimension_group: last_update {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_update_date ;;
  }

  dimension: last_update_id {
    hidden: yes
    type: string
    sql: ${TABLE}.last_update_id ;;
  }

  dimension: org_unit {
    hidden: yes
    type: number
    sql: ${TABLE}.org_unit ;;
  }

  dimension: set_of_books_id {
    hidden: yes
    type: number
    sql: ${TABLE}.set_of_books_id ;;
  }

  dimension: status {
    hidden: yes
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: user_id {
    hidden: yes
    type: string
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: []
  }
}
