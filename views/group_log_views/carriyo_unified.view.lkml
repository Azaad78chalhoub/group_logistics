include: "../_common/period_over_period.view"
  view: carriyo_unified {
    sql_table_name: `chb-prod-stage-oracle.prod_logistics.carriyo_unified`
      ;;


    extends: [period_over_period]
    dimension_group: pop_no_tz {
      sql: ${update_date_date} ;;
    }

    # dimension: approximate_creation_date_time {
    #   type: number
    #   hidden: yes
    #   sql: ${TABLE}.approximate_creation_date_time ;;
    # }

    dimension: pk {
      primary_key: yes
      hidden: yes
      sql: CONCAT(${TABLE}.shipmentId, ${TABLE}.status, ${TABLE}.carrier, ${TABLE}.tracking_no, ${TABLE}.update_date, ${TABLE}.latest_status) ;;
    }

    dimension: ecom_pickup_city {
      type: string
      sql: ${TABLE}.ecom_pickup_city ;;
    }

    dimension: ecom_dropoff_city {
      type: string
      sql: ${TABLE}.ecom_dropoff_city ;;
    }

    dimension: ecom_pickup_region {
      type: string
      sql: ${TABLE}.ecom_dropoff_region ;;
      map_layer_name: saudi_map_layer
    }

    dimension: ecom_dropoff_region {
      type: string
      sql: ${TABLE}.ecom_dropoff_region ;;
      map_layer_name: saudi_map_layer
    }

    dimension_group: order_creation {
      type: time
      group_label: "Date - Order Creation"
      description: "Date the order was created"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.order_creation_date AS TIMESTAMP) ;;
    }

    dimension_group: shipped_date {
      type: time
      group_label: "Date - Shipped At"
      description: "Date the order was shipped"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.shipped_date AS TIMESTAMP) ;;
    }

    # dimension_group: promisedDeliverydate {
    #   type: time
    #   group_label: "Date - Promised Delivery"
    #   description: "Date the order was created in OMS/WM9"
    #   timeframes: [
    #     raw,
    #     time,
    #     date,
    #     week,
    #     month,
    #     quarter,
    #     year
    #   ]
    #   sql: CAST(${TABLE}.promisedDeliverydate AS TIMESTAMP) ;;
    #   html: {{ rendered_value | date: "%d-%m-%Y" }};;
    # }

    dimension: latest_status {
      hidden: yes
      description: "The latest status a tracking # has been updated with by in a descending manner"
      type: string
      sql: ${TABLE}.latest_status ;;
    }

    dimension: latest_status_boolean {
      group_label: "Statuses"
      description: "When filtered by yes; this returns the latest status a tracking # received"
      label: "Filter by latest status"
      type: yesno
      sql: ${latest_status}=1 ;;
    }

    # dimension: quantity {
    #   group_label: "Payment Info"
    #   type: number
    #   sql: ${TABLE}.quantity ;;
    # }

    # dimension: dangerous_goods_BOOL  {
    #   type: yesno
    #   hidden: yes
    #   sql: ${TABLE}.dangerous_goods_BOOL ;;
    # }

    # dimension: price {
    #   group_label: "Payment Info"
    #   type: number
    #   sql: ${TABLE}.price ;;
    # }

    dimension: currency {
      group_label: "Payment Info"
      description: "Local currency used to make the payment"
      type: string
      sql: ${TABLE}.currency ;;
    }

    # dimension: origin_country {
    #   group_label: "Address Related Info"
    #   type: string
    #   sql: ${TABLE}.origin_country ;;
    # }

    # dimension: description {
    #   group_label: "Item Info"
    #   type: string
    #   sql: ${TABLE}.description ;;
    # }

    # dimension: SKU {
    #   group_label: "Item Info"
    #   type: string
    #   sql: ${TABLE}.SKU ;;
    # }

    dimension: event_name {
      group_label: "Statuses"
      description: "Event name from Carriyo (INSERT/MODIFY)"
      type: number
      sql: ${TABLE}.event_name ;;
    }

    # dimension: sequence_number {
    #   type: number
    #   hidden: yes
    #   sql: ${TABLE}.sequence_number ;;
    # }

    # dimension: hsCode {
    #   type: string
    #   hidden: yes
    #   sql: ${TABLE}.hsCode ;;
    # }

    dimension: notes {
      group_label: "Statuses"
      description: "Additional Notes by Carriyo"
      type: string
      sql: ${TABLE}.notes ;;
    }

    dimension_group: dsp_creation_date {
      type: time
      group_label: "1- Date - DSP Creation Date"
      description: "Date the order was created with the DSPs"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.dsp_creation_date AS TIMESTAMP) ;;
    }

    dimension: carrier {
      type: string
      group_label: "Carrier Info"
      description: "The DSP actioning the tracking #"
      sql: CASE
            WHEN ${TABLE}.carrier='FAREYE'
              THEN 'IN-HOUSE'
            ELSE ${TABLE}.carrier
           END;;
    }

    dimension: partnerShipmentReference {
      label: "Internal Order #"
      group_label: "Shipment Info"
      description: "The internal order # in OMS/SFCC"
      type: string
      sql: ${TABLE}.partnerShipmentReference ;;
    }

    dimension: partnerOrderReference {
      label: "External Order #"
      group_label: "Shipment Info"
      description: "The external order # in OMS/SFCC"
      type: string
      sql: ${TABLE}.partnerOrderReference ;;
    }


    dimension: carrier_account_name {
      type: string
      group_label: "Carrier Info"
      description: "Account number of DSP"
      sql: ${TABLE}.carrier_account_name ;;
    }

    dimension: carrier_id {
      type: string
      group_label: "Carrier Info"
      description: "ID number of DSP"
      sql: ${TABLE}.carrier_id ;;
    }

    dimension: carrier_status {
      type: string
      group_label: "Carrier Info"
      description: "The status of the tracking # as received from the DSP"
      sql: TRIM(upper(${TABLE}.carrier_status)) ;;
    }

    dimension: carrier_status_description {
      type: string
      group_label: "Carrier Info"
      description: "The status description of the tracking # as received from the DSP"
      sql: ${TABLE}.carrier_status_description ;;
    }

    dimension_group: confirmation_date {
      type: time
      group_label: "4- Date - Confirmed At"
      description: "Date the order was confirmed"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.confirmation_date AS TIMESTAMP) ;;
    }

    dimension_group: carriyo_creation_date {
      type: time
      group_label: "3- Date - Carriyo Created At"
      description: "Date the order was created by Carriyo"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.carriyo_creation_date AS TIMESTAMP) ;;
    }

    dimension: delivery_type {
      group_label: "Statuses"
      description: "The type of delivery denoting EXPRESS/STANDARD"
      type: string
      sql: ${TABLE}.deliveryType ;;
    }

    dimension: dropoff_address1 {
      group_label: "Address Related Info"
      description: "Address of the dropoff destination"
      type: string
      sql: ${TABLE}.dropoff_address1 ;;
    }

    dimension: dropoff_address2 {
      group_label: "Address Related Info"
      description: "Address of the dropoff destination"
      type: string
      sql: ${TABLE}.dropoff_address2 ;;
    }

    dimension: dropoff_city {
      group_label: "Address Related Info"
      description: "The dropoff city"
      type: string
      sql: ${TABLE}.dropoff_city ;;
    }

    dimension: dropoff_contact_email {
      group_label: "Address Related Info"
      description: "Recipient's contact e-mail"
      hidden: yes
      type: string
      sql: ${TABLE}.dropoff_contact_email ;;
    }

    dimension: dropoff_contact_name {
      group_label: "Address Related Info"
      description: "Recipient's contact name"
      hidden: yes
      type: string
      sql: ${TABLE}.dropoff_contact_name ;;
    }

    dimension: dropoff_contact_phone {
      group_label: "Address Related Info"
      description: "Recipient's contact phone number"
      hidden: yes
      type: string
      sql: ${TABLE}.dropoff_contact_phone ;;
    }

    dimension: dropoff_country {
      group_label: "Address Related Info"
      description: "The dropoff country"
      type: string
      sql: UPPER(${TABLE}.dropoff_country) ;;
    }

    dimension: dropoff_country_revised {
      group_label: "Address Related Info"
      description: "The dropoff country revised"
      type: string
      sql: CASE WHEN ${dropoff_country} = "BH" THEN "Bahrain"
                WHEN ${dropoff_country} = "OM" THEN "Oman"
                WHEN ${dropoff_country} = "AE" THEN "United Arab Emirates"
                WHEN ${dropoff_country} = "KW" THEN "Kuwait"
                WHEN ${dropoff_country} = "EG" THEN "Egypt"
                WHEN ${dropoff_country} = "SA" THEN "Saudi Arabia"
                WHEN ${dropoff_country} = "QA" THEN "Qatar"
                ELSE "Other"
                END;;
    }

    dimension: dropoff_notes {
      group_label: "Address Related Info"
      description: "Any notes related to the dropoff"
      type: string
      sql: ${TABLE}.dropoff_notes ;;
    }

    dimension: dropoff_post_code {
      group_label: "Address Related Info"
      description: "The recipient's postcode address"
      type: string
      sql: ${TABLE}.dropoff_post_code ;;
    }

    dimension: dropoff_state {
      group_label: "Address Related Info"
      description: "The recipient's dropoff state"
      type: string
      sql: ${TABLE}.dropoff_state ;;
    }

    # dimension_group: failed_attempt_at {
    #   type: time
    #   hidden: yes
    #   group_label: "Date - Failed Attempt At"
    #   timeframes: [
    #     raw,
    #     time,
    #     date,
    #     week,
    #     month,
    #     quarter,
    #     year
    #   ]
    #   sql: CAST(${TABLE}.failed_attempt_at AS TIMESTAMP) ;;
    # }

    dimension: merchant {
      group_label: "Item Info"
      label: "Brand"
      description: "Brand"
      type: string
      sql: CASE
            WHEN ${TABLE}.merchant='TORY-BURCH' THEN 'TORY BURCH'
            WHEN ${TABLE}.merchant IN('MOLTON-BROWN','MOLTON-BROWN-SA')THEN 'MOLTON BROWN'
            WHEN ${TABLE}.merchant='TANAGRA' THEN 'TANAGRA & ADV'
            WHEN ${TABLE}.merchant= 'BEAUTY DISTRICT' THEN 'BEAUTY DISTRICT'
            WHEN ${TABLE}.merchant='MUFE - MANAGEMENT' THEN 'MUFE MANAGEMENT'
            WHEN ${TABLE}.merchant='YSL' THEN 'SAJ'
            WHEN ${TABLE}.merchant='DIOR' THEN 'CHRISTIAN DIOR'
            WHEN ${TABLE}.merchant IN ('PENHALIGONS','PENHALOGONS')  THEN "PENHALIGON'S"
            WHEN ${TABLE}.merchant = 'CAROLINA-HERRERA' THEN 'CAROLINA HERRERA'
            WHEN ${TABLE}.merchant = 'GHAWALI-SA' THEN 'GHAWALI'
            WHEN ${TABLE}.merchant='THE-DEAL' THEN 'THE DEAL'
            WHEN ${TABLE}.merchant="L'OCCITANE" THEN 'LOCCITANE'
            WHEN ${TABLE}.merchant IN ("WEAR-THAT",'Wear That') THEN 'WEAR THAT'
            WHEN ${TABLE}.merchant="YOU-BEAUT" THEN 'YOU BEAUT'
            WHEN ${TABLE}.merchant="Level Shoes Dubai Mall" THEN 'LEVEL SHOES'
            WHEN ${TABLE}.merchant="D&G" THEN 'DOLCE & GABBANA'
            ELSE ${TABLE}.merchant
         END;;
    }

    dimension: bu_brand_for_access_filter {
      hidden: yes
      sql:  case when ${brand_vertical_mapping_log.bu_brand} is null then 'x' else ${brand_vertical_mapping_log.bu_brand} end ;;

    }

    dimension: vertical_for_access_filter {
      hidden: yes
      sql:  case when ${brand_vertical_mapping_log.vertical} is null then 'x' else ${brand_vertical_mapping_log.vertical} end ;;

    }



    # dimension_group: order_date {
    #   type: time
    #   group_label: "2- Date - Ordered at"
    #   timeframes: [
    #     raw,
    #     time,
    #     date,
    #     week,
    #     month,
    #     quarter,
    #     year
    #   ]
    #   sql: CAST(${TABLE}.order_date AS TIMESTAMP) ;;
    #   html: {{ rendered_value | date: "%d-%m-%Y" }};;
    # }

    dimension_group: out_for_delivery_at {
      type: time
      group_label: "6- Date - Out For Delivery"
      description: "Date the order was out for delivery"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.out_for_delivery_at AS TIMESTAMP) ;;
    }

    dimension: parcels {
      group_label: "Shipment Info"
      type: string
      description: "# of parcels"
      sql: ${TABLE}.parcels ;;
    }

    dimension: payment_mode {
      group_label: "Payment Info"
      description: "How the payment was made i.e. COD/Card"
      type: string
      sql: ${TABLE}.payment_mode ;;
    }

    dimension: payment_payment_currency {
      group_label: "Payment Info"
      type: string
      description: "Currency through which payment was made"
      sql: ${TABLE}.payment_payment_currency ;;
    }

    dimension: payment_total_amount {
      group_label: "Payment Info"
      description: "Total paid amount"
      hidden: yes
      type: string
      sql: ${TABLE}.payment_total_amount ;;
    }

    dimension: pickup_address1 {
      group_label: "Address Related Info"
      description: "Address of the pickup location"
      type: string
      sql: ${TABLE}.pickup_address1 ;;
    }

    dimension: pickup_city {
      group_label: "Address Related Info"
      description: "City of the pickup location"
      type: string
      sql:
      case
        when ${TABLE}.pickup_city ='37V2+P6 DUBAI - UNITED ARAB EMIRATES' then 'DUBAI'
        when ${TABLE}.pickup_city ='MOTORCITY DUBAI' then 'DUBAI'
        when ${TABLE}.pickup_city ='ABU DHBAI' then 'ABU DHABI'
        when ${TABLE}.pickup_city ='ABU-DHABI' then 'ABU DHABI'
        when ${TABLE}.pickup_city ='ABUDHABI ' then 'ABU DHABI'
        when ${TABLE}.pickup_city ='AD' then 'ABU DHABI'
        when ${TABLE}.pickup_city ='AUH' then 'ABU DHABI'
        when ${TABLE}.pickup_city ='AL-AIN' then 'AL AIN'
        when ${TABLE}.pickup_city ='ALAIN ' then 'AL AIN'
        when ${TABLE}.pickup_city ='KUWAIT' then 'KUWAIT CITY'
        when ${TABLE}.pickup_city ='KUWAIT-CITY' then 'KUWAIT CITY'
        when ${TABLE}.pickup_city ='QATAR' then 'DOHA'
        else ${TABLE}.pickup_city
      end;;
    }

    dimension: pickup_contact_email {
      group_label: "Address Related Info"
      description: "Email of the pickup contact"
      hidden: yes
      type: string
      sql: ${TABLE}.pickup_contact_email ;;
    }

    dimension: pickup_contact_name {
      group_label: "Address Related Info"
      description: "Name of the pickup contact"
      hidden: yes
      type: string
      sql: ${TABLE}.pickup_contact_name ;;
    }

    dimension: pickup_contact_phone {
      group_label: "Address Related Info"
      description: "Contact phone number of the pickup contact"
      hidden: yes
      type: string
      sql: ${TABLE}.pickup_contact_phone ;;
    }

    dimension: pickup_country {
      group_label: "Address Related Info"
      description: "Name of the pickup contact"
      type: string
      sql: CASE
            WHEN ${TABLE}.pickup_country='QTR' THEN 'QA'
            ELSE ${TABLE}.pickup_country
           END;;
    }

    dimension: pickup_post_code {
      group_label: "Address Related Info"
      description: "Postcode of the pickup contact"
      type: string
      sql: ${TABLE}.pickup_post_code ;;
    }

    dimension_group: returned_at {
      type: time
      group_label: "9- Date - Returned At"
      description: "Date the order was returned"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.returned_at AS TIMESTAMP) ;;
    }

    dimension: shipment_id {
      group_label: "Shipment Info"
      description: "Shipment ID of the order/tracking #"
      type: string
      sql: ${TABLE}.shipmentId ;;
    }

    dimension_group: collected_at {
      type: time
      group_label: "5- Date - Collected At"
      description: "Date the order was collected by DSP"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.collected_at AS TIMESTAMP) ;;
    }

    dimension: status {
      group_label: "Statuses"
      description: "The status of the order given by Carriyo"
      type: string
      sql: upper(${TABLE}.status) ;;
    }

    dimension: suppress_communication {
      hidden: yes
      type: string
      description: "Suppress communication ?"
      sql: ${TABLE}.suppress_communication ;;
    }

    dimension: tracking_no {
      group_label: "Shipment Info"
      description: "The tracking # or AWB attached to an order/shipment"
      type: string
      sql: ${TABLE}.tracking_no ;;
      link: {
        label: "Drill by tracking #"
        url: "https://chalhoubgroup.de.looker.com/looks/2321?&f[carriyo_unified.tracking_no]={{ value | url_encode }}"
      }
    }

    dimension_group: update_date {
      type: time
      label: "Update Date"
      group_label: "Date - Update Date"
      description: "Date at which the order was updated"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.update_date AS TIMESTAMP) ;;
    }

    dimension_group: delivered_at {
      type: time
      group_label: "7- Date - Delivered At"
      description: "Date the order was delivered"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql:  CAST(${TABLE}.delivered_at AS TIMESTAMP) ;;
    }

    dimension_group: first_attempt {
      type: time
      group_label: "8a - Date - First Attempt"
      description: "Date of the first attempt"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql:  CAST(${TABLE}.first_attempt AS TIMESTAMP) ;;
    }

    dimension_group: second_attempt {
      type: time
      group_label: "8b - Date - Second Attempt"
      description: "Date of the second attempt"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql:  CAST(${TABLE}.second_attempt AS TIMESTAMP) ;;
    }

    dimension_group: third_attempt {
      type: time
      group_label: "8c - Date - Third Attempt"
      description: "Date of the third attempt"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql:  CAST(${TABLE}.third_attempt AS TIMESTAMP) ;;
    }

    dimension: no_of_attempts {
      type: number
      group_label: "Statuses"
      description: "# of attempts the order has had"
      sql:${TABLE}.no_of_attempts ;;
    }

    dimension: no_of_attempts_boolean {
      description: "Boolean returning orders under three attempts and aren't delivered"
      type: yesno
      group_label: "Statuses"
      sql:  ${no_of_attempts}<=2 and ${chb_status}='Not Delivered';;
    }

    dimension_group: time_spent_pending {
      type: duration
      description: "Time spent in pending"
      hidden: yes
      intervals: [second]
      sql_start: ${confirmation_date_raw} ;;
      sql_end: ${collected_at_raw} ;;
    }

    #(customer placed returns) Time to Collect from Customer

    dimension_group: time_to_first_attempt {
      type: duration
      description: "Total time to first attempt from collection date"
      hidden: yes
      intervals: [second]
      sql_start: ${collected_at_raw} ;;
      sql_end: ${first_attempt_raw};;
    }

    dimension_group: time_to_deliver {
      type: duration
      description: "Total time to delivery from collection date"
      hidden: yes
      intervals: [second]
      sql_start: ${collected_at_raw} ;;
      sql_end: ${delivered_at_raw} ;;
    }

    dimension_group: time_to_out_to_deliver {
      type: duration
      description: "Total time to OFD from collection date"
      hidden: yes
      intervals: [second]
      sql_start: ${collected_at_raw} ;;
      sql_end: ${out_for_delivery_at_raw} ;;
    }

    dimension_group: time_to_returned {
      type: duration
      description: "Total time to return from collection date"
      hidden: yes
      intervals: [second]
      sql_start: ${collected_at_raw} ;;
      sql_end: ${returned_at_raw} ;;
    }

    dimension_group: time_to_promised_deliver {
      type: duration
      description: "Total time to first attempt from collection date"
      hidden: yes
      intervals: [second]
      sql_start: ${collected_at_raw} ;;
      sql_end: ${ca_SLA_first_attempt_by_raw} ;;
    }

    dimension_group: order_creation_to_booked {
      type: duration
      description: "Total time to dsp creation date from carriyo creation date"
      hidden: yes
      intervals: [second]
      sql_start: ${carriyo_creation_date_raw} ;;
      sql_end: ${dsp_creation_date_raw} ;;
    }

    dimension_group: order_creation_to_collected {
      type: duration
      description: "Total time to collection from dsp creation date"
      hidden: yes
      intervals: [second]
      sql_start: ${dsp_creation_date_raw} ;;
      sql_end: ${collected_at_raw} ;;
    }

    dimension_group: order_creation_to_out_for_delivery {
      type: duration
      description: "Total time to OFD from dsp creation date"
      hidden: yes
      intervals: [second]
      sql_start: ${dsp_creation_date_raw} ;;
      sql_end: ${out_for_delivery_at_raw} ;;
    }

    dimension_group: order_creation_to_first_attempt {
      type: duration
      description: "Total time to first attempt from dsp creation date"
      hidden: yes
      intervals: [second]
      sql_start: ${dsp_creation_date_raw} ;;
      sql_end: ${first_attempt_raw};;
    }

    dimension_group: order_creation_to_delivered {
      type: duration
      description: "Total time to delivery from dsp creation date"
      hidden: yes
      intervals: [second]
      sql_start: ${dsp_creation_date_raw} ;;
      sql_end: ${delivered_at_raw} ;;
    }

    dimension_group: order_creation_to_returned {
      type: duration
      description: "Total time to return from dsp creation date"
      hidden: yes
      intervals: [second]
      sql_start: ${dsp_creation_date_raw} ;;
      sql_end: ${returned_at_raw} ;;
    }

    dimension_group: confirmed_to_out_for_delivery{
      type: duration
      description: "Total time to OFD from confirmation date (Carriyo only)"
      hidden: yes
      intervals: [second]
      sql_start: ${confirmation_date_raw} ;;
      sql_end: ${out_for_delivery_at_raw} ;;
    }

    dimension_group: out_for_delivery_to_first_attempt {
      type: duration
      description: "Total time to first_attempt from OFD date"
      hidden: yes
      intervals: [second]
      sql_start: ${out_for_delivery_at_raw}  ;;
      sql_end: ${first_attempt_raw};;
    }

    dimension_group: first_attempt_to_delivered {
      type: duration
      description: "Total time to delivery from first attempt date"
      hidden: yes
      intervals: [second]
      sql_start: ${first_attempt_raw} ;;
      sql_end: ${delivered_at_raw} ;;
    }

    # dimension_group: delivered_to_returned {
    #   type: duration
    #   description: "Total time to collection from dsp creation date"
    #   hidden: yes
    #   intervals: [second]
    #   sql_start: ${delivered_at_raw} ;;
    #   sql_end: ${returned_at_raw} ;;
    # }

    dimension_group: creation_to_collected{
      type: duration
      description: "Total time to collection from dsp creation date"
      hidden: yes
      intervals: [second]
      sql_start: ${dsp_creation_date_raw} ;;
      sql_end: ${collected_at_raw}   ;;
    }

    dimension_group: collected_to_first_attempt{
      type: duration
      description: "Total time to first attempt from collection date"
      hidden: yes
      intervals: [second]
      sql_start: ${collected_at_raw} ;;
      sql_end: ${first_attempt_raw}   ;;
    }

    dimension_group: first_to_second_attempt{
      type: duration
      description: "Total time to second attempt from first attempt date"
      hidden: yes
      intervals: [second]
      sql_start: ${first_attempt_raw} ;;
      sql_end: ${second_attempt_raw}   ;;
    }

    dimension_group: second_to_third_attempt{
      type: duration
      description: "Total time to third attempt from second attempt date"
      hidden: yes
      intervals: [second]
      sql_start: ${second_attempt_raw} ;;
      sql_end: ${third_attempt_raw}   ;;
    }

    dimension_group: third_attempt_to_returned{
      type: duration
      description: "Total time to return from third attempt"
      hidden: yes
      intervals: [second]
      sql_start: ${third_attempt_raw} ;;
      sql_end: ${returned_at_raw}   ;;
    }

    dimension: delivery_Within_promised  {
      type: yesno
      group_label: "Within Promised Booleans"
      description: "Boolean denoting whether order was delivered within the promised time"
      sql: ${seconds_time_to_deliver}<=${seconds_time_to_promised_deliver};;
    }

    dimension: attempt_Within_promised  {
      type: yesno
      group_label: "Within Promised Booleans"
      description: "Boolean denoting whether order was attempted within the promised time"
      sql: CASE
              WHEN ${first_attempt_raw} IS NULL then ${delivered_at_raw}<=${ca_SLA_first_attempt_by_raw}
              ELSE ${first_attempt_raw}<=${ca_SLA_first_attempt_by_raw}
         END;;
    }

    parameter: date_granularity {
      type: string
      allowed_value: { value: "Hour" }
      allowed_value: { value: "Day" }
      allowed_value: { value: "Month" }
      allowed_value: { value: "Week" }
    }

    dimension: collected_equals_delivered {
      hidden: yes
      description: "Boolean denoting if collected date equals delivered date"
      type: yesno
      sql: ${collected_at_date}=${delivered_at_date} ;;
    }

    dimension: collected_equals_returned  {
      hidden: yes
      description: "Boolean denoting if collected date equals returned date"
      type: yesno
      sql: ${collected_at_date}=${returned_at_date} ;;

    }

    dimension:  chb_status{
      type: string
      group_label: "Statuses"
      description: "Mapped universal statuses based off carrier statuses"
      label: "Chalhoub statuses"
      sql: TRIM(${TABLE}.chb_status) ;;
    }

    dimension: warehouse_vs_store {
      type: string
      group_label: "Shipment Info"
      description: "Whether the order originates from warehouse or store"
      sql: ${TABLE}.warehouse_vs_store ;;
    }

    dimension: orders_delivered_based_on_delivery_date_boolean {
      hidden: yes
      description: "Boolean denoting if delivered date equals update date"
      type: yesno
      sql: ${delivered_at_date}=${update_date_date} ;;
    }

    dimension: orders_collected_based_on_collected_date_boolean {
      hidden: yes
      description: "Boolean denoting if collected date equals update date"
      type: yesno
      sql: ${collected_at_date}=${update_date_date} ;;
    }

    dimension: orders_returned_based_on_returned_date_boolean {
      hidden: yes
      description: "Boolean denoting if returned date equals update date"
      type: yesno
      sql: ${returned_at_date}=${update_date_date} ;;
    }

    dimension: latest_undelivered_reason {
      type: string
      group_label: "Statuses"
      label: "Latest Undelivered Reason"
      description: "The latest reason the order failed its delivery at any given failed attempt"
      order_by_field: fault_with
      #sql: ${TABLE}.latest_undelivered_reason ;;
      sql:
        case
          when ${TABLE}.latest_undelivered_reason like 'Held For Pickup%'
            then 'Held For Pickup'
            else ${TABLE}.latest_undelivered_reason
        end;;
    }

    dimension: fault_with {
      type: string
      group_label: "Statuses"
      label: "Fault With (Carrier/DSP vs Customer)"
      description: "Whether the reason the delivery failed is with the customer or carrier/DSP"
      sql: ${TABLE}.fault_with ;;
    }

    dimension: max_attempts{
      hidden: yes
      description: "The maximum number of attempts made for an order"
      type: number
      sql: ${TABLE}.max_attempts ;;
    }

    dimension: is_max_attempt{
      hidden: yes
      description: "Boolean denoting whether an attempt is the max attempt and is less than 3"
      type: yesno
      sql: ${max_attempts}=${no_of_attempts} and ${max_attempts}<3 ;;
    }

    dimension: SLA_REQ {
      label: "SLA Requirement"
      description: "The SLA requirement per order"
      group_label: "SLA"
      type: string
      sql: ${TABLE}.SLA_REQ;;
    }

    dimension_group: ca_SLA_first_attempt_by {
      type: time
      label: "SLA first attempt deadline"
      description: "The deadline by which the first attempt must be made by the DSP"
      group_label: "SLA"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.ca_SLA_first_attempt AS TIMESTAMP) ;;
    }

    dimension_group: ca_SLA_second_attempt_by {
      type: time
      label: "SLA second attempt deadline"
      description: "The deadline by which the second attempt must be made by the DSP"
      group_label: "SLA"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.ca_SLA_second_attempt AS TIMESTAMP) ;;
    }

    dimension_group: ca_SLA_third_attempt_by {
      type: time
      label: "SLA third attempt deadline"
      description: "The deadline by which the third attempt must be made by the DSP"
      group_label: "SLA"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.ca_SLA_third_attempt AS TIMESTAMP) ;;
    }

    dimension: ca_delivered_within_SLA_int_boolean {
      type: string
      label: "Delivered within SLA INT"
      description: "Boolean denoting whether the first attempt SLA has passed (based on integer)"
      group_label: "SLA"
      sql:  CASE
                WHEN CAST(${TABLE}.ca_delivery_within_SLA_int AS STRING)='true' THEN 'Yes'
                WHEN CAST(${TABLE}.ca_delivery_within_SLA_int AS STRING)='false' THEN 'No'
              ELSE NULL
            END;;
    }

    dimension: ca_delivered_within_SLA_circadian_boolean {
      type: string
      label: "Delivered within SLA Circadian"
      description: "Boolean denoting whether the first attempt SLA has passed (based on circadian)"
      group_label: "SLA"
      sql: CASE
                WHEN CAST(${TABLE}.ca_delivery_within_SLA_circadian AS STRING)='true' THEN 'Yes'
                WHEN CAST(${TABLE}.ca_delivery_within_SLA_circadian AS STRING)='false' THEN 'No'
              ELSE NULL
            END;;
    }

    dimension: ca_first_attempt_within_SLA_int_boolean {
      type: string
      label: "First Attempt within SLA INT"
      description: "Boolean denoting whether the first attempt SLA has passed (based on integer)"
      group_label: "SLA"
      sql:  CASE
                WHEN CAST(${TABLE}.ca_first_attempt_within_SLA_int AS STRING)='true' THEN 'Yes'
                WHEN CAST(${TABLE}.ca_first_attempt_within_SLA_int AS STRING)='false' THEN 'No'
              ELSE NULL
            END;;
    }

    dimension: ca_first_attempt_within_SLA_circadian_boolean {
      type: string
      label: "First Attempt within SLA Circadian"
      description: "Boolean denoting whether the first attempt SLA has passed (based on circadian)"
      group_label: "SLA"
      sql: CASE
                WHEN CAST(${TABLE}.ca_first_attempt_within_SLA_circadian AS STRING)='true' THEN 'Yes'
                WHEN CAST(${TABLE}.ca_first_attempt_within_SLA_circadian AS STRING)='false' THEN 'No'
              ELSE NULL
            END;;
    }

    dimension: ca_second_attempt_within_SLA_int_boolean {
      type: string
      label: "Second Attempt within SLA INT"
      description: "Boolean denoting whether the second attempt SLA has passed (based on integer)"
      group_label: "SLA"
      sql: CASE
                WHEN CAST(${TABLE}.ca_second_attempt_within_SLA_int AS STRING)='true' THEN 'Yes'
                WHEN CAST(${TABLE}.ca_second_attempt_within_SLA_int AS STRING)='false' THEN 'No'
              ELSE NULL
            END;;
    }

    dimension: ca_second_attempt_within_SLA_circadian_boolean {
      type: string
      label: "Second Attempt within SLA Circadian"
      description: "Boolean denoting whether the second attempt SLA has passed (based on circadian)"
      group_label: "SLA"
      sql: CASE
                WHEN CAST(${TABLE}.ca_second_attempt_within_SLA_circadian AS STRING)='true' THEN 'Yes'
                WHEN CAST(${TABLE}.ca_second_attempt_within_SLA_circadian AS STRING)='false' THEN 'No'
              ELSE NULL
            END;;
    }

    dimension: ca_third_attempt_within_SLA_int_boolean {
      type: string
      label: "Third Attempt within SLA INT"
      description: "Boolean denoting whether the third attempt SLA has passed (based on integer)"
      group_label: "SLA"
      sql: CASE
                WHEN CAST(${TABLE}.ca_third_attempt_within_SLA_int AS STRING)='true' THEN 'Yes'
                WHEN CAST(${TABLE}.ca_third_attempt_within_SLA_int AS STRING)='false' THEN 'No'
              ELSE NULL
            END;;
    }

    dimension: ca_third_attempt_within_SLA_circadian_boolean {
      type: string
      label: "Third Attempt within SLA Circadian"
      description: "Boolean denoting whether the third attempt SLA has passed (based on circadian)"
      group_label: "SLA"
      sql: CASE
                WHEN CAST(${TABLE}.ca_third_attempt_within_SLA_circadian AS STRING)='true' THEN 'Yes'
                WHEN CAST(${TABLE}.ca_third_attempt_within_SLA_circadian AS STRING)='false' THEN 'No'
              ELSE NULL
            END;;
    }

    dimension: first_return {
      label: "Earliest Return Trigger"
      description: "The minimum date at which the order changed from a 'Not Delivered' status to 'To be Returned'"
      group_label: "test"
      type: date
      sql: ${TABLE}.first_return ;;
    }

    dimension: attempt_lead_time {
      hidden: yes
      description: "Lead time between first attempt and collected at date"
      type: number
      sql: CASE
            WHEN {% parameter lead_time_delivery_time_option %} = 'Int'
              THEN date_diff(${first_attempt_date},${collected_at_date},day)
            WHEN {% parameter lead_time_delivery_time_option %} = 'Circadian'
              THEN timestamp_diff(${first_attempt_raw},${collected_at_raw},second)/3600
           ELSE date_diff(${first_attempt_date},${collected_at_date},day)
           END;;
    }

    dimension: attempt_lead_time_highest_boolean {
      hidden: yes
      description: "Boolean denoting whether leadtime is over 7 days"
      type: yesno
      sql: date_diff(${first_attempt_date},${collected_at_date}, day)>7 ;;
    }

    dimension: first_attempt_boolean {
      hidden: yes
      type: yesno
      description: "Boolean denoting whether first attempt exists for an order or not"
      sql: ${TABLE}.first_attempt IS NOT NULL ;;
    }

    dimension_group: ocd_SLA_first_attempt_by {
      type: time
      label: "SLA first attempt deadline (based on Order Creation Date)"
      description: "The deadline by which the first attempt must be made by the DSP based on Order Creation Date"
      group_label: "SLA"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.ocd_SLA_first_attempt AS TIMESTAMP) ;;
    }

    dimension_group: ocd_SLA_second_attempt_by {
      type: time
      label: "SLA second attempt deadline (based on Order Creation Date)"
      description: "The deadline by which the second attempt must be made by the DSP based on Order Creation Date"
      group_label: "SLA"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.ocd_SLA_second_attempt AS TIMESTAMP) ;;
    }

    dimension_group: ocd_SLA_third_attempt_by {
      type: time
      label: "SLA third attempt deadline (based on Order Creation Date)"
      description: "The deadline by which the third attempt must be made by the DSP based on Order Creation Date"
      group_label: "SLA"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.ocd_SLA_third_attempt AS TIMESTAMP) ;;
    }


    set: not_collected_drill_details {
      fields: [dsp_creation_date_time,order_creation_time,shipped_date_time,collected_at_time,first_attempt_time,second_attempt_time,third_attempt_time,delivered_at_time,returned_at_time,update_date_time,dropoff_city,dropoff_country,pickup_address1,pickup_city,pickup_country,carrier,carrier_account_name,carrier_status,merchant,payment_mode,payment_payment_currency,payment_total_amount,tracking_no, shipment_id, partnerOrderReference,partnerShipmentReference,parcels,warehouse_vs_store,ca_delivered_within_SLA_circadian_boolean,ca_delivered_within_SLA_int_boolean,ca_first_attempt_within_SLA_circadian_boolean,ca_first_attempt_within_SLA_int_boolean,ca_SLA_first_attempt_by_time,ca_second_attempt_within_SLA_circadian_boolean,ca_second_attempt_within_SLA_int_boolean,ca_SLA_second_attempt_by_time,ca_third_attempt_within_SLA_circadian_boolean,ca_third_attempt_within_SLA_int_boolean,ca_SLA_third_attempt_by_time,SLA_REQ, chb_status,delivery_type,event_name,fault_with,latest_status,latest_status_per_day,latest_undelivered_reason,no_of_attempts,notes,status]

    }

    set: not_delivered_drill_details {
      fields: [dsp_creation_date_time,order_creation_time,shipped_date_time,collected_at_time,first_attempt_time,second_attempt_time,third_attempt_time,delivered_at_time,returned_at_time,update_date_time,dropoff_city,dropoff_country,pickup_address1,pickup_city,pickup_country,carrier,carrier_account_name,carrier_status,merchant,payment_mode,payment_payment_currency,payment_total_amount,tracking_no, shipment_id, partnerOrderReference,partnerShipmentReference,parcels,warehouse_vs_store,ca_delivered_within_SLA_circadian_boolean,ca_delivered_within_SLA_int_boolean,ca_first_attempt_within_SLA_circadian_boolean,ca_first_attempt_within_SLA_int_boolean,ca_SLA_first_attempt_by_time,ca_second_attempt_within_SLA_circadian_boolean,ca_second_attempt_within_SLA_int_boolean,ca_SLA_second_attempt_by_time,ca_third_attempt_within_SLA_circadian_boolean,ca_third_attempt_within_SLA_int_boolean,ca_SLA_third_attempt_by_time,SLA_REQ, chb_status,delivery_type,event_name,fault_with,latest_status,latest_status_per_day,latest_undelivered_reason,no_of_attempts,notes,status]
    }

    set: delivered_drill_details {
      fields: [dsp_creation_date_time,order_creation_time,shipped_date_time,collected_at_time,first_attempt_time,second_attempt_time,third_attempt_time,delivered_at_time,returned_at_time,update_date_time,dropoff_city,dropoff_country,pickup_address1,pickup_city,pickup_country,carrier,carrier_account_name,carrier_status,merchant,payment_mode,payment_payment_currency,payment_total_amount,tracking_no, shipment_id, partnerOrderReference,partnerShipmentReference,parcels,warehouse_vs_store,ca_delivered_within_SLA_circadian_boolean,ca_delivered_within_SLA_int_boolean,ca_first_attempt_within_SLA_circadian_boolean,ca_first_attempt_within_SLA_int_boolean,ca_SLA_first_attempt_by_time,ca_second_attempt_within_SLA_circadian_boolean,ca_second_attempt_within_SLA_int_boolean,ca_SLA_second_attempt_by_time,ca_third_attempt_within_SLA_circadian_boolean,ca_third_attempt_within_SLA_int_boolean,ca_SLA_third_attempt_by_time,SLA_REQ, chb_status,delivery_type,event_name,fault_with,latest_status,latest_status_per_day,latest_undelivered_reason,no_of_attempts,notes,status]
    }

    set: RTO_drill_details {
      fields: [dsp_creation_date_time,order_creation_time,shipped_date_time,collected_at_time,first_attempt_time,second_attempt_time,third_attempt_time,delivered_at_time,returned_at_time,update_date_time,dropoff_city,dropoff_country,pickup_address1,pickup_city,pickup_country,carrier,carrier_account_name,carrier_status,merchant,payment_mode,payment_payment_currency,payment_total_amount,tracking_no, shipment_id, partnerOrderReference,partnerShipmentReference,parcels,warehouse_vs_store,ca_delivered_within_SLA_circadian_boolean,ca_delivered_within_SLA_int_boolean,ca_first_attempt_within_SLA_circadian_boolean,ca_first_attempt_within_SLA_int_boolean,ca_SLA_first_attempt_by_time,ca_second_attempt_within_SLA_circadian_boolean,ca_second_attempt_within_SLA_int_boolean,ca_SLA_second_attempt_by_time,ca_third_attempt_within_SLA_circadian_boolean,ca_third_attempt_within_SLA_int_boolean,ca_SLA_third_attempt_by_time,SLA_REQ, chb_status,delivery_type,event_name,fault_with,latest_status,latest_status_per_day,latest_undelivered_reason,no_of_attempts,notes,status]
    }

    set: returned_drill_details {
      fields: [dsp_creation_date_time,order_creation_time,shipped_date_time,collected_at_time,first_attempt_time,second_attempt_time,third_attempt_time,delivered_at_time,returned_at_time,update_date_time,dropoff_city,dropoff_country,pickup_address1,pickup_city,pickup_country,carrier,carrier_account_name,carrier_status,merchant,payment_mode,payment_payment_currency,payment_total_amount,tracking_no, shipment_id, partnerOrderReference,partnerShipmentReference,parcels,warehouse_vs_store,ca_delivered_within_SLA_circadian_boolean,ca_delivered_within_SLA_int_boolean,ca_first_attempt_within_SLA_circadian_boolean,ca_first_attempt_within_SLA_int_boolean,ca_SLA_first_attempt_by_time,ca_second_attempt_within_SLA_circadian_boolean,ca_second_attempt_within_SLA_int_boolean,ca_SLA_second_attempt_by_time,ca_third_attempt_within_SLA_circadian_boolean,ca_third_attempt_within_SLA_int_boolean,ca_SLA_third_attempt_by_time,SLA_REQ, chb_status,delivery_type,event_name,fault_with,latest_status,latest_status_per_day,latest_undelivered_reason,no_of_attempts,notes,status]
    }

    set: lost_drill_details {
      fields: [dsp_creation_date_time,order_creation_time,shipped_date_time,collected_at_time,first_attempt_time,second_attempt_time,third_attempt_time,delivered_at_time,returned_at_time,update_date_time,dropoff_city,dropoff_country,pickup_address1,pickup_city,pickup_country,carrier,carrier_account_name,carrier_status,merchant,payment_mode,payment_payment_currency,payment_total_amount,tracking_no, shipment_id, partnerOrderReference,partnerShipmentReference,parcels,warehouse_vs_store,ca_delivered_within_SLA_circadian_boolean,ca_delivered_within_SLA_int_boolean,ca_first_attempt_within_SLA_circadian_boolean,ca_first_attempt_within_SLA_int_boolean,ca_SLA_first_attempt_by_time,ca_second_attempt_within_SLA_circadian_boolean,ca_second_attempt_within_SLA_int_boolean,ca_SLA_second_attempt_by_time,ca_third_attempt_within_SLA_circadian_boolean,ca_third_attempt_within_SLA_int_boolean,ca_SLA_third_attempt_by_time,SLA_REQ, chb_status,delivery_type,event_name,fault_with,latest_status,latest_status_per_day,latest_undelivered_reason,no_of_attempts,notes,status]
    }

    set: totals_drill_details {
      fields: [dsp_creation_date_time,order_creation_time,shipped_date_time,collected_at_time,first_attempt_time,second_attempt_time,third_attempt_time,delivered_at_time,returned_at_time,update_date_time,dropoff_city,dropoff_country,pickup_address1,pickup_city,pickup_country,carrier,carrier_account_name,carrier_status,merchant,payment_mode,payment_payment_currency,payment_total_amount,tracking_no, shipment_id, partnerOrderReference,partnerShipmentReference,parcels,warehouse_vs_store,ca_delivered_within_SLA_circadian_boolean,ca_delivered_within_SLA_int_boolean,ca_first_attempt_within_SLA_circadian_boolean,ca_first_attempt_within_SLA_int_boolean,ca_SLA_first_attempt_by_time,ca_second_attempt_within_SLA_circadian_boolean,ca_second_attempt_within_SLA_int_boolean,ca_SLA_second_attempt_by_time,ca_third_attempt_within_SLA_circadian_boolean,ca_third_attempt_within_SLA_int_boolean,ca_SLA_third_attempt_by_time,SLA_REQ, chb_status,delivery_type,event_name,fault_with,latest_status,latest_status_per_day,latest_undelivered_reason,no_of_attempts,notes,status]
    }

    set: pending_drill_details {
      fields: [dsp_creation_date_time,order_creation_time,shipped_date_time,collected_at_time,first_attempt_time,second_attempt_time,third_attempt_time,delivered_at_time,returned_at_time,update_date_time,dropoff_city,dropoff_country,pickup_address1,pickup_city,pickup_country,carrier,carrier_account_name,carrier_status,merchant,payment_mode,payment_payment_currency,payment_total_amount,tracking_no, shipment_id, partnerOrderReference,partnerShipmentReference,parcels,warehouse_vs_store,ca_delivered_within_SLA_circadian_boolean,ca_delivered_within_SLA_int_boolean,ca_first_attempt_within_SLA_circadian_boolean,ca_first_attempt_within_SLA_int_boolean,ca_SLA_first_attempt_by_time,ca_second_attempt_within_SLA_circadian_boolean,ca_second_attempt_within_SLA_int_boolean,ca_SLA_second_attempt_by_time,ca_third_attempt_within_SLA_circadian_boolean,ca_third_attempt_within_SLA_int_boolean,ca_SLA_third_attempt_by_time,SLA_REQ, chb_status,delivery_type,event_name,fault_with,latest_status,latest_status_per_day,latest_undelivered_reason,no_of_attempts,notes,status]
    }

    set: attempt_drill_details {
      fields: [dsp_creation_date_time,order_creation_time,shipped_date_time,collected_at_time,first_attempt_time,second_attempt_time,third_attempt_time,delivered_at_time,returned_at_time,update_date_time,dropoff_city,dropoff_country,pickup_address1,pickup_city,pickup_country,carrier,carrier_account_name,carrier_status,merchant,payment_mode,payment_payment_currency,payment_total_amount,tracking_no, shipment_id, partnerOrderReference,partnerShipmentReference,parcels,warehouse_vs_store,ca_delivered_within_SLA_circadian_boolean,ca_delivered_within_SLA_int_boolean,ca_first_attempt_within_SLA_circadian_boolean,ca_first_attempt_within_SLA_int_boolean,ca_SLA_first_attempt_by_time,ca_second_attempt_within_SLA_circadian_boolean,ca_second_attempt_within_SLA_int_boolean,ca_SLA_second_attempt_by_time,ca_third_attempt_within_SLA_circadian_boolean,ca_third_attempt_within_SLA_int_boolean,ca_SLA_third_attempt_by_time,SLA_REQ, chb_status,delivery_type,event_name,fault_with,latest_status,latest_status_per_day,latest_undelivered_reason,no_of_attempts,notes,status]
      }

    set: sla_int_drill_details {
      fields: [dsp_creation_date_time,order_creation_time,shipped_date_time,collected_at_time,first_attempt_time,second_attempt_time,third_attempt_time,delivered_at_time,returned_at_time,update_date_time,dropoff_city,dropoff_country,pickup_address1,pickup_city,pickup_country,carrier,carrier_account_name,carrier_status,merchant,payment_mode,payment_payment_currency,payment_total_amount,tracking_no, shipment_id, partnerOrderReference,partnerShipmentReference,parcels,warehouse_vs_store,ca_delivered_within_SLA_circadian_boolean,ca_delivered_within_SLA_int_boolean,ca_first_attempt_within_SLA_circadian_boolean,ca_first_attempt_within_SLA_int_boolean,ca_SLA_first_attempt_by_time,ca_second_attempt_within_SLA_circadian_boolean,ca_second_attempt_within_SLA_int_boolean,ca_SLA_second_attempt_by_time,ca_third_attempt_within_SLA_circadian_boolean,ca_third_attempt_within_SLA_int_boolean,ca_SLA_third_attempt_by_time,SLA_REQ, chb_status,delivery_type,event_name,fault_with,latest_status,latest_status_per_day,latest_undelivered_reason,no_of_attempts,notes,status]
    }

    set: first_attempt_sla_circadian_drill_details {
      fields: [dsp_creation_date_time,order_creation_time,shipped_date_time,collected_at_time,first_attempt_time,second_attempt_time,third_attempt_time,delivered_at_time,returned_at_time,update_date_time,dropoff_city,dropoff_country,pickup_address1,pickup_city,pickup_country,carrier,carrier_account_name,carrier_status,merchant,payment_mode,payment_payment_currency,payment_total_amount,tracking_no, shipment_id, partnerOrderReference,partnerShipmentReference,parcels,warehouse_vs_store,ca_delivered_within_SLA_circadian_boolean,ca_delivered_within_SLA_int_boolean,ca_first_attempt_within_SLA_circadian_boolean,ca_first_attempt_within_SLA_int_boolean,ca_SLA_first_attempt_by_time,ca_second_attempt_within_SLA_circadian_boolean,ca_second_attempt_within_SLA_int_boolean,ca_SLA_second_attempt_by_time,ca_third_attempt_within_SLA_circadian_boolean,ca_third_attempt_within_SLA_int_boolean,ca_SLA_third_attempt_by_time,SLA_REQ, chb_status,delivery_type,event_name,fault_with,latest_status,latest_status_per_day,latest_undelivered_reason,no_of_attempts,notes,status]
    }

    set: second_attempt_sla_circadian_drill_details {
      fields: [dsp_creation_date_time,order_creation_time,shipped_date_time,collected_at_time,first_attempt_time,second_attempt_time,third_attempt_time,delivered_at_time,returned_at_time,update_date_time,dropoff_city,dropoff_country,pickup_address1,pickup_city,pickup_country,carrier,carrier_account_name,carrier_status,merchant,payment_mode,payment_payment_currency,payment_total_amount,tracking_no, shipment_id, partnerOrderReference,partnerShipmentReference,parcels,warehouse_vs_store,ca_delivered_within_SLA_circadian_boolean,ca_delivered_within_SLA_int_boolean,ca_first_attempt_within_SLA_circadian_boolean,ca_first_attempt_within_SLA_int_boolean,ca_SLA_first_attempt_by_time,ca_second_attempt_within_SLA_circadian_boolean,ca_second_attempt_within_SLA_int_boolean,ca_SLA_second_attempt_by_time,ca_third_attempt_within_SLA_circadian_boolean,ca_third_attempt_within_SLA_int_boolean,ca_SLA_third_attempt_by_time,SLA_REQ, chb_status,delivery_type,event_name,fault_with,latest_status,latest_status_per_day,latest_undelivered_reason,no_of_attempts,notes,status]
    }

    set: third_attempt_sla_circadian_drill_details {
      fields: [dsp_creation_date_time,order_creation_time,shipped_date_time,collected_at_time,first_attempt_time,second_attempt_time,third_attempt_time,delivered_at_time,returned_at_time,update_date_time,dropoff_city,dropoff_country,pickup_address1,pickup_city,pickup_country,carrier,carrier_account_name,carrier_status,merchant,payment_mode,payment_payment_currency,payment_total_amount,tracking_no, shipment_id, partnerOrderReference,partnerShipmentReference,parcels,warehouse_vs_store,ca_delivered_within_SLA_circadian_boolean,ca_delivered_within_SLA_int_boolean,ca_first_attempt_within_SLA_circadian_boolean,ca_first_attempt_within_SLA_int_boolean,ca_SLA_first_attempt_by_time,ca_second_attempt_within_SLA_circadian_boolean,ca_second_attempt_within_SLA_int_boolean,ca_SLA_second_attempt_by_time,ca_third_attempt_within_SLA_circadian_boolean,ca_third_attempt_within_SLA_int_boolean,ca_SLA_third_attempt_by_time,SLA_REQ, chb_status,delivery_type,event_name,fault_with,latest_status,latest_status_per_day,latest_undelivered_reason,no_of_attempts,notes,status]
    }

    ################################################
    #############      measure       ###############
    ################################################

    measure: order_count_of_missing_SLA{
      hidden: no
      label: "Orders with Missing SLA"
      description: "Count of orders that don't have an SLA attached to them"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: SLA_REQ
        value: "MISSING_SLA"
      }
      drill_fields: [sla_int_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: orders_passed_first_SLA_int {
    hidden: no
    label: "Count First Attempt Within SLA INT"
    description: "Count of orders that have passed their first attempt SLA (based on int)"
    group_label: "SLA"
    type: count_distinct
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    filters: {
      field: ca_first_attempt_within_SLA_int_boolean
      value: "Yes"
    }
    drill_fields: [sla_int_drill_details*]
    html: {% if fault_with._value == 'Customer' %}
          <p style="color: blue; font-size:100%; text-align:left"><a href: {{linked_value}} </a> </p>
          {% endif %};;
    sql: ${tracking_no} ;;
    }

    measure: orders_passed_delivery_SLA_int {
      hidden: no
      label: "Count Delivery Within SLA INT"
      description: "Count of orders that have passed their delivery SLA (based on int)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_delivered_within_SLA_int_boolean
        value: "Yes"
      }
      drill_fields: [sla_int_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: orders_passed_delivery_SLA_circadian {
      hidden: no
      label: "Count Delivery Within SLA Circadian"
      description: "Count of orders that have passed their first attempt SLA (based on circadian)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_delivered_within_SLA_circadian_boolean
        value: "Yes"
      }
      drill_fields: [first_attempt_sla_circadian_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: orders_passed_first_SLA_int_summary {
      hidden: no
      label: "Count First Attempt Within SLA INT - SLA Summary"
      description: "Count of orders that have passed their first attempt SLA (based on int)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_first_attempt_within_SLA_int_boolean
        value: "Yes"
      }
      drill_fields: [sla_int_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: orders_passed_first_SLA_circadian {
      hidden: no
      label: "Count First Attempt Within SLA Circadian"
      description: "Count of orders that have passed their first attempt SLA (based on circadian)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_first_attempt_within_SLA_circadian_boolean
        value: "Yes"
      }
      drill_fields: [first_attempt_sla_circadian_drill_details*]
      sql: ${tracking_no} ;;
    }

    # measure: orders_passed_first_SLA_trigger {
    #   label: "Count First Attempt Passed SLA"
    #   description: "Count of orders that have passed their first attempt SLA"
    #   group_label: "test"
    #   drill_fields: [first_attempt_sla_circadian_drill_details*]
    #   type: number
    #   sql: CASE
    #           WHEN {% parameter lead_time_delivery_time_option %} = 'Int' then ${orders_passed_first_SLA_int}
    #           WHEN {% parameter lead_time_delivery_time_option %} = 'Circadian' then ${orders_passed_first_SLA_circadian}
    #       ELSE ${orders_passed_first_SLA_int}
    #       END;;
    # }

    measure: orders_failed_delivery_SLA_int {
      hidden: no
      label: "Count Delivery Failed SLA INT"
      description: "Count of orders that have passed their delivery SLA (based on int)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_delivered_within_SLA_int_boolean
        value: "No"
      }
      drill_fields: [sla_int_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: orders_failed_delivery_SLA_circadian {
      hidden: no
      label: "Count Delivery Failed SLA Circadian"
      description: "Count of orders that have passed their first attempt SLA (based on circadian)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_delivered_within_SLA_circadian_boolean
        value: "No"
      }
      drill_fields: [first_attempt_sla_circadian_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: orders_failed_first_SLA_int {
      hidden: no
      label: "Count First Attempt Failed SLA INT"
      description: "Count of orders that have failed their first attempt SLA (based on int)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_first_attempt_within_SLA_int_boolean
        value: "No"
      }
      drill_fields: [sla_int_drill_details*]
      html: {% if fault_with._value == 'Customer' %}
        <p style="color: blue; font-size:100%; text-align:left"><a href: {{linked_value}} </a></p>
        {% endif %};;
      sql: ${tracking_no} ;;
    }

    measure: orders_failed_first_SLA_int_summary {
      hidden: no
      label: "Count First Attempt Failed SLA INT - SLA Summary"
      description: "Count of orders that have failed their first attempt SLA (based on int)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_first_attempt_within_SLA_int_boolean
        value: "No"
      }
      drill_fields: [sla_int_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: orders_failed_first_SLA_circadian {
      hidden: no
      label: "Count First Attempt Failed SLA Circadian"
      description: "Count of orders that have failed their first attempt SLA (based on circadian)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_first_attempt_within_SLA_circadian_boolean
        value: "No"
      }
      drill_fields: [first_attempt_sla_circadian_drill_details*]
      sql: ${tracking_no} ;;
    }

    # measure: orders_failed_first_SLA_trigger {
    #   label: "Count First Attempt Failed SLA"
    #   description: "Count of orders that have failed their first attempt SLA"
    #   group_label: "test"
    #   drill_fields: [first_attempt_sla_circadian_drill_details*]
    #   type: number
    #   sql: CASE
    #           WHEN {% parameter lead_time_delivery_time_option %} = 'Int' then ${orders_failed_first_SLA_int}
    #           WHEN {% parameter lead_time_delivery_time_option %} = 'Circadian' then ${orders_failed_first_SLA_circadian}
    #       ELSE ${orders_passed_first_SLA_int}
    #       END;;
    # }

    measure: orders_passed_second_SLA_int {
      hidden: no
      label: "Count Second Attempt Within SLA INT"
      description: "Count of orders that have passed their second attempt SLA (based on int)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_second_attempt_within_SLA_int_boolean
        value: "Yes"
      }
      drill_fields: [sla_int_drill_details*]
      html: {% if fault_with._value == 'Customer' %}
        <p style="color: blue; font-size:100%; text-align:left"><a href: {{linked_value}} </a></p>
        {% endif %};;
      sql: ${tracking_no} ;;
    }

    measure: orders_passed_second_SLA_int_summary {
      hidden: no
      label: "Count Second Attempt Within SLA INT - SLA Summary"
      description: "Count of orders that have passed their second attempt SLA (based on int)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_second_attempt_within_SLA_int_boolean
        value: "Yes"
      }
      drill_fields: [sla_int_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: orders_passed_second_SLA_circadian {
      hidden: no
      label: "Count Second Attempt Within SLA Circadian"
      description: "Count of orders that have passed their second attempt SLA (based on circadian)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_second_attempt_within_SLA_circadian_boolean
        value: "Yes"
      }
      drill_fields: [second_attempt_sla_circadian_drill_details*]
      sql: ${tracking_no} ;;
    }

    # measure: orders_passed_second_SLA_trigger {
    #   label: "Count Second Attempt Passed SLA"
    #   description: "Count of orders that have passed their second attempt SLA"
    #   group_label: "test"
    #   drill_fields: [second_attempt_sla_circadian_drill_details*]
    #   type: number
    #   sql: CASE
    #           WHEN {% parameter lead_time_delivery_time_option %} = 'Int' then ${orders_passed_second_SLA_int}
    #           WHEN {% parameter lead_time_delivery_time_option %} = 'Circadian' then ${orders_passed_second_SLA_circadian}
    #       ELSE ${orders_passed_second_SLA_int}
    #       END;;
    # }

    measure: orders_failed_second_SLA_int {
      hidden: no
      label: "Count Second Attempt Failed SLA INT"
      description: "Count of orders that have failed their second attempt SLA (based on int)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_second_attempt_within_SLA_int_boolean
        value: "No"
      }
      drill_fields: [sla_int_drill_details*]
      html: {% if fault_with._value == 'Customer' %}
        <p style="color: blue; font-size:100%; text-align:left"><a href: {{linked_value}} </a></p>
        {% endif %};;
      sql: ${tracking_no} ;;
    }

    measure: orders_failed_second_SLA_int_summary {
      hidden: no
      label: "Count Second Attempt Failed SLA INT - SLA Summary"
      description: "Count of orders that have failed their second attempt SLA (based on int)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_second_attempt_within_SLA_int_boolean
        value: "No"
      }
      drill_fields: [sla_int_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: orders_failed_second_SLA_circadian {
      hidden: no
      label: "Count Second Attempt Failed SLA Circadian"
      description: "Count of orders that have failed their second attempt SLA (based on circadian)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_second_attempt_within_SLA_circadian_boolean
        value: "No"
      }
      drill_fields: [second_attempt_sla_circadian_drill_details*]
      sql: ${tracking_no} ;;
    }

    # measure: orders_failed_second_SLA_trigger {
    #   label: "Count Second Attempt Failed SLA"
    #   description: "Count of orders that have failed their second attempt SLA"
    #   group_label: "test"
    #   drill_fields: [second_attempt_sla_circadian_drill_details*]
    #   type: number
    #   sql: CASE
    #           WHEN {% parameter lead_time_delivery_time_option %} = 'Int' then ${orders_failed_second_SLA_int}
    #           WHEN {% parameter lead_time_delivery_time_option %} = 'Circadian' then ${orders_failed_second_SLA_circadian}
    #       ELSE ${orders_passed_second_SLA_int}
    #       END;;
    # }

    measure: orders_passed_third_SLA_int {
      hidden: no
      label: "Count Third Attempt Within SLA INT"
      description: "Count of orders that have passed their third attempt SLA (based on int)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_third_attempt_within_SLA_int_boolean
        value: "Yes"
      }
      drill_fields: [sla_int_drill_details*]
      html: {% if fault_with._value == 'Customer' %}
        <p style="color: blue; font-size:100%; text-align:left"><a href: {{linked_value}} </a></p>
        {% endif %};;
      sql: ${tracking_no} ;;
    }

    measure: orders_passed_third_SLA_int_summary {
      hidden: no
      label: "Count Third Attempt Within SLA INT - SLA Summary"
      description: "Count of orders that have passed their third attempt SLA (based on int)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_third_attempt_within_SLA_int_boolean
        value: "Yes"
      }
      drill_fields: [sla_int_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: orders_passed_third_SLA_circadian {
      hidden: no
      label: "Count Third Attempt Within SLA Circadian"
      description: "Count of orders that have passed their third attempt SLA (based on circadian)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_third_attempt_within_SLA_circadian_boolean
        value: "Yes"
      }
      drill_fields: [third_attempt_sla_circadian_drill_details*]
      sql: ${tracking_no} ;;
    }

    # measure: orders_passed_third_SLA_trigger {
    #   label: "Count Third Attempt Passed SLA"
    #   description: "Count of orders that have passed their third attempt SLA"
    #   group_label: "test"
    #   drill_fields: [third_attempt_sla_circadian_drill_details*]
    #   type: number
    #   sql: CASE
    #           WHEN {% parameter lead_time_delivery_time_option %} = 'Int' then ${orders_passed_third_SLA_int}
    #           WHEN {% parameter lead_time_delivery_time_option %} = 'Circadian' then ${orders_passed_third_SLA_circadian}
    #       ELSE ${orders_passed_third_SLA_int}
    #       END;;
    # }

    measure: orders_failed_third_SLA_int {
      hidden: no
      label: "Count Third Attempt Failed SLA INT"
      description: "Count of orders that have failed their third attempt SLA (based on int)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_third_attempt_within_SLA_int_boolean
        value: "No"
      }
      drill_fields: [sla_int_drill_details*]
      html: {% if fault_with._value == 'Customer' %}
        <p style="color: blue; font-size:100%; text-align:left"><a href: {{linked_value}} </a></p>
        {% endif %};;
      sql: ${tracking_no} ;;
    }

    measure: orders_failed_third_SLA_int_summary {
      hidden: no
      label: "Count Third Attempt Failed SLA INT - SLA Summary"
      description: "Count of orders that have failed their third attempt SLA (based on int)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_third_attempt_within_SLA_int_boolean
        value: "No"
      }
      drill_fields: [sla_int_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: orders_failed_third_SLA_circadian {
      hidden: no
      label: "Count Third Attempt Failed SLA Circadian"
      description: "Count of orders that have failed their third attempt SLA (based on circadian)"
      group_label: "SLA"
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: ca_third_attempt_within_SLA_circadian_boolean
        value: "No"
      }
      drill_fields: [third_attempt_sla_circadian_drill_details*]
      sql: ${tracking_no} ;;
    }

    # measure: orders_failed_third_SLA_trigger {
    #   label: "Count Third Attempt Failed SLA"
    #   description: "Count of orders that have failed their third attempt SLA"
    #   group_label: "test"
    #   drill_fields: [third_attempt_sla_circadian_drill_details*]
    #   type: number
    #   sql: CASE
    #           WHEN {% parameter lead_time_delivery_time_option %} = 'Int' then ${orders_failed_third_SLA_int}
    #           WHEN {% parameter lead_time_delivery_time_option %} = 'Circadian' then ${orders_failed_third_SLA_circadian}
    #       ELSE ${orders_passed_third_SLA_int}
    #       END;;
    # }

    measure: Customer_latest_undelivered_reason_count {
      hidden: no
      label: "Customer Undelivered Reason Order Count"
      group_label: "Attempt Aggregations"
      description: "Count of Orders Undelivered with a Failed Attempt due to Customer"
      type: count_distinct
      filters: {
        field: fault_with
        value: "Customer"
      }
      filters: {
        field: no_of_attempts_boolean
        value: "Yes"
      }
      filters: {
        field: is_max_attempt
        value: "Yes"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      sql: ${tracking_no} ;;
      drill_fields: [not_delivered_drill_details*]
    }

    # measure: Customer_latest_undelivered_reason_count_with_trigger {
    # label: "Customer Undelivered Reason Order Count"
    #   group_label: "test"
    #   description: "Count of Orders Undelivered with a Failed Attempt due to Customer"
    #   value_format: "#,##0.#0"
    #   type: number
    #   sql: CASE
    #       WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${Customer_latest_undelivered_reason_count}
    #       WHEN {% parameter absolute_or_relative %} = 'Relative' then (${Customer_latest_undelivered_reason_count}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
    #       ELSE ${Customer_latest_undelivered_reason_count}
    #   END ;;
    #   drill_fields: [not_delivered_drill_details*]
    #   html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value }}%
    #           {% else %} {{ rendered_value | round }}
    #           {% endif %};;
    # }

    measure: DSP_latest_undelivered_reason_count {
      hidden: no
      label: "DSP Undelivered Reason Order Count"
      group_label: "Attempt Aggregations"
      description: "Count of Orders Undelivered with a Failed Attempt due to DSP"
      type: count_distinct
      filters: {
        field: fault_with
        value: "DSP"
      }
      filters: {
        field: no_of_attempts_boolean
        value: "Yes"
      }
      filters: {
        field: is_max_attempt
        value: "Yes"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      sql: ${tracking_no} ;;
      drill_fields: [not_delivered_drill_details*]
    }

    # measure: DSP_latest_undelivered_reason_count_with_trigger {
    #   label: "DSP Undelivered Reason Order Count"
    #   group_label: "test"
    #   description: "Count of Orders Undelivered with a Failed Attempt due to DSP"
    #   value_format: "#,##0.#0"
    #   type: number
    #   sql: CASE
    #       WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${DSP_latest_undelivered_reason_count}
    #       WHEN {% parameter absolute_or_relative %} = 'Relative' then (${DSP_latest_undelivered_reason_count}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
    #       ELSE ${DSP_latest_undelivered_reason_count}
    #   END ;;
    #   drill_fields: [not_delivered_drill_details*]
    #   html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value }}%
    #           {% else %} {{ rendered_value | round }}
    #           {% endif %};;
    # }

    measure: Merchant_latest_undelivered_reason_count {
      hidden: no
      label: "Merchant Undelivered Reason Order Count"
      group_label: "Attempt Aggregations"
      description: "Count of Orders Undelivered with a Failed Attempt due to Merchant"
      type: count_distinct
      filters: {
        field: fault_with
        value: "Merchant"
      }
      filters: {
        field: no_of_attempts_boolean
        value: "Yes"
      }
      filters: {
        field: is_max_attempt
        value: "Yes"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      sql: ${tracking_no} ;;
      drill_fields: [not_delivered_drill_details*]
    }

    measure: Other_latest_undelivered_reason_count {
      hidden: no
      label: "Other Undelivered Reason Order Count"
      group_label: "Attempt Aggregations"
      description: "Count of Orders Undelivered with a Failed Attempt due to Other"
      type: count_distinct
      filters: {
        field: fault_with
        value: "Other"
      }
      filters: {
        field: no_of_attempts_boolean
        value: "Yes"
      }
      filters: {
        field: is_max_attempt
        value: "Yes"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      sql: ${tracking_no} ;;
      drill_fields: [not_delivered_drill_details*]
    }

    measure: Not_attempted_yet_latest_undelivered_reason_count {
      hidden: no
      label: "Not Attempted Yet Undelivered Reason Order Count"
      group_label: "Attempt Aggregations"
      description: "Count of Orders Undelivered with a Failed Attempt due to not being attempted yet"
      type: count_distinct
      filters: {
        field: fault_with
        value: "Not attempted yet"
      }
      filters: {
        field: no_of_attempts_boolean
        value: "Yes"
      }
      filters: {
        field: is_max_attempt
        value: "Yes"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      sql: ${tracking_no} ;;
      drill_fields: [not_delivered_drill_details*]
    }

    measure: total_orders_with_zero_attempt {
      label: "Orders with 0 Attempt"
      group_label: "Attempt Aggregations"
      description: "Total number of orders with the most recent status being 0 attempts and are not delivered"
      value_format_name: decimal_0
      type: count_distinct
      filters: {
        field: no_of_attempts
        value: "0"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      drill_fields: [sla_int_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: total_orders_with_one_attempt {
      label: "Orders with 1 Attempts"
      group_label: "Attempt Aggregations"
      description: "Total number of orders with the most recent status being 1 attempts and are not delivered"
      value_format_name: decimal_0
      type: count_distinct
      filters: {
        field: no_of_attempts
        value: "1"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      drill_fields: [sla_int_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: total_orders_with_two_attempt {
      label: "Orders with 2 Attempts"
      group_label: "Attempt Aggregations"
      description: "Total number of orders with the most recent status being 2 attempts and are not delivered"
      value_format_name: decimal_0
      filters: {
        field: no_of_attempts
        value: "2"
      }
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      drill_fields: [sla_int_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: total_orders_with_three_attempt {
      label: "Orders with 3 Attempts"
      group_label: "Attempt Aggregations"
      description: "Total number of orders with the most recent status being 3 attempt"
      value_format_name: decimal_0
      filters: {
        field: no_of_attempts
        value: "3"
      }
      type: count_distinct
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      drill_fields: [sla_int_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: total_orders_attempts {
      label: "Total Orders - Attempts"
      group_label: "Attempt Aggregations"
      description: "Total number of orders - Attempts"
      value_format_name: decimal_0
      type: number
      # drill_fields: [carriyo_unified.collected_at_date, carriyo_unified.order_creation_date, carriyo_unified.dsp_creation_date_date, carriyo_unified.tracking_no, carriyo_unified.shipment_id, carriyo_unified.partnerOrderReference, carriyo_unified.carrier, carriyo_unified.merchant, carriyo_unified.pickup_city, carriyo_unified.dropoff_city, carriyo_unified.carrier_status, carriyo_unified.SLA_REQ, carriyo_unified.latest_undelivered_reason, carriyo_unified.chb_status, carriyo_unified.fault_with, carriyo_unified.no_of_attempts, carriyo_unified.total_orders_attempts, carriyo_unified.total_orders_with_zero_attempt, carriyo_unified.total_orders_with_one_attempt, carriyo_unified.total_orders_with_two_attempt, carriyo_unified.total_orders_with_three_attempt]
      # link: {
      # url:"{{link}}https://chalhoubgroup.de.looker.com/explore/group_supply_chain/carriyo_unified?fields=carriyo_unified.collected_at_date,carriyo_unified.order_creation_date,carriyo_unified.dsp_creation_date_date,carriyo_unified.tracking_no,carriyo_unified.shipment_id,carriyo_unified.partnerOrderReference,carriyo_unified.carrier,carriyo_unified.merchant,carriyo_unified.pickup_city,carriyo_unified.dropoff_city,carriyo_unified.carrier_status,carriyo_unified.SLA_REQ,carriyo_unified.latest_undelivered_reason,carriyo_unified.chb_status,carriyo_unified.fault_with,carriyo_unified.no_of_attempts,carriyo_unified.total_orders_attempts,carriyo_unified.total_orders_with_zero_attempt,carriyo_unified.total_orders_with_one_attempt,carriyo_unified.total_orders_with_two_attempt,carriyo_unified.total_orders_with_three_attempt&f[carriyo_unified.merchant]=&f[carriyo_unified.carrier]=&f[carriyo_unified.collected_at_date]=2020%2F11%2F17&f[carriyo_unified.dropoff_city]=&f[carriyo_unified.latest_status_boolean]=Yes&f[carriyo_unified.pickup_address1]=&f[carriyo_unified.pickup_city]=&f[carriyo_unified.pickup_country]=&f[carriyo_unified.total_orders_attempts]=%3E%3D1&sorts=carriyo_unified.collected_at_date+desc&limit=500&column_limit=50&total=on&row_total=right&query_timezone=Asia%2FDubai&vis=%7B%22show_view_names%22%3Afalse%2C%22show_row_numbers%22%3Atrue%2C%22transpose%22%3Afalse%2C%22truncate_text%22%3Atrue%2C%22hide_totals%22%3Atrue%2C%22hide_row_totals%22%3Afalse%2C%22size_to_fit%22%3Afalse%2C%22table_theme%22%3A%22transparent%22%2C%22limit_displayed_rows%22%3Afalse%2C%22enable_conditional_formatting%22%3Afalse%2C%22header_text_alignment%22%3A%22left%22%2C%22header_font_size%22%3A%2218%22%2C%22rows_font_size%22%3A%2216%22%2C%22conditional_formatting_include_totals%22%3Afalse%2C%22conditional_formatting_include_nulls%22%3Afalse%2C%22show_sql_query_menu_options%22%3Afalse%2C%22column_order%22%3A%5B%22carriyo_unified.collected_at_date%22%2C%22carriyo_unified.total_orders_attempts%22%2C%22carriyo_unified.total_orders_with_zero_attempt%22%2C%22carriyo_unified.total_orders_with_one_attempt%22%2C%22carriyo_unified.total_orders_with_two_attempt%22%2C%22carriyo_unified.total_orders_with_three_attempt%22%2C%220%22%2C%221%22%2C%222%22%2C%223%22%5D%2C%22show_totals%22%3Atrue%2C%22show_row_totals%22%3Atrue%2C%22series_labels%22%3A%7B%22carriyo_unified.count_distinct_shipment%22%3A%22%E2%80%8E%23%22%2C%22carriyo_unified.undelivered_reason%22%3A%22Latest+Undelivered+Reason%22%2C%22carriyo_unified.latest_undelivered_reason%22%3A%22Not+Delivered+Reason%22%2C%22carriyo_unified.total_orders_with_zero_attempt%22%3A%220%22%2C%22carriyo_unified.total_orders_with_one_attempt%22%3A%221%22%2C%22carriyo_unified.total_orders_with_two_attempt%22%3A%222%22%2C%22carriyo_unified.total_orders_with_three_attempt%22%3A%223%22%2C%22carriyo_unified.total_orders_attempts%22%3A%22Total%22%7D%2C%22series_column_widths%22%3A%7B%220%22%3A92%2C%221%22%3A92%2C%222%22%3A92%2C%223%22%3A92%2C%22carriyo_unified.shipped_at_date%22%3A804%2C%22carriyo_unified.collected_at_date%22%3A235%2C%22carriyo_unified.undelivered_reason%22%3A414%2C%22carriyo_unified.count_distinct_shipment%22%3A78%2C%22carriyo_unified.latest_undelivered_reason%22%3A333%2C%22carriyo_unified.total_orders_with_zero_attempt%22%3A78%2C%22carriyo_unified.total_orders_with_one_attempt%22%3A65%2C%22carriyo_unified.total_orders_with_two_attempt%22%3A65%2C%22carriyo_unified.total_orders_with_three_attempt%22%3A65%2C%22total%22%3A100%2C%22carriyo_unified.total_orders_attempts%22%3A100%7D%2C%22series_cell_visualizations%22%3A%7B%22carriyo_unified.orders_at_no_attempt%22%3A%7B%22is_active%22%3Afalse%7D%7D%2C%22series_text_format%22%3A%7B%22carriyo_unified.collected_at_date%22%3A%7B%22align%22%3A%22left%22%2C%22bold%22%3Atrue%7D%2C%22carriyo_unified.undelivered_reason%22%3A%7B%22align%22%3A%22left%22%2C%22bold%22%3Atrue%7D%2C%22carriyo_unified.count_distinct_shipment%22%3A%7B%22align%22%3A%22left%22%7D%2C%22carriyo_unified.no_of_attempts%22%3A%7B%22align%22%3A%22left%22%7D%7D%2C%22limit_displayed_rows_values%22%3A%7B%22show_hide%22%3A%22hide%22%2C%22first_last%22%3A%22first%22%2C%22num_rows%22%3A0%7D%2C%22series_collapsed%22%3A%7B%22carriyo_unified.collected_at_date%22%3Afalse%7D%2C%22truncate_column_names%22%3Afalse%2C%22theme%22%3A%22looker%22%2C%22layout%22%3A%22fixed%22%2C%22minWidthForIndexColumns%22%3Afalse%2C%22headerFontSize%22%3A12%2C%22bodyFontSize%22%3A12%2C%22showTooltip%22%3Atrue%2C%22showHighlight%22%3Atrue%2C%22columnOrder%22%3A%7B%7D%2C%22rowSubtotals%22%3Afalse%2C%22colSubtotals%22%3Atrue%2C%22spanRows%22%3Atrue%2C%22spanCols%22%3Atrue%2C%22calculateOthers%22%3Atrue%2C%22sortColumnsBy%22%3A%22pivots%22%2C%22useViewName%22%3Afalse%2C%22useHeadings%22%3Atrue%2C%22useShortName%22%3Afalse%2C%22useUnit%22%3Afalse%2C%22groupVarianceColumns%22%3Afalse%2C%22genericLabelForSubtotals%22%3Afalse%2C%22indexColumn%22%3Afalse%2C%22transposeTable%22%3Afalse%2C%22type%22%3A%22looker_grid%22%2C%22x_axis_gridlines%22%3Afalse%2C%22y_axis_gridlines%22%3Atrue%2C%22show_y_axis_labels%22%3Atrue%2C%22show_y_axis_ticks%22%3Atrue%2C%22y_axis_tick_density%22%3A%22default%22%2C%22y_axis_tick_density_custom%22%3A5%2C%22show_x_axis_label%22%3Atrue%2C%22show_x_axis_ticks%22%3Atrue%2C%22y_axis_scale_mode%22%3A%22linear%22%2C%22x_axis_reversed%22%3Afalse%2C%22y_axis_reversed%22%3Afalse%2C%22plot_size_by_field%22%3Afalse%2C%22trellis%22%3A%22%22%2C%22stacking%22%3A%22%22%2C%22legend_position%22%3A%22center%22%2C%22point_style%22%3A%22none%22%2C%22show_value_labels%22%3Afalse%2C%22label_density%22%3A25%2C%22x_axis_scale%22%3A%22auto%22%2C%22y_axis_combined%22%3Atrue%2C%22show_null_points%22%3Atrue%2C%22interpolation%22%3A%22linear%22%2C%22defaults_version%22%3A1%2C%22series_types%22%3A%7B%7D%2C%22hidden_fields%22%3A%5B%22carriyo_unified.total_orders_attempts%22%2C%22carriyo_unified.total_orders_with_zero_attempt%22%2C%22carriyo_unified.total_orders_with_one_attempt%22%2C%22carriyo_unified.total_orders_with_two_attempt%22%2C%22carriyo_unified.total_orders_with_three_attempt%22%5D%2C%22hidden_points_if_no%22%3A%5B%5D%7D&filter_config=%7B%22carriyo_unified.merchant%22%3A%5B%7B%22type%22%3A%22%3D%22%2C%22values%22%3A%5B%7B%22constant%22%3A%22%22%7D%2C%7B%7D%5D%2C%22id%22%3A28%2C%22error%22%3Afalse%7D%5D%2C%22carriyo_unified.carrier%22%3A%5B%7B%22type%22%3A%22%3D%22%2C%22values%22%3A%5B%7B%22constant%22%3A%22%22%7D%2C%7B%7D%5D%2C%22id%22%3A27%2C%22error%22%3Afalse%7D%5D%2C%22carriyo_unified.collected_at_date%22%3A%5B%7B%22type%22%3A%22on%22%2C%22values%22%3A%5B%7B%22date%22%3A%222020-11-17T12%3A53%3A43.426Z%22%2C%22unit%22%3A%22day%22%2C%22tz%22%3Atrue%7D%5D%2C%22id%22%3A22%2C%22error%22%3Afalse%7D%5D%2C%22carriyo_unified.dropoff_city%22%3A%5B%7B%22type%22%3A%22%3D%22%2C%22values%22%3A%5B%7B%22constant%22%3A%22%22%7D%2C%7B%7D%5D%2C%22id%22%3A31%2C%22error%22%3Afalse%7D%5D%2C%22carriyo_unified.latest_status_boolean%22%3A%5B%7B%22type%22%3A%22is%22%2C%22values%22%3A%5B%7B%22constant%22%3A%22Yes%22%7D%2C%7B%7D%5D%2C%22id%22%3A33%2C%22error%22%3Afalse%7D%5D%2C%22carriyo_unified.pickup_address1%22%3A%5B%7B%22type%22%3A%22%3D%22%2C%22values%22%3A%5B%7B%22constant%22%3A%22%22%7D%2C%7B%7D%5D%2C%22id%22%3A32%2C%22error%22%3Afalse%7D%5D%2C%22carriyo_unified.pickup_city%22%3A%5B%7B%22type%22%3A%22%3D%22%2C%22values%22%3A%5B%7B%22constant%22%3A%22%22%7D%2C%7B%7D%5D%2C%22id%22%3A30%2C%22error%22%3Afalse%7D%5D%2C%22carriyo_unified.pickup_country%22%3A%5B%7B%22type%22%3A%22%3D%22%2C%22values%22%3A%5B%7B%22constant%22%3A%22%22%7D%2C%7B%7D%5D%2C%22id%22%3A29%2C%22error%22%3Afalse%7D%5D%2C%22carriyo_unified.total_orders_attempts%22%3A%5B%7B%22type%22%3A%22%5Cu003e%3D%22%2C%22values%22%3A%5B%7B%22constant%22%3A%221%22%7D%2C%7B%7D%5D%2C%22id%22%3A34%2C%22error%22%3Afalse%7D%5D%7D&dynamic_fields=%5B%5D&origin=share-expanded"
      # }
      drill_fields: [sla_int_drill_details*]
      sql: ${total_orders_with_one_attempt}+${total_orders_with_zero_attempt}+${total_orders_with_two_attempt}+${total_orders_with_three_attempt} ;;
    }


    measure: orders_delivered_on_delivery_date {
      label: "Orders Delivered Based on Delivery Date"
      description: "# of orders delivered in the date specified"
      group_label: "Aggregations Statuses based on update date"
      type: count_distinct
      filters: {
        field: orders_delivered_based_on_delivery_date_boolean
        value: "Yes"
      }
      filters: {
        field: latest_status_per_day_boolean
        value: "Yes"
      }
      drill_fields: [delivered_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: orders_collected_based_on_collected_date {
      label: "Orders collected Based on collected Date"
      description: "# of orders collected in the date specified"
      group_label: "Aggregations Statuses based on update date"
      type: count_distinct
      filters: {
        field: orders_collected_based_on_collected_date_boolean
        value: "Yes"
      }
      filters: {
        field: latest_status_per_day_boolean
        value: "Yes"
      }
      drill_fields: [not_delivered_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: orders_returned_on_returned_date {
      label: "Orders Returned Based on Returned Date"
      description: "# of orders returned in the date specified"
      group_label: "Aggregations Statuses based on update date"
      type: count_distinct
      filters: {
        field: orders_returned_based_on_returned_date_boolean
        value: "Yes"
      }
      filters: {
        field: latest_status_per_day_boolean
        value: "Yes"
      }
      drill_fields: [returned_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: RTO_not_delivered {
      type: count_distinct
      hidden: yes
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: chb_status
        value: "Not Delivered"
      }
      filters: {
        field: no_of_attempts_boolean
        value: "No"
      }
      sql: ${tracking_no} ;;
    }

    measure: RTO_backlog_to_be_added {
      hidden: yes
      type: count_distinct
      filters: {
        field: chb_status
        value: "To be Returned, To be Cancelled, Cancelled"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
          }

      sql: ${tracking_no} ;;
    }

    # measure: RTO_backlog_aggregator {
    #   type: number
    #   label: "RTO Backlog Aggregator"
    #   description: "All shipments that will eventually end in a returned status"
    #   group_label: "1- Order Totals By Chb Status"
    #   sql: ${RTO_not_delivered}+${RTO_backlog_to_be_added} ;;
    #   drill_fields: [RTO_drill_details*]
    # }

    measure: RTO_backlog_aggregator {
      type: count_distinct
      label: "RTO Backlog Aggregator"
      description: "All shipments that will eventually end in a returned status"
      group_label: "1- Order Totals By Chb Status"
      sql: ${tracking_no} ;;
      filters: {
        field: chb_status
        value: "To be Returned, To be Cancelled, Cancelled, Not Delivered"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: no_of_attempts_boolean
        value: "No"
      }
      drill_fields: [RTO_drill_details*]
    }

    measure: chb_count_distinct_collected {
      label: "Total Collected Orders"
      group_label: "1- Order Totals By Chb Status"
      description: "Distinct shipment of Not Delivered Backlog Aggregator,Delivered,RTO Backlog Aggregator and Returned orders"
      type: count_distinct
      sql: ${tracking_no} ;;
      filters: {
        field: chb_status
        value: "Not Delivered, Delivered, To be Returned, To be Cancelled, Cancelled, Returned"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      drill_fields: [delivered_drill_details*]
    }

    # measure: total_RTO_backlog_with_trigger {
    #   hidden: yes
    #   label: "Total RTO Backlog Orders"
    #   description: "Orders that fit the status Not Delivered with More Than 2 Attempts,  and To be Returned"
    #   group_label: "1- Order Totals By Chb Status"
    #   value_format_name: decimal_0
    #   type: number
    #   sql: CASE
    #       WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${RTO_backlog_aggregator}
    #       WHEN {% parameter absolute_or_relative %} = 'Relative' then (${RTO_backlog_aggregator}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
    #       ELSE ${RTO_backlog_aggregator}
    #   END ;;
    #   html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value }}%
    #           {% else %} {{ rendered_value | round }}
    #           {% endif %};;
    # }

    measure: total_RTO_backlog_with_trigger {
      hidden: no
      label: "Total RTO Backlog Orders"
      description: "Orders that fit the status Not Delivered with More Than 2 Attempts,  and To be Returned"
      group_label: "1- Order Totals By Chb Status"
      type: count_distinct
      sql: ${tracking_no} ;;
      filters: {
        field: chb_status
        value: "To be Returned, To be Cancelled, Cancelled, Not Delivered"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: no_of_attempts_boolean
        value: "No"
      }
      drill_fields: [RTO_drill_details*]
    }

    measure: not_delivered_backlog {
      label: "Not Delivered Backlog Aggregator"
      description: "All shipments that are in a not delivered status and under 3 attempts"
      group_label: "1- Order Totals By Chb Status"
      type: count_distinct
      filters: {
        field: chb_status
        value: "Not Delivered"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      filters: {
        field: no_of_attempts_boolean
        value: "Yes"
      }
      drill_fields: [not_delivered_drill_details*]
      sql: ${tracking_no} ;;
    }

    measure: creation_to_collected_time {
      type: average
      label_from_parameter: date_granularity
      label: "Creation to collected Time"
      group_label: "2- Time Spent in Status"
      description: "Time spent from creation to collected"
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_creation_to_collected}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_creation_to_collected}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_creation_to_collected}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_creation_to_collected}*3.80514651484217549e-7
          ELSE ${seconds_creation_to_collected}*1.1574e-5
        END;;
    }

    measure: collected_to_first_attempt_time{
      type: average
      label_from_parameter: date_granularity
      label: "collected to First Attempt Time"
      group_label: "2- Time Spent in Status"
      description: "Time spent from collected to first attempt"
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_collected_to_first_attempt}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_collected_to_first_attempt}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_collected_to_first_attempt}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_collected_to_first_attempt}*3.80514651484217549e-7
          ELSE ${seconds_collected_to_first_attempt}*1.1574e-5
        END;;
    }

    measure: first_to_second_attempt_time{
      type: average
      label_from_parameter: date_granularity
      label: "First to Second Attempt Time"
      group_label: "2- Time Spent in Status"
      description: "Time spent from first to second attempt"
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_first_to_second_attempt}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_first_to_second_attempt}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_first_to_second_attempt}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_first_to_second_attempt}*3.80514651484217549e-7
          ELSE ${seconds_first_to_second_attempt}*1.1574e-5
        END;;
    }

    measure: second_to_third_attempt_time {
      type: average
      label_from_parameter: date_granularity
      label: "Second to Third Attempt Time"
      group_label: "2- Time Spent in Status"
      description: "Time spent from second to third attempt"
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_second_to_third_attempt}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_second_to_third_attempt}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_second_to_third_attempt}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_second_to_third_attempt}*3.80514651484217549e-7
          ELSE ${seconds_second_to_third_attempt}*1.1574e-5
        END;;
    }

    measure: third_attempt_to_returned_time {
      type: average
      label_from_parameter: date_granularity
      label: "Third Attempt To Returned Time"
      group_label: "2- Time Spent in Status"
      description: "Time spent from third attempt to returned"
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_third_attempt_to_returned}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_third_attempt_to_returned}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_third_attempt_to_returned}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_third_attempt_to_returned}*3.80514651484217549e-7
          ELSE ${seconds_third_attempt_to_returned}*1.1574e-5
        END;;
    }

    measure: time_spent_in_confirmed_status {
      type: average
      label_from_parameter: date_granularity
      label: "Time Spent in Confirmed Status"
      group_label: "2- Time Spent in Status"
      description: "Time spent from confirmed to out for delivery"
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_confirmed_to_out_for_delivery}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_confirmed_to_out_for_delivery}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_confirmed_to_out_for_delivery}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_confirmed_to_out_for_delivery}*3.80514651484217549e-7
          ELSE ${seconds_confirmed_to_out_for_delivery}*1.1574e-5
        END;;
    }

    measure: time_spent_in_out_for_delivery_status {
      type: average
      label_from_parameter: date_granularity
      label: "Time Spent in Out For Delivery Status"
      group_label: "2- Time Spent in Status"
      description: "Time spent from out for delivery to first attempt"
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_out_for_delivery_to_first_attempt}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_out_for_delivery_to_first_attempt}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_out_for_delivery_to_first_attempt}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_out_for_delivery_to_first_attempt}*3.80514651484217549e-7
          ELSE ${seconds_out_for_delivery_to_first_attempt}*1.1574e-5
        END;;
    }

    measure: time_spent_in_first_attempt_status {
      type: average
      label_from_parameter: date_granularity
      label: "Time Spent in First Attempt Status"
      group_label: "2- Time Spent in Status"
      description: "Time spent from first attempt to delivered"
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_first_attempt_to_delivered}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_first_attempt_to_delivered}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_first_attempt_to_delivered}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_first_attempt_to_delivered}*3.80514651484217549e-7
          ELSE ${seconds_first_attempt_to_delivered}*1.1574e-5
        END;;
    }

    # measure: time_spent_in_delivered_status {
    #   type: average
    #   label_from_parameter: date_granularity
    #   label: "Time Spent in Delivered Status"
    #   group_label: "2- Time Spent in Status"
    #   description: "Time spent from delivered to returned"
    #   sql:
    #     CASE
    #       WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_delivered_to_returned}/3600
    #       WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_delivered_to_returned}*1.1574e-5
    #       WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_delivered_to_returned}*1.6534285714e-6
    #       WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_delivered_to_returned}*3.80514651484217549e-7
    #       ELSE ${seconds_delivered_to_returned}*1.1574e-5
    #     END;;
    # }

    measure: order_creation_to_booked_time  {
      type: average
      label_from_parameter: date_granularity
      label: "Click to Collect"
      group_label: "3- Click to Status"
      description: "Time from carriyo order creation to DSP creation in days"
      value_format:"#.00"
      drill_fields: [attempt_drill_details*]
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_to_booked}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_to_booked}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_to_booked}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_to_booked}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_to_booked}*1.1574e-5
        END;;
    }

    measure: order_creation_to_collected_time {
      type: average
      label_from_parameter: date_granularity
      label: "Click to collected"
      group_label: "3- Click to Status"
      description: "Time from order creation to collected"
      value_format:"#.00"
      drill_fields: [attempt_drill_details*]
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_to_collected}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_to_collected}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_to_collected}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_to_collected}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_to_collected}*1.1574e-5
        END;;
    }

    measure: order_creation_to_out_for_delivery_ {
      type: average
      label_from_parameter: date_granularity
      label: "Click to Out For Delivery"
      group_label: "3- Click to Status"
      description: "Time from order creation to out for delivery"
      value_format:"#.00"
      drill_fields: [attempt_drill_details*]
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_to_out_for_delivery}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_to_out_for_delivery}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_to_out_for_delivery}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_to_out_for_delivery}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_to_out_for_delivery}*1.1574e-5
        END;;
    }

    measure: order_creation_to_first_attempt_ {
      type: average
      label_from_parameter: date_granularity
      label: "Click to First Attempt"
      group_label: "3- Click to Status"
      description: "Time from order creation to first attempt"
      value_format:"#.00"
      drill_fields: [attempt_drill_details*]
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_to_first_attempt}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_to_first_attempt}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_to_first_attempt}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_to_first_attempt}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_to_first_attempt}*1.1574e-5
        END;;
    }

    measure: order_creation_to_delivered_ {
      type: average
      label_from_parameter: date_granularity
      label: "Click to Delivered"
      group_label: "3- Click to Status"
      description: "Time from order creation to delivered"
      value_format:"#.00"
      drill_fields: [attempt_drill_details*]
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_to_delivered}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_to_delivered}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_to_delivered}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_to_delivered}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_to_delivered}*1.1574e-5
        END;;
    }

    measure: order_creation_to_returned_ {
      type: average
      label_from_parameter: date_granularity
      label: "Click to Returned"
      group_label: "3- Click to Status"
      description: "Time from order creation to returned"
      value_format:"#.00"
      drill_fields: [attempt_drill_details*]
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_to_returned}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_to_returned}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_to_returned}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_to_returned}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_to_returned}*1.1574e-5
        END;;
    }

    measure: time_spent_pending_time {
      type: average
      label_from_parameter: date_granularity
      label: "Time spent in pending"
      group_label: "4- DSP Efficiency"
      description: "Time from order confirmation to collected (Only available from Carriyo)"
      value_format:"#.00"
      drill_fields: [attempt_drill_details*]
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_time_spent_pending}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_time_spent_pending}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_time_spent_pending}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_time_spent_pending}*3.80514651484217549e-7
          ELSE ${seconds_time_spent_pending}*1.1574e-5
        END;;
    }

    measure: time_to_first_attempt_time{
      type: average
      label_from_parameter: date_granularity
      label: "Time to first attempt"
      group_label: "4- DSP Efficiency"
      description: "Time from collected at to first attempt at delivery"
      value_format:"#.00"
      drill_fields: [attempt_drill_details*]
      sql: CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_time_to_first_attempt}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_time_to_first_attempt}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_time_to_first_attempt}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_time_to_first_attempt}*3.80514651484217549e-7
          ELSE ${seconds_time_to_first_attempt}*1.1574e-5
        END;;
    }

    measure: time_to_deliver_ {
      type: average
      label_from_parameter: date_granularity
      group_label: "4- DSP Efficiency"
      description: "Time from collected at to delivered"
      value_format:"#.00"
      drill_fields: [attempt_drill_details*]
      sql:  CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_time_to_deliver}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_time_to_deliver}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_time_to_deliver}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_time_to_deliver}*3.80514651484217549e-7
          ELSE ${seconds_time_to_deliver}*1.1574e-5
        END;;
    }

    measure: time_to_out_to_deliver_ {
      type: average
      label_from_parameter: date_granularity
      group_label: "4- DSP Efficiency"
      description: "Time from collected at to out to deliver"
      value_format:"#.00"
      drill_fields: [attempt_drill_details*]
      sql:  CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_time_to_out_to_deliver}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_time_to_out_to_deliver}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_time_to_out_to_deliver}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_time_to_out_to_deliver}*3.80514651484217549e-7
          ELSE ${seconds_time_to_out_to_deliver}*1.1574e-5
        END;;
    }

    measure: time_to_return_failed_delivery {
      type: average
      label_from_parameter: date_granularity
      group_label: "4- DSP Efficiency"
      description: "Time from collected at to returned"
      value_format:"#.00"
      drill_fields: [attempt_drill_details*]
      sql:  CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_time_to_returned}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_time_to_returned}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_time_to_returned}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_time_to_returned}*3.80514651484217549e-7
          ELSE ${seconds_time_to_returned}*1.1574e-5
        END;;
    }

    # measure: delivery_within_promised_yes {
    #   type: count_distinct
    #   hidden: yes
    #   sql: ${shipment_id} ;;
    #   filters: [delivery_Within_promised: "Yes"]
    # }

    # measure: delivery_within_promised_no {
    #   type: count_distinct
    #   hidden: yes
    #   sql: ${shipment_id} ;;
    #   filters: [delivery_Within_promised: "No"]
    # }

    # measure: ecommerce_ontime_ratio {
    #   type: number
    #   description: "Ratio of deliveries made within promise time versus not made within promised time"
    #   group_label: "5- Last Mile Reliability"
    #   value_format_name: percent_2
    #   sql: ${delivery_within_promised_yes}/${delivery_within_promised_no} ;;
    # }

    parameter: lead_time_delivery_time_option {
      label: "Int vs Circadian"
      type: string
      allowed_value: { value: "Int" }
      allowed_value: { value: "Circadian" }
    }

    dimension: lead_time_delivery_creation{
      label: "Lead Time In Days Based on Creation Date"
      hidden: yes
      description: "Date difference between creation and delivery, in days"
      type: number
      value_format:"#.00"
      sql: CASE
              WHEN {% parameter lead_time_delivery_time_option %} = 'Int' THEN date_diff(${delivered_at_date},${dsp_creation_date_date},day)
              WHEN {% parameter lead_time_delivery_time_option %} = 'Circadian' THEN timestamp_diff(${delivered_at_raw},${dsp_creation_date_raw},second)/3600
              ELSE date_diff(${delivered_at_date},${dsp_creation_date_date},day)
            END;;
    }

    dimension: lead_time_delivery_collected{
      label: "Lead Time In Days Based on collected Date"
      hidden: yes
      description: "Date difference between collected and delivery, in days"
      type: number
      value_format:"#.00"
      sql: CASE
      WHEN {% parameter lead_time_delivery_time_option %} = 'Int' THEN date_diff(${delivered_at_date},${collected_at_date},day)
      WHEN {% parameter lead_time_delivery_time_option %} = 'Circadian' THEN timestamp_diff(${delivered_at_raw},${collected_at_raw},second)/3600
      ELSE date_diff(${delivered_at_date},${collected_at_date},day)
      END;;
    }

  measure: count_distinct_shipment {
    label: "Total Orders by Latest Status"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct count of orders by latest status"
    drill_fields: [attempt_drill_details*]
    type: count_distinct
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    sql: ${tracking_no} ;;
  }

    measure: count_distinct_shipment_ {
      label: "Total Orders"
      group_label: "1- Order Totals By Chb Status"
      description: "Distinct count of orders"
      type: count_distinct
      sql: ${tracking_no} ;;
    }

  measure: chb_count_distinct_cancelled {
    label: "Total Cancelled Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of cancelled orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "Cancelled"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    sql: ${tracking_no} ;;
  }

  measure: pending_orders {
    hidden: yes
    label: "Pending Orders"
    description: "Orders that fit the status Not Delivered and To be Returned"
    group_label: "1- Order Totals By Chb Status"
    filters: {
      field: chb_status
      value: "To be Returned, To be Cancelled, Cancelled, Not Delivered"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    type: count_distinct
    sql: ${tracking_no} ;;
  }

  measure: total_pending_with_trigger {
    label: "Total Pending Orders"
    description: "Orders that fit the status Not Delivered and To be Returned"
    group_label: "1- Order Totals By Chb Status"
    value_format_name: decimal_0
    type: number
    sql: CASE
          WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${pending_orders}
          WHEN {% parameter absolute_or_relative %} = 'Relative' then (${pending_orders}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
          ELSE ${pending_orders}
       END ;;
    drill_fields: [pending_drill_details*]
    html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value }}%
               {% else %} {{ rendered_value | round }}
               {% endif %};;
  }

  measure: chb_count_distinct_delivered {
    hidden: no
    label: "Total Delivered Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct count of delivered orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "Delivered"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    drill_fields: [delivered_drill_details*]
    sql: ${tracking_no} ;;
  }

  measure: total_delivered_with_trigger {
    label: "Total Delivered Orders - Lead Time Analysis"
    group_label: "7- Order Totals By Chb Status - Lead Time Analysis"
    description: "Distinct count of delivered orders"
    value_format_name: decimal_0
    type: number
    sql: CASE
            WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${chb_count_distinct_delivered}
            WHEN {% parameter absolute_or_relative %} = 'Relative' then (${chb_count_distinct_delivered}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
            ELSE ${chb_count_distinct_delivered}
         END ;;
    html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value | round }}%
           {% else %} {{ rendered_value }}
           {% endif %};;
  }

  measure: chb_count_distinct_lost {
    label: "Total Lost Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of lost orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "Lost"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    drill_fields: [lost_drill_details*]
    sql: ${tracking_no} ;;
  }

  measure: chb_count_distinct_not_delivered {
    label: "Total Not Delivered Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of not delivered orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "Not Delivered"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    drill_fields: [not_delivered_drill_details*]
    sql: ${tracking_no}  ;;
  }

  measure: chb_count_distinct_not_collected {
    label: "Total Not Collected Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of not collected orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "Not Collected"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    drill_fields: [not_collected_drill_details*]
    sql: ${tracking_no};;
  }

    measure: chb_count_distinct_cancelled_before_collection {
      label: "Total Cancelled Before Collected Orders"
      group_label: "1- Order Totals By Chb Status"
      description: "Distinct shipment of orders cancelled before being collected"
      type: count_distinct
      filters: {
        field: chb_status
        value: "Cancelled Before Collection"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      drill_fields: [not_collected_drill_details*]
      sql: ${tracking_no};;
    }

  measure: chb_count_distinct_returned {
    hidden: no
    label: "Total Returned Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of returned orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "Returned"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    drill_fields: [returned_drill_details*]
    sql: ${tracking_no} ;;
  }

  # measure: total_returned_with_trigger {
  #   label: "Total Returned Orders with trigger"
  #   group_label: "7- Order Totals By Chb Status - Lead Time Analysis"
  #   description: "Distinct shipment of returned orders"
  #   value_format_name: decimal_0
  #   type: number
  #   sql: CASE
  #         WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${chb_count_distinct_returned}
  #         WHEN {% parameter absolute_or_relative %} = 'Relative' then (${chb_count_distinct_returned}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
  #         ELSE ${chb_count_distinct_returned}
  #     END ;;
  #   html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value }}%
  #           {% else %} {{ rendered_value | round }}
  #           {% endif %};;
  # }

  measure: total_returned_with_trigger {
    label: "Total Returned Orders with trigger"
    group_label: "7- Order Totals By Chb Status - Lead Time Analysis"
    description: "Distinct shipment of returned orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "Returned"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    drill_fields: [returned_drill_details*]
    sql: ${tracking_no} ;;
  }

  measure: chb_count_distinct_to_be_cancelled {
    label: "Total To Be Cancelled Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of to be cancelled orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "To be Cancelled"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    sql: ${tracking_no} ;;
  }

  measure: chb_count_distinct_to_be_returned {
    label: "Total To Be Returned Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of to be returned orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "To be Returned"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    sql: ${tracking_no} ;;
  }

  parameter: creation_or_collected {
    type: string
    allowed_value: { value: "Based on Creation Date" }
    allowed_value: { value: "Based on collected Date" }
  }

  parameter: absolute_or_relative {
    type: string
    allowed_value: { value: "Absolute" }
    allowed_value: { value: "Relative" }
  }

  measure: total_orders_for_lead_time_analysis_look {
    hidden: yes
    label: "Delivered + Pending + Returned"
    description: "Totals of Delivered + Pending + Returned"
    group_label: "1- Order Totals By Chb Status"
    type: number
    sql: ${chb_count_distinct_delivered}+${pending_orders}+${chb_count_distinct_returned}+${chb_count_distinct_cancelled_before_collection} ;;
  }

  measure: total_orders_for_lead_time_analysis_look_with_trigger {
    label: "Delivery Lead Time Total"
    description: "Totals of Delivered + Pending + Returned"
    group_label: "7- Order Totals By Chb Status - Lead Time Analysis"
    type: number
    sql: CASE
              WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${total_orders_for_lead_time_analysis_look}
              WHEN {% parameter absolute_or_relative %} = 'Relative' then (${total_orders_for_lead_time_analysis_look}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
              ELSE ${total_orders_for_lead_time_analysis_look}
          END ;;
   html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value | round }}%
          {% else %} {{ rendered_value }}
          {% endif %};;
  }

    measure: total_orders_for_attempt_lead_time_analysis_look {
      hidden: no
      label: "Attempt Lead Time Total"
      description: "Totals of Attempted + Pending + Returned"
      group_label: "1- Order Totals By Chb Status"
      type: number
      sql: ${count_first_attempts}+${pending_orders}+${chb_count_distinct_returned}+${chb_count_distinct_cancelled_before_collection} ;;
    }

    measure: count_first_attempts {
      hidden: no
      label: "Count of First Attempts"
      description: "Total orders with a first attempt"
      group_label: "8- Order Attempt Lead Time"
      type: count_distinct
      filters: {
        field: first_attempt_boolean
        value: "Yes"
      }
      filters: {
        field: latest_status_boolean
        value: "Yes"
      }
      sql: ${tracking_no} ;;
    }

    measure: count_attempt_leadtime_0 {
      hidden: no
      label: "Same Day Attempt"
      description: "Orders attempted in same day"
      group_label: "8- Order Attempt Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      sql: CASE
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Int' AND ${attempt_lead_time} <= 0
                THEN ${tracking_no}
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${attempt_lead_time} < 24
                THEN ${tracking_no}
              ELSE null
           END ;;
    }

    measure: count_attempt_leadtime_1 {
      hidden: no
      label: "A+1"
      description: "Orders attempted in D+1"
      group_label: "8- Order Attempt Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      sql: CASE
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Int' AND ${attempt_lead_time} = 1
                THEN ${tracking_no}
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${attempt_lead_time} BETWEEN 24 AND 48
                THEN ${tracking_no}
              ELSE null
           END ;;
    }

    measure: count_attempt_leadtime_2 {
      hidden: no
      label: "A+2"
      description: "Orders attempted in D+2"
      group_label: "8- Order Attempt Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      sql: CASE
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Int' AND ${attempt_lead_time} = 2
                THEN ${tracking_no}
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${attempt_lead_time} BETWEEN 48 AND 72
                THEN ${tracking_no}
              ELSE null
           END ;;
    }

    measure: count_attempt_leadtime_3 {
      hidden: no
      label: "A+3"
      description: "Orders attempted in D+3"
      group_label: "8- Order Attempt Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      sql: CASE
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Int' AND ${attempt_lead_time} = 3
                THEN ${tracking_no}
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${attempt_lead_time} BETWEEN 72 AND 96
                THEN ${tracking_no}
              ELSE null
           END ;;
    }

    measure: count_attempt_leadtime_4 {
      hidden: no
      label: "A+4"
      description: "Orders attempted in D+4"
      group_label: "8- Order Attempt Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      sql: CASE
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Int' AND ${attempt_lead_time} = 4
                THEN ${tracking_no}
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${attempt_lead_time} BETWEEN 96 AND 120
                THEN ${tracking_no}
              ELSE null
           END ;;
    }

    measure: count_attempt_leadtime_5 {
      hidden: no
      label: "A+5"
      description: "Orders attempted in D+5"
      group_label: "8- Order Attempt Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      sql: CASE
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Int' AND ${attempt_lead_time} = 5
                THEN ${tracking_no}
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${attempt_lead_time} BETWEEN 120 AND 144
                THEN ${tracking_no}
              ELSE null
           END ;;
    }

    measure: count_attempt_leadtime_6 {
      hidden: no
      label: "A+6"
      description: "Orders attempted in D+6"
      group_label: "8- Order Attempt Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      sql: CASE
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Int' AND ${attempt_lead_time} = 6
                THEN ${tracking_no}
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${attempt_lead_time} BETWEEN 144 AND 168
                THEN ${tracking_no}
              ELSE null
           END ;;
    }

    measure: count_attempt_leadtime_7 {
      hidden: no
      label: "A+7"
      description: "Orders attempted in D+7"
      group_label: "8- Order Attempt Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      sql: CASE
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Int' AND ${attempt_lead_time} = 7
                THEN ${tracking_no}
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${attempt_lead_time} BETWEEN 168 AND 192
                THEN ${tracking_no}
              ELSE null
           END ;;
    }

    measure: count_attempt_leadtime_7_more {
      hidden: no
      label: "A>7"
      description: "Orders attempted in D>7"
      group_label: "8- Order Attempt Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      sql: CASE
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Int' AND ${attempt_lead_time} > 7
                THEN ${tracking_no}
              WHEN
              {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${attempt_lead_time} > 192
                THEN ${tracking_no}
              ELSE null
           END ;;
    }


    # measure: count_delivered_0 {
    #   hidden: no
    #   label: "Same Day"
    #   description: "Orders delivered in same day"
    #   group_label: "6- Order Delivery Lead Time"
    #   type: count_distinct
    #   drill_fields: [delivered_drill_details*]
    #   sql: CASE
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} <= 0
    #             THEN ${tracking_no}
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} < 24
    #             THEN ${tracking_no}
    #           ELSE null
    #       END ;;
    # }

    dimension: count_delivered_0_control {
      hidden: yes
      type: yesno
      sql: (   {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} <=0 ) OR
        ( {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} < 24 );;
    }

    measure: count_delivered_0 {
      hidden: no
      label: "Same Day"
      description: "Orders delivered in same day"
      group_label: "6- Order Delivery Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      filters: [count_delivered_0_control: "Yes"]
      sql: ${tracking_no} ;;
    }

    measure: count_delivered_0_with_trigger {
      hidden: yes
      label: "Same Day"
      description: "Orders delivered in same day"
      # label_from_parameter:  absolute_or_relative
      group_label: "6- Order Delivery Lead Time"
      drill_fields: [delivered_drill_details*]
      value_format_name: decimal_0
      type: number
      sql: CASE
              WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${count_delivered_0}
              WHEN {% parameter absolute_or_relative %} = 'Relative' then (${count_delivered_0}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
              ELSE ${count_delivered_0}
           END ;;
      html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value | round }}%
              {% else %} {{ rendered_value }}
              {% endif %};;
    }

    # measure: count_delivered_1 {
    #   hidden: no
    #   label: "1 Days"
    #   label_from_parameter: creation_or_collected
    #   description: "Orders delivered in one days of time"
    #   group_label: "6- Order Delivery Lead Time"
    #   type: count_distinct
    #   drill_fields: [delivered_drill_details*]
    #   sql:  CASE
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =1
    #             THEN ${tracking_no}
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 24 AND 48
    #             THEN ${tracking_no}
    #           ELSE null
    #       END ;;
    # }

    dimension: count_delivered_1_control {
      hidden: yes
      type: yesno
      sql: (   {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =1 ) OR
        ( {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 24 AND 48);;
    }

    measure: count_delivered_1 {
      hidden: no
      label: "1 Days"
      label_from_parameter: creation_or_collected
      description: "Orders delivered in one days of time"
      group_label: "6- Order Delivery Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      filters: [count_delivered_1_control: "Yes"]
      sql:  ${tracking_no} ;;
    }

    measure: count_delivered_1_with_trigger {
      hidden: yes
      label: "D+1"
      description: "Orders delivered in one days of time"
      # label_from_parameter:  absolute_or_relative
      group_label: "6- Order Delivery Lead Time"
      value_format_name: decimal_0
      type: number
      sql: CASE
              WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${count_delivered_1}
              WHEN {% parameter absolute_or_relative %} = 'Relative' then (${count_delivered_1}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
              ELSE ${count_delivered_1}
           END ;;
      drill_fields: [delivered_drill_details*]
      html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value | round }}%
            {% else %} {{ rendered_value }}
            {% endif %};;
    }

    # measure: count_delivered_2 {
    #   hidden: no
    #   label: "2 Days"
    #   description: "Orders delivered in two days of time"
    #   group_label: "6- Order Delivery Lead Time"
    #   type: count_distinct
    #   drill_fields: [delivered_drill_details*]
    #   sql: CASE
    #         WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =2
    #             THEN ${tracking_no}
    #         WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 48 AND 72
    #             THEN ${tracking_no}
    #           ELSE null
    #       END ;;
    # }

    dimension: count_delivered_2_control {
      hidden: yes
      type: yesno
      sql: (   {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =2 ) OR
        ( {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 48 AND 72);;
    }

    measure: count_delivered_2 {
      hidden: no
      label: "2 Days"
      description: "Orders delivered in two days of time"
      group_label: "6- Order Delivery Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      filters: [count_delivered_2_control: "Yes"]
      sql: ${tracking_no} ;;
    }

    measure: count_delivered_2_with_trigger {
      hidden: yes
      label: "D+2"
      description: "Orders delivered in two days of time"
      # label_from_parameter:  absolute_or_relative
      group_label: "6- Order Delivery Lead Time"
      value_format_name: decimal_0
      type: number
      sql: CASE
              WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${count_delivered_2}
              WHEN {% parameter absolute_or_relative %} = 'Relative' then (${count_delivered_2}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
              ELSE ${count_delivered_2}
           END ;;
      drill_fields: [delivered_drill_details*]
      html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value | round }}%
          {% else %} {{ rendered_value }}
          {% endif %};;
    }

    # measure: count_delivered_3 {
    #   hidden: no
    #   label: "3 Days"
    #   description: "Orders delivered in three days of time"
    #   group_label: "6- Order Delivery Lead Time"
    #   type: count_distinct
    #   drill_fields: [delivered_drill_details*]
    #   sql: CASE
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =3
    #             THEN ${tracking_no}
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 72 AND 96
    #             THEN ${tracking_no}
    #           ELSE null
    #       END ;;
    # }

    dimension: count_delivered_3_control {
      hidden: yes
      type: yesno
      sql: (   {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =3 ) OR
        ( {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 72 AND 96);;
    }

    measure: count_delivered_3 {
      hidden: no
      label: "3 Days"
      description: "Orders delivered in three days of time"
      group_label: "6- Order Delivery Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      filters: [count_delivered_3_control: "Yes"]
      sql: ${tracking_no} ;;
    }

    measure: count_delivered_3_with_trigger {
      hidden: yes
      label: "D+3"
      description: "Orders delivered in three days of time"
      # label_from_parameter:  absolute_or_relative
      group_label: "6- Order Delivery Lead Time"
      value_format_name: decimal_0
      type: number
      sql: CASE
              WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${count_delivered_3}
              WHEN {% parameter absolute_or_relative %} = 'Relative' then (${count_delivered_3}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
              ELSE ${count_delivered_3}
           END ;;
      drill_fields: [delivered_drill_details*]
      html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value | round }}%
            {% else %} {{ rendered_value }}
            {% endif %};;
    }

    # measure: count_delivered_4 {
    #   hidden: no
    #   label: "4 Days"
    #   description: "Orders delivered in four days of time"
    #   group_label: "6- Order Delivery Lead Time"
    #   type: count_distinct
    #   drill_fields: [delivered_drill_details*]
    #   sql: CASE
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =4
    #             THEN ${tracking_no}
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 96 AND 120
    #             THEN ${tracking_no}
    #           ELSE null
    #       END ;;
    # }

    dimension: count_delivered_4_control {
      hidden: yes
      type: yesno
      sql: (   {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =4 ) OR
        ( {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 96 AND 120);;
    }

    measure: count_delivered_4 {
      hidden: no
      label: "4 Days"
      description: "Orders delivered in four days of time"
      group_label: "6- Order Delivery Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      filters: [count_delivered_4_control: "Yes"]
      sql: ${tracking_no} ;;
    }

    measure: count_delivered_4_with_trigger {
      hidden: yes
      label: "D+4"
      description: "Orders delivered in four days of time"
      # label_from_parameter:  absolute_or_relative
      group_label: "6- Order Delivery Lead Time"
      value_format_name: decimal_0
      type: number
      sql: CASE
              WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${count_delivered_4}
              WHEN {% parameter absolute_or_relative %} = 'Relative' then (${count_delivered_4}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
              ELSE ${count_delivered_4}
           END ;;
      drill_fields: [delivered_drill_details*]
      html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value | round }}%
              {% else %} {{ rendered_value }}
              {% endif %};;
    }

    # measure: count_delivered_5 {
    #   hidden: no
    #   label: "5 Days"
    #   description: "Orders delivered in five days of time"
    #   group_label: "6- Order Delivery Lead Time"
    #   type: count_distinct
    #   drill_fields: [delivered_drill_details*]
    #   sql: CASE
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =5
    #             THEN ${tracking_no}
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 120 AND 144
    #             THEN ${tracking_no}
    #           ELSE null
    #       END ;;
    # }

    dimension: count_delivered_5_control {
      hidden: yes
      type: yesno
      sql: (   {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =5 ) OR
        ( {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 120 AND 144);;
    }

    measure: count_delivered_5 {
      hidden: no
      label: "5 Days"
      description: "Orders delivered in five days of time"
      group_label: "6- Order Delivery Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      filters: [count_delivered_5_control: "Yes"]
      sql: ${tracking_no} ;;
    }

    measure: count_delivered_5_with_trigger {
      hidden: yes
      label: "D+5"
      description: "Orders delivered in five days of time"
      # label_from_parameter:  absolute_or_relative
      group_label: "6- Order Delivery Lead Time"
      value_format_name: decimal_0
      type: number
      sql: CASE
              WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${count_delivered_5}
              WHEN {% parameter absolute_or_relative %} = 'Relative' then (${count_delivered_5}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
              ELSE ${count_delivered_5}
           END ;;
      drill_fields: [delivered_drill_details*]
      html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value | round }}%
              {% else %} {{ rendered_value }}
              {% endif %};;
    }

    # measure: count_delivered_6 {
    #   hidden: no
    #   label: "6 Days"
    #   description: "Orders delivered in six days of time"
    #   group_label: "6- Order Delivery Lead Time"
    #   type: count_distinct
    #   drill_fields: [delivered_drill_details*]
    #   sql: CASE
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =6
    #             THEN ${tracking_no}
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 144 AND 168
    #             THEN ${tracking_no}
    #           ELSE null
    #       END ;;
    # }

    dimension: count_delivered_6_control {
      hidden: yes
      type: yesno
      sql: (   {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =6 ) OR
        ( {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 144 AND 168);;
    }

    measure: count_delivered_6 {
      hidden: no
      label: "6 Days"
      description: "Orders delivered in six days of time"
      group_label: "6- Order Delivery Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      filters: [count_delivered_6_control: "Yes"]
      sql: ${tracking_no} ;;
    }

    measure: count_delivered_6_with_trigger {
      hidden: yes
      label: "D+6"
      description: "Orders delivered in six days of time"
      # label_from_parameter:  absolute_or_relative
      group_label: "6- Order Delivery Lead Time"
      value_format_name: decimal_0
      type: number
      sql: CASE
              WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${count_delivered_6}
              WHEN {% parameter absolute_or_relative %} = 'Relative' then (${count_delivered_6}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
              ELSE ${count_delivered_6}
           END ;;
      drill_fields: [delivered_drill_details*]
      html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value | round }}%
            {% else %} {{ rendered_value }}
            {% endif %};;
    }

    # measure: count_delivered_7 {
    #   hidden: no
    #   label: "7 Days"
    #   description: "Orders delivered in seven days of time"
    #   group_label: "6- Order Delivery Lead Time"
    #   type: count_distinct
    #   drill_fields: [delivered_drill_details*]
    #   sql: CASE
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =7
    #             THEN ${tracking_no}
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 168 AND 192
    #             THEN ${tracking_no}
    #           ELSE null
    #       END ;;
    # }

    dimension: count_delivered_7_control {
      hidden: yes
      type: yesno
      sql: (   {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} =7 ) OR
        ( {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} BETWEEN 168 AND 192);;
    }

    measure: count_delivered_7 {
      hidden: no
      label: "7 Days"
      description: "Orders delivered in seven days of time"
      group_label: "6- Order Delivery Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      filters: [count_delivered_7_control: "Yes"]
      sql: ${tracking_no} ;;
    }

    measure: count_delivered_7_with_trigger {
      hidden: yes
      label: "D+7"
      description: "Orders delivered in seven days of time"
      # label_from_parameter:  absolute_or_relative
      group_label: "6- Order Delivery Lead Time"
      value_format_name: decimal_0
      type: number
      sql: CASE
              WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${count_delivered_7}
              WHEN {% parameter absolute_or_relative %} = 'Relative' then (${count_delivered_7}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
              ELSE ${count_delivered_7}
           END ;;
      drill_fields: [delivered_drill_details*]
      html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value | round }}%
            {% else %} {{ rendered_value }}
            {% endif %};;
    }

    # measure: count_delivered_8 {
    #   hidden: no
    #   label: "D>7"
    #   description: "Orders delivered in more than 7 days of time"
    #   group_label: "6- Order Delivery Lead Time"
    #   type: count_distinct
    #   drill_fields: [delivered_drill_details*]
    #   sql: CASE
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} >7
    #             THEN ${tracking_no}
    #           WHEN
    #           {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} >192
    #             THEN ${tracking_no}
    #           ELSE null
    #       END ;;
    # }

    dimension: count_delivered_8_control {
      hidden: yes
      type: yesno
      sql: (   {% parameter lead_time_delivery_time_option %} = 'Int' AND ${lead_time_delivery_collected} >7 ) OR
        ( {% parameter lead_time_delivery_time_option %} = 'Circadian' AND ${lead_time_delivery_collected} >192);;
    }

    measure: count_delivered_8 {
      hidden: no
      label: "D>7"
      description: "Orders delivered in more than 7 days of time"
      group_label: "6- Order Delivery Lead Time"
      type: count_distinct
      drill_fields: [delivered_drill_details*]
      filters: [count_delivered_8_control: "Yes"]
      sql: ${tracking_no} ;;
    }

    measure: count_delivered_8_with_trigger {
      hidden: yes
      label: "D>7"
      description: "Orders delivered in more than 7 days of time"
      # label_from_parameter:  absolute_or_relative
      group_label: "6- Order Delivery Lead Time"
      value_format_name: decimal_0
      type: number
      sql: CASE
              WHEN {% parameter absolute_or_relative %} = 'Absolute' then ${count_delivered_8}
              WHEN {% parameter absolute_or_relative %} = 'Relative' then (${count_delivered_8}/NULLIF(${total_orders_for_lead_time_analysis_look},0))*100
              ELSE ${count_delivered_8}
           END ;;
      drill_fields: [delivered_drill_details*]
      html:  {% if absolute_or_relative._parameter_value ==  "'Relative'" %} {{ rendered_value | round }}%
              {% else %} {{ rendered_value }}
              {% endif %};;
    }


# SPD 267 End

# SPD 268 Start

    # measure: count_orders_collected {
    #   label: "Total Orders Collected"
    #   hidden: no
    #   group_label: "1- Order Totals By Milestone"
    #   description: "Orders Collected by DSP's"
    #   type: count_distinct
    #   filters: [status: "collected"]
    #   sql: ${shipment_id} ;;
    # }

    # measure: count_orders_not_collected {
    #   label: "Total Orders Not Collected"
    #   group_label: "1- Order Totals By Milestone"
    #   description: "Orders Not Collected by DSP's"
    #   type: count_distinct
    #   filters: [status: "-CANCELLED, -DELIVERED, -ERROR, -FAILED_ATTEMPT, -FAILED_DELIVERY_ATTEMPT, -IN_TRANSIT, -MISSING, -OUT_FOR_DELIVERY, -READY_FOR_RETURN, -RETURN_IN_TRANSIT, -RETURNED, -collected, -SUSPENDED"]
    #   sql: ${shipment_id} ;;
    # }

# SPD 268 End

# SPD 253 start

    measure: cancelled_order{
      type: count_distinct
      description: "Count of Cancelled Orders by chb_status"
      hidden: yes
      filters: {
        field: chb_status
        value: "Cancelled"
      }
      sql:${tracking_no};;
    }

    measure: refuse_rate {
      group_label: "5- Last Mile Reliability"
      label: "Refuse Rate"
      description: "Number of Cancelled Orders divided by Total Orders"
      type: number
      sql: SAFE_DIVIDE(${cancelled_order},${count_distinct_shipment}) ;;
      value_format_name: percent_2
    }

# SPD 253 End



# SPD 276 Start

    # measure: count_of_orders_deliverd_late {
    #   label: "Total Orders Delivered Late"
    #   group_label: "1- Order Totals By Milestone"
    #   type: count_distinct
    #   sql:case when ${delivered_at_date} >= ${promisedDeliverydate_date} then ${shipment_id} else null end;;
    # }
# SPD 276 End

# SPD 277 Start
    dimension_group: order_creation_in_carriyo {
      type: duration
      description: "Time to Carriyo creation from Order Creation date"
      hidden: yes
      intervals: [second]
      sql_start: ${order_creation_raw} ;;
      sql_end: ${carriyo_creation_date_raw} ;;
    }

    measure: carriyo_order_creation_in_carriyo_time  {
      type: average
      label_from_parameter: date_granularity
      label: "Click to Order Creation in Carriyo"
      group_label: "3- Click to Status"
      description: "Time from Order Placement by Customer to First Created in Carriyo"
      value_format:"#.00"
      sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_in_carriyo}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_in_carriyo}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_in_carriyo}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_in_carriyo}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_in_carriyo}*1.1574e-5
        END;;
    }

# SPD 277 End
    # dimension: ca_first_attempt_within_SLA_int {
    #   hidden: yes
    #   type: string
    #   sql:  ${TABLE}.ca_first_attempt_within_SLA_int ;;
    # }

    # dimension: ca_first_attempt_within_SLA_circadian {
    #   hidden: yes
    #   type: string
    #   sql: ${TABLE}.ca_first_attempt_within_SLA_circadian;;
    # }


# SPD 304 Start
    # measure: count_shipment_returned_on_collected_date {
    #   label: "Total Orders Returned Due to Failed Delivery Based On collected Date"
    #   group_label: "1- Order Totals By Milestone"
    #   description: "Distinct shipment orders returned by DSP"
    #   type: count_distinct
    #   filters: {
    #     field: status
    #     value: "RETURNED"
    #   }
    #   filters: {
    #     field: collected_equals_returned
    #     value: "yes"
    #   }
    #   sql: ${shipment_id} ;;
    # }

    # measure: count_shipment_returned {
    #   label: "Total Orders Returned Due to Failed Delivery"
    #   group_label: "1- Order Totals By Milestone"
    #   description: "Distinct shipment orders returned by DSP"
    #   type: count_distinct
    #   filters: {
    #     field: status
    #     value: "RETURNED"
    #   }
    #   sql: ${shipment_id} ;;
    # }
# SPD 304 End

#SPD 307 Start
    # measure: count_shipment_received {
    #   label: "Total Orders Received"
    #   group_label: "1- Order Totals By Milestone"
    #   description: "Distinct shipment orders Received by DSP"
    #   type: count_distinct
    #   sql: case
    #     when ${status} ='BOOKED' then ${shipment_id} else null end ;;
    # }
#SPD 307 End

    measure: c_to_p {
      label: "Service Level Agreement"
      group_label: "5- Last Mile Reliability"
      description: "Time (in days) under which a delivery for the shipment is promised"
      value_format:"#.00"
      type: number
      sql: date_diff(${first_promised_delivery_date},${first_order_date}, day) ;;
    }

#   measure: c_to_S {
#     label: "Click to Ship - C2S"
#     group_label: "4- Last Mile Reliability"
#     description: "Time (in days) taken to ship the shipment since its ordered date"
#     value_format:"#.00"
#     type: number
#     sql: date_diff(${first_collected_date}, ${first_order_date}, day) ;;
#   }
    measure: first_order_date {
      description: "The minimum order creation date"
      hidden: yes
      type: date
      sql:MIN(${order_creation_date});;
    }
    measure: first_promised_delivery_date {
      description: "The minimum first attempt date"
      hidden: yes
      type: date
      sql:MIN(${ca_SLA_first_attempt_by_date});;
    }
    # measure: first_failed_attempt_date {
    #   hidden: yes
    #   type: date
    #   sql:MIN(${failed_attempt_at_date});;
    # }
    measure: first_ofd_date {
      description: "The minimum OFD date"
      hidden: yes
      type: date
      sql:MIN(${out_for_delivery_at_date});;
    }
    measure: first_delivered_date {
      description: "The minimum delivery date"
      hidden: yes
      type: date
      sql:MIN(${delivered_at_date});;
    }
    measure: first_collected_date {
      description: "The minimum collection date"
      hidden: yes
      type: date
      sql:MIN(${collected_at_date});;
    }


    # SPD-547 starts ..

    measure: no_of_attempts_delivered_sum {
      label: "Total No.of Attempts Before Delivery"
      description: "Total No.of Attempts Before Delivery"
      type: sum
      filters: [chb_status: "Delivered",latest_status_boolean: "Yes"]
      sql: ${no_of_attempts} ;;

    }

    measure: attempts_to_order_ratio {
      label: "Attempts to Order Ratio"
      group_label: "4- DSP Efficiency"
      description: "Ratio between the Orders Delivered to No.of Attempts made to deliver them"
      type: number
      #value_format_name: percent_2
      sql: safe_divide(${no_of_attempts_delivered_sum}, ${chb_count_distinct_delivered});;
      value_format: "0.00\%"
    }

    # SPD-547 ends ..

#   measure: collected_to_first_attempt  {
#     label: "Ship from DC to First attempt - S2F"
#     group_label: "Courier Milestone Efficiency"
#     description: "Time (in days) taken to have the first attempt since the package physically was collected up from DC by the DSP "
#     value_format:"#.00"
#     type: number
#     sql:  (date_diff((coalesce((${first_failed_attempt_date}),(${first_ofd_date}),(${first_delivered_date}))),(${first_collected_date}),  day));;
#   }

#  measure: first_attempt_to_delivery{
#    label: "First OFD to Delivery (in days) - F2D"
#    description: "Time (in days) taken to have the delivered status since the first attempt"
#    type: number
#    sql:  (date_diff((coalesce((${first_failed_attempt_date}),(${first_ofd_date}),(${first_delivered_date}))),(${first_delivered_date}),  day));;
#  }

#  measure: dea {
#    label: "Delivery Estimate Accuracy"
#    description: "Count of the orders which were attempted at least once before the promise date"
#    type: number
#    sql:  case when (coalesce((${first_failed_attempt_date}),(${first_ofd_date}),(${first_delivered_date})) < ${promisedDeliverydate_date} then (${count_distinct_shipment} else 0 end;;
#  }
    # measure: is_fdds {
    #   type: yesno
    #   label: "Is First Day Delivery Success?"
    #   group_label: "5- Last Mile Reliability"
    #   description: "Has the shipment been delivered on its first attempt? Is it a first day delivery success?"
    #   sql: (date_diff(coalesce((${first_ofd_date}),(${first_failed_attempt_date}),(${first_delivered_date})), ${first_delivered_date},day))=0;;
    # }

# .......................BACKLOG TRACKER FIELDS STARTS....................................................

    dimension: latest_status_per_day {
      hidden: no
      group_label: "Statuses"
      description: "The latest status on any given day"
      type: number
      sql: ${TABLE}.latest_status_per_day ;;
    }

#     # .......................SUPPORTING DIMENSIONS......................................
#     dimension: no_of_attempts_num {
#       hidden: yes
#       type: number
#       sql: cast(${no_of_attempts} as numeric) ;;
#     }

#     dimension: latest_status_per_day {
#       hidden: yes
#       type: number
#       sql: ${TABLE}.latest_status_per_day ;;
#     }

    dimension: latest_status_per_day_boolean {
      description: "Boolean denoting whether a status is the latest status for the day"
      hidden: yes
      type: yesno
      sql: ${latest_status_per_day}=1  ;;
    }

#     dimension:is_collected_date
#     {
#       type: yesno
#       hidden: yes
#       sql: ${collected_at_date} = ${update_date_date} ;;
#     }

#     dimension:is_not_attempted
#     {
#       type: yesno
#       hidden: yes
#       sql: ${collected_at_date} is not null and  ${latest_status_per_day} = 1 and ${no_of_attempts} = 0;;
#     }

#     dimension:is_attempted_only_once
#     {
#       type: yesno
#       hidden: yes
#       sql: ${collected_at_date} is not null and  ${latest_status_per_day} = 1 and ${no_of_attempts} = 1;;
#     }

#     dimension:is_attempted_only_twice
#     {
#       type: yesno
#       hidden: yes
#       sql: ${collected_at_date} is not null and  ${latest_status_per_day} = 2 and ${no_of_attempts} = 2;;
#     }

#     # .......................SUPPORTING DIMENSIONS......................................

#     #........................PAREMETERS.................................................

#     parameter: fcast_del_perc_1st_attempt{
#       type: number
#       label: "% Delivered after 1st Attempt - Forecast"

#     }


#     parameter: fcast_del_perc_2nd_attempt{
#       type: number
#       label: "% Delivered after 2nd Attempt - Forecast"

#     }

#     parameter: fcast_del_perc_3rd_attempt{
#       type: number
#       label: "% Delivered after 3rd Attempt - Forecast"

#     }

#     # .......................MEASURE FOR NEWLY collected

#     measure: collected
#     {
#       label: "Orders collected"
#       group_label: "BL-Tracker"
#       description: "Orders Newly collected"
#       type: count_distinct
#       filters: [is_collected_date: "Yes"]
#       sql: ${tracking_no} ;;

#     }



#     # ....................... PREV DAY VALUES

#     #0 attempt

#     measure: ord_not_attempted_prev_day
#     {
#       label: "Orders Not Attempted Prev Day"
#       group_label: "BL-Tracker"
#       description: "Orders Not Attempted on previous days"
#       hidden: yes
#       type: max
#       sql: ${carriyo_unified_previous_day.prev_orders_not_attempted} ;;

#     }

#     #1 attempt
#     measure: ord_not_del_after_1st_attempt_prev {
#       hidden: yes
#       label: "Orders Not Delivered After First Attempt Prev"
#       group_label: "BL-Tracker"
#       description: "Orders Not Delivered After 1st Attempt Previous Day"
#       type: max
#       sql: ${carriyo_unified_previous_day.prev_orders_not_delivered_after_1st_attempt} ;;
#     }


#     measure: ord_attempted_once_prev {
#       hidden: yes
#       label: "Orders Attempted Once Prev"
#       group_label: "BL-Tracker"
#       description: "Orders Attempted Once Prev"
#       type: max
#       sql: ${carriyo_unified_previous_day.prev_orders_attempted_once} ;;
#     }


#     #1 attempt
#     measure: ord_not_del_after_2nd_attempt_prev {
#       hidden: yes
#       label: "Orders Not Delivered After 2nd Attempt Prev"
#       group_label: "BL-Tracker"
#       description: "Orders Not Delivered After 2nd Attempt Previous Day"
#       type: max
#       sql: ${carriyo_unified_previous_day.prev_orders_not_delivered_after_2nd_attempt} ;;
#     }


#     measure: ord_attempted_twice_prev {
#       hidden: yes
#       label: "Orders Attempted Twice Prev"
#       group_label: "BL-Tracker"
#       description: "Orders Attempted Twice Prev"
#       type: max
#       sql: ${carriyo_unified_previous_day.prev_orders_attempted_twice} ;;
#     }



#     # .......................MEASURE FOR ZERO ,1,2  ATTEMPTS

#     #0 attempt
#     measure: orders_not_attempted
#     {
#       label: "Not Attempted"
#       group_label: "BL-Tracker"
#       description: "Orders Not Attempted"
#       hidden: no
#       type: count_distinct
#       filters: [is_not_attempted: "Yes"]
#       sql: ${tracking_no} ;;

#     }

#     #1 attempt
#     measure: orders_attempted_once
#     {
#       label: "Attempted Once"
#       group_label: "BL-Tracker"
#       description: "Orders Attempted Once"
#       hidden: no
#       type: count_distinct
#       filters: [is_attempted_only_once: "Yes"]
#       sql: ${tracking_no} ;;

#     }

#     #2 attempt
#     measure: orders_attempted_twice
#     {
#       label: "Attempted Twice"
#       group_label: "BL-Tracker"
#       description: "Orders Attempted Twice"
#       hidden: no
#       type: count_distinct
#       filters: [is_attempted_only_twice: "Yes"]
#       sql: ${tracking_no} ;;

#     }


#     # .......................ORDERS TO BE ATTEMPTED...................

#     measure: orders_to_be_1st_attempted
#     {
#       label: "To Be Attempted 1st Time"
#       group_label: "BL-Tracker"
#       description: "Orders To Be Attempted 1st Time"
#       type: number
#       sql: ${collected} + ${ord_not_attempted_prev_day} ;;

#     }


#     measure: orders_to_be_2nd_attempted
#     {
#       label: "To Be Attempted 2nd Time"
#       group_label: "BL-Tracker"
#       description: "Orders To Be Attempted 2nd Time"
#       type: number
#       # sql: ${ord_not_del_after_1st_attempt_prev} + ${ord_attempted_once_prev} ;;
#       sql: ${ord_not_del_after_1st_attempt_prev} + ${orders_not_delivered_after_1st_attempt} ;;


#     }

#     measure: orders_to_be_3rd_attempted
#     {
#       label: "To Be Attempted 3rd Time"
#       group_label: "BL-Tracker"
#       description: "Orders To Be Attempted 3rd Time"
#       type: number
#       # sql: ${ord_not_del_after_2nd_attempt_prev} + ${ord_attempted_twice_prev} ;;
#       sql: ${ord_not_del_after_2nd_attempt_prev} + ${orders_not_delivered_after_2nd_attempt} ;;

#     }



#     # .......................FORECAST DELIVERY ...................
#     measure: fcast_delivery_after_1st_attempt{
#       hidden: no
#       label: "Forecasted Delivery After 1st Attempt"
#       group_label: "BL-Tracker"
#       description: "Forecasted Delivery After 1st Attempt"
#       type: number
#       value_format_name: decimal_0
#       sql: ${orders_to_be_1st_attempted} * {% parameter fcast_del_perc_1st_attempt %} /100 ;;
#     }


#     measure: fcast_delivery_after_2nd_attempt{
#       hidden: no
#       label: "Forecasted Delivery After 2nd Attempt"
#       group_label: "BL-Tracker"
#       description: "Forecasted Delivery After 2nd Attempt"
#       type: number
#       value_format_name: decimal_0
#       sql: ${orders_to_be_2nd_attempted} * {% parameter fcast_del_perc_2nd_attempt %} /100 ;;
#     }

#     measure: fcast_delivery_after_3rd_attempt{
#       hidden: no
#       label: "Forecasted Delivery After 3rd Attempt"
#       group_label: "BL-Tracker"
#       description: "Forecasted Delivery After 3rd Attempt"
#       type: number
#       value_format_name: decimal_0
#       sql: ${orders_to_be_3rd_attempted} * {% parameter fcast_del_perc_3rd_attempt %} /100 ;;
#     }


#     # .......................FORECAST BACKLOG ...................

#     measure: fcast_bklog_after_1st_attempt{
#       hidden: no
#       label: "Forecasted Backlog After 1st Attempt"
#       group_label: "BL-Tracker"
#       description: "Forecasted Backlog After 1st Attempt"
#       type: number
#       value_format_name: decimal_0
#       sql: ${orders_to_be_1st_attempted} - ${fcast_delivery_after_1st_attempt} ;;
#     }

#     measure: fcast_bklog_after_2nd_attempt{
#       hidden: no
#       label: "Forecasted Backlog After 2nd Attempt"
#       group_label: "BL-Tracker"
#       description: "Forecasted Backlog After 2nd Attempt"
#       type: number
#       value_format_name: decimal_0
#       sql: ${orders_to_be_2nd_attempted} - ${fcast_delivery_after_2nd_attempt} ;;
#     }


#     measure: fcast_bklog_after_3rd_attempt{
#       hidden: no
#       label: "Forecasted Backlog After 3rd Attempt"
#       group_label: "BL-Tracker"
#       description: "Forecasted Backlog After 3rd Attempt"
#       type: number
#       value_format_name: decimal_0
#       sql: ${orders_to_be_3rd_attempted} - ${fcast_delivery_after_3rd_attempt} ;;
#     }


#     # .......................ORDERS DELIVERED ...................
#     measure: orders_delivered_after_1st_attempt {
#       hidden: no
#       label: "Delivered After 1st Attempt"
#       group_label: "BL-Tracker"
#       description: "Orders Delivered After First Attempt"
#       type: count_distinct
#       filters: {
#         field: chb_status
#         value: "Delivered"
#       }
#       filters: [no_of_attempts_num: "=1"]

#       filters: {
#         field: latest_status_per_day_bool
#         value: "Yes"
#       }
#       sql: ${tracking_no} ;;
#     }

#     measure: orders_percent_delivered_after_1st_attempt {
#       hidden: no
#       label: "Delivered After 1st Attempt %"
#       group_label: "BL-Tracker"
#       description: "Percentage Orders Delivered After 1st Attempt"
#       type: number
#       value_format_name: percent_2
#       sql: safe_divide(${orders_delivered_after_1st_attempt},${orders_to_be_1st_attempted}) ;;
#     }



#     measure: orders_delivered_after_2nd_attempt {
#       hidden: no
#       label: "Delivered After 2nd Attempt"
#       group_label: "BL-Tracker"
#       description: "Orders Delivered After 2nd Attempt"
#       type: count_distinct
#       filters: {
#         field: chb_status
#         value: "Delivered"
#       }
#       filters: [no_of_attempts_num: "=2"]

#       filters: {
#         field: latest_status_per_day_bool
#         value: "Yes"
#       }
#       sql: ${tracking_no} ;;
#     }

#     measure: orders_percent_delivered_after_2nd_attempt {
#       hidden: no
#       label: "Delivered After 2nd Attempt %"
#       group_label: "BL-Tracker"
#       description: "Orders Delivered After 2nd Attempt %"
#       type: number
#       value_format_name: percent_2
#       sql: safe_divide(${orders_delivered_after_2nd_attempt},${orders_to_be_2nd_attempted}) ;;
#     }


#     measure: orders_delivered_after_3rd_attempt {
#       hidden: no
#       label: "Delivered After 3rd Attempt"
#       group_label: "BL-Tracker"
#       description: "Orders Delivered After 3rd Attempt"
#       type: count_distinct
#       filters: {
#         field: chb_status
#         value: "Delivered"
#       }
#       filters: [no_of_attempts_num: ">=3"]

#       filters: {
#         field: latest_status_per_day_bool
#         value: "Yes"
#       }
#       sql: ${tracking_no} ;;
#     }

#     measure: orders_percent_delivered_after_3rd_attempt {
#       hidden: no
#       label: "Delivered After 3rd Attempt %"
#       group_label: "BL-Tracker"
#       description: "Orders Delivered After 3rd Attempt %"
#       type: number
#       value_format_name: percent_2
#       sql: safe_divide(${orders_delivered_after_3rd_attempt},${orders_to_be_3rd_attempted}) ;;
#     }

#     # .......................ORDERS NOT DELIVERED ...................

#     measure: orders_not_delivered_after_1st_attempt {
#       hidden: no
#       label: "Not Delivered After 1st Attempt"
#       group_label: "BL-Tracker"
#       description: "Orders Not Delivered After 1st Attempt"
#       type: count_distinct
#       filters: {
#         field: chb_status
#         value: "Not Delivered"
#       }
#       filters: [no_of_attempts_num: "=1"]
#       filters: {
#         field: latest_status_per_day_bool
#         value: "Yes"
#       }
#       sql: ${tracking_no} ;;
#     }


#     measure: orders_not_delivered_after_2nd_attempt {
#       hidden: no
#       label: "Not Delivered After 2nd Attempt"
#       group_label: "BL-Tracker"
#       description: "Orders Not Delivered After 2nd Attempt"
#       type: count_distinct
#       filters: {
#         field: chb_status
#         value: "Not Delivered"
#       }
#       filters: [no_of_attempts_num: "=2"]
#       filters: {
#         field: latest_status_per_day_bool
#         value: "Yes"
#       }
#       sql: ${tracking_no} ;;
#     }

#     measure: orders_not_delivered_after_3rd_attempt {
#       hidden: no
#       label: "Not Delivered After 3rd Attempt"
#       group_label: "BL-Tracker"
#       description: "Orders Not Delivered After 3rd Attempt"
#       type: count_distinct
#       filters: {
#         field: chb_status
#         value: "Not Delivered"
#       }
#       filters: [no_of_attempts_num: ">=3"]
#       filters: {
#         field: latest_status_per_day_bool
#         value: "Yes"
#       }
#       sql: ${tracking_no} ;;
#     }

# # .......................ORDERS TO BE RETURNED ...................

#     measure: orders_to_be_returned_after_1st_attempt {
#       label: "To Be Returned After 1st Attempt"
#       group_label: "BL-Tracker"
#       description: "Orders To Be Returned After 1st Attempt"
#       type: count_distinct
#       filters: {
#         field: chb_status
#         value: "To be Returned"
#       }
#       filters: {
#         field: latest_status_per_day_bool
#         value: "Yes"
#       }
#       filters: [no_of_attempts_num: "=1"]
#       sql: ${tracking_no} ;;
#     }

#     measure: orders_to_be_returned_after_2nd_attempt {
#       label: "To Be Returned After 2nd Attempt"
#       group_label: "BL-Tracker"
#       description: "Orders To Be Returned After 2nd Attempt"
#       type: count_distinct
#       filters: {
#         field: chb_status
#         value: "To be Returned"
#       }
#       filters: {
#         field: latest_status_per_day_bool
#         value: "Yes"
#       }
#       filters: [no_of_attempts_num: "=2"]
#       sql: ${tracking_no} ;;
#     }

#     measure: orders_to_be_returned_after_3rd_attempt {
#       label: "To Be Returned After 3rd Attempt"
#       group_label: "BL-Tracker"
#       description: "Orders To Be Returned After 3rd Attempt"
#       type: count_distinct
#       filters: {
#         field: chb_status
#         value: "To be Returned"
#       }
#       filters: {
#         field: latest_status_per_day_bool
#         value: "Yes"
#       }
#       filters: [no_of_attempts_num: "=3"]
#       sql: ${tracking_no} ;;
#     }




#     #................SUMMARY BACKLOG................................

#     measure: total_to_be_delivered {
#       label: "Total To Be Delivered"
#       group_label: "BL-Tracker"
#       description: "Total To Be Delivered"
#       type: number
#       sql: ${orders_to_be_1st_attempted} +${orders_to_be_2nd_attempted}  +${orders_to_be_3rd_attempted}  ;;
#     }

#     measure: total_delivered {
#       label: "Total Delivered"
#       group_label: "BL-Tracker"
#       description: "Total Delivered"
#       type: number
#       sql: ${orders_delivered_after_1st_attempt} +${orders_delivered_after_2nd_attempt}  +${orders_delivered_after_3rd_attempt}  ;;
#     }

#     measure: success_delivery_ratio{
#       label: "Successful Delivery Ratio"
#       group_label: "BL-Tracker"
#       description: "Success Delivery Ratio"
#       type: number
#       sql: safe_divide(${total_delivered},${total_to_be_delivered})  ;;
#     }

#     measure: backlog_delivery{
#       label: "Total Backlog Delivery"
#       group_label: "BL-Tracker"
#       description: "Backlog Delivery"
#       type: number
#       # sql: ${orders_not_attempted} + ${orders_attempted_once} +${orders_attempted_twice}+ ${orders_not_delivered_after_1st_attempt} +${orders_not_delivered_after_2nd_attempt} +${orders_not_delivered_after_3rd_attempt}  ;;
#       sql:  ${orders_not_delivered_after_1st_attempt} +${orders_not_delivered_after_2nd_attempt} +${orders_not_delivered_after_3rd_attempt}  ;;

#     }

#     measure: backlog_rto{
#       label: "Total Backlog RTO"
#       group_label: "BL-Tracker"
#       description: "Backlog RTO"
#       type: number
#       sql: ${orders_to_be_returned_after_1st_attempt} + ${orders_to_be_returned_after_2nd_attempt} + ${orders_to_be_returned_after_3rd_attempt}  ;;
#     }

# # .......................BACKLOG TRACKER FIELDS STARTS....................................................

# # ...........................................BACKLOG SUMMARY STARTS 3:04 PM 10/31/2020 .................................................

#   measure: max_collected_date {
#     type: date
#     hidden: no
#     sql: max(${collected_at_date});;
#   }

#     measure: max_order_date {
#       type: date
#       hidden: no
#     sql: max(${order_date_date});;
#   }

#     measure: max_update_date {
#       type: date
#       hidden: no
#       sql: max(${update_date_date});;
#     }

#     measure: max_first_attempt {
#       type: date
#       hidden: no
#       sql: max(${first_attempt_date});;
#     }


#     measure: max_second_attempt {
#       type: date
#       hidden: no
#       sql: max(${second_attempt_date});;
#     }

#     measure: max_third_attempt {
#       type: date
#       hidden: no
#       sql: max(${third_attempt_date});;
#     }

#     dimension: summ_latest_status {
#       type: string
#       hidden: no
#       sql: case when ${latest_status} =1 then ${chb_status} else null end  ;;
#     }

# ...........................................BACKLOG SUMMARY STARTS 3:04 PM 10/31/2020 .................................................



#  measure: fdds {
#    label: "First Day Delivery Success"
#    description: "Count of orders which were delivered on the very fist attempt "
#    type: count_distinct
#    sql:  case when ${is_fdds}='yes' then ${shipment_id} else 0 end;;
#  }
#  measure: fdds_prepaid {
#    label: "First Day Delivery Success-Prepaid"
#    description: "Count of Prepaid orders which were delivered on the very fist attempt "
#    type: number
#    sql:  case when ${is_fdds} and (${payment_mode}='PRE_PAID') then (${count_distinct_shipment} else 0 end;;
#  }
#  measure: fdds_cod {
#    label: "First Day Delivery Success-Cash On Delivery"
#    description: "Count of Cash on Delivery orders which were delivered on the very fist attempt "
#    type: number
#    sql:  case when ${is_fdds} and (${payment_mode}='CASH_ON_DELIVERY') then (${count_distinct_shipment} else 0 end;;
#  }



#...............................................BACKLOG NEW VERSION.............................................................

# set: to_be_delivered_drill {fields:[tracking_no , pickup_country,collected_at_date,first_attempt_date,ca_SLA_first_attempt_by_date,second_attempt_date,ca_SLA_second_attempt_by_date,third_attempt_date,ca_SLA_third_attempt_by_date,to_be_next_attempted_date,chb_status]}

# dimension: service_type {
#   description: "Same Day or Standard"
#   type: string
#   sql: CASE
#         WHEN ${SLA_REQ} = 'same_day_SLA' OR  ${SLA_REQ} = '4_HOUR' THEN 'Same Day'
#       else 'Standard'
#     END ;;
# }
# # dimension_group: calendar_date {
# #   type: time
# #   convert_tz: no
# #   sql: ${carriyo_date_master.date_raw} ;;

# #   }



# dimension_group: collected_at_latest {
#   hidden: yes
#   type: time
#   timeframes: [
#     raw,
#     date,
#     week,
#     month,
#     quarter,
#     year
#   ]
#   convert_tz: no
#   datatype: date
#   sql: case when ${collected_at_date} < date_sub(current_date(),interval 4 day) then (date_add(current_date(),interval -4 day)) else ${collected_at_date} end ;;

#   }

# dimension: is_sla_breached {

#   hidden: yes
#   type: yesno
#   sql: ${ocd_SLA_first_attempt_by_date} < current_date() ;;
# }

# # dimension: is_sla_breached_asof_cal_date {
# #       hidden: yes
# #       type: yesno
# #       sql: ${ca_SLA_first_attempt_by_date} < ${calendar_date_date} ;;
# #     }

# dimension: is_not_delivered {

#   hidden: yes
#   type: yesno
#   sql: ${chb_status} !="Delivered" ;;
# }

# dimension: is_not_returned {
#   hidden: yes
#       type: yesno
#       sql: ${chb_status} !="Returned" ;;
# }

#     dimension: is_not_cancelled {
#       hidden: yes
#       type: yesno
#       sql: ${chb_status} !="Cancelled Before Collection" ;;
#     }

# dimension: is_collected {
#   hidden: yes
#   type: yesno
#   sql: ${collected_at_date} is not null ;;
# }

#     dimension: is_collected_in_past{
#       hidden: yes
#       type: yesno
#       sql: ${collected_at_date} < current_date() ;;
#     }

# # dimension: is_not_ret_or_del_asof_cal_date {
# #   type: yesno
# #   sql: (${delivered_at_date} > ${calendar_date_date}  or  ${returned_at_date} >  ${calendar_date_date}) OR
# #   (${delivered_at_date} is null and ${returned_at_date} is null ) ;;
# # }

# dimension_group: to_be_next_attempted {
#   hidden: yes
#   type: time
#   convert_tz: no
#   datatype: date
#   sql: greatest( case when ${first_attempt_date} is null then ${ocd_SLA_first_attempt_by_date}
#             when ${second_attempt_date} is null then ${ocd_SLA_second_attempt_by_date}
#             when ${third_attempt_date} is null then ${ocd_SLA_third_attempt_by_date}
#             else current_date()
#             end , current_date() ) ;;

# }



# # dimension:  {}


# measure: backlog {
#   label: "Backlog"
#   description: "Dynamic backlog - changes as and when the orders are delivered.Not Returned & Not Delivered & Not Cancelled"
#   type: count_distinct
#   drill_fields: [to_be_delivered_drill*]
#   filters: [is_sla_breached: "Yes",is_not_delivered: "Yes",is_not_returned: "Yes",is_not_cancelled: "Yes",is_collected: "Yes",latest_status_boolean: "Yes"]
#   sql:  ${tracking_no}  ;;
# }

# # measure: opening_backlog_snapshot {
# #   label: "Opening Backlog"
# #   description: "Snapshot of Backlog"
# #   type: count_distinct
# #   drill_fields: [to_be_delivered_drill*]
# #   filters: [is_sla_breached_asof_cal_date: "Yes",is_not_ret_or_del_asof_cal_date: "Yes",is_collected: "Yes",latest_status_boolean: "Yes"]
# #   sql:  ${tracking_no}   ;;
# #     }

# measure: to_be_delivered {
#   label: "To Be Delivered - Actual"
#   description: "To Be Delivered - From Actual Orders Placed"
#   type: count_distinct
#   drill_fields: [to_be_delivered_drill*]
#   # filters: [is_sla_breached: "No",is_not_delivered: "Yes",is_not_returned: "Yes",is_not_cancelled: "Yes",is_collected: "Yes",latest_status_boolean: "Yes"]
#   filters: [is_not_delivered: "Yes",is_not_returned: "Yes",is_not_cancelled: "Yes",is_collected: "Yes",latest_status_boolean: "Yes",is_collected_in_past: "Yes"]
#   sql: ${tracking_no} ;;
# }



# measure: to_be_delivered_1st_attmpt {
#   label: "To Be Delivered - Actual"
#   description: "To Be Delivered - From Actual Orders Placed"
#   type: count_distinct
#   drill_fields: [to_be_delivered_drill*]
#   # filters: [is_sla_breached: "No",is_not_delivered: "Yes",is_not_returned: "Yes",is_not_cancelled: "Yes",is_collected: "Yes",latest_status_boolean: "Yes"]
#   filters: [is_not_delivered: "Yes",is_not_returned: "Yes",is_not_cancelled: "Yes",is_collected: "Yes",latest_status_boolean: "Yes",is_collected_in_past: "Yes",no_of_attempts: "=0"]
#   sql: ${tracking_no} ;;
# }

# measure: to_be_delivered_2nd_attmpt {
#       label: "To Be Delivered - Actual"
#       description: "To Be Delivered - From Actual Orders Placed"
#       type: count_distinct
#       drill_fields: [to_be_delivered_drill*]
#       # filters: [is_sla_breached: "No",is_not_delivered: "Yes",is_not_returned: "Yes",is_not_cancelled: "Yes",is_collected: "Yes",latest_status_boolean: "Yes"]
#       filters: [is_not_delivered: "Yes",is_not_returned: "Yes",is_not_cancelled: "Yes",is_collected: "Yes",latest_status_boolean: "Yes",is_collected_in_past: "Yes",no_of_attempts: "=1"]
#       sql: ${tracking_no} ;;
#     }

#     measure: to_be_delivered_3rd_attmpt {
#       label: "To Be Delivered - Actual"
#       description: "To Be Delivered - From Actual Orders Placed"
#       type: count_distinct
#       drill_fields: [to_be_delivered_drill*]
#       # filters: [is_sla_breached: "No",is_not_delivered: "Yes",is_not_returned: "Yes",is_not_cancelled: "Yes",is_collected: "Yes",latest_status_boolean: "Yes"]
#       filters: [is_not_delivered: "Yes",is_not_returned: "Yes",is_not_cancelled: "Yes",is_collected: "Yes",latest_status_boolean: "Yes",is_collected_in_past: "Yes",no_of_attempts: "=2"]
#       sql: ${tracking_no} ;;
#     }





# measure: to_be_delivered_excl_backlog {
#       label: "To Be Delivered - Excluding Backlog"
#       description: "To Be Delivered - Excluding Backlog"
#       type: count_distinct
#       drill_fields: [to_be_delivered_drill*]
#       # filters: [is_sla_breached: "No",is_not_delivered: "Yes",is_not_returned: "Yes",is_not_cancelled: "Yes",is_collected: "Yes",latest_status_boolean: "Yes"]
#       filters: [is_sla_breached: "No",is_not_delivered: "Yes",is_not_returned: "Yes",is_not_cancelled: "Yes",is_collected: "Yes",latest_status_boolean: "Yes",is_collected_in_past: "Yes"]

#       sql: ${tracking_no} ;;
#     }



  }
