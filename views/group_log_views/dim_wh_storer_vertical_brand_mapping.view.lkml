view: dim_wh_storer_vertical_brand_mapping {
  #sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_wh_storer_vertical_brand_mapping`
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dim_wh_storer_vertical_brand_mapping`
    ;;

  dimension: pk {
    primary_key: yes
    type: string
    hidden: yes
    sql: ${company} ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: brand_access_filter {
    type: string
    sql: case when ${brand} is null then 'x' else ${brand} end  ;;
  }

  dimension: bu_type {
    hidden: yes
    type: string
    sql: ${TABLE}.bu_type ;;
  }

  dimension: company {
    hidden: yes
    type: string
    sql: ${TABLE}.company ;;
  }

  dimension: storer_key {
    hidden: no
    type: string
    sql: ${TABLE}.storer_key ;;
  }

  dimension: storer_key_company {
    hidden: yes
    type: string
    sql: ${TABLE}.storer_key_company ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.vertical ;;
  }

  dimension: warehouse_id {
    type: string
    sql: ${TABLE}.warehouse_id ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: []
  }
}
