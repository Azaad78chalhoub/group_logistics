
  view: dm_logistics_unified_percentile {
    derived_table: {
      explore_source: dm_logistics_unified_by_shipment_new {
        column: warehouse_id {}
        column: wm9_order_ID {}
        column: returned_date {}
        column: time_to_wm9_from_click {}
        derived_column: click2wm9_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_wm9_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: time_to_pick_from_click {}
        derived_column: click2pick_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_pick_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: time_to_pack_from_click {}
        derived_column: click2pack_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_pack_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: time_to_manifest_from_click {}
        derived_column: click2manifest_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_manifest_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: time_to_shipped_from_click {}
        derived_column: click2ship_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_shipped_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: time_to_ofd_from_click {}
        derived_column: click2ofd_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_ofd_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: time_to_FA_from_click {}
        derived_column: click2fa_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_FA_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: time_to_SA_from_click {}
        derived_column: click2sa_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_SA_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: time_to_TA_from_click {}
        derived_column: click2ta_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_TA_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: time_to_Delivered_from_click {}
        derived_column: click2deliver_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_Delivered_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: time_to_Returned_from_click {}
        derived_column: click2return_percentile {
                  sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id,time_to_Returned_from_click is not null ORDER BY time_to_Returned_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        bind_all_filters: yes
      }
    }
    dimension: warehouse_id {
      label: "Logistics by Shipment new (BETA) Warehouse ID"
    }
    dimension: wm9_order_ID {
      label: "Logistics by Shipment new (BETA) FC Order ID"
    }
    dimension: time_to_wm9_from_click {
      label: "Logistics by Shipment new (BETA) Click to FC"
      description: "Average time taken from order creation by the customer to order creation in FC"
      value_format: "H:MM"
      type: number
    }
    dimension: click2wm9_percentile {}
    dimension: click2pick_percentile {}
    dimension: click2pack_percentile {}
    dimension: click2manifest_percentile {}
    dimension: click2ship_percentile {}
    dimension: click2ofd_percentile {}
    dimension: click2fa_percentile {}
    dimension: click2sa_percentile {}
    dimension: click2ta_percentile {}
    dimension: click2deliver_percentile {}
    dimension: click2return_percentile {}
    }
