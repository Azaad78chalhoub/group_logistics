view: dm_logistics_unified_percentile_processing {


    derived_table: {
      explore_source: dm_logistics_unified_by_shipment_new {
        column: warehouse_id {}
        column: wm9_order_ID {}
        column: p_time_to_wm9_from_click {}
        derived_column: p_click2wm9_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id,p_time_to_wm9_from_click is not null ORDER BY p_time_to_wm9_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: p_time_to_pick_from_click {}
        derived_column: p_click2pick_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id,p_time_to_pick_from_click is not null ORDER BY p_time_to_pick_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: p_time_to_pack_from_click {}
        derived_column: p_click2pack_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id,p_time_to_pack_from_click is not null ORDER BY p_time_to_pack_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: p_time_to_manifest_from_click {}
        derived_column: p_click2manifest_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id,p_time_to_manifest_from_click is not null ORDER BY p_time_to_manifest_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: p_time_to_shipped_from_click {}
        derived_column: p_click2ship_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id,p_time_to_shipped_from_click is not null ORDER BY p_time_to_shipped_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: p_time_to_ofd_from_click {}
        derived_column: p_click2ofd_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id,p_time_to_ofd_from_click is not null ORDER BY p_time_to_ofd_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: p_time_to_FA_from_click {}
        derived_column: p_click2fa_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id,p_time_to_FA_from_click is not null ORDER BY p_time_to_FA_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: p_time_to_SA_from_click {}
        derived_column: p_click2sa_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id, p_time_to_SA_from_click is not null ORDER BY p_time_to_SA_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: p_time_to_TA_from_click {}
        derived_column: p_click2ta_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id,p_time_to_TA_from_click is not null ORDER BY p_time_to_TA_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: p_time_to_Delivered_from_click {}
        derived_column: p_click2deliver_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id,p_time_to_Delivered_from_click is not null ORDER BY p_time_to_Delivered_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        column: p_time_to_Returned_from_click {}
        derived_column: p_click2return_percentile {
          sql:
                  case
                    when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id,p_time_to_Returned_from_click is not null ORDER BY p_time_to_Returned_from_click)*100) <=95
                    then "Yes" else "No"
                  end;;
        }
        bind_all_filters: yes
      }
    }
    dimension: warehouse_id {
      label: "Logistics by Shipment new (BETA) Warehouse ID"
    }
    dimension: wm9_order_ID {
      label: "Logistics by Shipment new (BETA) FC Order ID"
    }
    dimension: time_to_wm9_from_click {
      label: "Logistics by Shipment new (BETA) Click to FC"
      description: "Average time taken from order creation by the customer to order creation in FC"
      value_format: "H:MM"
      type: number
    }
    dimension: p_click2wm9_percentile {}
    dimension: p_click2pick_percentile {}
    dimension: p_click2pack_percentile {}
    dimension: p_click2manifest_percentile {}
    dimension: p_click2ship_percentile {}
    dimension: p_click2ofd_percentile {}
    dimension: p_click2fa_percentile {}
    dimension: p_click2sa_percentile {}
    dimension: p_click2ta_percentile {}
    dimension: p_click2deliver_percentile {}
    dimension: p_click2return_percentile {}






}

# view: dm_logistics_unified_percentile_processing {
#   # Or, you could make this view a derived table, like this:
#   derived_table: {
#     sql: SELECT
#         user_id as user_id
#         , COUNT(*) as lifetime_orders
#         , MAX(orders.created_at) as most_recent_purchase_at
#       FROM orders
#       GROUP BY user_id
#       ;;
#   }
#
#   # Define your dimensions and measures here, like this:
#   dimension: user_id {
#     description: "Unique ID for each user that has ordered"
#     type: number
#     sql: ${TABLE}.user_id ;;
#   }
#
#   dimension: lifetime_orders {
#     description: "The total number of orders for each user"
#     type: number
#     sql: ${TABLE}.lifetime_orders ;;
#   }
#
#   dimension_group: most_recent_purchase {
#     description: "The date when each user last ordered"
#     type: time
#     timeframes: [date, week, month, year]
#     sql: ${TABLE}.most_recent_purchase_at ;;
#   }
#
#   measure: total_lifetime_orders {
#     description: "Use this for counting lifetime orders across many users"
#     type: sum
#     sql: ${lifetime_orders} ;;
#   }
# }
