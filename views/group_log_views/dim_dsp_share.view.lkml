view: dim_dsp_share {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dim_dsp_share`
    ;;

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: coverage {
    type: number
    sql: ${TABLE}.coverage ;;
  }

  dimension: provider {
    type: string
    sql: upper(${TABLE}.provider) ;;
  }

  dimension: service_type {
    type: string
    sql: ${TABLE}.service_type ;;
  }

  dimension: pk {
    hidden: yes
    primary_key: yes
    sql: concat(${country},"-",${provider},"-",${service_type}) ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }


  measure: forecasted_orders {
    label: "Orders Forecasted"
    type: sum
    sql:  ${dim_order_summary_forecast.forecasted_orders} * ${coverage} /100 ;;
  }


}
