view: dim_logistics_bu_mapping {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dim_logistics_bu_mapping`
    ;;

  dimension: brand_name {
    type: string
    sql: ${TABLE}.brand_name ;;
  }

  dimension: bu_code {
    label: "Bussiness Unit Code"
    type: string
    sql: ${TABLE}.bu_code ;;
  }

  dimension: bu_desc_icsb {
    label: "Bussiness Unit Desc"
    type: string
    sql: ${TABLE}.bu_desc_icsb ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: facility_name {
    type: string
    sql: ${TABLE}.facility_name ;;
  }

  dimension: legal_entity {
    type: string
    sql: ${TABLE}.legal_entity ;;
  }

  dimension: mapped_bu_brand {
    type: string
    sql: ${TABLE}.mapped_bu_brand ;;
  }

  dimension: mapped_criteria {
    type: string
    sql: ${TABLE}.mapped_criteria ;;
  }

  dimension: storer_key {
    type: string
    sql: ${TABLE}.storer_key ;;
  }

  dimension: vertical {
    label: "Vertical"
    type: string
    sql: ${TABLE}.vertical ;;
  }

  dimension: managed_by {
    label: "Managed By"
    type: string
    sql: ${TABLE}.managed_by ;;
  }

  measure: count {
    type: count
    drill_fields: [brand_name, facility_name]
  }
}
