view: dim_attempt_success_rates {
  derived_table: {
    sql:
 select 'SA' country,'Same Day' service_type, 80 first_attempt_success_rate , 50 second_attempt_success_rate, 20 third_attempt_success_rate
 union all
 select 'SA' ,'Standard', 75 , 40, 10
 union all

 select 'KW' ,'Same Day' , 80 , 50, 20
 union all
 select 'KW' ,'Standard', 75 , 40, 10
 union all

 select 'AE' ,'Same Day' , 80 , 50, 20
 union all
 select 'AE' ,'Standard', 75 , 40, 10
 union all

 select 'QA' ,'Same Day' , 80 , 50, 20
 union all
 select 'QA' ,'Standard', 75 , 40, 10
 union all

 select 'EG' ,'Same Day' , 80 , 50, 20
 union all
 select 'EG' ,'Standard', 75 , 40, 10
 union all

 select 'BH' ,'Same Day' , 80 , 50, 20
 union all
 select 'BH' ,'Standard', 75 , 40, 10
 union all


 select 'AE' ,'Same Day', 92 , 70, 10
 union all
 select 'AE' ,'Standard', 85 , 50, 10
  ;;


  }

  dimension: country {
  }

  dimension: service_type {
  }

  dimension: first_attempt_success_rate {
  }

  dimension: second_attempt_success_rate {
  }

  dimension: third_attempt_success_rate {
  }

  dimension: first_success_rate_on_total{
    sql: ${first_attempt_success_rate} /100  ;;
  }

  dimension: second_success_rate_on_total{
    sql: (1- ${first_success_rate_on_total} ) * ${second_attempt_success_rate} ;;
  }


  dimension: third_success_rate_on_total{
    sql: (1- ${first_success_rate_on_total} - ${second_success_rate_on_total} ) * ${third_attempt_success_rate} ;;
  }

  dimension: return_rate_on_total {
    sql: (1-${first_success_rate_on_total}- ${second_success_rate_on_total}- ${third_success_rate_on_total} ) ;;
  }

}
