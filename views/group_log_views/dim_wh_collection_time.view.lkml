view: dim_wh_collection_time {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dim_wh_collection_time`
    ;;


  dimension: country {
    type: string
    sql: ${TABLE}.country ;;
  }

  dimension: wh_id {
    type: string
    sql: ${TABLE}.WH_ID ;;
  }


  dimension: priority {
    type: number
    sql: ${TABLE}.priority ;;
  }

  dimension: dsp {
    type: string
    sql: ${TABLE}.dsp ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: weekday {
    type: string
    sql: ${TABLE}.WEEKDAY ;;
  }

  dimension: cutoff_time {
    type: string
    sql: ${TABLE}.cut_off ;;
  }

  dimension: collection_time {
    type: string
    sql: ${TABLE}.critical_collection_time ;;
  }

  dimension: shipout_time {
    type: string
    sql: ${TABLE}.ship_out_time ;;
  }

  # changed on 10-mar-2021 by sebin
  # dimension: dday {
  #   type: number
  #   sql: ${TABLE}.d_day ;;
  # }

  dimension: dday_before {
    type: number
    sql: ${TABLE}.d_day_before ;;
  }

  dimension: dday_after {
    type: number
    sql: ${TABLE}.d_day_after ;;
  }

  dimension: rule_start_date {
    type:  date
    sql:  ${TABLE}.rule_start_date ;;
  }

  dimension: rule_end_date {
    type:  date
    sql:  ${TABLE}.rule_end_date ;;
  }

  measure: count {
    type: count
  }
}
