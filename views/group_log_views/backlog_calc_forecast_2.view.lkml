view: backlog_calc_forecast_2 {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.backlog_calc_forecast_2`
    ;;

  dimension: pk {
    primary_key: yes
    sql: concat( ${order_creation_date} , ${date_date})  ;;
  }
  dimension: a1_sr {
    type: number
    sql: ${TABLE}.a1_sr ;;
  }

  dimension: a2_sr {
    type: number
    sql: ${TABLE}.a2_sr ;;
  }

  dimension: a3_sr {
    type: number
    sql: ${TABLE}.a3_sr ;;
  }

  dimension: act_orders_a1 {
    type: number
    sql: (${TABLE}.act_orders_a1) ;;
  }

  dimension: act_orders_a2 {
    type: number
    sql: (${TABLE}.act_orders_a2) ;;
  }

  dimension: act_orders_a3 {
    type: number
    sql: (${TABLE}.act_orders_a3) ;;
  }

  dimension: act_orders_a4_tbr {
    type: number
    sql: (${TABLE}.act_orders_a4_tbr) ;;
  }

  dimension: actual_orders {
    type: number
    sql: (${TABLE}.actual_orders) ;;
  }

  dimension: carry_over_a1 {
    type: number
    sql: (${TABLE}.carry_over_a1) ;;
  }

  dimension: carry_over_a2 {
    type: number
    sql: (${TABLE}.carry_over_a2) ;;
  }

  dimension: carry_over_a2_extra {
    type: number
    sql: (${TABLE}.carry_over_a2_extra) ;;
  }

  dimension_group: date {
    label:"Delivery"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: flag {
    type: string
    sql: ${TABLE}.flag ;;
  }

  dimension: merchant {
    type: string
    sql: ${TABLE}.merchant ;;
  }

  dimension: carrier {
    type: string
    sql: ${TABLE}.carrier ;;
  }

  dimension: bu_brand_for_access_filter {
    hidden: yes
    sql:  case when ${brand_vertical_mapping_log.bu_brand} is null then 'x' else ${brand_vertical_mapping_log.bu_brand} end ;;

  }

  dimension: vertical_for_access_filter {
    hidden: yes
    sql:  case when ${brand_vertical_mapping_log.vertical} is null then 'x' else ${brand_vertical_mapping_log.vertical} end ;;

  }


  dimension_group: order_creation {
    label:"Order"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.order_creation_date ;;
  }

  dimension: orders_a1 {
    type: number
    sql: (${TABLE}.orders_a1) ;;
  }

  dimension: orders_a2 {
    type: number
    sql: (${TABLE}.orders_a2) ;;
  }

  dimension: orders_a3 {
    type: number
    sql: (${TABLE}.orders_a3) ;;
  }

  dimension: orders_a4 {
    type: number
    sql: (${TABLE}.orders_a4) ;;
  }

  dimension: orders_to_be_delivered {
    type: number
    sql: (${TABLE}.orders_to_be_delivered ) ;;
  }

  dimension: orders_with_a1_sr {
    type: number
    sql: (${TABLE}.orders_with_a1_sr) ;;
  }

  dimension: orders_with_a2_sr {
    type: number
    sql: (${TABLE}.orders_with_a2_sr) ;;
  }

  dimension: orders_with_a3_sr {
    type: number
    sql: (${TABLE}.orders_with_a3_sr) ;;
  }

  dimension: pickup_country {
    type: string
    sql: ${TABLE}.pickup_country ;;
  }

  dimension: service_type {
    type: string
    sql: ${TABLE}.service_type ;;
  }

  dimension: to_be_delivered_actuals_with_sr {
    type: number
    sql: ${TABLE}.to_be_delivered_actuals_with_sr ;;
  }

dimension: backlog_total {
  type: number
  sql: ${TABLE}.backlog_total ;;
}

  measure: actual_tbd {
    type: sum
    value_format_name: decimal_0
    sql: (${actual_orders})   ;;
  }


  measure: actual_tbd_after_sr {
    type: sum
    value_format_name: decimal_0

    sql: (${to_be_delivered_actuals_with_sr})   ;;
      }

  measure: forecast_tbd {
    type: sum
    value_format_name: decimal_0
    sql: (${orders_to_be_delivered})   ;;
  }

  measure: forecast_tbd_after_sr {
    type: sum
    value_format_name: decimal_0
    sql: coalesce(${orders_with_a1_sr},0) + coalesce(${orders_with_a2_sr},0) + coalesce(${orders_with_a3_sr},0)  ;;
  }

  measure: tbd {
    type: number
    value_format_name: decimal_0
    description: "No.of orders to be delivered in ideal scenario"
    label: "TBD"
    sql: ${actual_tbd} + ${forecast_tbd} ;;
  }

  measure: tbd_after_sr {
    type: number
    value_format_name: decimal_0
    description: "No.of orders to be delivered subject to success rates.These values can also be carried forward from previous day's TBD"
    label: "TBD After Success"
    sql: ${actual_tbd_after_sr} + ${forecast_tbd_after_sr} ;;
  }

  measure: backlog {
    type: sum
    hidden: yes
    sql: ${backlog_total} ;;
  }

  measure: success_rate {
    type: number
    value_format_name: percent_0
    hidden: yes
    sql: safe_divide ((${tbd_after_sr}),(${tbd})) ;;
  }

  measure: count_tbd_a1 {
    type: sum
    value_format_name: decimal_0
    sql: (coalesce(${act_orders_a1},0)) + (coalesce(${orders_with_a1_sr},0))  ;;
  }

  measure: count_tbd_a2 {
    type: sum
    value_format_name: decimal_0
    sql: (coalesce(${act_orders_a2},0))+ (coalesce(${carry_over_a1},0)) + (coalesce(${orders_with_a2_sr},0))  ;;
  }


  measure: count_tbd_a3 {
    type: sum
    value_format_name: decimal_0
    sql: (coalesce(${act_orders_a3},0))+ (coalesce(${carry_over_a2},0)) + (coalesce(${carry_over_a2_extra},0))+ (coalesce(${orders_with_a2_sr},0))  ;;
  }



  measure: count {
    type: count
    drill_fields: []
  }
}
