view: dm_fulfillment_wm9_orders_city_sla_uat_test {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dm_fulfillment_wm9_orders_city_sla_uat`
    ;;

  dimension: _4_hour {
    type: number
    sql: ${TABLE}._4_hour ;;
  }

  dimension: codelkup_priority_description {
    type: string
    sql: ${TABLE}.codelkup_priority_description ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: d_0 {
    type: number
    sql: ${TABLE}.d_0 ;;
  }

  dimension: d_1 {
    type: number
    sql: ${TABLE}.d_1 ;;
  }

  dimension: d_2 {
    type: number
    sql: ${TABLE}.d_2 ;;
  }

  dimension: d_3 {
    type: number
    sql: ${TABLE}.d_3 ;;
  }

  dimension: d_4 {
    type: number
    sql: ${TABLE}.d_4 ;;
  }

  dimension: sla {
    type: number
    sql:   CASE WHEN d_1 = 1 THEN 0
            WHEN d_1 = 1 THEN 1
            WHEN d_1 = 1 THEN 1
            WHEN d_2 = 1 THEN 2
            WHEN d_3 = 1 THEN 3
            WHEN d_4 = 1 THEN 4
            ELSE null
          END ;;
  }

  dimension: facility_name {
    type: string
    sql: ${TABLE}.facility_name ;;
  }

  dimension: order_c_city {
    type: string
    sql: ${TABLE}.order_c_city ;;
  }

  dimension_group: order_creation {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_creation_date ;;
  }

  dimension: order_extern_orderkey {
    type: string
    sql: ${TABLE}.order_extern_orderkey ;;
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: possible_misspell {
    type: string
    sql: ${TABLE}.possible_misspell ;;
  }

  dimension: sla_city {
    type: string
    sql: ${TABLE}.sla_city ;;
  }

  dimension: sla_city_accuracy {
    type: string
    sql: ${TABLE}.sla_city_accuracy ;;
  }

  dimension: sla_rule {
    type: string
    sql: ${TABLE}.sla_rule ;;
  }

  dimension: warehouse_id {
    type: string
    sql: ${TABLE}.warehouse_id ;;
  }

  measure: count {
    type: count
    drill_fields: [facility_name]
  }
}
