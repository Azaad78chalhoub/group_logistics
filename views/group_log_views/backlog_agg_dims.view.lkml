# If necessary, uncomment the line below to include explore_source.

# include: "group_logistics.model.lkml"

view: backlog_agg_dims {
  derived_table: {
    explore_source: backlog_calc_forecast_2 {
      column: order_creation_date {}
      column: flag {}
      column: tbd {}
      column: tbd_after_sr {}
      column: backlog {}
      column: success_rate {}
      bind_all_filters: yes
    }
    }

  dimension: order_creation_date {
    label: "Backlog Calc Forecast 2 Order Date"
    type: date
  }

  dimension: pk {
    primary_key: yes
    sql: concat(${order_creation_date},${flag}) ;;
  }

  dimension: flag {
    type: string
  }
  dimension: tbd {
    label: "Total TBD"
    value_format: "#,##0"
    type: number
    description: "Total no.of orders to be delivered in ideal scenario"
    hidden: no
  }
  dimension: tbd_after_sr {
    value_format: "#,##0"
    type: number
    description: "Total no.of orders to be delivered subject to success rates"
    label: "Total TBD After Success"


  }


  dimension: backlog {
    type: number
    description: "No.of Orders supposed to be delivered by a past date and has not been delivered yet"
    label: "Backlog"

  }
  dimension: success_rate {
    value_format: "#,##0%"
    type: number
    description: "TBD After Success / TBD"
    label: "Success Rate"
  }



}
