view: carriyo_unified_latest_status_1 {
  sql_table_name: `chb-prod-stage-oracle.prod_supply_chain.carriyo_unified_latest_status_1`
    ;;

  dimension: _4_hour_sla {
    type: number
    sql: ${TABLE}._4_hour_SLA ;;
  }

  dimension: chb_status {
    type: string
    sql: ${TABLE}.CHB_STATUS ;;
  }

  dimension: clean_sla_city {
    type: string
    sql: ${TABLE}.clean_sla_city ;;
  }

  dimension: d_1_sla {
    type: number
    sql: ${TABLE}.D_1_SLA ;;
  }

  dimension: d_2_sla {
    type: number
    sql: ${TABLE}.D_2_SLA ;;
  }

  dimension: d_3_sla {
    type: number
    sql: ${TABLE}.D_3_SLA ;;
  }

  dimension: d_4_sla {
    type: number
    sql: ${TABLE}.D_4_SLA ;;
  }

  dimension: first_attempt_within_sla_circadian {
    type: string
    sql: ${TABLE}.first_attempt_within_SLA_circadian ;;
  }

  dimension: first_attempt_within_sla_int {
    type: string
    sql: ${TABLE}.first_attempt_within_SLA_int ;;
  }
  dimension: first_attempt_within_sla {
    type: string
    sql: ${TABLE}.first_attempt_within_SLA ;;
  }
  dimension_group: first_return {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.first_return ;;
  }

  dimension: latest_status {
    type: number
    sql: ${TABLE}.latest_status ;;
  }

  dimension: latest_status_per_day {
    type: number
    sql: ${TABLE}.latest_status_per_day ;;
  }

  dimension: max_attempts {
    type: number
    sql: ${TABLE}.max_attempts ;;
  }

  dimension: no_of_attempts_2 {
    type: number
    sql: ${TABLE}.no_of_attempts_2 ;;
  }

  dimension: second_attempt_within_sla_circadian {
    type: string
    sql: ${TABLE}.second_attempt_within_SLA_circadian ;;
  }

  dimension: second_attempt_within_sla_int {
    type: string
    sql: ${TABLE}.second_attempt_within_SLA_int ;;
  }

  dimension: sla_city {
    type: string
    sql: ${TABLE}.sla_city ;;
  }

  dimension: sla_city_accuracy {
    type: string
    sql: ${TABLE}.sla_city_accuracy ;;
  }

  dimension_group: sla_first_attempt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.SLA_first_attempt ;;
  }

  dimension: sla_req {
    type: string
    sql: ${TABLE}.SLA_REQ ;;
  }

  dimension: sla_rule {
    type: string
    sql: ${TABLE}.sla_rule ;;
  }

  dimension_group: sla_second_attempt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.SLA_second_attempt ;;
  }

  dimension_group: sla_third_attempt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.SLA_third_attempt ;;
  }

  dimension: third_attempt_within_sla_circadian {
    type: string
    sql: ${TABLE}.third_attempt_within_SLA_circadian ;;
  }

  dimension: third_attempt_within_sla_int {
    type: string
    sql: ${TABLE}.third_attempt_within_SLA_int ;;
  }

  dimension: tracking_no {
    type: string
    sql: ${TABLE}.tracking_no ;;
  }

  dimension_group: ocd_SLA_first_attempt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ocd_SLA_first_attempt ;;
  }

  dimension_group: ocd_SLA_second_attempt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ocd_SLA_second_attempt ;;
  }

  dimension_group: ocd_SLA_third_attempt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ocd_SLA_third_attempt ;;
  }

  dimension: ocd_first_attempt_within_SLA_int {
    type: string
    label: "FC Click First Attempt within SLA (Int)"
    sql: ${TABLE}.ocd_first_attempt_within_SLA_int ;;
  }

  dimension: ocd_second_attempt_within_SLA_int {
    type: string
    label: "FC Click Second Attempt within SLA (Int)"
    sql: ${TABLE}.ocd_second_attempt_within_SLA_int ;;
  }

  dimension: ocd_third_attempt_within_SLA_int {
    type: string
    label: "FC Click Third Attempt within SLA (Int)"
    sql: ${TABLE}.ocd_third_attempt_within_SLA_int ;;
  }

  dimension: ocd_first_attempt_within_SLA_circadian {
    type: string
    label: "FC Click First Attempt within SLA (Circ)"
    sql: ${TABLE}.ocd_first_attempt_within_SLA_circadian ;;
  }

  dimension: ocd_second_attempt_within_SLA_circadian {
    type: string
    label: "FC Click Second Attempt within SLA (Circ)"
    sql: ${TABLE}.ocd_second_attempt_within_SLA_circadian ;;
  }

  dimension: ocd_third_attempt_within_SLA_circadian  {
    type: string
    label: "FC Click Third Attempt within SLA (Circ)"
    sql: ${TABLE}.ocd_third_attempt_within_SLA_circadian   ;;
  }

  dimension_group: soi_SLA_first_attempt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.soi_SLA_first_attempt as timestamp) ;;
  }

  dimension_group: soi_SLA_second_attempt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.soi_SLA_second_attempt as timestamp);;
  }

  dimension_group: soi_SLA_third_attempt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.soi_SLA_third_attempt ;;
  }

  dimension: soi_first_attempt_within_SLA_int {
    type: string
    label: "E-Com Click First Attempt within SLA (Int)"
    sql: ${TABLE}.soi_first_attempt_within_SLA_int ;;
  }

  dimension: soi_second_attempt_within_SLA_int {
    type: string
    label: "E-Com Click Second Attempt within SLA (Int)"
    sql: ${TABLE}.soi_second_attempt_within_SLA_int ;;
  }

  dimension: soi_third_attempt_within_SLA_int {
    type: string
    label: "E-Com Click Third Attempt within SLA (Int)"
    sql: ${TABLE}.soi_third_attempt_within_SLA_int ;;
  }

  dimension: soi_first_attempt_within_SLA_circadian {
    type: string
    label: "E-Com Click First Attempt within SLA (Circ)"
    sql: ${TABLE}.soi_first_attempt_within_SLA_circadian ;;
  }

  dimension: soi_second_attempt_within_SLA_circadian {
    type: string
    label: "E-Com Click Second Attempt within SLA (Circ)"
    sql: ${TABLE}.soi_second_attempt_within_SLA_circadian ;;
  }

  dimension: soi_third_attempt_within_SLA_circadian  {
    type: string
    label: "E-Com Click Third Attempt within SLA (Circ)"
    sql: ${TABLE}.soi_third_attempt_within_SLA_circadian   ;;
  }
  dimension: latest_undelivered_reason {
    type: string
    group_label: "Statuses"
    label: "Latest Undelivered Reason"
    description: "The latest reason the order failed its delivery at any given failed attempt"
    order_by_field: fault_with
    sql: ${TABLE}.latest_undelivered_reason ;;
  }

  dimension: fault_with {
    type: string
    group_label: "Statuses"
    label: "Fault With (Carrier/DSP vs Customer)"
    description: "Whether the reason the delivery failed is with the customer or carrier/DSP"
    sql: ${TABLE}.fault_with ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
