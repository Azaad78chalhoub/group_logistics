view: dm_logistics_unified_by_shipment {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.dm_logistics_unified_by_shipment`
    ;;

  dimension: awb {
    type: string
    sql: ${TABLE}.awb ;;
  }

  dimension_group: booked {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast (${TABLE}.booked_at as TIMESTAMP) ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension_group: car_order {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.car_order_date ;;
  }

  dimension: carrier {
    type: string
    sql: ${TABLE}.carrier ;;
  }

  dimension: carrier_code {
    type: string
    sql: ${TABLE}.carrier_code ;;
  }

  dimension: carrier_id {
    type: string
    sql: ${TABLE}.carrier_id ;;
  }

  dimension: carrier_name {
    type: string
    sql: ${TABLE}.carrier_name ;;
  }
  dimension: order_status_code {
    hidden: yes
    type: string
    sql: ${TABLE}.order_status_code ;;
  }
  dimension: codelkup_otype_description {
    type: string
    hidden: yes
    sql: ${TABLE}.codelkup_otype_description ;;
  }

  dimension: codelkup_priority_description {
    type: string
    hidden: yes
    sql: ${TABLE}.codelkup_priority_description ;;
  }

  dimension: codelkup_type_description {
    type: string
    hidden: yes
    sql: ${TABLE}.codelkup_type_description ;;
  }

  dimension_group: confirmation {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.confirmation_date ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension_group: creation {
    type: time
    label: "DSP creation"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.dsp_creation_date ;;
  }

  dimension_group: delivered {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql:CAST( ${TABLE}.delivered_at as TIMESTAMP) ;;
  }

  dimension: dropoff_city {
    type: string
    sql: ${TABLE}.dropoff_city ;;
  }

  dimension: dropoff_country {
    type: string
    sql: ${TABLE}.dropoff_country ;;
  }

  dimension: external_factor {
    type: string
    hidden: yes
    sql: ${TABLE}.external_factor ;;
  }

  dimension: facility_name {
    label: "Facility"
    description: "Warehouse name"
    type: string
    sql:
    case
      when ${TABLE}.facility_name = 'REAL Emirates - DIP' then 'DIP'
      when ${TABLE}.facility_name = 'REAL KSA RIYADH' then 'RYD'
      when ${TABLE}.facility_name = 'CADF' then 'CADF'
      when ${TABLE}.facility_name = 'REAL KSA JEDDAH' then 'JED'
      when ${TABLE}.facility_name = 'H&C KUWAIT' then 'H&C'
      when ${TABLE}.facility_name = 'ASHRAF BGDC' then 'BAH'
      when ${TABLE}.facility_name = 'MAC' then 'MAC'
    end ;;
  }

  dimension_group: failed_attempt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.failed_attempt_at ;;
  }

  dimension_group: first_attempt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.first_attempt as TIMESTAMP);;
  }

  dimension: giftmessage2 {
    type: string
    sql: ${TABLE}.giftmessage2 ;;
  }

  dimension: giftmessage3 {
    type: string
    sql: ${TABLE}.giftmessage3 ;;
  }

  dimension: giftmessage4 {
    type: string
    sql: ${TABLE}.giftmessage4 ;;
  }

  dimension: giftwrapstraptype {
    type: string
    sql: ${TABLE}.giftwrapstraptype ;;
  }

  dimension: gitfmessage {
    type: string
    sql: ${TABLE}.gitfmessage ;;
  }

  dimension: merchant {
    type: string
    sql: ${TABLE}.merchant ;;
  }

  dimension: no_of_attempts {
    type: number
    sql: ${TABLE}.no_of_attempts ;;
  }
  dimension: order_priority {
    type: string
    sql: ${TABLE}.order_priority ;;
  }
  dimension: order_priority_detail {
    label: "Order Priority Type"
    type: string
    sql:case when ${order_priority} = "1" then "Same Day"
             when ${order_priority} = "3" then "Next Day"
             when ${order_priority} = "5" then "Standard" end;;
  }

  dimension: order_c_city {
    type: string
    sql: ${TABLE}.order_c_city ;;
  }

  dimension: order_consigneekey {
    type: string
    sql: ${TABLE}.order_consigneekey ;;
  }

  dimension_group: order_creation {
    type: time
    label: "FC Order creation"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST( ${TABLE}.order_creation_date as TIMESTAMP);;
  }

  dimension_group: order_creation_date_gmt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_creation_date_gmt ;;
  }

  dimension_group: order_datetime {
    type: time
    label: "E-COM creation date"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql:  CAST(${TABLE}.order_datetime as TIMESTAMP) ;;
  }

  dimension: order_extern_orderkey {
    type: string
    sql: ${TABLE}.order_extern_orderkey ;;
  }

  dimension: order_status_description {
    label: "FC Order Status"
    type: string
    hidden: no
    sql: ${TABLE}.order_status_description ;;
  }

  dimension: order_storerkey {
    type: string
    sql: ${TABLE}.order_storerkey ;;
  }

  dimension_group: out_for_delivery {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast( ${TABLE}.out_for_delivery_at as TIMESTAMP);;
  }

  dimension: partner_order_reference {
    type: string
    label: "DSP partner order"
    sql: ${TABLE}.partnerOrderReference ;;
  }

  dimension: partner_shipment_reference {
    type: string
    sql: ${TABLE}.partnerShipmentReference ;;
  }

  dimension: payment_mode {
    type: string
    sql: ${TABLE}.payment_mode ;;
  }

  dimension: pickup_city {
    type: string
    sql: ${TABLE}.pickup_city ;;
  }

  dimension: pickup_country {
    type: string
    sql: ${TABLE}.pickup_country ;;
  }

  dimension_group: returned {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.returned_at as TIMESTAMP) ;;
  }

  dimension_group: second_attempt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.second_attempt AS TIMESTAMP);;
  }

  dimension_group: shipped {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.shipped_at as TIMESTAMP) ;;
  }

  dimension: so_order_id {
    type: string
    label: "E-COM Order ID"
    sql: ${TABLE}.so_order_id ;;
  }

  dimension: source {
    type: string
    label: "E-COM Source"
    sql: ${TABLE}.source ;;
  }

  dimension: store_country {
    type: string
    sql: ${TABLE}.store_country ;;
  }

  dimension: storer_customer {
    type: string
    sql: ${TABLE}.storer_customer ;;
  }

  dimension: storer_owner {
    type: string
    sql: ${TABLE}.storer_owner ;;
  }

  dimension_group: third_attempt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.third_attempt AS TIMESTAMP);;
  }

  dimension: tracking_no {
    type: string
    label: "DSP Tracking no"
    sql: ${TABLE}.tracking_no ;;
  }

  dimension_group: transmitlog_status_c_mctorderpicked_date_gmt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_c_mctorderpicked_date_gmt ;;
  }

  dimension_group: transmitlog_status_c_orderallocationrun_date_gmt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_c_orderallocationrun_date_gmt ;;
  }

  dimension_group: transmitlog_status_c_orderpacked_date_gmt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_c_orderpacked_date_gmt ;;
  }

  dimension_group: transmitlog_status_c_ordershipped_date_gmt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_c_ordershipped_date_gmt ;;
  }
  dimension_group: picked_date {
    type: time
    label: "FC Picked date"
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.picked_date as TIMESTAMP) ;;
  }
  dimension_group: ready_to_ship_date {
    type: time
    hidden: yes
    label: "FC Ready to ship date"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.ready_to_ship_date as TIMESTAMP) ;;
  }

  dimension_group: transmitlog_status_mctorderpicked_date_gmt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_mctorderpicked_date_gmt ;;
  }

  dimension_group: transmitlog_status_orderallocationrun_date_gmt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_orderallocationrun_date_gmt ;;
  }

  dimension_group: transmitlog_status_ordercancelled_date_gmt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_ordercancelled_date_gmt ;;
  }

  dimension_group: packed_date {
    label: "WM9 Packed date"
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql:cast( ${TABLE}.packed_date as TIMESTAMP);;
  }

  dimension_group: transmitlog_status_orderpacked_date_gmt {
    description: "FC order packed timestamp GMT"
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_orderpacked_date_gmt ;;
  }

  dimension_group: shipped_date {
    hidden: yes
    type: time
    label: "FC Shipped date"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.shipped_date as TIMESTAMP) ;;
  }

  dimension_group: transmitlog_status_ordershipped_date_gmt {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_ordershipped_date_gmt ;;
  }

  dimension_group: transmitlog_status_so_date_gmt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_SO_date_gmt ;;
  }

  dimension: vas_applied {
    type: yesno
    sql: ${TABLE}.vas_applied ;;
  }
  dimension: wm9_order_ID {
    type: string
    label: "FC Order ID"
    sql: ${TABLE}.wm9_order_id ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.vertical ;;
  }

  dimension: warehouse_id {
    type: string
    sql: ${TABLE}.warehouse_id ;;
  }

   measure: count_processed {
    label: "Count - Total Processed Orders"
    group_label: "Order Count"
    description: "Count of total picked and packed orders"
    type: count_distinct
    sql: ${wm9_order_ID} ;;
    filters: [picked_date_date:"NOT NULL"]
  }

  measure: count_shipped {
    label: "Count - Total Shipped Orders"
    group_label: "Order Count"
    description: "Count of total shipped orders"
    type: count_distinct
    sql: ${wm9_order_ID} ;;
    filters: [order_status_code: "95,102"]
  }

   measure: count_cancelled {
    label: "Count - Total Cancelled Orders"
    group_label: "Order Count"
    description: "Count of total cancelled orders"
    type: count_distinct
    sql: ${wm9_order_ID} ;;
    filters: [order_status_code: "00,104"]
  }

  measure: count_in_progress {
    label: "Count - Total In Progress Orders"
    group_label: "Order Count"
    description: "Count of total orders in progress"
    type: number
    sql: ${count_wm9_orders}-(${count_shipped}+${count_cancelled}) ;;
  }

  dimension: hasClick_date {
    type: yesno
    label: "Is Click Date available ?"
    description: "If Click Date not available, take the order creation date in FC as the click date. Otherwise take Click date from E-Com data"
    sql: (${TABLE}.so_order_id IS NOT NULL) or ${order_datetime_date} is not null;;
    }
  dimension: isFulfilled_byStore {
    type: yesno
    label: "Is Fulfilled by Store ?"
    sql: ${TABLE}.so_order_id IS NOT NULL and ${TABLE}.partnerOrderReference is NOT NULL and ${order_extern_orderkey} is NULL;;
  }
  measure: count_wm9_orders {
    label: "Count Of FC created orders"
    group_label: "Order Count"
    type: count_distinct
    drill_fields: [order_creation_raw,facility_name,awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
    sql: ${wm9_order_ID} ;;
    }
  measure: actuals_percnt {
    label: "Actual %"
    type: number
    value_format: "0.00\%"
    sql: safe_divide(${count_wm9_orders},${dm_wh_ecom_order_forecast_data.forecast_order_count})*100;;
  }

  measure: count_wm9_orders_total {
    label: "Count Of FC created orders"
    group_label: "Order Count"
    type: count_distinct
    sql: ${wm9_order_ID} ;;
    drill_fields: [order_creation_date,count_wm9_orders_total]
    link: {
      label: "Order Count Time Series"
      url: "
      {% assign vis_config = '{
      \"type\" : \"looker_line\"
      }' %}
      {{ link }}&vis_config={{ vis_config | encode_uri }}&toggle=dat,pik,vis&limit=5000"
    }
  }
  measure: count_p1 {
    label: "Count FC - Same Day Orders"
    group_label: "Order Count"
    description: "Count of priority 1 same day orders"
    type: count_distinct
    sql: ${wm9_order_ID} ;;
    filters: [order_priority: "1"]
    drill_fields: [order_creation_raw,facility_name,awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
  }

  measure: count_p3 {
    label: "Count FC - Next Day Orders"
    group_label: "Order Count"
    description: "Count of priority 3 next day orders"
    type: count_distinct
    sql: ${wm9_order_ID} ;;
    filters: [order_priority: "3"]
    drill_fields: [order_creation_raw,facility_name,awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
  }

  measure: count_p5 {
    label: "Count FC - Standard Orders"
    group_label: "Order Count"
    description: "Count of priority 5 standard orders"
    type: count_distinct
    sql: ${wm9_order_ID} ;;
    filters: [order_priority: "5"]
    drill_fields: [order_creation_raw,facility_name,awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
  }

  parameter: date_granularity {
    type: string
    allowed_value: { value: "Hour" }
    allowed_value: { value: "Day" }
    allowed_value: { value: "Week" }
    allowed_value: { value: "Month" }
  }
  dimension_group: click_to_wm9 {
    hidden: no
    type: duration
    intervals: [hour, minute]
    sql_start: coalesce(${order_datetime_raw}, ${order_creation_raw}) ;;
    sql_end: ${order_creation_raw} ;;
  }
  measure: time_to_wm9_from_click  {
    label: "Click to FC"
    group_label: "Click to Status metrics"
    description: "Average time taken from order creation by the customer to order creation in FC"
    type: average
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
    sql: ${hours_click_to_wm9};;
  }
  measure: time_to_wm9_from_click_80th_percentile {
    type: percentile
    label: "Click to FC-80"
    percentile: 80
    sql: ${hours_click_to_wm9};;
  }
  measure: time_to_wm9_from_click_90th_percentile {
    type: percentile
    label: "Click to FC-90"
    percentile: 90
    sql: ${hours_click_to_wm9};;
  }
  measure: time_to_wm9_from_click_95th_percentile {
    type: percentile
    label: "Click to FC-95"
    percentile: 95
    sql: ${hours_click_to_wm9};;
  }
  measure: click_to_wm9_p1_new{
    label: "Avg Click to WM9 Time - Same Day Orders"
    group_label: "Click To WM9 by priority"
    description: "Average Click to WM9 time for a priority 1 same day order"
    type: average
    label_from_parameter: date_granularity
    sql: ${hours_click_to_wm9};;
    filters: [order_priority: "1"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description ,awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
  }
  measure: click_to_wm9_p3_new{
    label: "Avg Click to WM9 Time - Next day Orders"
    group_label: "Click To WM9 by priority"
    description: "Average Click to WM9 time for a priority 3 next day order"
    type: average
    sql: ${hours_click_to_wm9};;
    filters: [order_priority: "3"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description ,awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
  }
  measure:  click_to_wm9_p5_new{
    label: "Click to WM9 Time - Standard Orders"
    group_label: "Click To WM9 by priority"
    description: "Average Click to WM9 time for a priority 5 standard order"
    type: average
    sql: ${hours_click_to_wm9};;
    filters: [order_priority: "5"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
  }
  dimension_group: click_to_pick {
    hidden: yes
    type: duration
    intervals: [hour]
    sql_start: coalesce(${order_datetime_raw}, ${order_creation_raw}) ;;
    sql_end: ${picked_date_raw} ;;
  }
  measure: time_to_pick_from_click  {
    label: "Click to Pick"
    group_label: "Click to Status metrics"
    description: "Average time taken from order creation by the customer to pick complete in WM9"
    type: average
    value_format:"#.00"
    sql:${hours_click_to_pick};;
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
  }
  measure: Click_to_Pick_80th_percentile {
    type: percentile
    label: "Click to Pick-80"
    percentile: 80
    sql: ${hours_click_to_pick};;
  }
  measure: Click_to_Pick_90th_percentile {
    type: percentile
    label: "Click to Pick-90"
    percentile: 90
    sql: ${hours_click_to_pick};;
  }
  measure: Click_to_Pick_95th_percentile {
    type: percentile
    label: "Click to Pick-95"
    percentile: 95
    sql: ${hours_click_to_pick};;
  }
  measure: click_to_pick_p1_new {
    label: "Avg Click to Pick Time - Same Day Orders"
    group_label: "Click To Pick by priority"
    description: "Average pick time for a priority 1 same day order"
    type: average
    sql: ${hours_click_to_pick};;
    filters: [order_priority: "1"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
  }
  measure: click_to_pick_p3_new{
    label: "Avg Click to Pick Time - Next day Orders"
    group_label: "Click To Pick by priority"
    description: "Average pick time for a priority 3 next day order"
    type: average
    sql: ${hours_click_to_pick};;
    filters: [order_priority: "3"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
  }
  measure:  click_to_pick_p5_new{
    label: "Click to Pick Time - Standard Orders"
    group_label: "Click To Pick by priority"
    description: "Average pick time for a priority 5 standard order"
    type: average
    sql: ${hours_click_to_pick};;
    filters: [order_priority: "5"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
  }
  dimension_group: click_to_pack {
    hidden: yes
    type: duration
    intervals: [hour]
    sql_start: coalesce(${order_datetime_raw}, ${order_creation_raw}) ;;
    sql_end: ${packed_date_raw} ;;
  }
  measure: time_to_pack_from_click  {
    label: "Click to Pack"
    group_label: "Click to Status metrics"
    description: "Average time taken from order creation by the customer to pack complete in WM9"
    type: average
    value_format:"#.00"
    sql:${hours_click_to_pack};;
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]

  }
  measure: Click_to_Pack_80th_percentile {
    type: percentile
    label: "Click to Pack-80"
    percentile: 80
    sql: ${hours_click_to_pack};;
  }
  measure: Click_to_Pack_90th_percentile {
    type: percentile
    label: "Click to Pack-90"
    percentile: 90
    sql: ${hours_click_to_pack};;
  }
  measure: Click_to_Pack_95th_percentile {
    type: percentile
    label: "Click to Pack-95"
    percentile: 95
    sql: ${hours_click_to_pack};;
  }
  measure: click_to_pack_p1_new{
    label: "Avg Click to Pack Time - Same Day Orders"
    group_label: "Click To Pack by priority"
    description: "Average pack time for a priority 1 same day order"
    type: average
    sql: ${hours_click_to_pack};;
    filters: [order_priority: "1"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
  }
  measure: click_to_pack_p3_new{
    label: "Avg Click to Pack Time - Next day Orders"
    group_label: "Click To Pack by priority"
    description: "Average pack time for a priority 3 next day order"
    type: average
    sql: ${hours_click_to_pack};;
    filters: [order_priority: "3"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
  }
  measure:  click_to_pack_p5_new{
    label: "Click to Pack Time - Standard Orders"
    group_label: "Click To Pack by priority"
    description: "Average pack time for a priority 5 standard order"
    type: average
    sql: ${hours_click_to_pack};;
    filters: [order_priority: "5"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw]
  }
  dimension_group: click_to_shipped {
    hidden: yes
    type: duration
    intervals: [hour]
    sql_start: coalesce(${order_datetime_raw}, ${order_creation_raw});;
    sql_end: ${shipped_date_raw} ;;
  }
  measure: time_to_shipped_from_click  {
    label: "Click to Ship"
    group_label: "Click to Status metrics"
    description: "Average time taken from order creation by the customer to order being shipped out of WM9"
    type: average
    value_format:"#.00"
    sql: ${hours_click_to_shipped};;
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city]
  }
  measure: Click_to_shipped_80th_percentile {
    type: percentile
    label: "Click to Ship-80"
    percentile: 80
    sql: ${hours_click_to_shipped};;
  }
  measure: Click_to_shipped_90th_percentile {
    type: percentile
    label: "Click to Ship-90"
    percentile: 90
    sql: ${hours_click_to_shipped};;
  }
  measure: Click_to_shipped_95th_percentile {
    type: percentile
    label: "Click to Ship-95"
    percentile: 95
    sql: ${hours_click_to_shipped};;
  }
  measure: click_to_shipped_p1_new{
    label: "Avg Click to Ship Time - Same Day Orders"
    group_label: "Click To Ship by priority"
    description: "Average ship time for a priority 1 same day order"
    type: average
    sql: ${hours_click_to_shipped};;
    filters: [order_priority: "1"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city]
  }
  measure: click_to_shipped_p3_new{
    label: "Avg Click to Ship Time - Next day Orders"
    group_label: "Click To Ship by priority"
    description: "Average Ship time for a priority 3 next day order"
    type: average
    sql: ${hours_click_to_shipped};;
    filters: [order_priority: "3"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city]
  }
  measure:  click_to_shipped_p5_new{
    label: "Click to Ship Time - Standard Orders"
    group_label: "Click To Ship by priority"
    description: "Average Ship time for a priority 5 standard order"
    type: average
    sql: ${hours_click_to_shipped};;
    filters: [order_priority: "5"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city]
  }
  dimension_group: click_to_collect {
    hidden: yes
    type: duration
    intervals: [second]
    sql_start: coalesce(${order_datetime_raw}, ${order_creation_raw}) ;;
    sql_end: ${shipped_raw} ;;
  }
  measure: time_to_collect_from_click  {
    label: "Click to Collect"
    hidden: yes
    group_label: "Click to Status metrics"
    description: "Average time taken from order creation by the customer to collected by the DSP"
    type: average
    label_from_parameter: date_granularity
    value_format:"#.00"
    sql:
    CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_click_to_collect}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_click_to_collect}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_click_to_collect}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_click_to_collect}*3.80514651484217549e-7
          ELSE ${seconds_click_to_collect}*1.1574e-5
        END;;
  }
  dimension_group: click_to_ofd {
    hidden: yes
    type: duration
    intervals: [hour]
    sql_start: coalesce(${order_datetime_raw}, ${order_creation_raw}) ;;
    sql_end:coalesce( ${out_for_delivery_raw}, ${first_attempt_raw},${delivered_raw});;
  }
  measure: time_to_ofd_from_click  {
    label: "Click to Out For Delivery"
    group_label: "Click to Status metrics"
    description: "Average time taken from order creation by the customer to out for delivery by the DSP"
    type: average
    value_format:"#.00"
    sql:${hours_click_to_ofd};;
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw]
  }
  measure: Click_to_ofd_80th_percentile {
    type: percentile
    label: "Click to Out For Delivery-80"
    percentile: 80
    sql: ${hours_click_to_ofd};;
  }
  measure: Click_to_ofd_90th_percentile {
    type: percentile
    label: "Click to Out For Delivery-90"
    percentile: 90
    sql: ${hours_click_to_ofd};;
  }
  measure: Click_to_ofd_95th_percentile {
    type: percentile
    label: "Click to Out For Delivery-95"
    percentile: 95
    sql: ${hours_click_to_ofd};;
  }
  measure: click_to_OFD_p1_new{
    label: "Avg Click to OFD Time - Same Day Orders"
    group_label: "Click To OFD by priority"
    description: "Average OFD time for a priority 1 same day order"
    type: average
    sql: ${hours_click_to_ofd};;
    filters: [order_priority: "1"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw]
  }
  measure: click_to_OFD_p3_new{
    label: "Avg Click to OFD Time - Next day Orders"
    group_label: "Click To OFD by priority"
    description: "Average OFD time for a priority 3 next day order"
    type: average
    sql: ${hours_click_to_ofd};;
    filters: [order_priority: "3"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw]
  }
  measure:  click_to_OFD_p5_new{
    label: "Click to OFD Time - Standard Orders"
    group_label: "Click To OFD by priority"
    description: "Average OFD time for a priority 5 standard order"
    type: average
    sql:${hours_click_to_ofd};;
    filters: [order_priority: "5"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw]
  }
  dimension_group: click_to_FA {
    hidden: yes
    type: duration
    intervals: [hour]
    sql_start: coalesce(${order_datetime_raw}, ${order_creation_raw}) ;;
    sql_end:coalesce( ${first_attempt_raw},${out_for_delivery_raw},${delivered_raw});;
  }
  measure: time_to_FA_from_click  {
    label: "Click to First Attempt"
    group_label: "Click to Status metrics"
    description: "Average time taken from order creation by the customer to make the first delivery attempt by the DSP"
    type: average
    value_format:"#.00"
    sql:${hours_click_to_FA};;
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw]
  }
  measure: Click_to_FA_80th_percentile {
    type: percentile
    label: "Click to First Attempt-80"
    percentile: 80
    sql: ${hours_click_to_FA};;
  }
  measure: Click_to_FA_90th_percentile {
    type: percentile
    label: "Click to First Attempt-90"
    percentile: 90
    sql: ${hours_click_to_FA};;
  }
  measure: Click_to_FA_95th_percentile {
    type: percentile
    label: "Click to First Attempt-95"
    percentile: 95
    sql: ${hours_click_to_FA};;
  }
  measure: click_to_FA_p1_new{
    label: "Avg Click to FA Time - Same Day Orders"
    group_label: "Click To FA by priority"
    description: "Average FA time for a priority 1 same day order"
    type: average
    sql: ${hours_click_to_FA};;
    filters: [order_priority: "1"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw]
  }
  measure: click_to_FA_p3_new{
    label: "Avg Click to FA Time - Next day Orders"
    group_label: "Click To FA by priority"
    description: "Average FA time for a priority 3 next day order"
    type: average
    sql: ${hours_click_to_FA};;
    filters: [order_priority: "3"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw]
  }
  measure:  click_to_FA_p5_new{
    label: "Click to FA Time - Standard Orders"
    group_label: "Click To FA by priority"
    description: "Average FA time for a priority 5 standard order"
    type: average
    sql: ${hours_click_to_FA};;
    filters: [order_priority: "5"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw]
  }
  dimension_group: click_to_SA {
    hidden: yes
    type: duration
    intervals: [hour]
    sql_start: coalesce(${order_datetime_raw}, ${order_creation_raw}) ;;
    sql_end:coalesce( ${second_attempt_raw});;
  }
  measure: time_to_SA_from_click  {
    label: "Click to Second Attempt"
    group_label: "Click to Status metrics"
    hidden: yes
    description: "Average time taken from order creation by the customer to make the second delivery attempt by the DSP"
    type: average
    value_format:"#.00"
    sql:${hours_click_to_SA};;
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw,second_attempt_raw]
  }
  dimension_group: click_to_TA {
    hidden: yes
    type: duration
    intervals: [hour]
    sql_start: coalesce(${order_datetime_raw}, ${order_creation_raw}) ;;
    sql_end:coalesce( ${third_attempt_raw});;
  }
  measure: time_to_TA_from_click  {
    label: "Click to Third Attempt"
    group_label: "Click to Status metrics"
    hidden: yes
    description: "Average time taken from order creation by the customer to make the third delivery attempt by the DSP"
    type: average
    value_format:"#.00 \" Days\""
    sql: ${hours_click_to_TA};;
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw,second_attempt_raw,third_attempt_raw]
  }
  dimension_group: click_to_delivered {
    hidden: yes
    type: duration
    intervals: [hour]
    sql_start: coalesce(${order_datetime_raw}, ${order_creation_raw});;
    sql_end:coalesce( ${delivered_raw});;
  }

  measure: time_to_Delivered_from_click  {
    label: "Click to Delivery"
    group_label: "Click to Status metrics"
    description: "Average time taken from order creation by the customer to the order delivery by the DSP"
    type: average
    value_format:"#.00"
    sql:${hours_click_to_delivered};;
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_date,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw,second_attempt_raw,delivered_raw,time_to_Delivered_from_click]

  }
  measure: time_to_Delivered_from_click_80th_percentile {
    type: percentile
    label: "Click to Delivery-80"
    percentile: 80
    sql: ${hours_click_to_delivered};;
  }

  measure: time_to_Delivered_from_click_20th_percentile {
    type: percentile
    label: "Click to Delivery-20"
    percentile: 20
    sql: ${hours_click_to_delivered};;
  }
  measure: time_to_Delivered_from_click_10th_percentile {
    type: percentile
    label: "Click to Delivery-10"
    percentile: 10
    sql: ${hours_click_to_delivered};;
  }
  measure: time_to_Delivered_from_click_5th_percentile {
    type: percentile
    label: "Click to Delivery-5"
    percentile: 5
    sql: ${hours_click_to_delivered};;
  }
  measure: time_to_Delivered_from_click_90th_percentile {
    type: percentile
    label: "Click to Delivery-90"
    percentile: 90
    sql: ${hours_click_to_delivered}  ;;
  }
  measure: time_to_Delivered_from_click_95th_percentile {
    type: percentile
    label: "Click to Delivery-95"
    percentile: 95
    sql: ${hours_click_to_delivered}  ;;
  }
  measure: click_to_delivered_p1_new{
    label: "Avg Click to Delivery Time - Same Day Orders"
    group_label: "Click To Delivery by priority"
    description: "Average Delivery time for a priority 1 same day order"
    type: average
    sql: ${hours_click_to_delivered};;
    filters: [order_priority: "1"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw,second_attempt_raw,delivered_raw]
    }
  measure: click_to_delivered_p3_new{
    label: "Avg Click to Delivery Time - Next day Orders"
    group_label: "Click To Delivery by priority"
    description: "Average Delivery time for a priority 3 next day order"
    type: average
    sql: ${hours_click_to_delivered};;
    filters: [order_priority: "3"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw,second_attempt_raw,delivered_raw]
    }
  measure:  click_to_delivered_p5_new{
    label: "Click to Delivery Time - Standard Orders"
    group_label: "Click To Delivery by priority"
    description: "Average Delivery time for a priority 5 standard order"
    type: average
    sql: ${hours_click_to_delivered};;
    filters: [order_priority: "5"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw,second_attempt_raw,delivered_raw]
    }

  dimension: shipments_from_wm9{
    hidden: no
    label: "Shipments from FC"
    type: yesno
    sql: ${order_extern_orderkey} is not null;;
  }
  dimension: shipments_from_dsp{
    hidden: no
    type: yesno
    sql: ${partner_order_reference} is not null and ${delivered_date} is not null;;
  }
  dimension_group: click_to_returned {
    hidden: yes
    type: duration
    intervals: [hour]
    sql_start: coalesce(${order_datetime_raw}, ${order_creation_raw}) ;;
    sql_end:coalesce( ${returned_raw});;
  }
  measure: time_to_Returned_from_click  {
    label: "Click to Return"
    group_label: "Click to Status metrics"
    description: "Average time taken from order creation by the customer to the order returned by the DSP"
    type: average
    value_format:"#.00"
    sql:${hours_click_to_returned};;
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw,second_attempt_raw,delivered_raw,returned_raw]

  }
  measure: Click_to_return_80th_percentile {
    type: percentile
    label: "Click to Return-80"
    percentile: 80
    sql: ${hours_click_to_returned};;
  }
  measure: Click_to_return_90th_percentile {
    type: percentile
    label: "Click to Return-90"
    percentile: 90
    sql: ${hours_click_to_returned};;
  }
  measure: Click_to_return_95th_percentile {
    type: percentile
    label: "Click to Return-95"
    percentile: 95
    sql: ${hours_click_to_returned};;
  }
  measure: click_to_return_p1_new{
    label: "Avg Click to Return Time - Same Day Orders"
    group_label: "Click To Return by priority"
    description: "Average Return time for a priority 1 same day order"
    type: average
    sql: ${hours_click_to_returned};;
    filters: [order_priority: "1"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw,second_attempt_raw,delivered_raw,returned_raw]

  }
  measure: click_to_return_p3_new{
    label: "Avg Click to Return Time - Next day Orders"
    group_label: "Click To Return by priority"
    description: "Average Return time for a priority 3 next day order"
    type: average
    sql:  ${hours_click_to_returned};;
    filters: [order_priority: "3"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw,second_attempt_raw,delivered_raw,returned_raw]

  }
  measure:  click_to_return_p5_new{
    label: "Click to Return Time - Standard Orders"
    group_label: "Click To Return by priority"
    description: "Average Return time for a priority 5 standard order"
    type: average
    sql: ${hours_click_to_returned};;
    filters: [order_priority: "5"]
    value_format:"#.00"
    drill_fields: [order_datetime_raw,so_order_id,source,order_creation_raw,facility_name,order_status_description, awb,order_extern_orderkey,wm9_order_ID,order_priority, vas_applied,picked_date_raw,packed_date_raw,ready_to_ship_date_raw,shipped_date_raw,creation_raw,carrier,carrier_id,merchant,tracking_no,pickup_country,pickup_city,dropoff_country,dropoff_city,out_for_delivery_raw,first_attempt_raw,second_attempt_raw,delivered_raw,returned_raw]

  }
  measure: days_to_first_attempt {
    group_label: "SLA Metrics"
    description: "Creation to First Attempt in Days"
    type: average
    sql: DATE_DIFF(${first_attempt_date}, ${creation_date}, DAY) ;;
  }
  # dimension: first_attempt_within_SLA {
  #   hidden: yes
  #   type: yesno
  #   sql: DATE_DIFF(${first_attempt_date}, ${creation_date}, DAY) <= ${dm_fulfillment_wm9_orders_city_sla_uat_test.sla} ;;
  # }
  # measure: first_attempt_within_SLA_count {
  #   group_label: "SLA Metrics"
  #   description: "Number of tracking #s where the first attempt was made within the SLA"
  #   type: count_distinct
  #   filters: [first_attempt_within_SLA: "Yes"]
  #   sql: ${tracking_no} ;;
  # }
  # measure: days_to_second_attempt {
  #   group_label: "SLA Metrics"
  #   description: "Creation to Second Attempt in Days"
  #   type: average
  #   sql: DATE_DIFF(${second_attempt_date}, ${creation_date}, DAY) ;;
  # }
  # dimension: second_attempt_within_SLA {
  #   hidden: yes
  #   type: yesno
  #   sql: DATE_DIFF(${first_attempt_date}, ${creation_date}, DAY) <= ${dm_fulfillment_wm9_orders_city_sla_uat_test.sla}+1 ;;
  # }
  # measure: second_attempt_within_SLA_count {
  #   group_label: "SLA Metrics"
  #   description: "Number of tracking #s where the second attempt was made within the SLA"
  #   type: count_distinct
  #   filters: [second_attempt_within_SLA: "Yes"]
  #   sql: ${tracking_no} ;;
  # }
  # measure: days_to_third_attempt {
  #   group_label: "SLA Metrics"
  #   description: "Creation to Third Attempt in Days"
  #   type: average
  #   sql: DATE_DIFF(${third_attempt_date}, ${creation_date}, DAY) ;;
  # }
  # dimension: third_attempt_within_SLA {
  #   hidden: yes
  #   type: yesno
  #   sql: DATE_DIFF(${first_attempt_date}, ${creation_date}, DAY) <= ${dm_fulfillment_wm9_orders_city_sla_uat_test.sla}+2 ;;
  # }
  # measure: third_attempt_within_SLA_count {
  #   group_label: "SLA Metrics"
  #   description: "Number of tracking #s where the third attempt was made within the SLA"
  #   type: count_distinct
  #   filters: [third_attempt_within_SLA: "Yes"]
  #   sql: ${tracking_no} ;;
  # }
  measure: count {
    type: count
    drill_fields: [facility_name, carrier_name]
  }
  measure: count_dist_awb {
    type: count_distinct
    sql: ${awb} ;;
  }
  measure: count_dist_trackn {
    type: count_distinct
    sql: ${tracking_no} ;;
  }
  measure: count_dist {
    type: count_distinct
    drill_fields: [facility_name, carrier_name]
  }
}
