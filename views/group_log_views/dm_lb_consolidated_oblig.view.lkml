view: dm_lb_consolidated_oblig {
  #sql_table_name: `chb-prod-stage-oracle.prod_supply_chain.ref_union_consolidated_oblig_actvities_asn_reference`;;

  derived_table: {
    sql_trigger_value: SELECT FLOOR((TIMESTAMP_DIFF(CURRENT_TIMESTAMP(),'1970-01-01 00:00:00',SECOND)) / (12*60*60)) ;;
    sql:
      select STORERKEY,ASN,BU_CODE,BU_DESC_ICSB,LEGAL_ENTITY,
             ACTIVITY_NAME,FACILITY_NAME,period_from,period_to,
             location,CHARGE_RATE,CHARGE_CURRENCY,
             sum(cast(QUANTITY as float64)) QUANTITY ,
             sum(cast(Total_Amount as float64)) Total_Amount,
             row_number() over(order by 1) as prim_key
      FROM `chb-prod-stage-oracle.prod_supply_chain.ref_union_consolidated_oblig_actvities_asn_reference`
      group by STORERKEY,ASN,BU_CODE,BU_DESC_ICSB,LEGAL_ENTITY,
               ACTIVITY_NAME,FACILITY_NAME,period_from,period_to,
               location,CHARGE_RATE,CHARGE_CURRENCY
      ;;
  }

  dimension: pk {
    primary_key: yes
    type: number
    hidden: yes
    sql:${TABLE}.prim_key;;
  }

  dimension: activity_name {
    type: string
    sql: ${TABLE}.ACTIVITY_NAME ;;
  }

  dimension: asn {
    type: string
    sql: ${TABLE}.ASN ;;
  }

  dimension: bu_code {
    type: string
    sql: ${TABLE}.BU_CODE ;;
  }

  dimension: bu_desc_icsb {
    type: string
    sql: ${TABLE}.BU_DESC_ICSB ;;
  }

  dimension: charge_currency {
    type: string
    sql: ${TABLE}.CHARGE_CURRENCY ;;
  }

  dimension: charge_rate {
    type: string
    sql: ${TABLE}.CHARGE_RATE ;;
  }

  dimension: facility_name {
    type: string
    sql: ${TABLE}.FACILITY_NAME ;;
  }

  dimension: legal_entity {
    type: string
    sql: ${TABLE}.LEGAL_ENTITY ;;
  }

  dimension: location {
    type: string
    sql: ${TABLE}.Location ;;
  }

  dimension: period_from {
    type: string
    sql: ${TABLE}.period_from ;;
  }

  dimension: period_to {
    type: string
    sql: ${TABLE}.period_to ;;
  }

  dimension: storerkey {
    type: string
    sql: ${TABLE}.STORERKEY ;;
  }

  dimension: quantity {
    type: number
    sql: ${TABLE}.QUANTITY ;;
  }

  dimension: total_amount {
    type: number
    sql: ${TABLE}.Total_Amount ;;
  }

  measure: count {
    type: count
  }

  # dimension: column_name {
  #   type: string
  #   sql: ${TABLE}.COLUMN_NAME ;;
  # }
  # dimension: country {
  #   type: string
  #   map_layer_name: countries
  #   sql: ${TABLE}.COUNTRY ;;
  # }
  # dimension: e_com_y_n {
  #   type: string
  #   sql: ${TABLE}.E_COM_Y_N ;;
  # }
  # dimension: final_brand_name {
  #   type: string
  #   sql: ${TABLE}.FINAL_BRAND_NAME ;;
  # }
  # dimension: icsb_service_name {
  #   type: string
  #   sql: ${TABLE}.ICSB_SERVICE_NAME ;;
  # }
  # dimension: service_type {
  #   type: string
  #   sql: ${TABLE}.SERVICE_TYPE ;;
  # }
  # dimension: loc_location {
  #   type: string
  #   sql: ${TABLE}.loc_location ;;
  # }
  # dimension: quantity {
  #   type: string
  #   sql: ${TABLE}.QUANTITY ;;
  # }
  # dimension: manual_y_n {
  #   type: string
  #   sql: ${TABLE}.MANUAL_Y_N ;;
  # }
  # dimension: summary_percentage {
  #   type: string
  #   sql: ${TABLE}.Summary_Percentage ;;
  # }
  # dimension: total_amount {
  #   type: string
  #   sql: ${TABLE}.Total_Amount ;;
  # }
}
