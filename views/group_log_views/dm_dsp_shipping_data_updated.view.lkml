view: dm_dsp_shipping_data_updated {
  sql_table_name: `chb-prod-stage-oracle.prod_supply_chain.dm_dsp_shipping_data_updated`
    ;;

dimension: awb_number {
  type: string
  sql: ${TABLE}.AWB_number ;;
}

dimension: brand {
  label: "Brand"
  type: string
  sql: ${TABLE}.brand ;;
}

dimension: brand_upper {
  label: "BU Brand"
  type: string
  sql: UPPER(${brand});;
}

dimension: city {
  label: "City Name"
  type: string
  sql: ${TABLE}.city ;;
}

  dimension: attempt_1 {
    label: "First Attempt Date"
    type: date
    sql: ${TABLE}.attempt_1  ;;
  }

  dimension: attempt_2 {
    label: "Second Attempt Date"
    type: date
    sql: ${TABLE}.attempt_2  ;;
  }

  dimension: attempt_3 {
    label: "Third Attempt Date"
    type: date
    sql: ${TABLE}.attempt_3  ;;
  }

  dimension: attempt_4 {
    label: "Fourth Attempt Date"
    type: date
    sql: ${TABLE}.attempt_4  ;;
  }

  dimension: undelivered_reason {
    label: "Undelivered Reason"
    type: string
    sql: ${TABLE}.undelivered_reason ;;
  }

dimension: country {
  label: "Country"
  type: string
  map_layer_name: countries
  sql: upper(${TABLE}.country) ;;
}

dimension: no_of_attempts {
  label: "No of Attempts"
  type: number
  sql: ${TABLE}.no_of_attempts ;;
}

measure: no_of_attempts_sum {
    label: "Total Attempt Count"
    type: sum
    sql: ${no_of_attempts};;
  }

dimension: lead_time {
  label : "Lead Time In Days"
  type: number
  value_format_name: decimal_0
  sql: date_diff (${delivery_date_date}, ${pickup_date_date},day) ;;
}

dimension: lead_time_tier_number {
  label : "Lead Time In Day - Buckets Number"
  hidden :  yes
  type: number
  value_format_name: decimal_0
  sql: case when ${lead_time} <= 0 then 0
              when ${lead_time} = 1 then 1
              when ${lead_time} = 2 then 2
              when ${lead_time} = 3 then 3
              when ${lead_time} = 4 then 4
              when ${lead_time} = 5 then 5
              when ${lead_time} = 6 then 6
              when ${lead_time} = 7 then 7
              else  8
         end;;
}

dimension: lead_time_tier {
  label : "Lead Time In Day - Buckets"
  type: string
  value_format_name: decimal_0
  order_by_field: lead_time_tier_number
  sql: case when ${lead_time} <= 0 then "Same Day"
              when ${lead_time} = 1 then "Day 1"
              when ${lead_time} = 2 then "Day 2"
              when ${lead_time} = 3 then "Day 3"
              when ${lead_time} = 4 then "Day 4"
              when ${lead_time} = 5 then "Day 5"
              when ${lead_time} = 6 then "Day 6"
              when ${lead_time} = 7 then "Day 7"
              else "> 7"
         end;;
}

dimension_group: delivery_date {
  label: "Delivery"
  type: time
  timeframes: [
    raw,
    date,
    week,
    month,
    quarter,
    year
  ]
  convert_tz: no
  datatype: date
  sql: ${TABLE}.delivery_date ;;
}

dimension: dsp_name {
  label: "DSP"
  type: string
  sql: ${TABLE}.dsp_name ;;
}

dimension: dsp_reference {
  label: "Reference Number"
  type: string
  sql: ${TABLE}.dsp_reference ;;
}

dimension_group: ingestion_date {
  label: "Reporting"
  type: time
  timeframes: [
    raw,
    date,
    week,
    month,
    quarter,
    year
  ]
  convert_tz: no
  datatype: date
  sql: ${TABLE}.ingestion_date ;;
}

dimension: ownership {
  type: string
  sql: ${TABLE}.ownership ;;
}

dimension_group: pickup_date {
  label: "Pickup"
  type: time
  timeframes: [
    raw,
    date,
    week,
    month,
    quarter,
    year
  ]
  convert_tz: no
  datatype: date
  sql: ${TABLE}.pickup_date ;;
}

  dimension_group: return_date {
    label: "Return"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.returned_date ;;
  }

dimension: status {
  label: "Status"
  type: string
  sql: upper(${TABLE}.status) ;;
}

dimension: count_delivered_d {
  label: "Delivered Orders"
  type: number
  sql: case when upper(${status}) = "DELIVERED" then 1 else 0 end ;;
  drill_fields: [dsp_name]
}

measure: count {
  type: count
  drill_fields: [dsp_name]
}

measure: count_picked {
  label: "Picked orders"
  type: sum
  sql: case when upper(${status}) != "NOT PICKED" then 1 else 0 end ;;
  drill_fields: [dsp_name]
}

measure: count_notpicked {
  label: "Not Picked Orders"
  type: sum
  sql: case when upper(${status}) = "NOT PICKED" then 1 else 0 end ;;
  drill_fields: [dsp_name]
}

measure: count_delivered {
  label: "Delivered Orders"
  type: sum
  sql: case when upper(${status}) = "DELIVERED" then 1 else 0 end ;;
  drill_fields: [dsp_name]
}

measure: count_delivered_0 {
  label: "Same Day"
  type: sum
  sql: case when ${lead_time} <= 0 then ${count_delivered_d} else 0
    end ;;
  drill_fields: [dsp_name]
}

measure: count_delivered_1 {
  label: "1 Day"
  type: sum
  sql: case when ${lead_time} = 1 then ${count_delivered_d} else 0
    end ;;
  drill_fields: [dsp_name]
}

measure: count_delivered_2 {
  label: "2 Day"
  type: sum
  sql: case when ${lead_time} = 2 then ${count_delivered_d} else 0
    end ;;
  drill_fields: [dsp_name]
}


measure: count_delivered_3 {
  label: "3 Day"
  type: sum
  sql: case when ${lead_time} = 3 then ${count_delivered_d} else 0
    end ;;
  drill_fields: [dsp_name]
}

measure: count_delivered_4 {
  label: "4 Day"
  type: sum
  sql: case when ${lead_time} = 4 then ${count_delivered_d} else 0
    end ;;
  drill_fields: [dsp_name]
}

measure: count_delivered_5 {
  label: "5 Day"
  type: sum
  sql: case when ${lead_time} = 5 then ${count_delivered_d} else 0
    end ;;
  drill_fields: [dsp_name]
}

measure: count_delivered_6 {
  label: "6 Day"
  type: sum
  sql: case when ${lead_time} = 6 then ${count_delivered_d} else 0
    end ;;
  drill_fields: [dsp_name]
}

measure: count_delivered_7 {
  label: "7 Day"
  type: sum
  sql: case when ${lead_time} = 7 then ${count_delivered_d} else 0
    end ;;
  drill_fields: [dsp_name]
}

measure: count_delivered_8 {
  label: ">7"
  type: sum
  sql: case when ${lead_time} > 7 then ${count_delivered_d} else 0
    end ;;
  drill_fields: [dsp_name]
}

measure: count_notdelivered_DSP {
  label: "Not Delivered Orders DSP"
  type: sum
  sql: case when upper(${status}) = "NOT DELIVERED" and upper(${ownership}) != "CUSTOMER" then 1 else 0 end ;;
  drill_fields: [dsp_name]
}

measure: count_notdelivered_CUSTOMER {
  label: "Not Delivered Orders Customer"
  type: sum
  sql: case when upper(${status}) = "NOT DELIVERED" and upper(${ownership}) = "CUSTOMER" then 1 else 0 end ;;
  drill_fields: [dsp_name]
}

measure: count_returned {
  label: "Returned Orders"
  type: sum
  sql: case when upper(${status}) = "RETURNED" then 1 else 0 end ;;
  drill_fields: [dsp_name]
}



# measure: pick_actdate {
#   type: sum
#
#   sql: case when (${date_master.date_date}) =${pickup_date_date} then  case when upper(${status}) != "NOT PICKED" then 1 else 0 end  else 0 end  ;;
# }
#
# measure: del_actdate {
#     type: sum
#
#     sql: case when (${date_master.date_date}) = ${delivery_date_date} then case when upper(${status}) = "DELIVERED" then 1 else 0 end else 0 end  ;;
# }
#
#
# measure: ret_actdate {
#     type: sum
#
#     sql: case when (${date_master.date_date}) = ${return_date_date} then case when upper(${status}) = "RETURNED" then 1 else 0 end   else 0 end  ;;
# }

}
