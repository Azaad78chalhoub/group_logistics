view: dm_lb_shipping_logitude {
  sql_table_name: `chb-prod-stage-oracle.prod_supply_chain.ref_union_shipping_logitude_activities_reference`
    ;;

  dimension: activity_name {
    type: string
    sql: ${TABLE}.ACTIVITY_NAME ;;
  }

  dimension: bu_code {
    type: string
    sql: ${TABLE}.BU_CODE ;;
  }

  dimension: bu_code_logitude {
    type: string
    sql: ${TABLE}.BU_CODE_LOGITUDE ;;
  }

  dimension: bu_desc_icsb {
    type: string
    sql: ${TABLE}.BU_DESC_ICSB ;;
  }

  dimension: charge_country {
    type: string
    sql: ${TABLE}.CHARGE_COUNTRY ;;
  }

  dimension: charge_currency {
    type: string
    sql: ${TABLE}.CHARGE_CURRENCY ;;
  }

  dimension: charge_rate {
    type: string
    sql: ${TABLE}.CHARGE_RATE ;;
  }

  dimension: column_name {
    type: string
    sql: ${TABLE}.COLUMN_NAME ;;
  }

  dimension: consignee {
    type: string
    sql: ${TABLE}.Consignee ;;
  }

  dimension: consignee_not_importer {
    type: string
    sql: ${TABLE}.Consignee_Not_Importer ;;
  }

  dimension: consignee_receivables_accounting_card {
    type: string
    sql: ${TABLE}.Consignee_Receivables_Accounting_Card ;;
  }

  dimension: count_of_certificate_of_conformity {
    type: string
    sql: ${TABLE}.Count_of_Certificate_of_Conformity ;;
  }

  dimension: count_of_certificate_of_origin {
    type: string
    sql: ${TABLE}.Count_of_Certificate_of_Origin ;;
  }

  dimension: count_of_invoices {
    type: string
    sql: ${TABLE}.Count_of_invoices ;;
  }

  dimension: count_of_legalised_documents {
    type: string
    sql: ${TABLE}.Count_of_Legalised_Documents ;;
  }

  dimension: count_of_manual_documentation {
    type: string
    sql: ${TABLE}.Count_of_Manual_Documentation ;;
  }

  dimension: count_of_undertaking_letter {
    type: string
    sql: ${TABLE}.Count_of_Undertaking_Letter ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.COUNTRY ;;
  }

  dimension: country_of_departure {
    type: string
    sql: ${TABLE}.Country_of_Departure ;;
  }

  dimension: country_of_destination {
    type: string
    sql: ${TABLE}.Country_of_Destination ;;
  }

  dimension: counts_of_cites_local_authority {
    type: string
    sql: ${TABLE}.Counts_of_CITES_Local_Authority ;;
  }

  dimension: create_date {
    type: string
    sql: ${TABLE}.Create_Date ;;
  }

  dimension: customer_external_id {
    type: string
    sql: ${TABLE}.Customer_External_ID ;;
  }

  dimension: customs_declaration {
    type: string
    sql: ${TABLE}.Customs_Declaration ;;
  }

  dimension: delivery_date {
    type: string
    sql: ${TABLE}.Delivery_Date ;;
  }

  dimension: dgr {
    type: string
    sql: ${TABLE}.DGR ;;
  }

  dimension: direction {
    type: string
    sql: ${TABLE}.Direction ;;
  }

  dimension: e_com_y_n {
    type: string
    sql: ${TABLE}.E_COM_Y_N ;;
  }

  dimension: facility_name {
    type: string
    sql: ${TABLE}.FACILITY_NAME ;;
  }

  dimension: freight_forwarder {
    type: string
    sql: ${TABLE}.Freight_Forwarder ;;
  }

  dimension: icsb_service_name {
    type: string
    sql: ${TABLE}.ICSB_SERVICE_NAME ;;
  }

  dimension: incoterm {
    type: string
    sql: ${TABLE}.Incoterm ;;
  }

  dimension: insurance_claim_amount {
    type: string
    sql: ${TABLE}.Insurance_Claim_Amount ;;
  }

  dimension: is_courier_ {
    type: string
    sql: ${TABLE}.Is_Courier_ ;;
  }

  dimension: legal_entity {
    type: string
    sql: ${TABLE}.LEGAL_ENTITY ;;
  }

  dimension: location {
    type: string
    sql: ${TABLE}.location ;;
  }

  dimension: logitude_charge_type {
    type: string
    sql: ${TABLE}.Logitude_Charge_Type ;;
  }

  dimension: manual_y_n {
    type: string
    sql: ${TABLE}.MANUAL_Y_N ;;
  }

  dimension: no_of_brands {
    type: string
    sql: ${TABLE}.NO_OF_BRANDS ;;
  }

  dimension: notify_1 {
    type: string
    sql: ${TABLE}.Notify_1 ;;
  }

  dimension: notify_1_bu_code {
    type: string
    sql: ${TABLE}.Notify_1_BU_CODE ;;
  }

  dimension: notify_1_receivables_accounting_card {
    type: string
    sql: ${TABLE}.Notify_1_Receivables_Accounting_Card ;;
  }

  dimension: notify_2 {
    type: string
    sql: ${TABLE}.Notify_2 ;;
  }

  dimension: notify_2_bu_code {
    type: string
    sql: ${TABLE}.Notify_2_BU_CODE ;;
  }

  dimension: notify_2_receivables_accounting_card {
    type: string
    sql: ${TABLE}.Notify_2_Receivables_Accounting_Card ;;
  }

  dimension: number_of_deliveries {
    type: string
    sql: ${TABLE}.Number_of_Deliveries ;;
  }

  dimension: period_from {
    type: string
    sql: ${TABLE}.period_from ;;
  }

  dimension: period_to {
    type: string
    sql: ${TABLE}.period_to ;;
  }

  dimension: pick_up_from_date {
    type: string
    sql: ${TABLE}.Pick_up_From_Date ;;
  }

  dimension: quantity {
    type: string
    sql: ${TABLE}.QUANTITY ;;
  }

  dimension: service_type {
    type: string
    sql: ${TABLE}.SERVICE_TYPE ;;
  }

  dimension: shipment_id {
    type: string
    sql: ${TABLE}.Shipment_ID ;;
  }

  dimension: shipper {
    type: string
    sql: ${TABLE}.Shipper ;;
  }

  dimension: shipper_not_exporter {
    type: string
    sql: ${TABLE}.Shipper_Not_Exporter ;;
  }

  dimension: shipper_receivables_accounting_card {
    type: string
    sql: ${TABLE}.Shipper_Receivables_Accounting_Card ;;
  }

  dimension: split_percentage {
    type: string
    sql: ${TABLE}.SPLIT_PERCENTAGE ;;
  }

  dimension: total_amount {
    type: string
    sql: ${TABLE}.Total_Amount ;;
  }

  measure: count {
    type: count
    drill_fields: [activity_name, column_name, facility_name, icsb_service_name]
  }
}
