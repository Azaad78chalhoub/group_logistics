view: logitude_summary {
  derived_table: {
    sql:
WITH recs_and_pays AS(

SELECT
pay.shipment_no,
pay.invoice_no,
pay.charge_type_code,
pay.accounted_payables_usd
FROM
`chb-prod-supplychain-data.static_files.ing_recs_and_pays` as pay
INNER JOIN
(SELECT
shipment_no,
MAX(ingestion_time) as recenttime
FROM
`chb-prod-supplychain-data.static_files.ing_recs_and_pays`
GROUP BY shipment_no
ORDER BY recenttime DESC
) as py
ON pay.shipment_no = py.shipment_no AND pay.ingestion_time = py.recenttime

),
details AS(

SELECT
  tab.direction,
  tab.shipment_id,
  tab.shipper,
  tab.shipper_ref_1,
  tab.shipper_ref_2,
  tab.shipper_invoice_number,
  tab.consignee,
  tab.consignee_ref_1,
  tab.consignee_ref_2,
  tab.agent,
  tab.incoterm,
  tab.main_carriage_transport_mode,
  tab.on_carriage_transport_mode,
  tab.type,
  tab.freight_forwarder,
  tab.clearing_agent_import,
  tab.main_carriage_carrier,
  tab.vessel,
  tab.carrier_number,
  tab.trucker_name,
  tab.mawb_mbl,
  tab.hawb_hbl,
  tab.hawb_date,
  tab.gross_weight_kgs,
  tab.volume_m3,
  tab.chargeable_weight,
  tab.unit,
  tab.number_of_pieces,
  tab.total_packages_received,
  tab.count_of_teu,
  tab.count_of_pallets,
  tab.count_of_cartons,
  tab.count_of_cases,
  tab.count_of_others,
  tab.dgr,
  tab.routing,
  tab.number_of_pick_ups,
  tab.last_pickup_arrival_date,
  tab.pick_up_city,
  tab.pickup_country,
  tab.port_of_departure,
  tab.country_of_departure,
  tab.via_city,
  tab.country_of_destination,
  tab.port_of_destination,
  tab.number_of_deliveries,
  tab.last_delivery_arrival_date,
  tab.delivery_to_name,
  tab.delivery_to_city,
  tab.create_date,
  tab.notification_date,
  tab.goods_readiness_date,
  tab.clubbing_instruction__appointment_date,
  tab.pick_up_from_date,
  tab.required_delivery_date,
  tab.date_on_board_origin,
  tab.date_of_arrival_to_port,
  tab.import_declaration_date,
  tab.customs_clearance_date,
  tab.delivery_date,
  tab.closed_date,
  tab.include_customs,
  tab.import_declaration_number,
  tab.customs_declaration,
  tab.customs_inspection,
  tab.export_declaration_number,
  tab.export_declaration_date,
  tab.local_inspection,
  tab.shipper_invoice_value,
  tab.currency_of_shipper_invoice,
  tab.refund_inv_no_,
  tab.insurance_claim_number,
  tab.insurance_claim_currency,
  tab.insurance_claim_amount,
  tab.insurance_claim_date,
  tab.status,
  tab.is_courier,
  tab.notify_1,
  tab.notify_2,
  tab.customer_external_id,
  tab.counts_of_cites__local_authority,
  tab.count_of_manual_documentation,
  tab.consignee_receivables_accounting_card,
  tab.shipper_receivables_accounting_card,
  tab.opened_by
FROM
`chb-prod-supplychain-data.static_files.ing_shipment_details` as tab
INNER JOIN
(SELECT
shipment_id,
MAX(ingestion_time) as recenttime
FROM
`chb-prod-supplychain-data.static_files.ing_shipment_details`
GROUP BY shipment_id
ORDER BY recenttime DESC) AS ms
ON tab.shipment_id = ms.shipment_id AND tab.ingestion_time = ms.recenttime


),
events AS(

SELECT
tab1.ingestion_time,
tab1.shipment_number,
string_agg(tab1.event_name) AS event_name,
string_agg(tab1.notes) AS comments
FROM
`chb-prod-supplychain-data.static_files.ing_shipment_events` as tab1
INNER JOIN
(SELECT
shipment_number,
MAX(ingestion_time) as recenttime
FROM
`chb-prod-supplychain-data.static_files.ing_shipment_events`
GROUP BY shipment_number
ORDER BY recenttime DESC) AS ms
ON tab1.shipment_number = ms.shipment_number AND tab1.ingestion_time = ms.recenttime
GROUP BY
tab1.ingestion_time,
tab1.shipment_number
)
,
verticalmap_shipper AS(

SELECT
Name,
Vertical_Customer,
Brand
FROM
`chb-prod-supplychain-data.logitude_gs.shipper_consignee_vertical_mapping`
)
,
verticalmap_consignee AS(

SELECT
Name,
Vertical_Customer,
Brand
FROM
`chb-prod-supplychain-data.logitude_gs.shipper_consignee_vertical_mapping`

)
SELECT
  COALESCE(NULLIF(details.direction,'nan'),'0') AS direction,
  COALESCE(NULLIF(details.shipment_id,'nan'),'0') AS shipment_id,
  COALESCE(NULLIF(details.shipper,'nan'),'0') AS shipper,
  COALESCE(NULLIF(verticalmap_shipper.Vertical_Customer,'nan'),'0') AS vertical_shipper,
  COALESCE(NULLIF(verticalmap_shipper.Brand,'nan'),'0') AS brand_shipper,
  COALESCE(NULLIF(details.shipper_ref_1,'nan'),'0') AS shipper_ref,
  COALESCE(NULLIF(details.shipper_ref_2,'nan'),'0') AS purchaseorder_ref,
  COALESCE(NULLIF(details.shipper_invoice_number,'nan'),'0') AS shipper_invoice_number,
  COALESCE(NULLIF(details.consignee,'nan'),'0') AS consignee,
  COALESCE(NULLIF(verticalmap_consignee.Vertical_Customer,'nan'),'0') AS vertical_consignee,
  COALESCE(NULLIF(verticalmap_consignee.Brand,'nan'),'0') AS brand_consignee,
  COALESCE(NULLIF(details.consignee_ref_1,'nan'),'0') AS appointment_no,
  COALESCE(NULLIF(details.consignee_ref_2 ,'nan'),'0')AS po_no_nonstock,
  COALESCE(NULLIF(details.agent,'nan'),'0') AS agent,
  COALESCE(NULLIF(details.incoterm,'nan'),'0') AS incoterm,
  COALESCE(NULLIF(details.opened_by,'nan'),'0') AS opened_by,
  COALESCE(NULLIF(details.main_carriage_transport_mode,'nan'),'0') AS main_carriage_transport_mode,
  COALESCE(NULLIF(details.on_carriage_transport_mode,'nan'),'0')AS on_carriage_transport_mode,
  COALESCE(NULLIF(details.type,'nan'),'0') AS type,
  COALESCE(NULLIF(details.freight_forwarder,'nan'),'0') AS freight_forwarder,
  COALESCE(NULLIF(details.clearing_agent_import,'nan'),'0') AS clearing_agent_import,
  COALESCE(NULLIF(details.main_carriage_carrier,'nan'),'0') AS main_carriage_carrier,
  COALESCE(NULLIF(details.vessel,'nan'),'0') AS vessel,
  COALESCE(NULLIF(details.carrier_number,'nan'),'0') AS carrier_number,
  COALESCE(NULLIF(details.trucker_name,'nan'),'0') AS trucker_name,
  COALESCE(NULLIF(details.mawb_mbl,'nan'),'0') AS mawb_mbl,
  COALESCE(NULLIF(details.hawb_hbl,'nan'),'0') AS hawb_hbl,
  COALESCE(NULLIF(details.hawb_date,'nan'),'0') AS hawb_date,
  COALESCE(NULLIF(details.gross_weight_kgs,'nan'),'0') AS gross_weight_kgs,
  COALESCE(NULLIF(details.volume_m3,'nan'),'0') AS volume_m3,
  COALESCE(NULLIF(details.chargeable_weight,'nan'),'0') AS chargeable_weight,
  COALESCE(NULLIF(details.unit,'nan'),'0') AS unit,
  COALESCE(NULLIF(details.number_of_pieces,'nan'),'0') AS number_of_pieces,
  COALESCE(NULLIF(details.total_packages_received,'nan'),'0') AS total_packages_received,
  COALESCE(NULLIF(details.count_of_teu,'nan'),'0') AS count_of_teu,
  COALESCE(NULLIF(details.count_of_pallets,'nan'),'0') AS count_of_pallets,
  COALESCE(NULLIF(details.count_of_cartons,'nan'),'0') AS count_of_cartons,
  COALESCE(NULLIF(details.count_of_cases,'nan'),'0') AS count_of_cases,
  COALESCE(NULLIF(details.count_of_others,'nan'),'0') AS count_of_others,
  COALESCE(NULLIF(details.dgr,'nan'),'0') AS dgr,
  COALESCE(NULLIF(details.routing,'nan'),'0') AS routing,
  COALESCE(NULLIF(details.number_of_pick_ups,'nan'),'0') AS sales_invoice_no,
  COALESCE(NULLIF(details.last_pickup_arrival_date,'nan'),'0') AS last_pickup_arrival_date,
  COALESCE(NULLIF(details.pick_up_city,'nan'),'0') AS pick_up_city,
  COALESCE(NULLIF(details.pickup_country,'nan'),'0') AS pickup_country,
  COALESCE(NULLIF(details.port_of_departure,'nan'),'0') AS port_of_departure,
  COALESCE(NULLIF(details.country_of_departure,'nan'),'0') AS country_of_departure,
  b.continent_of_departure AS continent_of_departure,
  COALESCE(NULLIF(details.via_city,'nan'),'0') AS via_city,
  COALESCE(NULLIF(details.country_of_destination,'nan'),'0') AS country_of_destination,
  c.continent_of_destination as destination,
  COALESCE(NULLIF(details.port_of_destination,'nan'),'0') AS port_of_destination,
  COALESCE(NULLIF(details.number_of_deliveries,'nan'),'0') AS number_of_deliveries,
  COALESCE(NULLIF(details.last_delivery_arrival_date,'nan'),'0') AS last_delivery_arrival_date,
  COALESCE(NULLIF(details.delivery_to_name,'nan'),'0') AS delivery_to_name,
  COALESCE(NULLIF(details.delivery_to_city,'nan'),'0') AS delivery_to_city,
  COALESCE(NULLIF(details.customer_external_id,'nan'),'0') AS customer_external_id,
  COALESCE(NULLIF(details.counts_of_cites__local_authority,'nan'),'0') AS counts_of_cites__local_authority,
  COALESCE(NULLIF(details.count_of_manual_documentation,'nan'),'0') AS count_of_manual_documentation,
  COALESCE(NULLIF(details.consignee_receivables_accounting_card,'nan'),'0') AS consignee_receivables_accounting_card,
  COALESCE(NULLIF(details.shipper_receivables_accounting_card,'nan'),'0') AS shipper_receivables_accounting_card,
  NULLIF(details.create_date,'nan') AS create_date,
  NULLIF(details.notification_date , "nan")AS notification_date,
  NULLIF(details.goods_readiness_date ,"nan") AS goods_readiness_date,
  CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS DATE) AS clubbing_instruction__appointment_date ,
  NULLIF(details.pick_up_from_date,"nan") AS pick_up_from_date,
  CAST((CASE WHEN details.required_delivery_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y', details.required_delivery_date)END) AS DATE) AS required_delivery_date,
  CAST((CASE WHEN details.date_on_board_origin = "nan" OR details.date_on_board_origin = "NaT"THEN NULL
    WHEN details.date_on_board_origin LIKE "2021-%" OR details.date_on_board_origin LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(details.date_on_board_origin,'-')[offset(2)],0,2),'-',SPLIT(details.date_on_board_origin,'-')[offset(1)],'-',SPLIT(details.date_on_board_origin,'-')[offset(0)],' ',SPLIT(details.date_on_board_origin,' ')[offset(1)]))
  ELSE PARSE_TIMESTAMP('%d %b %Y', details.date_on_board_origin)END) AS DATE) AS date_on_board_origin,
  CAST((CASE WHEN details.date_of_arrival_to_port = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y', details.date_of_arrival_to_port)END) AS DATE) AS date_of_arrival_to_port,
  CAST((CASE WHEN details.import_declaration_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y', details.import_declaration_date)END) AS DATE) AS import_declaration_date,
  CAST((CASE WHEN details.customs_clearance_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y', details.customs_clearance_date)END) AS DATE) AS customs_clearance_date,
  NULLIF(details.delivery_date,'nan') AS delivery_date,
  CAST((CASE WHEN details.closed_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y', details.closed_date)END) AS DATE) AS closed_date,
  COALESCE(NULLIF(details.include_customs,'nan'),'0') AS include_customs,
  COALESCE(NULLIF(details.import_declaration_number,'nan'),'0') AS import_declaration_number,
  COALESCE(NULLIF(details.customs_declaration,'nan'),'0') AS customs_declaration,
  COALESCE(NULLIF(details.customs_inspection,'nan'),'0') AS customs_inspection,
  COALESCE(NULLIF(details.export_declaration_number,'nan'),'0') AS export_declaration_number,
  COALESCE(NULLIF(details.export_declaration_date,'nan'),'0') AS export_declaration_date,
  COALESCE(NULLIF(details.local_inspection,'nan'),'0') AS local_inspection,
  COALESCE(NULLIF(details.shipper_invoice_value,'nan'),'0') AS shipper_invoice_value,
  COALESCE(NULLIF(details.currency_of_shipper_invoice,'nan'),'0') AS currency_of_shipper_invoice,
  COALESCE(NULLIF(details.refund_inv_no_,'nan'),'0') AS refund_inv_no_,
  COALESCE(NULLIF(details.insurance_claim_number,'nan'),'0') AS insurance_claim_number,
  COALESCE(NULLIF(details.insurance_claim_currency,'nan'),'0') AS insurance_claim_currency,
  COALESCE(NULLIF(details.insurance_claim_amount,'nan'),'0') AS insurance_claim_amount,
  COALESCE(NULLIF(details.insurance_claim_date,'nan'),'0') AS insurance_claim_date,
  COALESCE(NULLIF(details.status,'nan'),'0') AS status,
  (CASE WHEN details.status = "Order" OR details.status = "Pick Up"  THEN " Estimated"
  ELSE "Actual" END) AS pick_up_ESTIMATED_or_ACTUAL,
  (CASE  WHEN details.status IN ("Booking arrangement","On Hand") AND details.status NOT IN("Order","Pick Up") THEN "Estimated"
  WHEN details.status NOT IN ("Booking arrangement","On Hand","Order","Pick Up") THEN "Actual" END) as ETD_or_ATD,
  (CASE WHEN details.status  = "Departed" AND details.status NOT IN("Order","Pick Up","Booking arrangement","On Hand") THEN "Estimated"
  WHEN details.status NOT IN ("Booking arrangement","On Hand","Order","Pick Up","Departed") THEN "Actual" END) AS ETA_or_ATA,
  (CASE WHEN details.status IN ("Delivery","Import Custom Clearance","Delivered to Customs Agent","Cleared") AND details.status NOT IN("Order","Pick Up","Booking arrangement","On Hand","Departed","Arrived")  THEN "Estimated"
    WHEN details.status = "Delivered" THEN "Actual" END) as delivery_ESTIMATED_or_ACTUAL,
  COALESCE(NULLIF(details.is_courier,'nan'),'False') AS is_courier,
  COALESCE(NULLIF(details.notify_1,'nan'),'0') AS notify_1,
  COALESCE(NULLIF(details.notify_2,'nan'),'0') AS notify_2,
  ROUND(SUM(CASE WHEN charge_type_code IN("AFT", "IFT", "OFT") THEN CAST(REGEXP_REPLACE(accounted_payables_usd, r',', '') AS FLOAT64) ELSE 0 END),2) AS freight_charges_usd,
  ROUND(SUM(CASE WHEN charge_type_code IN("CHD") THEN CAST(REGEXP_REPLACE(accounted_payables_usd, r',', '') AS FLOAT64) ELSE 0 END),2) AS destination_clearance_charges_usd,
  ROUND(SUM(CASE WHEN charge_type_code IN("STOR") THEN CAST(REGEXP_REPLACE(accounted_payables_usd, r',', '') AS FLOAT64) ELSE 0 END),2) AS demurrage_charges_usd,
  ROUND(SUM(CASE WHEN charge_type_code IN("INS") THEN CAST(REGEXP_REPLACE(accounted_payables_usd, r',', '') AS FLOAT64) ELSE 0 END),2) AS insurance_charges_usd,
  ROUND(SUM(CASE WHEN charge_type_code IN("CHO") THEN CAST(REGEXP_REPLACE(accounted_payables_usd, r',', '') AS FLOAT64) ELSE 0 END),2) AS origin_clearance_charges_usd,
  ROUND(SUM(CASE WHEN charge_type_code IN("CIT","COC","COO","DCC","DM") THEN CAST(REGEXP_REPLACE(accounted_payables_usd, r',', '') AS FLOAT64) ELSE 0 END),2) AS documentation_charges_usd,
  ROUND(SUM(CASE WHEN charge_type_code IN("OTHC") THEN CAST(REGEXP_REPLACE(accounted_payables_usd, r',', '') AS FLOAT64) ELSE 0 END),2) AS other_charges_usd,
  ROUND(SUM(CASE WHEN charge_type_code IN("CUDU","CG") THEN CAST(REGEXP_REPLACE(accounted_payables_usd, r',', '') AS FLOAT64) ELSE 0 END),2) AS custom_duty_charges_usd,
  ROUND(SUM(CASE WHEN charge_type_code IN("TAX") THEN CAST(REGEXP_REPLACE(accounted_payables_usd, r',', '') AS FLOAT64) ELSE 0 END),2) AS tax_usd,
  ROUND(SUM(CASE WHEN charge_type_code IN("VAT") THEN CAST(REGEXP_REPLACE(accounted_payables_usd, r',', '') AS FLOAT64) ELSE 0 END),2) AS vat_usd,
  string_agg(recs_and_pays.invoice_no ) AS invoices,
  events.event_name AS events,
  events.comments AS comments,
  (CASE WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Air" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 2
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Ocean" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Inland" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  ELSE d.Lead_time END )AS shipping_leadtime,
  TIMESTAMP_ADD((CASE
  WHEN CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS TIMESTAMP)
  <=
CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS TIMESTAMP) OR CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)AS TIMESTAMP) IS NULL
  THEN
  CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS TIMESTAMP)
   WHEN CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS TIMESTAMP)
  >=
CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS TIMESTAMP) OR CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS TIMESTAMP) IS NULL
THEN
 CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS TIMESTAMP)
  END),INTERVAL (CASE WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Air" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 2
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Ocean" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Inland" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  ELSE d.Lead_time END) DAY) AS target_delivery_date,
  (CASE
  WHEN details.delivery_date ="nan" AND CAST(CURRENT_TIMESTAMP AS DATE) > DATE_ADD((CASE
  WHEN CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS DATE)
  <=
CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE) OR CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)AS DATE) IS NULL
  THEN
  CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE)
   WHEN CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS DATE)
  >=
CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE) OR CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE) IS NULL
THEN
 CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS DATE)
  END),INTERVAL (CASE WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Air" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 2
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Ocean" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Inland" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Inland" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  ELSE d.Lead_time END) DAY) THEN "Late Pending Shipments"
  WHEN details.delivery_date !="nan" AND DATE_ADD((CASE
  WHEN CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS DATE)
  <=
CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE) OR CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)AS DATE) IS NULL
  THEN
  CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE)
   WHEN CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS DATE)
  >=
CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE) OR CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE) IS NULL
THEN
 CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS DATE)
  END),INTERVAL (CASE WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Air" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 2
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Ocean" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Inland" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  ELSE d.Lead_time END) DAY) < (CAST((CASE WHEN details.delivery_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y', details.delivery_date)END)  AS DATE))THEN "Late"
  WHEN DATE_ADD((CASE
  WHEN CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS DATE)
  <=
CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE) OR CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)AS DATE) IS NULL
  THEN
  CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE)
   WHEN CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS DATE)
  >=
CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE) OR CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE) IS NULL
THEN
 CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS DATE)
  END),INTERVAL (CASE WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Air" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 2
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Ocean" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Inland" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  ELSE d.Lead_time END) DAY) IS NULL THEN "No Target Delivery Date set"
   WHEN details.delivery_date ="nan" AND CAST(CURRENT_TIMESTAMP AS DATE) <= DATE_ADD((CASE
  WHEN CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS DATE)
  <=
CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE) OR CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)AS DATE) IS NULL
  THEN
  CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE)
   WHEN CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS DATE)
  >=
CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE) OR CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE) IS NULL
THEN
 CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS DATE)
  END),INTERVAL (CASE WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Air" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 2
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Ocean" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Inland" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  ELSE d.Lead_time END) DAY) THEN "Pending Delivery - Within SLA"
When details.delivery_date !="nan" OR details.delivery_date IS NOT NULL  AND TIMESTAMP_ADD((CASE
  WHEN CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS TIMESTAMP)
  <=
CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS TIMESTAMP) OR CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)AS TIMESTAMP) IS NULL
  THEN
  CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS TIMESTAMP)
   WHEN CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS TIMESTAMP)
  >=
CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS TIMESTAMP) OR CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS TIMESTAMP) IS NULL
THEN
 CAST((CASE WHEN details.clubbing_instruction__appointment_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.clubbing_instruction__appointment_date)END)   AS TIMESTAMP)
  END),INTERVAL (CASE WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Air" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 2
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Ocean" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  WHEN details.direction = "Import" AND details.main_carriage_transport_mode ="Inland" AND details.incoterm IN("CIF","CIP","CPT","CFR","DAP","DAT","DDP") THEN 3
  ELSE d.Lead_time END) DAY)>= CAST((CASE WHEN details.delivery_date = "nan" OR details.delivery_date = "NaT"THEN NULL
           WHEN details.delivery_date LIKE "2021-%" OR details.delivery_date LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(details.delivery_date,'-')[offset(2)],0,2),'-',SPLIT(details.delivery_date,'-')[offset(1)],'-',SPLIT(details.delivery_date,'-')[offset(0)],' ',SPLIT(details.delivery_date,' ')[offset(1)]))
      ELSE PARSE_TIMESTAMP('%d %b %Y',details.delivery_date)END) AS TIMESTAMP) THEN "On Time"
END) as delivery_sla,
(e.Lead_time + (DATE_DIFF(DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL e.Lead_time DAY),CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),WEEK))*2 +
(CASE WHEN format_datetime('%A',CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE))='Saturday' THEN 1 ELSE 0 END)+
(CASE WHEN format_datetime('%A',DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL e.Lead_time DAY)) = 'Friday' THEN 1 ELSE 0 END)
) as documentation_leadtime,
(DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL (e.Lead_time + (DATE_DIFF(DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL e.Lead_time DAY),CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),WEEK))*2 +
(CASE WHEN format_datetime('%A',CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE))='Saturday' THEN 1 ELSE 0 END)+
(CASE WHEN format_datetime('%A',DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL e.Lead_time DAY)) = 'Friday' THEN 1 ELSE 0 END)
) DAY )) as target_documentation_date,
(CASE
  WHEN details.goods_readiness_date ="nan" AND CAST(CURRENT_TIMESTAMP AS DATE) >=(DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL (e.Lead_time + (DATE_DIFF(DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL e.Lead_time DAY),CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),WEEK))*2 +
(CASE WHEN format_datetime('%A',CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE))='Saturday' THEN 1 ELSE 0 END)+
(CASE WHEN format_datetime('%A',DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL e.Lead_time DAY)) = 'Friday' THEN 1 ELSE 0 END)
) DAY )) THEN "Late Pending Shipments"
 WHEN details.goods_readiness_date ="nan" AND CAST(CURRENT_TIMESTAMP AS DATE) <= (DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL (e.Lead_time + (DATE_DIFF(DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL e.Lead_time DAY),CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),WEEK))*2 +
(CASE WHEN format_datetime('%A',CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE))='Saturday' THEN 1 ELSE 0 END)+
(CASE WHEN format_datetime('%A',DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL e.Lead_time DAY)) = 'Friday' THEN 1 ELSE 0 END)
) DAY )) THEN "Pending Documentation -Within SLA"
WHEN details.goods_readiness_date !="nan" AND CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE)
  > (DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL (e.Lead_time + (DATE_DIFF(DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL e.Lead_time DAY),CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),WEEK))*2 +
(CASE WHEN format_datetime('%A',CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE))='Saturday' THEN 1 ELSE 0 END)+
(CASE WHEN format_datetime('%A',DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL e.Lead_time DAY)) = 'Friday' THEN 1 ELSE 0 END)
) DAY )) THEN "Late"
WHEN details.direction = "Import" THEN "No Documentation SLA - Import Shipments"
WHEN CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE) IS NULL THEN "No Target documentation date set"
WHEN details.goods_readiness_date !="nan" AND CAST((CASE WHEN details.goods_readiness_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE)
  <= (DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL (e.Lead_time + (DATE_DIFF(DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL e.Lead_time DAY),CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),WEEK))*2 +
(CASE WHEN format_datetime('%A',CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE))='Saturday' THEN 1 ELSE 0 END)+
(CASE WHEN format_datetime('%A',DATE_ADD(CAST((CASE WHEN details.notification_date = "nan" THEN NULL ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.notification_date)END) AS DATE),INTERVAL e.Lead_time DAY)) = 'Friday' THEN 1 ELSE 0 END)
) DAY )) THEN "On Time"
END) as documentation_sla,
(CASE WHEN DATE_DIFF(CAST((CASE WHEN details.delivery_date = "nan" OR details.delivery_date = "NaT"THEN NULL
           WHEN details.delivery_date LIKE "2021-%" OR details.delivery_date LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(details.delivery_date,'-')[offset(2)],0,2),'-',SPLIT(details.delivery_date,'-')[offset(1)],'-',SPLIT(details.delivery_date,'-')[offset(0)],' ',SPLIT(details.delivery_date,' ')[offset(1)]))
      ELSE PARSE_TIMESTAMP('%d %b %Y',details.delivery_date)END) AS DATE),CAST((CASE WHEN details.goods_readiness_date = "nan" OR details.goods_readiness_date = "NaT" THEN NULL
  WHEN details.goods_readiness_date LIKE "2021-%" OR details.goods_readiness_date LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(details.goods_readiness_date,'-')[offset(2)],0,2),'-',SPLIT(details.goods_readiness_date,'-')[offset(1)],'-',SPLIT(details.goods_readiness_date,'-')[offset(0)],' ',SPLIT(details.goods_readiness_date,' ')[offset(1)]))
  ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE),DAY) IS NULL THEN NULL
  WHEN DATE_DIFF(CAST((CASE WHEN details.delivery_date = "nan" OR details.delivery_date = "NaT"THEN NULL
           WHEN details.delivery_date LIKE "2021-%" OR details.delivery_date LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(details.delivery_date,'-')[offset(2)],0,2),'-',SPLIT(details.delivery_date,'-')[offset(1)],'-',SPLIT(details.delivery_date,'-')[offset(0)],' ',SPLIT(details.delivery_date,' ')[offset(1)]))
      ELSE PARSE_TIMESTAMP('%d %b %Y',details.delivery_date)END) AS DATE),CAST((CASE WHEN details.goods_readiness_date = "nan" OR details.goods_readiness_date = "NaT" THEN NULL
  WHEN details.goods_readiness_date LIKE "2021-%" OR details.goods_readiness_date LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(details.goods_readiness_date,'-')[offset(2)],0,2),'-',SPLIT(details.goods_readiness_date,'-')[offset(1)],'-',SPLIT(details.goods_readiness_date,'-')[offset(0)],' ',SPLIT(details.goods_readiness_date,' ')[offset(1)]))
  ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE),DAY) IS NOT NULL AND ROUND(PERCENT_RANK() OVER (PARTITION BY details.main_carriage_transport_mode,details.direction,details.is_courier,details.dgr ,details.country_of_departure,details.country_of_destination ORDER BY DATE_DIFF(CAST((CASE WHEN details.delivery_date = "nan" OR details.delivery_date = "NaT"THEN NULL
           WHEN details.delivery_date LIKE "2021-%" OR details.delivery_date LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(details.delivery_date,'-')[offset(2)],0,2),'-',SPLIT(details.delivery_date,'-')[offset(1)],'-',SPLIT(details.delivery_date,'-')[offset(0)],' ',SPLIT(details.delivery_date,' ')[offset(1)]))
      ELSE PARSE_TIMESTAMP('%d %b %Y',details.delivery_date)END) AS DATE),CAST((CASE WHEN details.goods_readiness_date = "nan" OR details.goods_readiness_date = "NaT" THEN NULL
  WHEN details.goods_readiness_date LIKE "2021-%" OR details.goods_readiness_date LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(details.goods_readiness_date,'-')[offset(2)],0,2),'-',SPLIT(details.goods_readiness_date,'-')[offset(1)],'-',SPLIT(details.goods_readiness_date,'-')[offset(0)],' ',SPLIT(details.goods_readiness_date,' ')[offset(1)]))
  ELSE PARSE_TIMESTAMP('%d %b %Y %T', details.goods_readiness_date)END)  AS DATE),DAY))*100)<=95 THEN "YES" ELSE "NO" END) AS tp95_yes_no
FROM
  details
LEFT JOIN
  recs_and_pays
ON
  details.shipment_id = recs_and_pays.shipment_no
LEFT JOIN
  events
ON
  details.shipment_id = events.shipment_number
LEFT JOIN
  verticalmap_shipper
ON
  details.shipper = verticalmap_shipper.Name
LEFT JOIN
  verticalmap_consignee
ON
  details.consignee = verticalmap_consignee.Name
LEFT JOIN
`chb-prod-supplychain-data.logitude_gs.country_mapping`  AS b
ON details.country_of_departure = b.country_of_departure
LEFT JOIN
`chb-prod-supplychain-data.logitude_gs.country_mapping`  AS c
ON details.country_of_destination = c.country_of_destination
LEFT JOIN
`chb-prod-supplychain-data.logitude_gs.logitude_summary_freight_leadtime_mapping` AS d
ON details.main_carriage_transport_mode = d.Mode_of_transport
AND CAST(COALESCE(NULLIF(details.is_courier,'nan'),'False') as BOOLEAN)  = d.is_courier
AND CAST(details.DGR as BOOLEAN) = d.is_DG
AND (details.country_of_departure = d.Country_of_departure
OR b.continent_of_departure = d.Continent_of_departure )
AND
(c.country_of_destination = d.country_of_destination OR c.continent_of_destination = d.Mapped_country_of_destination)
LEFT JOIN `chb-prod-supplychain-data.logitude_gs.documentation_lead_time` AS e
ON details.country_of_departure = e.Origin
AND (details.country_of_destination = e.Destination OR c.continent_of_destination = e.Destination)
GROUP BY
  details.direction,
  details.shipment_id,
  details.shipper,
  verticalmap_shipper.Vertical_Customer,
  verticalmap_shipper.Brand,
  details.shipper_ref_1,
  details.shipper_ref_2,
  details.shipper_invoice_number,
  details.consignee,
  verticalmap_consignee.Vertical_Customer,
  verticalmap_consignee.Brand,
  details.consignee_ref_1,
  details.consignee_ref_2,
  details.agent,
  details.incoterm,
  details.opened_by,
  details.main_carriage_transport_mode,
  details.on_carriage_transport_mode,
  details.type,
  details.freight_forwarder,
  details.clearing_agent_import,
  details.main_carriage_carrier,
  details.vessel,
  details.carrier_number,
  details.trucker_name,
  details.mawb_mbl,
  details.hawb_hbl,
  details.hawb_date,
  details.gross_weight_kgs,
  details.volume_m3,
  details.chargeable_weight,
  details.unit,
  details.number_of_pieces,
  details.total_packages_received,
  details.count_of_teu,
  details.count_of_pallets,
  details.count_of_cartons,
  details.count_of_cases,
  details.count_of_others,
  details.dgr,
  details.routing,
  details.number_of_pick_ups,
  details.last_pickup_arrival_date,
  details.pick_up_city,
  details.pickup_country,
  details.port_of_departure,
  details.country_of_departure,
  details.via_city,
  details.country_of_destination,
  details.port_of_destination,
  details.number_of_deliveries,
  details.last_delivery_arrival_date,
  details.delivery_to_name,
  details.delivery_to_city,
  details.create_date,
  details.notification_date,
  details.goods_readiness_date,
  details.clubbing_instruction__appointment_date,
  details.pick_up_from_date,
  details.required_delivery_date,
  details.date_on_board_origin,
  details.date_of_arrival_to_port,
  details.import_declaration_date,
  details.customs_clearance_date,
  details.delivery_date,
  details.closed_date,
  details.include_customs,
  details.import_declaration_number,
  details.customs_declaration,
  details.customs_inspection,
  details.export_declaration_number,
  details.export_declaration_date,
  details.local_inspection,
  details.shipper_invoice_value,
  details.currency_of_shipper_invoice,
  details.refund_inv_no_,
  details.insurance_claim_number,
  details.insurance_claim_currency,
  details.insurance_claim_amount,
  details.insurance_claim_date,
  details.status,
  details.is_courier,
  details.notify_1,
  details.notify_2,
  c.continent_of_destination,
  b.continent_of_departure,
  details.customer_external_id,
  details.counts_of_cites__local_authority,
  details.count_of_manual_documentation,
  details.consignee_receivables_accounting_card,
  details.shipper_receivables_accounting_card,
  events,
  comments,
  d.Lead_time,
  e.Lead_time
  ;;
  }


  dimension: agent {
    type: string
    sql: ${TABLE}.agent ;;
  }
  dimension: customer_external_id {
    type: string
    sql: ${TABLE}.customer_external_id ;;
  }
  dimension: counts_of_cites__local_authority {
    type: string
    sql: ${TABLE}.counts_of_cites__local_authority ;;
  }
  dimension: count_of_manual_documentation {
    type: string
    sql: ${TABLE}.count_of_manual_documentation ;;
  }
  dimension: consignee_receivables_accounting_card {
    type: string
    sql:  ${TABLE}.consignee_receivables_accounting_card;;
  }
    dimension: shipper_receivables_accounting_card {
      type: string
      sql:  ${TABLE}.shipper_receivables_accounting_card;;
    }

  dimension: carrier_number {
    type: string
    sql: ${TABLE}.carrier_number ;;
  }
  dimension: tp95_yes_no {
    type: string
    sql: ${TABLE}.tp95_yes_no ;;
  }

  dimension: chargeable_weight {
    type: string
    sql: ${TABLE}.chargeable_weight ;;
  }
  dimension: opened_by {
    type: string
    sql: CASE WHEN ${direction}= "Export" THEN ${TABLE}.opened_by ELSE NULL END  ;;
  }

  dimension: destination_clearance_charges_usd {
    type: number
    sql: ${TABLE}.destination_clearance_charges_usd ;;
  }

  dimension: origin_clearance_charges_usd {
    type: number
    sql: ${TABLE}.origin_clearance_charges_usd ;;
  }

  dimension: demurrage_charges_usd {
    type: number
    sql: ${TABLE}.demurrage_charges_usd ;;
  }

  dimension: insurance_charges_usd {
    hidden: no
    type: number
    sql: ${TABLE}.insurance_charges_usd ;;
  }

  dimension: documentation_charges_usd {
    type: number
    sql: ${TABLE}.documentation_charges_usd ;;
  }


  dimension: clearing_agent_import {
    type: string
    sql: ${TABLE}.clearing_agent_import ;;
  }

  dimension: closed_date {
    type: string
    sql: ${TABLE}.closed_date ;;
  }

  dimension: clubbing_instruction__appointment_date {
    type: string
    sql: ${TABLE}.clubbing_instruction__appointment_date ;;
  }

  dimension: delivery_sla {
    type: string
    sql: ${TABLE}.delivery_sla ;;
  }

  dimension: target_delivery_date {
    type: string
    sql: ${TABLE}.target_delivery_date;;
  }

  dimension_group: target_delivery_date_filter {
    type: time
    timeframes: [raw,
      time,
      date,
      week,
      month,
      quarter,
      year]
    sql: CAST(${TABLE}.target_delivery_date AS TIMESTAMP) ;;
  }

  dimension: consignee {
    type: string
    sql: TRIM(${TABLE}.consignee) ;;
  }

  dimension: appointment_no {
    type: string
    sql: ${TABLE}.appointment_no ;;
  }

  dimension: po_no_nonstock {
    type: string
    sql: ${TABLE}.po_no_nonstock ;;
  }

  dimension: count_of_cartons {
    type: string
    sql: ${TABLE}.count_of_cartons ;;
  }

  dimension: count_of_cases {
    type: string
    sql: ${TABLE}.count_of_cases ;;
  }

  dimension: count_of_others {
    type: string
    sql: ${TABLE}.count_of_others ;;
  }

  dimension: count_of_teu {
    type: string
    sql: ${TABLE}.count_of_teu ;;
  }

  dimension: country_of_destination {
    type: string
    sql: ${TABLE}.country_of_destination ;;
  }

  dimension: country_of_departure {
    type: string
    sql: ${TABLE}.country_of_departure ;;
  }

  dimension: currency_of_shipper_invoice {
    type: string
    sql: ${TABLE}.currency_of_shipper_invoice ;;
  }

  dimension: custom_duty_charges_usd {
    type: number
    sql: ${TABLE}.custom_duty_charges_usd ;;
  }

  dimension: customs_clearance_date {
    type: string
    sql: ${TABLE}.customs_clearance_date ;;
  }

  dimension: customs_declaration {
    type: string
    sql: ${TABLE}.customs_declaration ;;
  }

  dimension: customs_inspection {
    type: string
    sql: ${TABLE}.customs_inspection ;;
  }

  dimension: date_of_arrival_to_port {
    type: string
    sql: ${TABLE}.date_of_arrival_to_port ;;
  }

  dimension: date_on_board_origin {
    type: string
    sql: ${TABLE}.date_on_board_origin ;;
  }

  dimension: delivery_date {
    type: string
    sql: ${TABLE}.delivery_date ;;
  }

  dimension: delivery_to_city {
    type: string
    sql: ${TABLE}.delivery_to_city ;;
  }

  dimension: delivery_to_name {
    type: string
    sql: ${TABLE}.delivery_to_name ;;
  }

  dimension: dgr {
    type: string
    sql: ${TABLE}.dgr ;;
  }

  dimension: direction {
    type: string
    sql: ${TABLE}.direction ;;
  }

  dimension: events {
    type: string
    sql: ${TABLE}.events ;;
  }
  dimension: documentation_sla {
    type: string
    sql: ${TABLE}.documentation_sla ;;
  }

  dimension: comments {
    type: string
    sql: ${TABLE}.comments ;;
  }

  dimension: export_declaration_date {
    type: string
    sql: ${TABLE}.export_declaration_date ;;
  }

  dimension: export_declaration_number {
    type: string
    sql: ${TABLE}.export_declaration_number ;;
  }

  dimension: freight_charges_usd {
    type: number
    sql: ${TABLE}.freight_charges_usd ;;
  }

  dimension: freight_forwarder {
    type: string
    sql: ${TABLE}.freight_forwarder ;;
  }

  dimension: goods_readiness_date {
    type: string
    sql: ${TABLE}.goods_readiness_date ;;
  }

  dimension: gross_weight_kgs {
    type: string
    sql: ${TABLE}.gross_weight_kgs ;;
  }

  dimension: hawb_date {
    type: string
    sql: ${TABLE}.hawb_date ;;
  }

  dimension: hawb_hbl {
    type: string
    sql: ${TABLE}.hawb_hbl ;;
  }

  dimension: import_declaration_date {
    type: string
    sql: ${TABLE}.import_declaration_date ;;
  }

  dimension: import_declaration_number {
    type: string
    sql: ${TABLE}.import_declaration_number ;;
  }

  dimension: include_customs {
    type: string
    sql: ${TABLE}.include_customs ;;
  }

  dimension: incoterm {
    type: string
    sql: ${TABLE}.incoterm ;;
  }

  dimension: insurance_claim_amount {
    type: string
    sql: ${TABLE}.insurance_claim_amount ;;
  }

  dimension: insurance_claim_currency {
    type: string
    sql: ${TABLE}.insurance_claim_currency ;;
  }

  dimension: continent_of_departure {
    type: string
    sql: ${TABLE}.continent_of_departure ;;
  }

  dimension: destination {
    type: string
    sql: ${TABLE}.destination ;;
  }

  dimension: insurance_claim_number {
    type: string
    sql: ${TABLE}.insurance_claim_number ;;
  }

  dimension: invoices {
    type: string
    sql: ${TABLE}.invoices ;;
  }

  dimension: is_courier {
    type: string
    sql: ${TABLE}.is_courier ;;
  }

  dimension: last_delivery_arrival_date {
    type: string
    sql: ${TABLE}.last_delivery_arrival_date ;;
  }

  dimension: last_pickup_arrival_date {
    type: string
    sql: ${TABLE}.last_pickup_arrival_date ;;
  }

  dimension: local_inspection {
    type: string
    sql: ${TABLE}.local_inspection ;;
  }

  dimension: main_carriage_carrier {
    type: string
    sql: ${TABLE}.main_carriage_carrier ;;
  }

  dimension: shipping_leadtime {
    type: number
    sql: ${TABLE}.shipping_leadtime ;;
  }

  dimension: documentation_leadtime {
    type: number
    sql: ${TABLE}.documentation_leadtime ;;
  }

  dimension: target_documentation_date {
    type: string
    sql: ${TABLE}.target_documentation_date ;;
  }
  dimension_group: target_documentation_date_filter {
    type: time
    timeframes: [raw,
      time,
      date,
      week,
      month,
      quarter,
      year]
    sql: CAST(${TABLE}.target_documentation_date AS TIMESTAMP) ;;
  }


  dimension: main_carriage_transport_mode {
    type: string
    sql: ${TABLE}.main_carriage_transport_mode ;;
  }

  dimension: mawb_mbl {
    type: string
    sql: ${TABLE}.mawb_mbl ;;
  }

  dimension: notification_date {
    type: string
    sql: ${TABLE}.notification_date ;;
  }

  dimension: create_date {
    type: string
    sql: ${TABLE}.create_date ;;
  }

  dimension_group: create_date_filter {
    label: "Creation Date"
    type: time
    timeframes: [raw,time,date,week,month,quarter,year]
    sql: CAST((CASE WHEN ${create_date} = "nan" OR ${create_date} = "NaT"THEN NULL
           WHEN ${create_date} LIKE "2021-%" OR ${create_date} LIKE "2020-%" THEN PARSE_TIMESTAMP("%d-%m-%Y %T",CONCAT(SUBSTRING(SPLIT(${create_date},'-')[offset(2)],0,2),'-',SPLIT(${create_date},'-')[offset(1)],'-',SPLIT(${create_date},'-')[offset(0)],' ',SPLIT(${create_date},' ')[offset(1)]))
      ELSE PARSE_TIMESTAMP("%d/%m/%Y",${create_date})END) AS TIMESTAMP);;
  }
  dimension_group: goods_readiness_date_filter  {
    label: "Goods Readiness Date"
    type: time
    timeframes: [raw,time,date,week,month,quarter,year]
    sql:  CAST((CASE WHEN ${goods_readiness_date} = "nan" OR ${goods_readiness_date} = "NaT" THEN NULL
  WHEN ${goods_readiness_date} LIKE "2021-%" OR ${goods_readiness_date} LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(${goods_readiness_date},'-')[offset(2)],0,2),'-',SPLIT(${goods_readiness_date},'-')[offset(1)],'-',SPLIT(${goods_readiness_date},'-')[offset(0)],' ',SPLIT(${goods_readiness_date},' ')[offset(1)]))
  ELSE PARSE_TIMESTAMP('%d %b %Y %T', ${goods_readiness_date})END)  AS TIMESTAMP) ;;
  }

  dimension_group: delivery_date_filter {
    label: "Delivery Date"
    type: time
    timeframes: [raw,time,date,week,month,quarter,year]
    sql: CAST((CASE WHEN ${delivery_date} = "nan" OR ${delivery_date} = "NaT"THEN NULL
           WHEN ${delivery_date} LIKE "2021-%" OR ${delivery_date} LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(${delivery_date},'-')[offset(2)],0,2),'-',SPLIT(${delivery_date},'-')[offset(1)],'-',SPLIT(${delivery_date},'-')[offset(0)],' ',SPLIT(${delivery_date},' ')[offset(1)]))
      ELSE PARSE_TIMESTAMP('%d %b %Y',${delivery_date})END) AS TIMESTAMP) ;;
  }
  dimension_group: notification_date_filter {
    label: "Notification Date"
    type: time
    timeframes: [raw,time,date,week,month,quarter,year]
    sql: CAST((CASE WHEN ${notification_date} = "nan" OR ${notification_date} = "NaT" THEN NULL
    WHEN ${notification_date} LIKE "2021-%" OR ${notification_date} LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(${notification_date},'-')[offset(2)],0,2),'-',SPLIT(${notification_date},'-')[offset(1)],'-',SPLIT(${notification_date},'-')[offset(0)],' ',SPLIT(${notification_date},' ')[offset(1)]))
    ELSE PARSE_TIMESTAMP('%d %b %Y %T',${notification_date})END) AS TIMESTAMP) ;;
  }
  dimension_group: pickup_date_filter {
    label: "Pickup Date"
    type: time
    timeframes: [raw,time,date,week,month,quarter,year]
    sql: CAST((CASE WHEN ${pick_up_from_date} = "nan" OR ${pick_up_from_date} = "NaT" THEN NULL
          WHEN ${pick_up_from_date} LIKE "2021-%" OR ${pick_up_from_date} LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(${pick_up_from_date},'-')[offset(2)],0,2),'-',SPLIT(${pick_up_from_date},'-')[offset(1)],'-',SPLIT(${pick_up_from_date},'-')[offset(0)],' ',SPLIT(${pick_up_from_date},' ')[offset(1)]))
          ELSE PARSE_TIMESTAMP('%d %b %Y',${pick_up_from_date})END) AS TIMESTAMP) ;;
  }
dimension: transit_time {
  type: number
  sql: CASE WHEN ${delivery_date_filter_date} IS NULL THEN NULL ELSE DATE_DIFF(${delivery_date_filter_date},${goods_readiness_date_filter_date},DAY) END ;;
}

  dimension: notify_1 {
    type: string
    sql: ${TABLE}.notify_1 ;;
  }

  dimension: notify_2 {
    type: string
    sql: ${TABLE}.notify_2 ;;
  }

  dimension: number_of_deliveries {
    type: string
    sql: ${TABLE}.number_of_deliveries ;;
  }

  dimension: sales_invoice_no {
    type: string
    sql: ${TABLE}.sales_invoice_no ;;
  }

  dimension: number_of_pieces {
    type: string
    sql: ${TABLE}.number_of_pieces ;;
  }

  dimension: other_charges_usd {
    type: number
    sql: ${TABLE}.other_charges_usd ;;
  }

  dimension: pick_up_from_date {
    type: string
    sql: ${TABLE}.pick_up_from_date ;;
  }

  dimension: pickup_country {
    type: string
    sql: ${TABLE}.pickup_country ;;
  }

  dimension: port_of_departure {
    type: string
    sql: ${TABLE}.port_of_departure ;;
  }

  dimension: port_of_destination {
    type: string
    sql: ${TABLE}.port_of_destination ;;
  }

  dimension: refund_inv_no_ {
    type: string
    sql: ${TABLE}.refund_inv_no_ ;;
  }

  dimension: required_delivery_date {
    type: string
    sql: ${TABLE}.required_delivery_date ;;
  }

  dimension: routing {
    type: string
    sql: ${TABLE}.routing ;;
  }

  dimension: shipment_id {
    type: string
    sql: ${TABLE}.shipment_id ;;
  }

  dimension: shipper {
    type: string
    sql: TRIM(${TABLE}.shipper) ;;
  }

  dimension: vertical_shipper {
    type: string
    sql: TRIM(UPPER(${TABLE}.vertical_shipper)) ;;
  }

  dimension: brand_shipper {
    type: string
    sql: TRIM(UPPER(${TABLE}.brand_shipper)) ;;
  }

  dimension: vertical_consignee {
    type: string
    sql: TRIM(UPPER(${TABLE}.vertical_consignee)) ;;
  }

  dimension: brand_consignee {
    type: string
    sql: TRIM(UPPER(${TABLE}.brand_consignee)) ;;
  }
  dimension: brand_consignee_accessfilter {
    hidden: yes
    type: string
    sql:  CASE WHEN ${brand_consignee} IS NULL THEN "None"ELSE ${brand_consignee} END ;;
  }
  dimension: brand_shipper_accessfilter {
    hidden: yes
    type: string
    sql: CASE WHEN ${brand_shipper} IS NULL THEN "None" ELSE ${brand_shipper} END ;;
  }


  dimension: shipper_invoice_number {
    type: string
    sql: ${TABLE}.shipper_invoice_number ;;
  }

  dimension: shipper_invoice_value {
    type: string
    sql: ${TABLE}.shipper_invoice_value ;;
  }

  dimension: shipper_ref {
    type: string
    sql: ${TABLE}.shipper_ref ;;
  }

  dimension: purchaseorder_ref {
    type: string
    sql: ${TABLE}.purchaseorder_ref;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: pick_up_ESTIMATED_or_ACTUAL{
    type: string
    sql: ${TABLE}.pick_up_ESTIMATED_or_ACTUAL ;;
  }

  dimension: ETD_or_ATD {
    type: string
    sql:  ${TABLE}.ETD_or_ATD;;
  }

  dimension: delivery_ESTIMATED_or_ACTUAL{
    type: string
    sql: ${TABLE}.delivery_ESTIMATED_or_ACTUAL ;;
  }

  dimension: ETA_or_ATA {
    type: string
    sql:  ${TABLE}.ETA_or_ATA;;
  }

  dimension: total_packages_received {
    type: string
    sql: ${TABLE}.total_packages_received ;;
  }

  dimension: trucker_name {
    type: string
    sql: ${TABLE}.trucker_name ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension: unit {
    type: string
    sql: ${TABLE}.unit ;;
  }

  dimension: vat_usd {
    type: number
    sql: ${TABLE}.vat_usd ;;
  }

  dimension: tax_usd {
    type: number
    sql: ${TABLE}.tax_usd ;;
  }

  dimension: vessel {
    type: string
    sql: ${TABLE}.vessel ;;
  }

  dimension: via_city {
    type: string
    sql: ${TABLE}.via_city ;;
  }

  dimension: volume_m3 {
    type: string
    sql: ${TABLE}.volume_m3 ;;
  }

  measure: count {
    type: count
    drill_fields: [delivery_country_drill_fields*]
  }
  measure: sum_cw {
    label: "Chargeable Weight"
    type: sum
    sql:  CASE WHEN ${TABLE}.chargeable_weight = "nan" OR ${TABLE}.chargeable_weight ="0" THEN NULL ELSE CAST(${TABLE}.chargeable_weight AS FLOAT64) END;;
    drill_fields: [delivery_country_drill_fields*]
    value_format: "0.00"
  }
  measure: sum_gw {
    label: "Gross Weight"
    type: sum
    sql:  CASE WHEN ${TABLE}.gross_weight_kgs = "nan" OR ${TABLE}.gross_weight_kgs ="0" THEN NULL ELSE CAST(${TABLE}.gross_weight_kgs AS FLOAT64) END;;
    drill_fields: [delivery_country_drill_fields*]
    value_format: "0.00"
  }

  measure: sum_pc {
    label: "Freight Charges (USD)"
    type: sum
    sql:  ${TABLE}.freight_charges_usd;;
    drill_fields: [delivery_country_drill_fields*]
    value_format: "0.00"
  }
  measure: sum_or {
    label: "Origin Clearance Charges (USD)"
    type: sum
    sql:  ${TABLE}.origin_clearance_charges_usd;;
    drill_fields: [delivery_country_drill_fields*]
    value_format: "0.00"
  }
  measure: sum_de {
    label: "Destination Clearance Charges (USD)"
    type: sum
    sql:  ${TABLE}.destination_clearance_charges_usd;;
    drill_fields: [delivery_country_drill_fields*]
    value_format: "0.00"
  }
  measure: sum_dem {
    label: "Demurrage Charges (USD)"
    type: sum
    sql:  ${TABLE}.demurrage_charges_usd;;
    drill_fields: [delivery_country_drill_fields*]
    value_format: "0.00"
  }
  measure: sum_oth {
    label: "Other Charges (USD)"
    type: sum
    sql:  ${TABLE}.other_charges_usd;;
    drill_fields: [delivery_country_drill_fields*]
    value_format: "0.00"
  }
  measure: sum_doc {
    label: "Documentation Charges (USD)"
    type: sum
    sql:  ${TABLE}.documentation_charges_usd;;
    drill_fields: [delivery_country_drill_fields*]
    value_format: "0.00"
  }
  measure: sum_cust {
    label: "Customs Duty Charges (USD)"
    type: sum
    sql:  ${TABLE}.custom_duty_charges_usd;;
    drill_fields: [delivery_country_drill_fields*]
    value_format: "0.00"
  }
  measure: sum_ins {
    label: "Insurance Charges (USD)"
    type: sum
    sql:  ${TABLE}.insurance_charges_usd;;
    drill_fields: [delivery_country_drill_fields*]
    value_format: "0.00"
  }
  measure: sum_vat {
    label: "VAT Charges (USD)"
    type: sum
    sql:  ${TABLE}.vat_usd;;
    drill_fields: [delivery_country_drill_fields*]
    value_format: "0.00"
  }
  measure: sum_tax {
    label: "TAX Charges (USD)"
    type: sum
    sql:  ${TABLE}.tax_usd;;
    drill_fields: [delivery_country_drill_fields*]
    value_format: "0.00"
  }


  measure: Late_by {
    label: "Late by in days (For Late shipments only)"
    type: number
    sql: CASE WHEN ${delivery_sla} = "Late" THEN DATE_DIFF(${delivery_date_filter_date},${target_delivery_date_filter_date},DAY) ELSE NULL END  ;;
  }

  measure:  documentation_days_countdown{
    hidden: no
    label: "Documentation days due"
    type: number
    sql: CASE WHEN ${direction} = "Export" THEN DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY)
          ELSE NULL
          END;;
  }
  measure: delivery_days_countdown {
    hidden: no
    label: "Delivery days due (For pending shipments only)"
    type: number
    sql: CASE WHEN ${target_delivery_date} IS NOT NULL AND ${delivery_date_filter_date} IS NULL THEN DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_import_france_saudi_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="France" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
    ELSE NULL
    END;;
  }
  dimension: ocean_import_france_bahrain_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="France" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_import_france_kuwait_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="France" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_import_US_saudi_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="United States" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
    dimension: ocean_import_US_bahrain_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="United States" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: ocean_import_US_qatar_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="United States" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: ocean_import_ITA_qatar_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="Italy" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: ocean_import_HK_qatar_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: ocean_import_france_qatar_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="France" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_import_US_kuwait_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="United States" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_import_US_saudi_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="United States" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_import_HK_saudi_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_import_HK_kuwait_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: ocean_import_HK_bahrain_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_import_ITA_saudi_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="Italy" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: ocean_import_ITA_bahrain_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="Italy" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_import_ITA_kuwait_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="Italy" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_export_UAE_saudi_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: ocean_export_UAE_egypt_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Egypt" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: land_export_UAE_egypt_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Egypt" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_export_UAE_egypt_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Egypt" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_export_UAE_egypt_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Egypt" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: ocean_export_UAE_qatar_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_export_UAE_kuwait_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: ocean_export_UAE_bahrain_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }


  dimension: air_import_US_saudi_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United States" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_US_qatar_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United States" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_US_bahrain_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United States" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_US_saudi_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United States" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_US_qatar_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United States" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_US_bahrain_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United States" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_US_kuwait_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United States" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_france_kuwait_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_france_qatar_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_france_qatar_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_ITA_kuwait_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Italy" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_ITA_egypt_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Italy" AND ${country_of_destination}="Egypt" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_HK_egypt_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="Egypt" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_france_egypt_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="Egypt" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_france_egypt_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="Egypt" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_ITA_qatar_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Italy" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_ITA_qatar_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Italy" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }



  dimension: air_import_ITA_bahrain_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Italy" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_HK_kuwait_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_HK_qatar_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_HK_qatar_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_US_kuwait_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United States" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_france_kuwait_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_ITA_kuwait_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Italy" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_ITA_bahrain_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Italy" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_HK_kuwait_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_ITA_saudi_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Italy" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_ITA_saudi_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Italy" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_HK_saudi_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_HK_bahrain_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_HK_saudi_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_import_HK_bahrain_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_france_saudi_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
    dimension: air_import_france_bahrain_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_france_saudi_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_france_bahrain_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_france_saudi_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_france_bahrain_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }


  dimension: land_export_uae_saudi_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: land_export_uae_kuwait_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: land_export_uae_bahrain_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: land_export_uae_qatar_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: land_export_uae_saudi_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: land_export_uae_qatar_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: land_export_uae_kuwait_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: land_export_uae_bahrain_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }



  dimension: land_export_uae_saudi_courier_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: land_export_uae_qatar_courier_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }


  dimension: land_export_uae_saudi_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: land_export_uae_qatar_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: land_export_uae_kuwait_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: land_export_uae_bahrain_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Inland" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_export_uae_saudi_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_export_uae_qatar_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_export_uae_bahrain_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_export_uae_kuwait_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }


  dimension: air_export_uae_saudi_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(${delivery_date_filter_date},CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_export_uae_qatar_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(${delivery_date_filter_date},CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_export_uae_bahrain_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(${delivery_date_filter_date},CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_export_uae_kuwait_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(${delivery_date_filter_date},CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_export_uae_kuwait_courier_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(${delivery_date_filter_date},CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_export_uae_qatar_courier_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(${delivery_date_filter_date},CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_export_uae_bahrain_courier_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Bahrain" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(${delivery_date_filter_date},CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_export_uae_kuwait_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Kuwait" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(${delivery_date_filter_date},CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  dimension: air_export_uae_qatar_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Qatar" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(${delivery_date_filter_date},CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

measure: tp95_air_export_uae_saudi_courier{
    hidden: yes
    type: string
    sql: CASE WHEN ${air_export_uae_saudi_courier_leadtime} IS NULL THEN NULL
    WHEN ${air_export_uae_saudi_courier_leadtime} IS NOT NULL AND ROUND(PERCENT_RANK() OVER(ORDER BY ${air_export_uae_saudi_courier_leadtime})*100)<=95 THEN "YES" ELSE "NO" END ;;
  }


  measure: tp95_avg {
    type: average
    sql: CASE WHEN ${tp95_logitude.tp95_air_export_uae_saudi_courier} = "YES" THEN ${air_export_uae_saudi_courier_leadtime}
          WHEN ${tp95_logitude.tp95_air_export_uae_saudi} = "YES" THEN ${air_export_uae_saudi_leadtime}
        WHEN ${tp95_logitude.tp95_air_export_uae_saudi_dg}="YES" THEN ${air_export_uae_saudi_dg_leadtime}
        WHEN ${tp95_logitude.tp95_air_export_uae_saudi_courier_dg} = "YES" THEN ${air_export_uae_saudi_courier_dg_leadtime}
        WHEN ${tp95_logitude.tp95_land_export_uae_saudi_dg}="YES" THEN ${land_export_uae_saudi_dg_leadtime}
        WHEN ${tp95_logitude.tp95_land_export_uae_saudi_courier_dg}="YES" THEN ${land_export_uae_saudi_courier_dg_leadtime}
        WHEN ${tp95_logitude.tp95_land_export_uae_saudi_courier}="YES" THEN ${land_export_uae_saudi_courier_leadtime}
        WHEN ${tp95_logitude.tp95_land_export_uae_saudi} ="YES" THEN ${land_export_uae_saudi_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_US_saudi_dg} ="YES" THEN ${air_import_US_saudi_dg_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_US_saudi} ="YES" THEN ${air_import_US_saudi_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_ITA_saudi_dg} ="YES" THEN ${air_import_ITA_saudi_dg_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_ITA_saudi} ="YES" THEN ${air_import_ITA_saudi_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_HK_saudi_dg} ="YES" THEN ${air_import_HK_saudi_dg_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_HK_saudi} ="YES" THEN ${air_import_HK_saudi_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_france_saudi_dg} ="YES" THEN ${air_import_france_saudi_dg_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_france_saudi_courier} ="YES" THEN ${air_import_france_saudi_courier_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_france_saudi} ="YES" THEN ${air_import_france_saudi_leadtime}
        WHEN ${tp95_logitude.tp95_ocean_import_france_saudi} ="YES" THEN ${ocean_import_france_saudi_leadtime}
        WHEN ${tp95_logitude.tp95_ocean_import_US_saudi} ="YES" THEN ${ocean_import_US_saudi_leadtime}
        WHEN ${tp95_logitude.tp95_ocean_import_ITA_saudi} ="YES" THEN ${ocean_import_ITA_saudi_leadtime}
         WHEN ${tp95_logitude.tp95_ocean_import_HK_saudi} ="YES" THEN ${ocean_import_HK_saudi_leadtime}
         WHEN ${tp95_logitude.tp95_ocean_import_france_uae} ="YES" THEN ${ocean_import_france_uae_leadtime}
        WHEN ${tp95_logitude.tp95_ocean_import_ita_uae} ="YES" THEN
          ${ocean_import_ita_uae_leadtime}
        WHEN ${tp95_logitude.tp95_ocean_import_HK_uae} ="YES" THEN
          ${ocean_import_HK_uae_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_france_uae_dg} ="YES" THEN
          ${air_import_france_uae_dg_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_france_uae_courier} ="YES" THEN
          ${air_import_france_uae_courier_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_france_uae} ="YES" THEN
          ${air_import_france_uae_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_HK_uae_dg} ="YES" THEN
          ${air_import_HK_uae_dg_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_HK_uae} ="YES" THEN
          ${air_import_HK_uae_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_ITA_uae_dg} ="YES" THEN
          ${air_import_ITA_uae_dg_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_ITA_uae} ="YES" THEN
          ${air_import_ITA_uae_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_ITA_uae_courier} ="YES" THEN
          ${air_import_ITA_uae_courier_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_US_uae_dg} ="YES" THEN
          ${air_import_US_uae_dg_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_US_uae} ="YES" THEN
          ${air_import_US_uae_leadtime}
        WHEN ${tp95_logitude.tp95_ocean_import_US_kuwait} ="YES" THEN
          ${ocean_import_US_kuwait_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_US_kuwait} ="YES" THEN
          ${air_import_US_kuwait_leadtime}
         WHEN ${tp95_logitude.tp95_air_import_US_kuwait_dg} ="YES" THEN
          ${air_import_US_kuwait_dg_leadtime}
         WHEN ${tp95_logitude.tp95_ocean_import_france_kuwait} ="YES" THEN
          ${ocean_import_france_kuwait_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_france_kuwait} ="YES" THEN
          ${air_import_france_kuwait_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_france_kuwait_dg} ="YES" THEN
          ${air_import_france_kuwait_dg_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_ITA_kuwait} ="YES" THEN
          ${air_import_ITA_kuwait_leadtime}
         WHEN ${tp95_logitude.tp95_air_import_ITA_kuwait_dg} ="YES" THEN
          ${air_import_ITA_kuwait_dg_leadtime}
         WHEN ${tp95_logitude.tp95_ocean_import_ITA_kuwait} ="YES" THEN
          ${ocean_import_ITA_kuwait_leadtime}
         WHEN ${tp95_logitude.tp95_ocean_import_HK_kuwait} ="YES" THEN
          ${ocean_import_HK_kuwait_leadtime}
         WHEN ${tp95_logitude.tp95_air_import_HK_kuwait_dg} ="YES" THEN
          ${air_import_HK_kuwait_dg_leadtime}
         WHEN ${tp95_logitude.tp95_air_import_HK_kuwait} ="YES" THEN
          ${air_import_HK_kuwait_leadtime}
         WHEN ${tp95_logitude.tp95_ocean_export_UAE_kuwait} ="YES" THEN
          ${ocean_export_UAE_kuwait_leadtime}
        WHEN ${tp95_logitude.tp95_land_export_uae_kuwait_dg} ="YES" THEN
          ${land_export_uae_kuwait_dg_leadtime}
        WHEN ${tp95_logitude.tp95_land_export_uae_kuwait_dg} ="YES" THEN
          ${land_export_uae_kuwait_dg_leadtime}
        WHEN ${tp95_logitude.tp95_land_export_uae_kuwait_courier} ="YES" THEN
          ${land_export_uae_kuwait_courier_leadtime}
        WHEN ${tp95_logitude.tp95_land_export_uae_kuwait} ="YES" THEN
          ${land_export_uae_kuwait_leadtime}
        WHEN ${tp95_logitude.tp95_air_export_uae_kuwait_dg} ="YES" THEN
          ${air_export_uae_kuwait_dg_leadtime}
        WHEN ${tp95_logitude.tp95_air_export_uae_kuwait_courier_dg} ="YES" THEN
          ${air_export_uae_kuwait_courier_dg_leadtime}
        WHEN ${tp95_logitude.tp95_air_export_uae_kuwait_courier} ="YES" THEN
          ${air_export_uae_kuwait_courier_leadtime}
        WHEN ${tp95_logitude.tp95_air_export_uae_kuwait} ="YES" THEN
          ${air_export_uae_kuwait_leadtime}
        WHEN ${tp95_logitude.tp95_ocean_import_france_bahrain} ="YES" THEN
          ${ocean_import_france_bahrain_leadtime}
         WHEN ${tp95_logitude.tp95_ocean_import_US_bahrain} ="YES" THEN
          ${ocean_import_US_bahrain_leadtime}
        WHEN ${tp95_logitude.tp95_ocean_import_HK_bahrain} ="YES" THEN
          ${ocean_import_HK_bahrain_leadtime}
        WHEN ${tp95_logitude.tp95_ocean_import_ITA_bahrain} ="YES" THEN
          ${ocean_import_ITA_bahrain_leadtime}
        WHEN ${tp95_logitude.tp95_ocean_export_UAE_bahrain} ="YES" THEN
          ${ocean_export_UAE_bahrain_leadtime}
        WHEN ${tp95_logitude.tp95_air_import_US_bahrain_dg} ="YES" THEN
          ${air_import_US_bahrain_dg_leadtime}
         WHEN ${tp95_logitude.tp95_air_import_US_bahrain} ="YES" THEN
          ${air_import_US_bahrain_leadtime}
         WHEN ${tp95_logitude.tp95_air_import_ITA_bahrain_dg} ="YES" THEN
          ${air_import_ITA_bahrain_dg_leadtime}
         WHEN ${tp95_logitude.tp95_air_import_ITA_bahrain} ="YES" THEN
          ${air_import_ITA_bahrain_leadtime}
         WHEN ${tp95_logitude.tp95_air_import_HK_bahrain_dg} ="YES" THEN
          ${air_import_HK_bahrain_dg_leadtime}
         WHEN ${tp95_logitude.tp95_air_import_HK_bahrain} ="YES" THEN
          ${air_import_HK_bahrain_leadtime}
         WHEN ${tp95_logitude.tp95_air_import_france_bahrain_dg} ="YES" THEN
          ${air_import_france_bahrain_dg_leadtime}
         WHEN ${tp95_logitude.tp95_air_import_france_bahrain} ="YES" THEN
          ${air_import_france_bahrain_leadtime}
           WHEN ${tp95_logitude.tp95_air_import_france_bahrain_courier} ="YES" THEN
          ${air_import_france_bahrain_courier_leadtime}
         WHEN ${tp95_logitude.tp95_land_export_uae_bahrain} ="YES" THEN
          ${land_export_uae_bahrain_leadtime}
          WHEN ${tp95_logitude.tp95_land_export_uae_bahrain_dg} ="YES" THEN
          ${land_export_uae_bahrain_dg_leadtime}
          WHEN ${tp95_logitude.tp95_land_export_uae_bahrain_courier} ="YES" THEN
          ${land_export_uae_bahrain_courier_leadtime}
          WHEN ${tp95_logitude.tp95_air_export_uae_bahrain} ="YES" THEN
          ${air_export_uae_bahrain_leadtime}
          WHEN ${tp95_logitude.tp95_air_export_uae_bahrain_courier_dg} ="YES" THEN
          ${air_export_uae_bahrain_courier_dg_leadtime}
          WHEN ${tp95_logitude.tp95_air_export_uae_bahrain_dg} ="YES" THEN
          ${air_export_uae_bahrain_dg_leadtime}
          WHEN ${tp95_logitude.tp95_ocean_import_US_qatar} ="YES" THEN
          ${ocean_import_US_qatar_leadtime}
          WHEN ${tp95_logitude.tp95_ocean_import_ITA_qatar} ="YES" THEN
          ${ocean_import_ITA_qatar_leadtime}
          WHEN ${tp95_logitude.tp95_ocean_import_france_qatar} ="YES" THEN
          ${ocean_import_france_qatar_leadtime}
          WHEN ${tp95_logitude.tp95_ocean_import_HK_qatar} ="YES" THEN
          ${ocean_import_HK_qatar_leadtime}
          WHEN ${tp95_logitude.tp95_ocean_export_UAE_qatar} ="YES" THEN
          ${ocean_export_UAE_qatar_leadtime}
          WHEN ${tp95_logitude.tp95_air_import_US_qatar_dg} ="YES" THEN
          ${air_import_US_qatar_dg_leadtime}
          WHEN ${tp95_logitude.tp95_air_import_US_qatar} ="YES" THEN
          ${air_import_US_qatar_leadtime}
          WHEN ${tp95_logitude.tp95_air_import_france_qatar} ="YES" THEN
          ${air_import_france_qatar_leadtime}
          WHEN ${tp95_logitude.tp95_air_import_france_qatar_dg} ="YES" THEN
          ${air_import_france_qatar_dg_leadtime}
          WHEN ${tp95_logitude.tp95_air_import_ITA_qatar} ="YES" THEN
          ${air_import_ITA_qatar_leadtime}
          WHEN ${tp95_logitude.tp95_air_import_ITA_qatar_dg} ="YES" THEN
          ${air_import_ITA_qatar_dg_leadtime}
          WHEN ${tp95_logitude.tp95_air_import_HK_qatar} ="YES" THEN
          ${air_import_HK_qatar_leadtime}
          WHEN ${tp95_logitude.tp95_air_import_HK_qatar_dg} ="YES" THEN
          ${air_import_HK_qatar_dg_leadtime}
          WHEN ${tp95_logitude.tp95_land_export_uae_qatar_dg} ="YES" THEN
          ${land_export_uae_qatar_dg_leadtime}
          WHEN ${tp95_logitude.tp95_land_export_uae_qatar_courier} ="YES" THEN
          ${land_export_uae_qatar_courier_leadtime}
          WHEN ${tp95_logitude.tp95_land_export_uae_qatar_courier_dg} ="YES" THEN
          ${land_export_uae_qatar_courier_dg_leadtime}
          WHEN ${tp95_logitude.tp95_land_export_uae_qatar} ="YES" THEN
          ${land_export_uae_qatar_leadtime}
          WHEN ${tp95_logitude.tp95_air_export_uae_qatar_dg} ="YES" THEN
          ${air_export_uae_qatar_dg_leadtime}
          WHEN ${tp95_logitude.tp95_air_export_uae_qatar_courier} ="YES" THEN
          ${air_export_uae_qatar_courier_leadtime}
          WHEN ${tp95_logitude.tp95_air_export_uae_qatar_courier_dg} ="YES" THEN
          ${air_export_uae_qatar_courier_dg_leadtime}
          WHEN ${tp95_logitude.tp95_air_export_uae_qatar} ="YES" THEN
          ${air_export_uae_qatar_leadtime}
          WHEN ${tp95_logitude.tp95_air_export_UAE_egypt} ="YES" THEN
          ${air_export_UAE_egypt_leadtime}
          WHEN ${tp95_logitude.tp95_air_export_UAE_egypt_courier} ="YES" THEN
          ${air_export_UAE_egypt_courier_leadtime}
           WHEN ${tp95_logitude.tp95_land_export_UAE_egypt} ="YES" THEN
          ${land_export_UAE_egypt_leadtime}
           WHEN ${tp95_logitude.tp95_ocean_export_UAE_egypt} ="YES" THEN
          ${ocean_export_UAE_egypt_leadtime}
          WHEN ${tp95_logitude.tp95_air_import_ITA_egypt} ="YES" THEN
          ${air_import_ITA_egypt_leadtime}
          WHEN ${tp95_logitude.tp95_air_import_HK_egypt} ="YES" THEN
          ${air_import_HK_egypt_leadtime}
          WHEN ${tp95_logitude.tp95_air_import_france_egypt} ="YES" THEN
          ${air_import_france_egypt_leadtime}
          WHEN ${tp95_logitude.tp95_air_import_france_egypt_dg} ="YES" THEN
          ${air_import_france_egypt_dg_leadtime}
      END;;
    value_format: "0.00"
    drill_fields: [data_drill_fields*]
  }


  dimension: air_export_uae_saudi_courier_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_export_uae_saudi_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="Saudi Arabia" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_import_france_uae_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="France" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_import_ita_uae_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="Italy" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_import_HK_uae_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: ocean_import_ITA_uae_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Ocean" AND ${country_of_departure}="Italy" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_france_uae_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_france_uae_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_france_uae_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="France" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_HK_uae_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date_filter_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_HK_uae_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Hong Kong" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_ITA_uae_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Italy" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_ITA_uae_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Italy" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_ITA_uae_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="Italy" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_US_uae_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United States" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_import_US_uae_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Import" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United States" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_export_uae_uae_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: air_export_uae_uae_courier_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: land_export_uae_uae_dg_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS TRUE AND ${delivery_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: land_export_uae_uae_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS FALSE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }

  dimension: land_export_uae_uae_courier_leadtime {
    hidden: yes
    type: number
    sql: CASE WHEN ${direction}="Export" AND ${main_carriage_transport_mode} = "Air" AND ${country_of_departure}="United Arab Emirates" AND ${country_of_destination}="United Arab Emirates" AND CAST(${is_courier} as BOOLEAN) IS TRUE AND CAST(${dgr} as BOOLEAN) IS FALSE AND ${delivery_date} IS NOT NULL AND ${goods_readiness_date_filter_date} IS NOT NULL THEN DATE_DIFF(CAST(${delivery_date_filter_date} AS DATE),CAST(${goods_readiness_date_filter_date} AS DATE),DAY)
          ELSE NULL
          END;;
  }
  set: delivery_country_drill_fields {
    fields: [direction,shipment_id,shipper,consignee,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,incoterm,freight_forwarder,gross_weight_kgs,chargeable_weight,freight_charges_usd,notification_date,goods_readiness_date,clubbing_instruction__appointment_date,pick_up_from_date,date_on_board_origin,date_of_arrival_to_port,delivery_date,target_delivery_date,shipping_leadtime,delivery_sla,events,comments,status,Late_by,delivery_days_countdown,opened_by]
  }
  set: doc_country_drill_fields {
    fields: [direction,shipment_id,shipper,consignee,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,incoterm,freight_forwarder,gross_weight_kgs,chargeable_weight,freight_charges_usd,notification_date,goods_readiness_date,clubbing_instruction__appointment_date,pick_up_from_date,date_on_board_origin,date_of_arrival_to_port,delivery_date,target_documentation_date,documentation_leadtime,documentation_sla,events,comments,status,documentation_days_countdown,opened_by]
  }
  set: data_drill_fields {
    fields: [direction,shipment_id,shipper,consignee,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,incoterm,freight_forwarder,gross_weight_kgs,chargeable_weight,freight_charges_usd,notification_date,goods_readiness_date,clubbing_instruction__appointment_date,pickup_date_filter_date,date_on_board_origin,date_of_arrival_to_port,delivery_date,target_delivery_date,shipping_leadtime,delivery_sla,events,comments,status,opened_by]
  }
  set: col_drill_fields {
    fields: [direction,shipment_id,shipper,consignee,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,incoterm,freight_forwarder,gross_weight_kgs,chargeable_weight,freight_charges_usd,notification_date,goods_readiness_date,clubbing_instruction__appointment_date,pickup_date_filter_date,date_on_board_origin,date_of_arrival_to_port,delivery_date,events,comments,status,opened_by]
  }
  set: dep_drill_fields {
    fields: [direction,shipment_id,shipper,consignee,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,incoterm,freight_forwarder,gross_weight_kgs,chargeable_weight,freight_charges_usd,notification_date,goods_readiness_date,clubbing_instruction__appointment_date,pickup_date_filter_date,date_on_board_origin,date_of_arrival_to_port,delivery_date,events,comments,status,opened_by]
  }

  measure: uae {
    label: "UAE"
    type: count
    filters: [country_of_destination: "United Arab Emirates"]
    sql: ${country_of_destination} ;;
    drill_fields: [delivery_country_drill_fields*]
  }
  measure: uae_doc {
    type: count
    filters: [country_of_destination: "United Arab Emirates"]
    sql: ${country_of_destination} ;;
    drill_fields: [doc_country_drill_fields*]
  }
  measure: uae_gr {
    type: count
    filters: [country_of_destination: "United Arab Emirates"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: uae_col {
    type: count
    filters: [country_of_destination: "United Arab Emirates"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: uae_dep {
    type: count
    filters: [country_of_destination: "United Arab Emirates"]
    sql: ${country_of_destination} ;;
    drill_fields: [dep_drill_fields*]
  }

  measure: kuwait {
    type: count
    filters: [country_of_destination: "Kuwait"]
    sql: ${country_of_destination} ;;
    drill_fields: [delivery_country_drill_fields*]
  }
  measure: kuwait_doc {
    type: count
    filters: [country_of_destination: "Kuwait"]
    sql: ${country_of_destination} ;;
    drill_fields: [doc_country_drill_fields*]
  }
  measure: kuwait_gr {
    type: count
    filters: [country_of_destination: "Kuwait"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: kuwait_col {
    type: count
    filters: [country_of_destination: "Kuwait"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: kuwait_dep {
    type: count
    filters: [country_of_destination: "Kuwait"]
    sql: ${country_of_destination} ;;
    drill_fields: [dep_drill_fields*]
  }
  measure: KSA{
    type: count
    filters: [country_of_destination: "Saudi Arabia"]
    sql: ${country_of_destination} ;;
    drill_fields: [delivery_country_drill_fields*]
  }
  measure: KSA_doc{
    type: count
    filters: [country_of_destination: "Saudi Arabia"]
    sql: ${country_of_destination} ;;
    drill_fields: [doc_country_drill_fields*]
  }
  measure: KSA_gr{
    type: count
    filters: [country_of_destination: "Saudi Arabia"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: KSA_col{
    type: count
    filters: [country_of_destination: "Saudi Arabia"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: KSA_dep{
    type: count
    filters: [country_of_destination: "Saudi Arabia"]
    sql: ${country_of_destination} ;;
    drill_fields: [dep_drill_fields*]
  }
  measure: Bahrain{
    type: count
    filters: [country_of_destination: "Bahrain"]
    sql: ${country_of_destination} ;;
    drill_fields: [delivery_country_drill_fields*]
  }
  measure: Bahrain_doc{
    type: count
    filters: [country_of_destination: "Bahrain"]
    sql: ${country_of_destination} ;;
    drill_fields: [doc_country_drill_fields*]
  }
  measure: Bahrain_gr{
    type: count
    filters: [country_of_destination: "Bahrain"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: Bahrain_dep{
    type: count
    filters: [country_of_destination: "Bahrain"]
    sql: ${country_of_destination} ;;
    drill_fields: [dep_drill_fields*]
  }
  measure: Bahrain_col{
    type: count
    filters: [country_of_destination: "Bahrain"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: qatar{
    type: count
    filters: [country_of_destination: "Qatar"]
    sql: ${country_of_destination} ;;
    drill_fields: [delivery_country_drill_fields*]
  }

  measure: qatar_doc{
    type: count
    filters: [country_of_destination: "Qatar"]
    sql: ${country_of_destination} ;;
    drill_fields: [doc_country_drill_fields*]
  }
  measure: qatar_gr{
    type: count
    filters: [country_of_destination: "Qatar"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: qatar_col{
    type: count
    filters: [country_of_destination: "Qatar"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: qatar_dep{
    type: count
    filters: [country_of_destination: "Qatar"]
    sql: ${country_of_destination} ;;
    drill_fields: [dep_drill_fields*]
  }
  measure: Egypt{
    type: count
    filters: [country_of_destination: "Egypt"]
    sql: ${country_of_destination} ;;
    drill_fields: [delivery_country_drill_fields*]
  }



  measure: Egypt_doc{
    type: count
    filters: [country_of_destination: "Egypt"]
    sql: ${country_of_destination} ;;
    drill_fields: [doc_country_drill_fields*]
  }
  measure: Egypt_gr{
    type: count
    filters: [country_of_destination: "Egypt"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: Egypt_col{
    type: count
    filters: [country_of_destination: "Egypt"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: Egypt_dep{
    type: count
    filters: [country_of_destination: "Egypt"]
    sql: ${country_of_destination} ;;
    drill_fields: [dep_drill_fields*]
  }
  measure: ROW_gr {
    type: count
    filters: [country_of_destination: "-United Arab Emirates,-Saudi Arabia,-Kuwait,-Bahrain,-Egypt,-Qatar"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: ROW_col {
    type: count
    filters: [country_of_destination: "-United Arab Emirates,-Saudi Arabia,-Kuwait,-Bahrain,-Egypt,-Qatar"]
    sql: ${country_of_destination} ;;
    drill_fields: [col_drill_fields*]
  }
  measure: ROW_dep {
    type: count
    filters: [country_of_destination: "-United Arab Emirates,-Saudi Arabia,-Kuwait,-Bahrain,-Egypt,-Qatar"]
    sql:  ${country_of_destination};;
    drill_fields: [dep_drill_fields*]
  }
  measure: ROW_doc {
    type: count
    filters: [country_of_destination: "-United Arab Emirates,-Saudi Arabia,-Kuwait,-Bahrain,-Egypt,-Qatar"]
    sql: ${country_of_destination}  ;;
    drill_fields: [doc_country_drill_fields*]
  }
  measure: ROW{
    type: count
    filters: [country_of_destination: "-United Arab Emirates,-Saudi Arabia,-Kuwait,-Bahrain,-Egypt,-Qatar"]
    sql: ${country_of_destination}  ;;
    drill_fields: [delivery_country_drill_fields*]
  }
  dimension: Country_of_Destination{
    type: string
    sql: CASE WHEN ${country_of_destination} NOT IN ("United Arab Emirates","Kuwait","Bahrain","Saudi Arabia","Egypt","Qatar","Bahrain") THEN "ROW" ELSE ${country_of_destination} END ;;
  }
  dimension: ranking_Country_of_Destination{
    type: number
    sql:  CASE WHEN ${Country_of_Destination} = "United Arab Emirates" THEN 1
          WHEN ${Country_of_Destination} = "Saudi Arabia" THEN 2
          WHEN ${Country_of_Destination} = "Kuwait" THEN 3
          WHEN ${Country_of_Destination} = "Bahrain" THEN 4
          WHEN ${Country_of_Destination} = "Qatar" THEN 5
          WHEN ${Country_of_Destination} = "Egypt" THEN 6
          WHEN ${Country_of_Destination} = "ROW" THEN 7
    END      ;;
  }
  dimension: Weeks{
    type: number
    sql: EXTRACT(WEEK FROM ${target_delivery_date_filter_date}) ;;
  }
  dimension: Weeks_txt {
    type: string
    sql: CONCAT("Week"," ",FORMAT_DATE('%W', ${target_delivery_date_filter_date})) ;;
  }
  dimension: Months {
    type: string
    sql: FORMAT_DATE('%B', ${target_delivery_date_filter_date}) ;;
  }
  dimension: Months_rep {
    type: string
    sql: FORMAT_DATE('%B', ${create_date_filter_date}) ;;
  }

  dimension: rank_Months_rep {
    type: number
    sql: CASE WHEN FORMAT_DATE('%B', ${create_date_filter_date})="January" THEN 1
    WHEN FORMAT_DATE('%B', ${create_date_filter_date})="February" THEN 2
    WHEN FORMAT_DATE('%B', ${create_date_filter_date})="March" THEN 3
    WHEN FORMAT_DATE('%B', ${create_date_filter_date})="April" THEN 4
    WHEN FORMAT_DATE('%B', ${create_date_filter_date})="May" THEN 5
    WHEN FORMAT_DATE('%B', ${create_date_filter_date})="June" THEN 6
    WHEN FORMAT_DATE('%B', ${create_date_filter_date})="July" THEN 7
    WHEN FORMAT_DATE('%B', ${create_date_filter_date})="August" THEN 8
    WHEN FORMAT_DATE('%B', ${create_date_filter_date})="September" THEN 9
    WHEN FORMAT_DATE('%B', ${create_date_filter_date})="October" THEN 10
    WHEN FORMAT_DATE('%B', ${create_date_filter_date})="November" THEN 11
    WHEN FORMAT_DATE('%B', ${create_date_filter_date})="December" THEN 12
    END
    ;;
  }


  dimension: no_of_shipments {
    type: string
    sql: CASE WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND CAST(${target_delivery_date} AS DATE) = CAST(CURRENT_TIMESTAMP AS DATE) THEN "Due Today"
              WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 1 THEN "Due Tomorrow"
              WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 2 THEN "Due in 2 days"
              WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 3 THEN "Due in 3 days"
              WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 4 THEN "Due in 4 days"
              WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 5 THEN "Due in 5 days"
              WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) > 5 THEN "Due in More than 5 days"
              WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) < 0 THEN "Late Shipments"
    END ;;
  }
  dimension: no_of_shipments_doc {
    type: string
    sql: CASE WHEN ${direction} = "Export" AND ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND  ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND CAST(${target_documentation_date} AS DATE) = CAST(CURRENT_TIMESTAMP AS DATE) THEN "Due Today"
              WHEN ${direction} = "Export" AND ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND  ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 1 THEN "Due Tomorrow"
              WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND  ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 2 THEN "Due in 2 days"
              WHEN ${direction} = "Export" AND ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 3 THEN "Due in 3 days"
              WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND  ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 4 THEN "Due in 4 days"
              WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived")AND  ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 5 THEN "Due in 5 days"
              WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) > 5 THEN "Due in More than 5 days"
              WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) < 0 THEN "Late Shipments"
    END ;;
  }
dimension: gr_date_diff {
  hidden: yes
  type: number
  sql: DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),CAST(${notification_date_filter_date} AS DATE),DAY) ;;
}
dimension: no_of_shipments_gr {
  type: string
  sql: CASE WHEN ${gr_date_diff} = 0 THEN "Notified Today"
            WHEN ${gr_date_diff}= 1 THEN "Notified Yesterday"
            WHEN ${gr_date_diff}= 2 THEN "Notified 2 days ago"
            WHEN ${gr_date_diff}= 3 THEN "Notified 3 days ago"
            WHEN ${gr_date_diff}=4 THEN "Notified 4 days ago"
            WHEN ${gr_date_diff}=5 THEN "Notified 5 days ago"
            WHEN ${gr_date_diff}>5 THEN "Notified More than 5 days ago"
END ;;
}
dimension: ranking_no_of_shipments_gr {
  type: number
  sql: CASE WHEN ${gr_date_diff} = 0 THEN 1
            WHEN ${gr_date_diff}= 1 THEN 2
            WHEN ${gr_date_diff}= 2 THEN 3
            WHEN ${gr_date_diff}= 3 THEN 4
            WHEN ${gr_date_diff}=4 THEN 5
            WHEN ${gr_date_diff}=5 THEN 6
            WHEN ${gr_date_diff}>5 THEN 7
END ;;
  }
  dimension: no_of_shipments_dep {
    type: string
    sql: CASE WHEN CAST(CURRENT_TIMESTAMP AS DATE) = ${pickup_date_filter_date} THEN "Picked up Today"
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${pickup_date_filter_date},DAY)=1 THEN "Picked up Yesterday"
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${pickup_date_filter_date},DAY)=2 THEN "Picked up 2 days ago"
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${pickup_date_filter_date},DAY)=3 THEN "Picked up 3 days ago"
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${pickup_date_filter_date},DAY)=4 THEN "Picked up 4 days ago"
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${pickup_date_filter_date},DAY)=5 THEN "Picked up 5 days ago"
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${pickup_date_filter_date},DAY)>5 THEN "Picked up more than 5 days ago"
END ;;
  }
  dimension: no_of_shipments_col {
    type: string
    sql: CASE WHEN CAST(CURRENT_TIMESTAMP AS DATE) = ${goods_readiness_date_filter_date} THEN "Goods ready Today"
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${goods_readiness_date_filter_date},DAY)=1 THEN "Goods ready Yesterday"
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${goods_readiness_date_filter_date},DAY)=2 THEN "Goods ready 2 days ago"
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${goods_readiness_date_filter_date},DAY)=3 THEN "Goods ready 3 days ago"
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${goods_readiness_date_filter_date},DAY)=4 THEN "Goods ready 4 days ago"
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${goods_readiness_date_filter_date},DAY)=5 THEN "Goods ready 5 days ago"
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${goods_readiness_date_filter_date},DAY)>5 THEN "Goods ready more than 5 days ago"
END ;;
  }
  dimension: ranking_no_of_shipments_col {
    type: number
    sql: CASE WHEN CAST(CURRENT_TIMESTAMP AS DATE) = ${goods_readiness_date_filter_date} THEN 1
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${goods_readiness_date_filter_date},DAY)=1 THEN 2
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${goods_readiness_date_filter_date},DAY)=2 THEN 3
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${goods_readiness_date_filter_date},DAY)=3 THEN 4
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${goods_readiness_date_filter_date},DAY)=4 THEN 5
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${goods_readiness_date_filter_date},DAY)=5 THEN 6
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${goods_readiness_date_filter_date},DAY)>5 THEN 7
END ;;
  }
  dimension: ranking_no_of_shipments_dep {
    type: number
    sql: CASE WHEN CAST(CURRENT_TIMESTAMP AS DATE) = ${pickup_date_filter_date} THEN 1
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${pickup_date_filter_date},DAY)=1 THEN 2
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${pickup_date_filter_date},DAY)=2 THEN 3
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${pickup_date_filter_date},DAY)=3 THEN 4
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${pickup_date_filter_date},DAY)=4 THEN 5
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${pickup_date_filter_date},DAY)=5 THEN 6
            WHEN DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${pickup_date_filter_date},DAY)>5 THEN 7
END ;;
  }
dimension: ranking_no_of_shipments_doc {
  type: number
  sql: CASE WHEN ${direction} = "Export" AND ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND  ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND CAST(${target_documentation_date} AS DATE) = CAST(CURRENT_TIMESTAMP AS DATE) THEN 2
  WHEN ${direction} = "Export" AND ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND  ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 1 THEN 3
  WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 2 THEN 4
  WHEN ${direction} = "Export" AND ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 3 THEN 5
  WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND  ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 4 THEN 6
  WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 5 THEN 7
  WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND  ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) > 5 THEN 8
  WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND  ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) < 0 THEN 1
  END ;;
}
  dimension: ranking_no_of_shipments {
    type: number
    sql: CASE WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND CAST(${target_delivery_date} AS DATE) = CAST(CURRENT_TIMESTAMP AS DATE) THEN 2
    WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 1 THEN 3
    WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 2 THEN 4
    WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 3 THEN 5
    WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 4 THEN 6
    WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = 5 THEN 7
    WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) > 5 THEN 8
    WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) < 0 THEN 1
    END ;;
  }
dimension: Late_Shipments_Del {
  label: "Late Shipments"
  type: string
  sql: CASE WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY)= -1 THEN "Late by 1 day"
    WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = -2 THEN "Late by 2 days"
    WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = -3 THEN "Late by 3 days"
    WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = -4 THEN "Late by 4 days"
    WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY)=-5 THEN "Late by 5 days"
    WHEN ${status} NOT IN ("Delivered","Delivered to Customs Agent") AND DATE_DIFF(CAST(${target_delivery_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY)< -5 THEN "More than 5 days" END;;
}
dimension: Late_Shipments_Doc {
  type: string
  sql:CASE WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) < -5 THEN "More than 5 days"
  WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = -1 THEN "Late by 1 day"
   WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = -2 THEN "Late by 2 days"
  WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = -3 THEN "Late by 3 days"
  WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = -4 THEN "Late by 4 days"
  WHEN ${direction} = "Export" AND  ${status} NOT IN ("Delivered","Delivered to Customs Agent","Departed","Arrived") AND ${documentation_sla} IN ("Late Pending Shipments","Pending Documentation -Within SLA") AND DATE_DIFF(CAST(${target_documentation_date} AS DATE),CAST(CURRENT_TIMESTAMP AS DATE),DAY) = -5 THEN "Late by 5 days"
  END ;;
}
dimension: no_of_shipments_pending_closed_date {
  type: string
  sql:  CASE WHEN ${status}="Delivered" and ${delivery_date_filter_date} IS NOT NULL AND ${closed_date} IS NULL AND DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${delivery_date_filter_date},DAY) = 1 THEN "Delivered 1 day ago"
            WHEN ${status}="Delivered" and ${delivery_date_filter_date} IS NOT NULL AND ${closed_date} IS NULL AND DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${delivery_date_filter_date},DAY) = 2 THEN "Delivered 2 days ago"
            WHEN ${status}="Delivered" and ${delivery_date_filter_date} IS NOT NULL AND ${closed_date} IS NULL AND DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${delivery_date_filter_date},DAY) = 3 THEN "Delivered 3 days ago"
            WHEN ${status}="Delivered" and ${delivery_date_filter_date} IS NOT NULL AND ${closed_date} IS NULL AND DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${delivery_date_filter_date},DAY) = 4 THEN "Delivered 4 days ago"
            WHEN ${status}="Delivered" and ${delivery_date_filter_date} IS NOT NULL AND ${closed_date} IS NULL AND DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${delivery_date_filter_date},DAY) = 5 THEN "Delivered 5 days ago"
            WHEN ${status}="Delivered" and ${delivery_date_filter_date} IS NOT NULL AND ${closed_date} IS NULL AND DATE_DIFF(CAST(CURRENT_TIMESTAMP AS DATE),${delivery_date_filter_date},DAY) > 5 THEN "Delivered more than 5 days ago"
        END;;
}
dimension: shipments_with_future_delivery_date{
  type: yesno
  sql: ${delivery_date_filter_date} > CAST(CURRENT_TIMESTAMP AS DATE);;
  drill_fields: [delivery_country_drill_fields*]
}
measure: Total_del {
  type: number
  sql: ${uae}+${kuwait}+${KSA}+${qatar}+${Bahrain}+${Egypt}+${ROW} ;;
  drill_fields: [delivery_country_drill_fields*]
}
measure: Total_doc {
  type: number
  sql: ${uae_doc}+${kuwait_doc}+${KSA_doc}+${qatar_doc}+${Bahrain_doc}+${Egypt_doc}+${ROW_doc} ;;
  drill_fields: [doc_country_drill_fields*]
}
measure: Total_gr {
    type: number
    sql: ${uae_gr}+${kuwait_gr}+${KSA_gr}+${qatar_gr}+${Bahrain_gr}+${Egypt_gr}+${ROW_gr} ;;
    drill_fields: [col_drill_fields*]
  }
measure: Total_col {
    type: number
    sql: ${uae_col}+${kuwait_col}+${KSA_col}+${qatar_col}+${Bahrain_col}+${Egypt_col}+${ROW_col} ;;
    drill_fields: [col_drill_fields*]
  }
measure: Total_dep {
    type: number
    sql: ${uae_dep}+${kuwait_dep}+${KSA_dep}+${qatar_dep}+${Bahrain_dep}+${Egypt_dep}+${ROW_dep} ;;
    drill_fields: [dep_drill_fields*]
  }
dimension: event_type {
  label: "Event Types"
  type: string
  sql: CASE WHEN ${events} IS NULL OR ${events}="nan" OR ${events}=" " THEN "Events Empty"
  WHEN ${events} LIKE "%Damage%" or ${events} LIKE "%damage%" THEN "Damages"
  WHEN ${events} LIKE "%Short Landing%" OR ${events} LIKE "Pilferage" OR ${events} LIKE "%Shortage%" THEN "Shortages"
  WHEN ${events} LIKE "%Space Issue%" THEN "Space Issue"
  WHEN ${events} LIKE "%Shipping Documents Lost%" THEN "Shipping Documents Lost or Misplaced"
  WHEN ${events} LIKE "%Incomplete%" THEN "Incomplete or Without Original Documents"
  WHEN ${events} LIKE "%Insufficient Fund%" THEN "Insufficient Fund in Duty Account"
  WHEN ${events} LIKE "%Unforeseen%" THEN "Unforeseen Events (Strike, Weather, etc)"
  WHEN ${events} LIKE "%Business Unit / Store Confirmation Delay%" THEN "Business Unit / Store Confirmation Delay"
  WHEN ${events} LIKE "%Customs System / Customs Issue%" OR ${events} LIKE "%Custom Inspection%" OR ${events} LIKE "%Freight / Customs Duty Invoice Delayed%" OR ${events} LIKE "%Late Clearance%" THEN "Customs Issue"
  WHEN ${events} LIKE "%Delay in Product Registration from BU%" THEN "Delay in Product Registration from BU"
  WHEN ${events} LIKE "%PO And /Or Payment Invoice Delay%" THEN "PO And /Or Payment Invoice Delay"
  WHEN ${events} LIKE "%Inspections And Approval Delay%" THEN "Inspections And Approval Delay"
  WHEN ${events} LIKE "%Delays Notification from BU-CIF/DAP/DDU%" THEN "Delays Notification from BU-CIF/DAP/DDU"
  WHEN ${events} LIKE "%Weekend%" THEN "Weekend Delays"
  WHEN ${events} LIKE "%Public Holiday%" THEN "Public Holiday Delays"
  WHEN ${events} LIKE "%DO Readiness Delay%" THEN "DO Readiness Delays"
  WHEN ${events} LIKE "%Error in Documentation%" OR ${events} LIKE "%Documents Issue%" OR ${events} LIKE "%Error in Shipping Documents%" OR ${events} LIKE "%Goods / Documents Readiness Delay%" OR ${events} LIKE "%Incorrect DG Declaration / Packaging%" THEN "Documentation Issues Delays"
  WHEN ${events} LIKE "%Freighter Flights Availability%" THEN "Freighter Flights Availability Delays"
  WHEN ${events} LIKE "%Inspections And Approval Delay%" OR ${events} LIKE "%Last Ministry Approvals%" OR ${events} LIKE "%Late Ministry Approvals%" OR ${events} LIKE "%Origin Inspection%" THEN "Inspections And Approval Delay"
  WHEN ${events} LIKE "%Late Booking%" THEN "Late Booking"
  WHEN ${events} LIKE "%Port / Airport Congestion%" THEN "Port Issues"
  WHEN ${events} LIKE "%Late Collection%" THEN "Late Collection"
  WHEN ${events} LIKE "%Late Delivery%" THEN "Late Delivery"
  WHEN ${events} LIKE "%Part Landing%" OR ${events} LIKE "%Partial Arrival%" OR ${events} LIKE "%Partial Delivery%" THEN "Partial Shipments"
  ELSE "Miscellaneous Delays" END;;
}
  measure: Damages_Shortages_total{
    label: "Damages and Shortages"
    type: count
    filters: [events: "%Damage%,%damage%,%Short Landing%,%Pilferage%,%Shortage%"]
    sql: (${delivery_sla});;
    drill_fields: [delivery_country_drill_fields*]
  }
dimension:is_cancelled {
  type:string
  sql: CASE WHEN ${events} LIKE "%Deleted ID%" THEN "YES" ELSE "NO" END ;;
}
dimension: is_external {
  type: string
  sql: CASE WHEN ${vertical_consignee}="EXTERNAL" AND ${incoterm} IN ("EXW","FCA","FOB") THEN "YES" ELSE "NO" END ;;
}
dimension: Mode {
  type: string
  sql: CASE WHEN ${main_carriage_transport_mode}="Air" and CAST(${is_courier} as BOOLEAN) IS TRUE THEN "Air Courier"
  WHEN ${main_carriage_transport_mode} = "Air" and CAST(${is_courier} as BOOLEAN) IS FALSE THEN "Air Freight"
  ELSE ${main_carriage_transport_mode} END ;;
}

measure: Damages_Shortages{
  hidden: yes
  label: "Damages and Shortages (On Time)"
  type: count
  filters: [delivery_sla: "On Time",events: "%Damage%,%damage%,%Short Landing%,%Pilferage%,%Shortage%"]
  sql: (${delivery_sla});;
  drill_fields: [delivery_country_drill_fields*]
}
measure: OTIF {
  label: "OTIF"
  type: number
  sql: ${ontime} - ${Damages_Shortages} ;;
  drill_fields: [delivery_country_drill_fields*]
}
measure: percent_of_total {
  type: percent_of_total
  sql: ${count};;
  drill_fields: [delivery_country_drill_fields*]
}
measure: ontime{
  label: "Delivered On Time"
  hidden: no
  type: count
  filters: [delivery_sla: "On Time"]
  sql: ${delivery_sla} ;;
  drill_fields: [delivery_country_drill_fields*]
}
  measure: late{
    label: "Delivered Late"
    hidden: no
    type: count
    filters: [delivery_sla: "Late"]
    sql: ${delivery_sla} ;;
    drill_fields: [delivery_country_drill_fields*]
  }
  measure: late_pending_shipments{
    label: "Late Pending Shipments"
    type: count
    filters: [delivery_sla: "Late Pending Shipments"]
    drill_fields: [delivery_country_drill_fields*]
  }
  measure: pending_shipments{
    label: "Pending Shipments - Within SLA"
    type: count
    filters: [delivery_sla: "Pending Delivery - Within SLA"]
    sql: ${delivery_sla};;
    drill_fields: [delivery_country_drill_fields*]
  }
  measure: total_shipments {
    label: "Total Shipments"
    type: number
    sql: ${ontime}+${late}+${late_pending_shipments}+${pending_shipments} ;;
    drill_fields: [delivery_country_drill_fields*]
  }

  measure: vertical_managed {
    hidden: yes
    type: string
    sql: CASE WHEN ${vertical_shipper} ="MANAGED COMPANIES" OR ${vertical_consignee} = "MANAGED COMPANIES" THEN "MANAGED COMPANIES" END;;
  }
  measure: vertical_joint {
    hidden: yes
    type: string
    sql: CASE WHEN ${vertical_shipper} ="JOINT VENTURES" OR ${vertical_consignee} = "JOINT VENTURES" THEN "JOINT VENTURES" END  ;;
  }


   }
#}
