view: dim_order_summary_forecast {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dim_order_summary_forecast`
    ;;


  dimension: pk {
    hidden: yes
    primary_key: yes
    sql: concat(${country},"-",${brand},"-",${service_type},"-",${date_date}) ;;
  }


  dimension: brand {
    type: string
    sql: upper(${TABLE}.brand) ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: forecasted_orders {
    type: number
    sql: ${TABLE}.forecasted_orders ;;
  }

  dimension: service_type {
    type: string
    sql: ${TABLE}.service_type ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.vertical ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
