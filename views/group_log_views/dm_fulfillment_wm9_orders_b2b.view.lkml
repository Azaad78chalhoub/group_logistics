include: "../group_log_views/dm_fulfillment_wm9_orders.view"

view: dm_fulfillment_wm9_orders_b2b {

  extends: [dm_fulfillment_wm9_orders]

  set: b2b_orders {
    fields: [facility_name,
             order_id,
             order_custom_type,
             order_extern_orderkey,
             order_status_description,
             storerkey,
             owner,
             codelkup_type_description,
             vas_applied,
             vas_reason,
             external_factor,
             fc_controllable,
             order_creation_raw,
             picked_raw,
             packed_raw,
             ready_to_ship_raw,
             shipped_raw,
             order_requested_ship_date_raw,
             dm_fulfillment_wm9_orders_details_b2b.order_order_quantity,
             dm_fulfillment_wm9_orders_details_b2b.order_ship_quantity,
            dm_fulfillment_wm9_orders_details_b2b.order_picked_quantity,
             dim_order_qty_exclusion.total_division_issue,
             dm_fulfillment_wm9_orders_details_b2b.count,
             b2b_order_fulfillment.is_order_fulfilled,
             dim_logistics_claims.valid_claims,
             pack_on_time,
             perfect_order]
  }

  dimension_group: new_order_creation {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql:
     case
      when ${order_creation_date} = ${dim_logistics_public_holiday.holiday_start_date}
      then
        date_add(${order_creation_raw},interval ${dim_logistics_public_holiday.no_of_days_until_reopening} day)
      else
        ${order_creation_raw}
     end;;
  }


  #Order Count START
  measure: count {
    label: "Count Of Orders"
    group_label: "Order Count"
    type: count_distinct
    sql: ${order_id} ;;
    drill_fields: [b2b_orders*]
  }

  measure: count_full {
    label: "Count - Total Orders"
    group_label: "Order Count"
    description: "Count of total orders"
    type: count_distinct
    sql: ${order_id} ;;
    drill_fields: [b2b_orders*]
  }

  measure: count_shipped {
    label: "Count - Total Shipped Orders"
    group_label: "Order Count"
    description: "Count of total shipped orders"
    type: count_distinct
    sql: ${order_id} ;;
    filters: [order_status_code: "95,101"]
    drill_fields: [b2b_orders*]
  }

  measure: count_packed {
    label: "Count - Total Packed Orders"
    group_label: "Order Count"
    description: "Count of total Packed orders"
    type: count_distinct
    sql: ${order_id} ;;
    filters: [order_status_code: "102,88,95,101"]
    drill_fields: [b2b_orders*]
  }

  measure: count_manifested {
    label: "Count - Total Manifested Orders"
    group_label: "Order Count"
    description: "Count of total manifested orders"
    type: count_distinct
    sql: ${order_id} ;;
    filters: [order_status_code: "88,95,101"]
    drill_fields: [b2b_orders*]
  }

  measure: count_cancelled {
    label: "Count - Total Cancelled Orders"
    group_label: "Order Count"
    description: "Count of total cancelled orders"
    type: count_distinct
    sql: ${order_id} ;;
    #filters: [order_status_code: "00,104"]
    filters: [order_status_code: "00"]
    drill_fields: [b2b_orders*]
  }

  measure: count_cancel_delivery {
    label: "Count - Total Cancel Delivery Orders"
    group_label: "Order Count"
    description: "Count of total cancel delivery orders"
    type: count_distinct
    sql: ${order_id} ;;
    filters: [order_status_code: "104"]
    drill_fields: [b2b_orders*]
  }

  measure: count_in_progress {
    label: "Count - Total In Progress Orders"
    group_label: "Order Count"
    description: "Count of total orders in progress"
    type: count_distinct
    sql: ${order_id} ;;
    filters: [order_status_code: "-00,-104,-95,-101"]
    drill_fields: [b2b_orders*]
  }

  measure: active_orders{
    label: "Count - Total Active Orders"
    group_label: "Order Count"
    description: "Count of total orders which are not cancelled"
    type: number
    sql: ${count_full}-${count_cancelled} ;;
    drill_fields: [b2b_orders*]
  }

  measure: vas_count {
    label: "Order Count - VAS Activity"
    group_label: "Order Count"
    description: "Count - Different VAS activities applied on the order"
    hidden: yes
    type: count_distinct
    sql: ${order_id} ;;
    filters: [ vas_reason: "-NULL"]
  }

  measure: total_pick_count {
    label: "Pick Count"
    group_label: "Order Count"
    description: "Number of picks done on the order"
    type: sum
    sql: ${pick_count} ;;
  }

  dimension: order_custom_type {
    group_label: "Pack On Time SLA"
    type: string
    sql: case
            when ${facility_name} = 'DIP' then
              case when ${codelkup_otype_description} = 'RTV' then 'RTV'
                   when ${codelkup_otype_description} = 'Internal Kitting' then 'KITTING'
                   when ${iso_country_code_desc} <> 'United Arab Emirates' then 'EXPORT'
                   when ${order_c_city} IN (' DUBAI',' DUBAI ','DEIRA, DUBAI','Dubai','DUBAI ','DUBAI UAE','DUBAI-DIP','DUBAI') then 'LOCAL'
                   when ${order_c_city} NOT IN (' DUBAI',' DUBAI ','DEIRA, DUBAI','Dubai','DUBAI ','DUBAI UAE','DUBAI-DIP','DUBAI') then 'OOS'
                   else 'LOCAL'
              end
            when ${facility_name} = 'BAH' then
              case when ${codelkup_otype_description} = 'RTV' then 'RTV'
                   when ${codelkup_otype_description} = 'Internal Kitting' then 'KITTING'
                   when ${iso_country_code_desc} <> 'Bahrain' then 'EXPORT'
                   else 'LOCAL'
              end
            when ${facility_name} = 'QLC' then
              case when ${codelkup_otype_description} = 'RTV' then 'RTV'
                   when ${codelkup_otype_description} = 'Internal Kitting' then 'KITTING'
                   when ${iso_country_code_desc} <> 'Qatar' then 'EXPORT'
                   else 'LOCAL'
              end
            when ${facility_name} = 'MAC' then
              case when ${codelkup_otype_description} = 'RTV' then 'RTV'
                   when ${codelkup_otype_description} = 'Internal Kitting' then 'KITTING'
                   when ${order_c_city} IN ('NEW CAIRO','NEW-CAIRO-CITY','CAIRO','Cairo') then 'LOCAL'
                   when ${order_c_city} NOT IN ('NEW CAIRO','NEW-CAIRO-CITY','CAIRO','Cairo') then 'OOS'
                   else 'LOCAL'
              end
            when ${facility_name} = 'CADF' then 'LOCAL'
            when ${facility_name} = 'JED' then
              case when ${codelkup_otype_description} = 'RTV' then 'RTV'
                   when ${codelkup_otype_description} = 'Internal Kitting' then 'KITTING'
                   when ${iso_country_code_desc} <> 'Saudi Arabia' then 'EXPORT'
                   when ${order_c_city} IN ('JEDDAH','JIDDAH','JJEDDAH') then 'LOCAL'
                   when ${order_c_city} NOT IN ('JEDDAH','JIDDAH','JJEDDAH') then 'OOS'
                   else 'LOCAL'
              end
            when ${facility_name} = 'RYD' then
              case when ${codelkup_otype_description} = 'RTV' then 'RTV'
                   when ${codelkup_otype_description} = 'Internal Kitting' then 'KITTING'
                   when ${iso_country_code_desc} <> 'Saudi Arabia' then 'EXPORT'
                   when ${order_c_city} IN ('RIYADH','RIYADH AL KHABRA','RIYADH-AL-KHABRA','RYADH','RYIADH') then 'LOCAL'
                   when ${order_c_city} NOT IN ('RIYADH','RIYADH AL KHABRA','RIYADH-AL-KHABRA','RYADH','RYIADH') then 'OOS'
                   else 'LOCAL'
              end
            when ${facility_name} = 'H&C' then
              case when ${codelkup_otype_description} = 'RTV' then 'RTV'
                   when ${codelkup_otype_description} = 'Internal Kitting' then 'KITTING'
                   when ${iso_country_code_desc} <> 'Kuwait' then 'EXPORT'
                   else 'LOCAL'
              end
            when ${facility_name} = 'JAFZA' then
                case when ${codelkup_otype_description} = 'RTV' then 'RTV'
                   when ${codelkup_otype_description} = 'Internal Kitting' then 'KITTING'
                   --when ${codelkup_otype_description} = 'Crossdock' then 'CROSSDOCK'
                   when ${iso_country_code_desc} <> 'United Arab Emirates' then 'EXPORT'
                   else 'LOCAL'
                end
         end;;
  }

  dimension: current_date {
    group_label: "Pack On Time SLA"
    type: string
    sql: case
          when ${country}="UAE" then  DATETIME(current_timestamp(), "Asia/Dubai")
          when ${country}="KUWAIT" then DATETIME(current_timestamp(), "Asia/Riyadh")
          when ${country}="KSA" then DATETIME(current_timestamp(), "Asia/Riyadh")
          when ${country}="QATAR" then DATETIME(current_timestamp(), "Asia/Riyadh")
          when ${country}="BAHRAIN" then DATETIME(current_timestamp(), "Asia/Riyadh")
          when ${country}="EGYPT" then DATETIME(current_timestamp(), "Egypt")
          else null
         end;;
  }

  # dimension: expected_pack_time_1 {
  #   group_label: "Pack On Time SLA"
  #   hidden: no
  #   type: string
  #   sql:
  #     case
  #       when time(${order_creation_raw}) <= cast(${dim_wh_pack_on_time_sla.cutoff_time} as time)
  #         then datetime( date_add(${order_creation_date}, interval ${dim_wh_pack_on_time_sla.before_d_day} day), ${dim_wh_pack_on_time_sla.pack_time})
  #       when time(${order_creation_raw}) > cast(${dim_wh_pack_on_time_sla.cutoff_time} as time)
  #         then datetime( date_add(${order_creation_date}, interval ${dim_wh_pack_on_time_sla.after_d_day} day), ${dim_wh_pack_on_time_sla.pack_time})
  #     end ;;
  # }

  dimension: expected_pack_time_1 {
    group_label: "Pack On Time SLA"
    hidden: no
    type: string
    sql:
      case
        when time(${new_order_creation_raw}) <= cast(${dim_wh_pack_on_time_sla.cutoff_time} as time)
           then datetime( date_add(${new_order_creation_date}, interval ${dim_wh_pack_on_time_sla.before_d_day} day), ${dim_wh_pack_on_time_sla.pack_time})
        when time(${new_order_creation_raw}) > cast(${dim_wh_pack_on_time_sla.cutoff_time} as time)
           then datetime( date_add(${new_order_creation_date}, interval ${dim_wh_pack_on_time_sla.after_d_day} day), ${dim_wh_pack_on_time_sla.pack_time})
      end ;;
  }

    dimension:  expected_pack_time_2{
    group_label: "Pack On Time SLA"
    #note- to check the expected pack on time is on a public holiday or not, then add reopening days
    type: string
    sql:
    case
      when date(${expected_pack_time_1}) = ${dim_logistics_public_holiday2.holiday_start_date}
      then
        date_add(${expected_pack_time_1},interval ${dim_logistics_public_holiday2.no_of_days_until_reopening} day)
      else
        ${expected_pack_time_1}
    end;;
  }

  dimension:  expected_pack_time_3{
    hidden: no
    group_label: "Pack On Time SLA"
    #note- to check the expected pack on time is on a friday or not, then add 2 days
    type: string
    sql:
    case
      when format_datetime('%A',date(${expected_pack_time_2})) ='Friday'
        then date_add(${expected_pack_time_2},interval 2 day)
        else ${expected_pack_time_2}
    end;;
  }

  dimension: is_b2b_vas_applied {
    group_label: "Pack On Time SLA"
    label: "B2B Vas Activity"
    #note- identifying vas exclusion condition
    hidden: no
    description: "Key to identify any VAS activities applied on the orders or not"
    type: yesno
    sql: ${b2b_vas_applied} is true
      and ${b2b_order_fulfillment.order_order_quantity} >=50 ;;
  }

  # dimension:  expected_pack_time{
  #   label: "Expected Pack Time"
  #   group_label: "Pack On Time SLA"
  #   type: string
  #   sql:
  #     case
  #     when ${is_b2b_vas_applied} is true then
  #       case
  #         when ${facility_name} = 'DIP' then
  #           date_add(${expected_pack_time_1},interval 2 day)
  #         when ${facility_name} = 'JED' then
  #           date_add(${expected_pack_time_1},interval 3 day)
  #         when ${facility_name} = 'RYD' then
  #           date_add(${expected_pack_time_1},interval 3 day)
  #         when ${facility_name} = 'JAFZA' then
  #           date_add(${expected_pack_time_1},interval 4 day)
  #         else ${expected_pack_time_1}
  #       end
  #     else ${expected_pack_time_1}
  #     end;;
  # }

  dimension:  expected_pack_time{
    label: "Expected Pack Time"
    group_label: "Pack On Time SLA"
    #note- adding 4 days if
    type: string
    sql:
      case
      when ${is_b2b_vas_applied} is true
        then date_add(${expected_pack_time_3},interval 4 day)
        else ${expected_pack_time_3}
      end;;
  }

  dimension: pack_on_time {
    group_label: "Pack On Time SLA"
    #Note1: Crossdock by default is pack on time MET
    #Note2: For kitting packed time is not mandatory for all WH's, it is directly shipped, hence coalesce used
    type: string
    sql:
    case
      when ${codelkup_otype_description} = 'Crossdock'
        then "Met"
      when coalesce(${packed_raw},${shipped_raw}) <= cast(${expected_pack_time} as timestamp)
        then "Met"
      when coalesce(${packed_raw},${shipped_raw}) is null and (
        cast(${current_date} as timestamp) > cast(${expected_pack_time} as timestamp)
        and  cast(${current_date} as timestamp) > ${order_requested_ship_date_raw})
        then "Missed"
      when coalesce(${packed_raw},${shipped_raw}) > cast(${expected_pack_time} as timestamp)
        then
          case
           when coalesce(${packed_raw},${shipped_raw}) <= ${order_requested_ship_date_raw}
             then "Met"
             else "Missed"
          end
    end ;;
  }

  measure: count_pack_on_time {
    group_label: "Pack On Time SLA"
    label: "Pack On Time Met"
    type: count_distinct
    sql:  ${order_id};;
    #filters: [pack_on_time: "Met",order_status_code: "-00,-104"]
    filters: [pack_on_time: "Met",order_status_code: "-00"]
    drill_fields: [b2b_orders*]
  }

  measure: pack_on_time_missed {
    group_label: "Pack On Time SLA"
    label: "Pack On Time Missed"
    type: count_distinct
    sql:  ${order_id};;
    #filters: [pack_on_time: "Missed",order_status_code: "-00,-104"]
    filters: [pack_on_time: "Missed",order_status_code: "-00"]
    drill_fields: [b2b_orders*]
  }

  measure: pcnt_pack_on_time {
    group_label: "Pack On Time SLA"
    label: "Pack On Time Met %"
    type: number
    value_format_name: percent_2
    sql: safe_divide(${count_pack_on_time}+${missed_pack_on_time_ext_factor},${count_pack_on_time}+${pack_on_time_missed});;
  }

  measure: missed_pack_on_time_pcnt {
    group_label: "Pack On Time SLA"
    label: "Pack On Time Missed %"
    type: number
    value_format_name: percent_2
    sql: safe_divide(${pack_on_time_missed},${count_pack_on_time}+${pack_on_time_missed});;
  }

  measure: missed_pack_on_time_ext_factor {
    group_label: "Pack On Time SLA"
    label: "Missed - External Factor - Non Controllable"
    description: "Count of orders with non controllable external factors"
    type: count_distinct
    sql:
        case when ${fc_controllable} = "No" then  ${order_id}
            else null
        end;;
    #filters: [pack_on_time: "Missed",order_status_code: "-00,-104"]
    filters: [pack_on_time: "Missed",order_status_code: "-00"]
    drill_fields: [b2b_orders*]
  }

  measure: missed_pack_on_time_ext_factor_non {
    group_label: "Pack On Time SLA"
    label: "Missed - External Factor - Controllable"
    description: "Count of orders with controllable external factors"
    type: count_distinct
    sql:
        case when ${fc_controllable} = "Yes" then  ${order_id}
            else null
        end;;
    #filters: [pack_on_time: "Missed",order_status_code: "-00,-104"]
    filters: [pack_on_time: "Missed",order_status_code: "-00"]
    drill_fields: [b2b_orders*]
  }

  # dimension: perfect_order {
  #   group_label: "Perfect Order"
  #   type: yesno
  #   sql:
  #         ${pack_on_time} = "Met"
  #         and ${b2b_order_fulfillment.is_order_fulfilled} is true
  #         and coalesce(${dim_logistics_claims.valid_claims},-1) <> 1;;
  # }

  dimension: pack_on_time_po_check {
    group_label: "Perfect Order"
    type: string
    #note: perfect order check is valid, for those with pack on time as met or missed with fc non controllable
    sql:
      case
          when ${pack_on_time} = "Met"
             then "Yes"
          when ${pack_on_time} = "Missed" and  ${fc_controllable} = "No"
             then "Yes"
          else
            "No"
      end;;
  }

  dimension: perfect_order {
    group_label: "Perfect Order"
    type: string
    #note: to check orders in only those status for perfect order
    #and ${pack_on_time} = "Met"
    sql:
      case
          when ${dm_fulfillment_wm9_orders_b2b.order_status_code} not in ('102','88','95','101')
            then "NA"
          when ${dm_fulfillment_wm9_orders_b2b.order_status_code} in ('102','88','95','101')
          and ${pack_on_time_po_check} ="Yes"
          and ${b2b_order_fulfillment.is_order_fulfilled} is true
          and coalesce(${dim_logistics_claims.valid_claims},-1) <> 1
            then "Yes"
          else
            "No"
      end;;
    }

  measure: count_perfect_order {
    group_label: "Perfect Order"
    label: "Count Of Perfect Orders"
    type: count_distinct
    sql: ${order_id} ;;
    filters: [perfect_order: "Yes"]
    drill_fields: [b2b_orders*]
  }

  measure: count_non_perfect_order {
    group_label: "Perfect Order"
    label: "Count Of Non Perfect Orders"
    type: count_distinct
    sql: ${order_id} ;;
    filters: [perfect_order: "No"]
    drill_fields: [b2b_orders*]
  }

  measure: count_4_perfect_order {
    hidden: no
    label: "Count for Orders"
    group_label: "Perfect Order"
    type: count_distinct
    sql: ${order_id} ;;
    filters: [order_status_code: "102,88,95,101"]
  }

  measure: pcnt_perfect_order {
    group_label: "Perfect Order"
    label: "Perfect Order %"
    type: number
    value_format_name: percent_2
    #sql: safe_divide(${count_perfect_order},${count}-${count_cancelled});;
    sql: safe_divide(${count_perfect_order},${count_4_perfect_order});;
  }

  measure: count_orders_invalid_claims {
    group_label: "Claims"
    label: "Count Of Orders - Without Valid Claims"
    type: count_distinct
    sql: ${order_id} ;;
    filters: [dim_logistics_claims.valid_claims: "null,0"]
    drill_fields: [b2b_orders*]
  }

  measure: pcnt_order_invalid_claims {
    group_label: "Claims"
    label: "Orders Without Valid Claims %"
    type: number
    value_format_name: percent_2
    sql: safe_divide(${count_orders_invalid_claims},${count});;
  }

 }
