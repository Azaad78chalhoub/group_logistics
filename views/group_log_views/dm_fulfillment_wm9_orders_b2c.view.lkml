include: "../group_log_views/dm_fulfillment_wm9_orders.view"

view: dm_fulfillment_wm9_orders_b2c {

  extends: [dm_fulfillment_wm9_orders]

# TIME TO PICK START
  measure: time_to_pick_time {
    group_label: "Time To Pick"
    label: "Avg Time Spent In Picking"
    description: "Average time taken from order creation to pick complete"
    hidden: no
    type: average
    # sql: DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    # value_format: "D:H:MM"
    sql: cast(DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric);;
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_pick_95th_percentile {
    hidden: no
    label: "Time To Pick - 95"
    group_label: "Time To Pick"
    type: percentile
    percentile: 95
    # sql: DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    # value_format: "D:H:MM"
    sql: cast(DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;

  }

  measure: time_to_pick_p1{
    label: "Avg Pick Time - Same Day Orders"
    group_label: "Time To Pick"
    description: "Average pick time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "1"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_pick_p3{
    label: "Avg Pick Time - Next Day Orders"
    group_label: "Time To Pick"
    description: "Average pick time for a priority 3 next day order"
    type: average
    #sql: DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "3"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_pick_p5 {
    label: "Avg Pick Time - Standard Orders"
    group_label: "Time To Pick"
    description: "Average pick time for a priority 5 standard order"
    type: average
    #sql: DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "5"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }
  # TIME TO PICK END

  # TIME TO PACK START
  measure: time_to_pack_time {
    group_label: "Time To Pack"
    label: "Avg Time Spent In Packing"
    description: "Average time taken from pick complete to pack complete"
    hidden: no
    type: average
    #sql: DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_pack_95th_percentile {
    hidden: no
    label: "Time To Pack - 95"
    group_label: "Time To Pack"
    type: percentile
    percentile: 95
    #sql: DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_pack_p1 {
    label: "Avg Pack Time - Same Day Orders"
    group_label: "Time To Pack"
    description: "Average pack time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "1"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_pack_p3 {
    label: "Avg Pack Time - Next Day Orders"
    group_label: "Time To Pack"
    description: "Average pack time for a priority 3 next day order"
    type: average
    #sql: DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "3"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_pack_p5 {
    label: "Avg Pack Time - Standard Orders"
    group_label: "Time To Pack"
    description: "Average pack time for a priority 5 standard order"
    type: average
    #sql: DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "5"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }
  # TIME TO PACK END

  # TIME TO READY TO SHIP START
  measure: time_to_ready_to_ship_time {
    label: "Avg Time Spent In Ready To Ship"
    group_label: "Time To Ready To Ship"
    description: "Average time taken from Pack complete to ready to ship"
    hidden: no
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_ready_to_ship_95th_percentile {
    hidden: no
    label: "Time To Ready To Ship - 95"
    group_label: "Time To Ready To Ship"
    type: percentile
    percentile: 95
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_ready_ship_p1 {
    label: "Avg Ready To Ship Time - Same Day Orders"
    group_label: "Time To Ready To Ship"
    description: "Average ready to ship time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "1"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_ready_ship_p3 {
    label: "Avg Ready To Ship Time - Next Day Orders"
    group_label: "Time To Ready To Ship"
    description: "Average ready to ship time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "3"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_ready_ship_p5 {
    label: "Avg Ready To Ship Time - Standard Orders"
    group_label: "Time To Ready To Ship"
    description: "Average ready to ship time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "5"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }
  # TIME TO READY TO SHIP END

  # TIME TO SHIP START
  measure: time_to_ship_time {
    label: "Avg Time Spent To Ship Out"
    group_label: "Time To Ship"
    description: "Average time taken from pack complete to ship out"
    hidden: no
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_ship_95th_percentile {
    hidden: no
    label: "Time To Ship - 95"
    group_label: "Time To Ship"
    type: percentile
    percentile: 95
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_ship_p1 {
    label: "Avg Ship Time - Same Day Orders"
    group_label: "Time To Ship"
    description: "Average ship time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "1"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_ship_p3 {
    label: "Avg Ship Time - Next Day Orders"
    group_label: "Time To Ship"
    description: "Average ship time for a priority 3 next day order"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "3"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: time_to_ship_p5 {
    label: "Avg Ship Time - Standard Orders"
    group_label: "Time To Ship"
    description: "Average ship time for a priority 5 standard order"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "5"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }
  # TIME TO SHIP END

# CLick To Manifest Time START
  measure: total_manifest_time {
    label: "Avg Total Time Spent To Manifest"
    group_label: "Click To Manifest"
    description: "Average time taken from order creation to manifest"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: total_manifest_95th_percentile {
    label: "Total Time - 95"
    group_label: "Click To Manifest"
    type: percentile
    percentile: 95
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: total_manifest_time_p1 {
    label: "Avg Total Manifest Time - Same Day Orders"
    group_label: "Click To Manifest"
    description: "Average manifest time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "1"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: total_manifest_time_p3 {
    label: "Avg Total Manifest Time - Next Day Orders"
    group_label: "Click To Manifest"
    description: "Average manifest time for a priority 3 next day order"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "3"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: total_manifest_time_p5 {
    label: "Avg Total Manifest Time - Standard Orders"
    group_label: "Click To Manifest"
    description: "Average manifest time for a priority 5 standard order"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "5"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }
  # CLick To Manifest Time END

  # CLick To Ship Time START
  measure: total_ship_time {
    label: "Avg Total Time Spent To Ship Out"
    group_label: "Click To Ship"
    description: "Average time taken from order creation to ship out"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: total_ship_95th_percentile {
    hidden: no
    label: "Total Time - 95"
    group_label: "Click To Ship"
    type: percentile
    percentile: 95
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: total_ship_time_p1 {
    label: "Avg Total Ship Time - Same Day Orders"
    group_label: "Click To Ship"
    description: "Average ship time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "1"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: total_ship_time_p3 {
    label: "Avg Total Ship Time - Next Day Orders"
    group_label: "Click To Ship"
    description: "Average ship time for a priority 3 next day order"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "3"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: total_ship_time_p5 {
    label: "Avg Total Ship Time - Standard Orders"
    group_label: "Click To Ship"
    description: "Average ship time for a priority 5 standard order"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [priority: "5"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }
  # CLick To Ship Time END

  #Order Count START
  measure: count {
    label: "Count Of Orders"
    group_label: "Order Count"
    type: count_distinct
    sql: ${order_id} ;;
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw,expected_manifested_date_raw,to_be_manifested_in_hrs,ready_to_ship_raw,shipped_raw,order_once_in_error_status]
    # drill_fields: [order_creation_date,count,count_sla_missed]
    # link: {
    #   label: "Order Count Time Series"
    #   url: "
    #   {% assign vis_config = '{
    #   \"type\" : \"looker_line\"
    #   }' %}
    #   {{ link }}&vis_config={{ vis_config | encode_uri }}&toggle=dat,pik,vis&limit=5000"
    # }
  }
  dimension_group: expected_manifested_date {
    group_label: "Live Dashboard"
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CASE WHEN ${priority}='1' THEN TIMESTAMP_ADD(${order_creation_raw}, INTERVAL 2 HOUR)
          WHEN ${priority} = '3' THEN TIMESTAMP_ADD(${order_creation_raw}, INTERVAL 4 HOUR)
          WHEN ${priority} = '5' THEN TIMESTAMP_ADD(${order_creation_raw}, INTERVAL 1 DAY)
          END ;;
  }

dimension: manifest_time {
  group_label: "Live Dashboard"
  hidden: yes
  type: string
  sql: EXTRACT(TIME FROM ${expected_manifested_date_raw});;
}
  dimension: manifest_day {
    group_label: "Live Dashboard"
    hidden: yes
    type: string
    sql: EXTRACT(DAYOFWEEK FROM ${expected_manifested_date_raw});;
  }
dimension: to_be_manifested_in_hrs {
  group_label: "Live Dashboard"
  hidden: yes
  type: number
  sql:TIMESTAMP_DIFF(${expected_manifested_date_raw},CAST(${current_date} as TIMESTAMP),HOUR);;
}
dimension: exceed_cutoff {
  group_label: "Live Dashboard"
  hidden: yes
  type: yesno
  sql: CAST(${manifest_time} AS TIME) BETWEEN '0:00:00' AND '23:59:00' ;;
}
  dimension_group: target_manifest_date {
    group_label: "Live Dashboard"
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year]
    sql: CAST(CONCAT(${expected_manifested_date_date}," ",${manifest_time}) AS TIMESTAMP) ;;
  }
dimension: to_be_manifested_in {
  group_label: "Live Dashboard"
  type: string
  sql: CASE WHEN ${ready_to_ship_raw} IS NULL AND TIMESTAMP_DIFF(${expected_manifested_date_raw},CAST(${current_date} as TIMESTAMP),HOUR)= 1 THEN "1 Hour"
            WHEN ${ready_to_ship_raw} IS NULL AND TIMESTAMP_DIFF(${expected_manifested_date_raw},CAST(${current_date} as TIMESTAMP),HOUR)= 2 THEN "2 Hours"
            WHEN ${ready_to_ship_raw} IS NULL AND TIMESTAMP_DIFF(${expected_manifested_date_raw},CAST(${current_date} as TIMESTAMP),HOUR)= 3 THEN "3 Hours"
            WHEN ${ready_to_ship_raw} IS NULL AND TIMESTAMP_DIFF(${expected_manifested_date_raw},CAST(${current_date} as TIMESTAMP),HOUR)= 4 THEN "4 Hours"
            WHEN ${ready_to_ship_raw} IS NULL AND TIMESTAMP_DIFF(${expected_manifested_date_raw},CAST(${current_date} as TIMESTAMP),HOUR)= 5 THEN "5 Hours"
            WHEN ${ready_to_ship_raw} IS NULL AND TIMESTAMP_DIFF(${expected_manifested_date_raw},CAST(${current_date} as TIMESTAMP),HOUR)>= 5 THEN "More than 5 Hours"
      END;;
}
dimension: missed_manifest_by {
  group_label: "Live Dashboard"
  type: string
  sql: CASE WHEN ${ready_to_ship_raw} IS NULL AND  TIMESTAMP_DIFF(CAST(${current_date} as TIMESTAMP),${expected_manifested_date_raw},HOUR)=1 THEN "1 Hour"
            WHEN ${ready_to_ship_raw} IS NULL AND TIMESTAMP_DIFF(CAST(${current_date} as TIMESTAMP),${expected_manifested_date_raw},HOUR)=2 THEN "2 Hours"
            WHEN ${ready_to_ship_raw} IS NULL AND  TIMESTAMP_DIFF(CAST(${current_date} as TIMESTAMP),${expected_manifested_date_raw},HOUR)=3 THEN "3 Hours"
            WHEN ${ready_to_ship_raw} IS NULL AND  TIMESTAMP_DIFF(CAST(${current_date} as TIMESTAMP),${expected_manifested_date_raw},HOUR)=4 THEN "4 Hours"
            WHEN ${ready_to_ship_raw} IS NULL AND TIMESTAMP_DIFF(CAST(${current_date} as TIMESTAMP),${expected_manifested_date_raw},HOUR)=5 THEN "5 Hours"
            WHEN ${ready_to_ship_raw} IS NULL AND TIMESTAMP_DIFF(CAST(${current_date} as TIMESTAMP),${expected_manifested_date_raw},HOUR)>=5 THEN "More than 5 hours"
      END;;

}

  measure: count_full {
    label: "Count - Total Orders"
    group_label: "Order Count"
    description: "Count of total orders"
    type: count_distinct
    sql: ${order_id} ;;
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
    #drill_fields: [order_creation_date,count_full,count_sla_missed]
    # link: {
    #   label: "Order Count Time Series"
    #   url: "
    #   {% assign vis_config = '{
    #   \"type\" : \"looker_line\"
    #   }' %}
    #   {{ link }}&vis_config={{ vis_config | encode_uri }}&toggle=dat,pik,vis&limit=5000"
    # }
  }

  measure: count_p1 {
    label: "Count - Same Day Orders"
    group_label: "Order Count"
    description: "Count of priority 1 same day orders"
    type: count_distinct
    sql: ${order_id} ;;
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
    filters: [priority: "1"]
  }

  measure: count_p3 {
    label: "Count - Next Day Orders"
    group_label: "Order Count"
    description: "Count of priority 3 next day orders"
    type: count_distinct
    sql: ${order_id} ;;
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
    filters: [priority: "3"]
  }

  measure: count_p5 {
    label: "Count - Standard Orders"
    group_label: "Order Count"
    description: "Count of priority 5 standard orders"
    type: count_distinct
    sql: ${order_id} ;;
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
    filters: [priority: "5"]
  }

  measure: count_shipped {
    label: "Count - Total Shipped Orders"
    group_label: "Order Count"
    description: "Count of total shipped orders"
    type: count_distinct
    sql: ${order_id} ;;
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
    filters: [order_status_code: "95,101"]
  }

  measure: count_packed {
    label: "Count - Total Packed Orders"
    group_label: "Order Count"
    description: "Count of total Packed orders"
    type: count_distinct
    sql: ${order_id} ;;
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
    filters: [order_status_code: "102,88,95,101"]
  }

  measure: count_manifested {
    label: "Count - Total Manifested Orders"
    group_label: "Order Count"
    description: "Count of total manifested orders"
    type: count_distinct
    sql: ${order_id} ;;
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
    filters: [order_status_code: "88,95,101"]
  }

  measure: count_cancelled {
    label: "Count - Total Cancelled Orders"
    group_label: "Order Count"
    description: "Count of total cancelled orders"
    type: count_distinct
    sql: ${order_id} ;;
    filters: [order_status_code: "00,104"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: count_in_progress {
    label: "Count - Total In Progress Orders"
    group_label: "Order Count"
    description: "Count of total orders in progress"
    type: count_distinct
    sql: ${order_id} ;;
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw,expected_manifested_date_raw,to_be_manifested_in_hrs,ready_to_ship_raw,shipped_raw,order_once_in_error_status]
    filters: [order_status_code: "-00,-104,-95,-101"]
  }

  measure: active_orders{
    label: "Count - Total Active Orders"
    group_label: "Order Count"
    description: "Count of total orders which are not cancelled"
    type: number
    sql: ${count_full}-${count_cancelled} ;;
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: vas_count {
    label: "Order Count - VAS Activity"
    group_label: "Order Count"
    description: "Count - Different VAS activities applied on the order"
    hidden: yes
    type: count_distinct
    sql: ${order_id} ;;
    filters: [ vas_reason: "-NULL"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  # TIME TO PICK 95 PT AVG
  measure: time_to_pick_95th_pt_avg {
    hidden: no
    label: "Time To Pick - 95 AVG"
    group_label: "Time To Pick"
    type: average
    #sql: DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [dm_fulfillment_percentile.order2pick_percentile: "Yes" ]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  # TIME TO PACK 95 PT AVG
  measure: time_to_pack_95th_percentile_avg {
    hidden: no
    label: "Time To Pack - 95 AVG"
    group_label: "Time To Pack"
    type: average
    #sql: DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND) as numeric) ;;
    filters: [dm_fulfillment_percentile.pack2pick_percentile: "Yes" ]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  # TIME TO READY TO SHIP  95 PT AVG
  measure: time_to_ready_to_ship_95th_percentile_avg {
    hidden: no
    label: "Time To Ready To Ship - 95 AVG"
    group_label: "Time To Ready To Ship"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND) as numeric) ;;
    filters: [dm_fulfillment_percentile.pack2readytoship_percentile: "Yes" ]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

   # TIME TO SHIP 95 PT AVG
  measure: time_to_ship_95th_percentile_avg {
    hidden: no
    label: "Time To Ship - 95 AVG"
    group_label: "Time To Ship"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND) as numeric) ;;
    filters: [dm_fulfillment_percentile.readytoship2ship_percentile: "Yes" ]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  # CLick To Manifest Time 95 PT AVG
  measure: total_manifest_95th_percentile_avg {
    label: "Total Time - 95 AVG"
    group_label: "Click To Manifest"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [dm_fulfillment_percentile.click2manifest_percentile: "Yes" ]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  # CLick To Ship Time 95 PT AVG
  measure: total_ship_95th_percentile_avg {
    hidden: no
    label: "Total Time - 95 AVG"
    group_label: "Click To Ship"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [dm_fulfillment_percentile.click2ship_percentile: "Yes" ]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  # CLICK TO COLLECT SLA START

  # dimension: cutoff_collection_day {
  #   group_label: "Click To Collect"
  #   type: string
  #   sql:
  #   case
  #       when ${country} = "EGYPT" and upper(${carrier_code}) ="ARAMEX" and time(${order_creation_raw}) <= time "11:30:00"
  #       then datetime(${order_creation_date}, time "17:00:00")
  #       when ${country} = "EGYPT" and upper(${carrier_code}) ="ARAMEX" and time(${order_creation_raw}) > time "11:30:00"
  #       then datetime(date_add(${order_creation_date}, interval 1 day), time "17:00:00" )
  #       when ${country} = "EGYPT" and ${carrier_code} ="MYLERZ" and time(${order_creation_raw}) <= time "13:00:00"
  #       then datetime(${order_creation_date}, time "18:30:00" )
  #       when ${country} = "EGYPT" and ${carrier_code} ="MYLERZ" and time(${order_creation_raw}) > time "13:00:00"
  #       then datetime(date_add(${order_creation_date}, interval 1 day), time "18:30:00" )
  #       when ${country} = "UAE" and ${carrier_code} ="DHL" and time(${order_creation_raw}) <= time "23:59:59"
  #       then datetime(date_add(${order_creation_date}, interval 1 day), time "16:30:00" )
  #       when ${country} = "UAE" and ${carrier_code} ="FODEL" and time(${order_creation_raw}) <= time "23:59:59"
  #       then datetime(date_add(${order_creation_date}, interval 1 day), time "15:30:00" )
  #       when ${country} = "KSA" and format_datetime('%A',${order_creation_date}) = "Friday"
  #       then datetime( date_add(${order_creation_date}, interval 1 day),time "17:30:00" )
  #       when time(${order_creation_raw}) <= cast(${dim_wh_collection_time.cutoff_time} as time)
  #       then datetime( date_add(${order_creation_date}, interval ${dim_wh_collection_time.dday} day),${dim_wh_collection_time.shipout_time})
  #       when time(${order_creation_raw}) > cast(${dim_wh_collection_time.cutoff_time} as time)
  #       then datetime( date_add(${order_creation_date}, interval ${dim_wh_collection_time.dday}+1 day),${dim_wh_collection_time.shipout_time})
  #   end ;;
  # }

  # changed on 10-mar-2021 by sebin
  # dimension: cutoff_collection_day {
  #   group_label: "Click To Collect SLA"
  #   type: string
  #   sql: case
  #       when time(${order_creation_raw}) <= cast(${dim_wh_collection_time.cutoff_time} as time)
  #       then datetime( date_add(${order_creation_date}, interval ${dim_wh_collection_time.dday} day),${dim_wh_collection_time.shipout_time})
  #       when time(${order_creation_raw}) > cast(${dim_wh_collection_time.cutoff_time} as time)
  #       then datetime( date_add(${order_creation_date}, interval ${dim_wh_collection_time.dday}+1 day),${dim_wh_collection_time.shipout_time})
  #   end ;;
  # }

  dimension: cutoff_collection_day {
    group_label: "Click To Collect SLA"
    type: string
    sql: case
        when time(${order_creation_raw}) <= cast(${dim_wh_collection_time.cutoff_time} as time)
         then datetime( date_add(${order_creation_date}, interval ${dim_wh_collection_time.dday_before} day),${dim_wh_collection_time.shipout_time})
        when time(${order_creation_raw}) > cast(${dim_wh_collection_time.cutoff_time} as time)
         then datetime( date_add(${order_creation_date}, interval ${dim_wh_collection_time.dday_after} day),${dim_wh_collection_time.shipout_time})
    end ;;
  }

  dimension: current_date {
    group_label: "Click To Collect SLA"
    type: string
    sql: case
          when ${country}="UAE" then  DATETIME(current_timestamp(), "Asia/Dubai")
          when ${country}="KUWAIT" then DATETIME(current_timestamp(), "Asia/Riyadh")
          when ${country}="KSA" then DATETIME(current_timestamp(), "Asia/Riyadh")
          when ${country}="QATAR" then DATETIME(current_timestamp(), "Asia/Riyadh")
          when ${country}="BAHRAIN" then DATETIME(current_timestamp(), "Asia/Riyadh")
          when ${country}="EGYPT" then DATETIME(current_timestamp(), "Egypt")
          else null
         end;;
  }

  dimension: click_to_collect {
    group_label: "Click To Collect SLA"
    type: string
    sql:
    case
       when ${shipped_raw} <= cast(${cutoff_collection_day} as timestamp)
         then "Met"
        when ${shipped_raw} is null and cast(${current_date} as timestamp) > cast(${cutoff_collection_day} as timestamp)
         then "Missed"
         when ${shipped_raw} > cast(${cutoff_collection_day} as timestamp)
         then  "Missed"
        else null
    end ;;
  }

  measure: count_sla_met {
    label: "SLA Met - Total Orders"
    group_label: "Click To Collect SLA"
    description: "Count of total orders with SLA met."
    type: count_distinct
    sql: ${order_id} ;;
    filters: [click_to_collect: "Met",order_status_code: "-00,-104"]
    #filters: [click_to_collect: "Met",order_status_code: "95,101"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: sla_met_percnt {
    label: "SLA Met - Total Percnt"
    group_label: "Click To Collect SLA"
    description: "Percentage of total orders with SLA met."
    type: number
    value_format: "0.00\%"
    #sql: safe_divide(${count_sla_met},${active_orders})*100;;
    sql: safe_divide(${count_sla_met},(${count_sla_met}+${count_sla_missed}))*100;;
  }

  measure: count_sla_missed {
    label: "SLA Missed - Total Orders"
    group_label: "Click To Collect SLA"
    description: "Count of total orders with SLA missed."
    type: count_distinct
    sql: ${order_id} ;;
    filters: [click_to_collect: "Missed",order_status_code: "-00,-104"]
    #filters: [click_to_collect: "Missed",order_status_code: "95,101"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: sla_missed_percnt {
    label: "SLA Missed - Total Percnt"
    group_label: "Click To Collect SLA"
    description: "Percentage of total orders with SLA missed."
    type: number
    value_format: "0.00\%"
    #sql: safe_divide(${count_sla_missed},${active_orders})*100;;
    sql: safe_divide(${count_sla_missed},(${count_sla_met}+${count_sla_missed}))*100;;
    #html: <p style="color: black; background-color: lightblue; font-size:100%; text-align:center">{{ rendered_value }}</p> ;;
  }

  measure: shipped_missed_manifest_missed{
    label: "Shipped Missed Manifest Missed Count"
    group_label: "Click To Collect SLA"
    description: "Count of total orders with Manifest SLA missed."
    type: count_distinct
    sql:  ${order_id};;
    filters: [click_to_collect: "Missed",click_to_Manifest: "Missed",order_status_code: "-00,-104"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: count_sla_missed_reason1 {
    label: "SLA Missed -Manifest Missed - Total Orders"
    group_label: "Click To Collect SLA"
    description: "Count of total orders with SLA missed."
    type: count_distinct
    sql:
      case when ${fc_controllable} = "No" then  ${order_id}
         when ${order_once_in_error_status} is True then ${order_id}
         else null
    end;;
      #filters: [click_to_Manifest: "Missed",click_to_collect: "Missed",order_status_code: "95,101"]
      filters: [click_to_collect: "Missed",click_to_Manifest: "-Missed",order_status_code: "-00,-104"]
      drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: count_sla_missed_reason2 {
      label: "SLA Missed - Actual Missed- Total Orders"
      group_label: "Click To Collect SLA"
      description: "Count of total orders with SLA missed."
      type: count_distinct
      sql:
      case when (${fc_controllable} = "Yes" or ${fc_controllable} is null)  and ${order_once_in_error_status} is False
        then ${order_id}
        else null
       end;;
        #filters: [click_to_Manifest: "Met",click_to_collect: "Missed",order_status_code: "95,101"]
        filters: [click_to_collect: "Missed",click_to_Manifest: "-Missed",order_status_code: "-00,-104"]
        drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }
  # CLICK TO COLLECT SLA END

  # Manifest SLA BEGIN
  dimension: critical_pull_time {
    group_label: "Click To Manifest SLA"
    label: "Critical Pull Time"
    type: string
    sql: TIMESTAMP_SUB(cast(${cutoff_collection_day} as timestamp), interval 90 minute) ;;
  }

  dimension: click_to_Manifest {
    group_label: "Click To Manifest SLA"
    label: "Click To Manifest"
    type: string
    sql:
            case
                when ${ready_to_ship_raw}<=cast(${critical_pull_time} as timestamp)
                 then "Met"
                when ${ready_to_ship_raw} is null and cast(${current_date} as timestamp) > cast(${critical_pull_time} as timestamp)
                 then "Missed"
                 when ${ready_to_ship_raw} > cast(${critical_pull_time} as timestamp)
                 then  "Missed"
                else null
            end ;;
  }

  measure: count_manifest_sla_met {
    label: "Manifest SLA Met - Total Orders"
    group_label: "Click To Manifest SLA"
    description: "Count of total orders with SLA met."
    type: count_distinct
    sql: ${order_id} ;;
    #filters: [ click_to_Manifest: "Met",order_status_code: "88,95,101"]
    filters: [ click_to_Manifest: "Met",order_status_code: "-00,-104"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }


  measure: manifest_sla_met_percnt {
    label: "Manifest SLA Met - Total Percnt"
    group_label: "Click To Manifest SLA"
    description: "Percentage of total orders with SLA met."
    type: number
    value_format: "0.00\%"
    #sql: safe_divide(${count_manifest_sla_met},${active_orders})*100;;
    sql: safe_divide(${count_manifest_sla_met},(${count_manifest_sla_met}+${count_manifest_sla_missed}))*100;;
  }

  measure: count_manifest_sla_missed {
    label: "Manifest SLA Missed - Total Orders"
    group_label: "Click To Manifest SLA"
    description: "Count of total orders with SLA missed."
    type: count_distinct
    sql: ${order_id} ;;
    #filters: [ click_to_Manifest: "Missed",order_status_code: "88,95,101"]
    filters: [ click_to_Manifest: "Missed",order_status_code: "-00,-104"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: manifest_sla_missed_percnt {
    label: "Manifest SLA Missed - Total Percnt"
    group_label: "Click To Manifest SLA"
    description: "Percentage of total orders with SLA missed."
    type: number
    value_format: "0.00\%"
    #sql: safe_divide(${count_manifest_sla_missed},${active_orders})*100;;
    sql: safe_divide(${count_manifest_sla_missed},(${count_manifest_sla_met}+${count_manifest_sla_missed}))*100;;
  }

  measure: external_factor_count_manifest {
    label: "Order Count - External Factor Activity Manifest Missed"
    group_label: "Click To Manifest SLA"
    description: "Count - Different external factor activities applied on the order"
    type: count_distinct
    sql:
            case when ${fc_controllable} = "No" then  ${order_id}
                 when ${order_once_in_error_status} is True then ${order_id}
                 else null
            end;;
            #filters: [click_to_Manifest: "Missed",order_status_code: "88,95,101"]
      filters: [click_to_Manifest: "Missed",order_status_code: "-00,-104"]
      drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: external_factor_count_null_manifest {
      label: "Order Count - NON External Factor Activity Manifest Missed"
      group_label: "Click To Manifest SLA"
      description: "Count - Different external factor activities applied on the order"
      type: count_distinct
      sql:
              case when (${fc_controllable} = "Yes" or ${fc_controllable} is null)  and ${order_once_in_error_status} is False
                   then ${order_id}
                   else null
              end;;
              #filters: [click_to_Manifest: "Missed",order_status_code: "88,95,101"]
        filters: [click_to_Manifest: "Missed",order_status_code: "-00,-104"]
        drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }
  # Manifest SLA END

  measure: actuals_percnt {
    group_label: "Order Count"
    label: "Actual Order Percnt"
    type: number
    value_format: "0.00\%"
    sql: safe_divide(${count},${dm_wh_ecom_order_forecast_data.forecast_order_count})*100;;
  }

  measure: external_factor_count {
    label: "Order Count - External Factor Activity"
    group_label: "Order Count"
    description: "Count - Different external factor activities applied on the order"
    type: count_distinct
    sql: ${order_id} ;;
    filters: [ external_factor: "-NULL",click_to_collect: "Missed",order_status_code: "95,101"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  #Process Date START
  measure: p_orders {
    label: "Actual Orders"
    group_label: "WH Capacity"
    type: sum
    sql:
      case
        when ${date_master.date_date} = cast(${order_creation_raw} as date) then 1 else 0
      end ;;
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: p_orders_count_p1 {
    label: "Count - Same Day Orders"
    group_label: "WH Capacity"
    description: "Count of priority 1 same day orders"
    type: sum
    sql:
      case
        when ${date_master.date_date} = cast(${order_creation_raw} as date) then 1 else 0
      end ;;
    filters: [priority: "1"]
  }

  measure: p_orders_count_p3 {
    label: "Count - Next Day Orders"
    group_label: "WH Capacity"
    description: "Count of priority 3 next day orders"
    type: sum
    sql:
      case
        when ${date_master.date_date} = cast(${order_creation_raw} as date) then 1 else 0
      end ;;
    filters: [priority: "3"]
  }

  measure: p_orders_count_p5 {
    label: "Count - Standard Orders"
    group_label: "WH Capacity"
    description: "Count of priority 5 standard orders"
    type: sum
    sql:
      case
        when ${date_master.date_date} = cast(${order_creation_raw} as date) then 1 else 0
      end ;;
    filters: [priority: "5"]
  }

  measure: p_actuals_percnt {
    group_label: "WH Capacity"
    label: "Actual Order Percnt"
    type: number
    value_format: "0.00\%"
    sql: safe_divide(${p_orders},${dm_wh_ecom_order_forecast_data.p_forecast})*100;;
  }

  dimension: p_cancelled_yn {
    hidden: yes
    label: "Cancelled Orders Yes NO"
    group_label: "WH Capacity"
    type: yesno
    sql: ${date_master.date_date} = cast(${edit_date_raw} as date) ;;
  }

  measure: porder_cancelled {
    label: "Cancelled Orders"
    group_label: "WH Capacity"
    description: "Count of total cancelled orders"
    type: count_distinct
    sql: ${order_id};;
    filters: [p_cancelled_yn: "Yes",order_status_code: "00,104"]
    #filters: [order_status_code: "00,104"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  dimension: p_picked_yn {
    hidden: yes
    label: "Picked Orders Yes NO"
    group_label: "WH Capacity"
    type: yesno
    sql: ${date_master.date_date} = cast(${picked_raw} as date) ;;
  }

  measure: p_picked {
    label: "Picked Orders"
    group_label: "WH Capacity"
    type: count_distinct
    sql: ${order_id};;
    filters: [p_picked_yn: "Yes"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  #Time to Pick BEGIN
  measure: p_time_to_pick_time {
    group_label: "WH Capacity"
    label: "Avg Time Spent In Picking"
    description: "Average time taken from order creation to pick complete"
    type: average
    #sql: DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_picked_yn: "Yes"]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_pick_95th_percentile {
    hidden: no
    label: "Time To Pick - 95"
    group_label: "WH Capacity"
    type: percentile
    percentile: 95
    #sql: DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_picked_yn: "Yes"]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_pick_95th_pt_avg {
    hidden: no
    label: "Time To Pick - 95 AVG"
    group_label: "WH Capacity"
    type: average
    #sql: DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [dm_fulfillment_percentile.order2pick_percentile: "Yes",p_picked_yn: "Yes"]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_pick_p1{
    label: "Avg Pick Time - Same Day Orders"
    group_label: "WH Capacity"
    description: "Average pick time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_picked_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "1"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_pick_p3{
    label: "Avg Pick Time - Next Day Orders"
    group_label: "WH Capacity"
    description: "Average pick time for a priority 3 next day order"
    type: average
    #sql: DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_picked_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "3"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_pick_p5 {
    label: "Avg Pick Time - Standard Orders"
    group_label: "WH Capacity"
    description: "Average pick time for a priority 5 standard order"
    type: average
    #sql: DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${picked_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_picked_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "5"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }
  #Time to Pick END

  dimension: p_packed_yn {
    hidden: yes
    label: "Packed Orders Yes NO"
    group_label: "WH Capacity"
    type: yesno
    sql:${date_master.date_date} = cast(${packed_raw} as date) ;;
  }

  measure: p_packed {
    label: "Packed Orders"
    group_label: "WH Capacity"
    type: count_distinct
    sql: ${order_id};;
    filters: [p_packed_yn: "Yes"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  #Time to Pack BEGIN
  measure: p_time_to_pack_time {
    group_label: "WH Capacity"
    label: "Avg Time Spent In Packing"
    description: "Average time taken from pick complete to pack complete"
    hidden: no
    type: average
    #sql: DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_packed_yn: "Yes"]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_pack_95th_percentile {
    hidden: no
    label: "Time To Pack - 95"
    group_label: "WH Capacity"
    type: percentile
    percentile: 95
    #sql: DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_packed_yn: "Yes"]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_pack_95th_percentile_avg {
    hidden: no
    label: "Time To Pack - 95 AVG"
    group_label: "WH Capacity"
    type: average
    #sql: DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND) as numeric) ;;
    filters: [dm_fulfillment_percentile.pack2pick_percentile: "Yes",p_packed_yn: "Yes" ]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_pack_p1 {
    label: "Avg Pack Time - Same Day Orders"
    group_label: "WH Capacity"
    description: "Average pack time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND) as numeric) ;;
    #value_format: "D:H:MM"
    filters: [p_packed_yn: "Yes"]
    filters: [priority: "1"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_pack_p3 {
    label: "Avg Pack Time - Next Day Orders"
    group_label: "WH Capacity"
    description: "Average pack time for a priority 3 next day order"
    type: average
    #sql: DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_packed_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "3"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_pack_p5 {
    label: "Avg Pack Time - Standard Orders"
    group_label: "WH Capacity"
    description: "Average pack time for a priority 5 standard order"
    type: average
    #sql: DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${packed_raw} as datetime), cast(${picked_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_packed_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "5"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }
  #Time to Pack END

  dimension: p_manifested_yn {
    hidden: yes
    label: "Manifested Orders Yes NO"
    group_label: "WH Capacity"
    type: yesno
    sql:${date_master.date_date} = cast(${ready_to_ship_raw} as date) ;;
  }

  measure: p_manifested {
    label: "Manifested Orders"
    group_label: "WH Capacity"
    type: count_distinct
    sql: ${order_id};;
    filters: [p_manifested_yn: "Yes"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  #MANIFEST SLA BEGIN
  measure: p_manifested_met {
    label: "Manifested Orders Met"
    group_label: "WH Capacity"
    type: count_distinct
    sql: ${order_id};;
    filters: [p_manifested_yn: "Yes",click_to_Manifest: "Met"]
  }

  measure: p_manifested_missed {
    label: "Manifested Orders Missed"
    group_label: "WH Capacity"
    type: count_distinct
    sql: ${order_id};;
    filters: [p_manifested_yn: "Yes",click_to_Manifest: "Missed"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: p_manifest_sla_met_percnt {
    label: "Manifest SLA Met - Total Percnt"
    group_label: "WH Capacity"
    description: "Percentage of total orders with SLA met."
    type: number
    value_format: "0.00\%"
    sql: safe_divide(${p_manifested_met},(${p_manifested_met}+${p_manifested_missed}))*100;;
  }

  measure: p_manifest_sla_missed_percnt {
    label: "Manifest SLA Missed - Total Percnt"
    group_label: "WH Capacity"
    description: "Percentage of total orders with SLA met."
    type: number
    value_format: "0.00\%"
    sql: safe_divide(${p_manifested_missed},(${p_manifested_met}+${p_manifested_missed}))*100;;
  }

  measure: manifest_fc_non_controllable {
    label: "Manifest Missed FC Non Controllable Count"
    group_label: "WH Capacity"
    description: "Count - Different external factor activities applied on the order"
    type: count_distinct
    sql:
      case
        when ${fc_controllable} = "No" then  ${order_id}
        when ${order_once_in_error_status} is True then ${order_id}
        else null
      end;;
    filters: [p_manifested_yn: "Yes",click_to_Manifest: "Missed",order_status_code: "-00,-104"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: manifest_fc_controllable {
    label: "Manifest Missed FC Controllable Count"
    group_label: "WH Capacity"
    description: "Count - Different external factor activities applied on the order"
    type: count_distinct
    sql:
      case
        when (${fc_controllable} = "Yes" or ${fc_controllable} is null)  and ${order_once_in_error_status} is False
          then ${order_id}
        else null
    end;;
    filters: [p_manifested_yn: "Yes",click_to_Manifest: "Missed",order_status_code: "-00,-104"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }
  #MANIFEST SLA END

  #Time to MANIFEST BEGIN
  measure: p_time_to_ready_to_ship_time {
    label: "Avg Time Spent In Ready To Ship"
    group_label: "WH Capacity"
    description: "Average time taken from Pack complete to ready to ship"
    hidden: no
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_manifested_yn: "Yes"]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_ready_to_ship_95th_percentile {
    hidden: no
    label: "Time To Ready To Ship - 95"
    group_label: "WH Capacity"
    type: percentile
    percentile: 95
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_manifested_yn: "Yes"]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_ready_to_ship_95th_percentile_avg {
    hidden: no
    label: "Time To Ready To Ship - 95 AVG"
    group_label: "WH Capacity"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND) as numeric) ;;
    filters: [dm_fulfillment_percentile.pack2readytoship_percentile: "Yes",p_manifested_yn: "Yes" ]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_ready_ship_p1 {
    label: "Avg Ready To Ship Time - Same Day Orders"
    group_label: "WH Capacity"
    description: "Average ready to ship time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_manifested_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "1"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_ready_ship_p3 {
    label: "Avg Ready To Ship Time - Next Day Orders"
    group_label: "WH Capacity"
    description: "Average ready to ship time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_manifested_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "3"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_ready_ship_p5 {
    label: "Avg Ready To Ship Time - Standard Orders"
    group_label: "WH Capacity"
    description: "Average ready to ship time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${packed_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_manifested_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "5"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }
  #Time to MANIFEST END

  #FC to MANIFEST BEGIN
  measure: p_total_manifest_time {
    label: "Avg Total Time Spent To Manifest"
    group_label: "WH Capacity"
    description: "Average time taken from order creation to manifest"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_manifested_yn: "Yes"]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_total_manifest_95th_percentile {
    label: "Total Manifest Time - 95"
    group_label: "WH Capacity"
    type: percentile
    percentile: 95
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_manifested_yn: "Yes"]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_total_manifest_95th_percentile_avg {
    label: "Total Manifest Time - 95 AVG"
    group_label: "WH Capacity"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [dm_fulfillment_percentile.click2manifest_percentile: "Yes",p_manifested_yn: "Yes" ]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_total_manifest_time_p1 {
    label: "Avg Total Manifest Time - Same Day Orders"
    group_label: "WH Capacity"
    description: "Average manifest time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_manifested_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "1"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_total_manifest_time_p3 {
    label: "Avg Total Manifest Time - Next Day Orders"
    group_label: "WH Capacity"
    description: "Average manifest time for a priority 3 next day order"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_manifested_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "3"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_total_manifest_time_p5 {
    label: "Avg Total Manifest Time - Standard Orders"
    group_label: "WH Capacity"
    description: "Average manifest time for a priority 5 standard order"
    type: average
    #sql: DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${ready_to_ship_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_manifested_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "5"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }
  #FC to MANIFEST END

  dimension: p_shipped_yn {
    hidden: yes
    label: "Shipped Orders Yes NO"
    group_label: "WH Capacity"
    type: yesno
    sql: ${date_master.date_date} = cast(${shipped_raw} as date) ;;
  }

  measure: p_shipped {
    label: "Shipped Orders"
    group_label: "WH Capacity"
    type: count_distinct
    sql: ${order_id};;
    filters: [p_shipped_yn: "Yes"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  #SHIPPED SLA BEGIN
  measure: p_collect_met {
    label: "Collected Orders Met"
    group_label: "WH Capacity"
    type: count_distinct
    sql: ${order_id};;
    filters: [p_shipped_yn: "Yes",click_to_collect: "Met"]
  }

  measure: p_collect_missed {
    label: "Collected Orders Missed"
    group_label: "WH Capacity"
    type: count_distinct
    sql: ${order_id};;
    filters: [p_shipped_yn: "Yes",click_to_collect: "Missed"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: p_collect_sla_met_percnt {
    label: "Collected SLA Met - Total Percnt"
    group_label: "WH Capacity"
    description: "Percentage of total orders with SLA met."
    type: number
    value_format: "0.00\%"
    sql: safe_divide(${p_collect_met},(${p_collect_met}+${p_collect_missed}))*100;;
  }

  measure: p_collect_sla_missed_percnt {
    label: "Collected SLA Missed - Total Percnt"
    group_label: "WH Capacity"
    description: "Percentage of total orders with SLA met."
    type: number
    value_format: "0.00\%"
    sql: safe_divide(${p_collect_missed},(${p_collect_met}+${p_collect_missed}))*100;;
  }

  measure: p_shipped_missed_manifest_missed{
    label: "Shipped Missed Manifest Missed Count"
    group_label: "WH Capacity"
    description: "Count of total orders with SLA missed."
    type: count_distinct
    sql:  ${order_id};;
    filters: [p_shipped_yn: "Yes",click_to_collect: "Missed",click_to_Manifest: "Missed",order_status_code: "-00,-104"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: shipped_fc_non_controllable {
    label: "Shipped Missed FC Non Controllable Count"
    group_label: "WH Capacity"
    description: "Count of total orders with SLA missed."
    type: count_distinct
    sql:
      case
        when ${fc_controllable} = "No" then  ${order_id}
        when ${order_once_in_error_status} is True then ${order_id}
        else null
    end;;
    #filters: [click_to_Manifest: "Missed",click_to_collect: "Missed",order_status_code: "95,101"]
    filters: [p_shipped_yn: "Yes",click_to_collect: "Missed",click_to_Manifest: "-Missed",order_status_code: "-00,-104"]
    drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }

  measure: shipped_fc_controllable {
      label: "Shipped Missed FC Controllable Count"
      group_label: "WH Capacity"
      description: "Count of total orders with SLA missed."
      type: count_distinct
      sql:
        case
          when (${fc_controllable} = "Yes" or ${fc_controllable} is null)  and ${order_once_in_error_status} is False
            then ${order_id}
          else null
      end;;
      #filters: [click_to_Manifest: "Met",click_to_collect: "Missed",order_status_code: "95,101"]
      filters: [p_shipped_yn: "Yes",click_to_collect: "Missed",click_to_Manifest: "-Missed",order_status_code: "-00,-104"]
      drill_fields: [facility_name,order_id,order_extern_orderkey,order_status_description,storerkey,owner,dim_wh_storer_vertical_brand_mapping.brand,priority,carrier_code,vas_applied,vas_reason,external_factor,order_creation_raw,picked_raw,packed_raw, ready_to_ship_raw,shipped_raw,order_once_in_error_status]
  }
  #SHIPPED SLA END

  #Time to SHIP BEGIN
  measure: p_time_to_ship_time {
    label: "Avg Time Spent To Ship Out"
    group_label: "WH Capacity"
    description: "Average time taken from pack complete to ship out"
    hidden: no
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND) as numeric) ;;
    filters: [p_shipped_yn: "Yes"]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_ship_95th_percentile {
    hidden: no
    label: "Time To Ship - 95"
    group_label: "WH Capacity"
    type: percentile
    percentile: 95
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND) as numeric) ;;
    filters: [p_shipped_yn: "Yes"]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_ship_95th_percentile_avg {
    hidden: no
    label: "Time To Ship - 95 AVG"
    group_label: "WH Capacity"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND) as numeric) ;;
    filters: [dm_fulfillment_percentile.readytoship2ship_percentile: "Yes",p_shipped_yn: "Yes" ]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_ship_p1 {
    label: "Avg Ship Time - Same Day Orders"
    group_label: "WH Capacity"
    description: "Average ship time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND) as numeric) ;;
    filters: [p_shipped_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "1"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_ship_p3 {
    label: "Avg Ship Time - Next Day Orders"
    group_label: "WH Capacity"
    description: "Average ship time for a priority 3 next day order"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND) as numeric) ;;
    filters: [p_shipped_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "3"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_time_to_ship_p5 {
    label: "Avg Ship Time - Standard Orders"
    group_label: "WH Capacity"
    description: "Average ship time for a priority 5 standard order"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), ifnull(cast(${ready_to_ship_raw} as datetime),cast(${packed_raw} as datetime)), SECOND) as numeric) ;;
    filters: [p_shipped_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "5"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }
  #Time to SHIP END

  #FC to SHIP BEGIN
  measure: p_total_ship_time {
    label: "Avg Total Time Spent To Ship Out"
    group_label: "WH Capacity"
    description: "Average time taken from order creation to ship out"
    hidden: no
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_shipped_yn: "Yes"]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_total_ship_95th_percentile {
    hidden: no
    label: "Total SHIP Time - 95"
    group_label: "WH Capacity"
    type: percentile
    percentile: 95
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_shipped_yn: "Yes"]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_total_ship_95th_percentile_avg {
    hidden: no
    label: "Total SHIP Time - 95 AVG"
    group_label: "WH Capacity"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [dm_fulfillment_percentile.click2ship_percentile: "Yes",p_shipped_yn: "Yes" ]
    #value_format: "D:H:MM"
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_total_ship_time_p1 {
    label: "Avg Total Ship Time - Same Day Orders"
    group_label: "WH Capacity"
    description: "Average ship time for a priority 1 same day order"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_shipped_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "1"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_total_ship_time_p3 {
    label: "Avg Total Ship Time - Next Day Orders"
    group_label: "WH Capacity"
    description: "Average ship time for a priority 3 next day order"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_shipped_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "3"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }

  measure: p_total_ship_time_p5 {
    label: "Avg Total Ship Time - Standard Orders"
    group_label: "WH Capacity"
    description: "Average ship time for a priority 5 standard order"
    type: average
    #sql: DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND)/86400 ;;
    sql: cast(DATETIME_DIFF(cast(${shipped_raw} as datetime), cast(${order_creation_raw} as datetime), SECOND) as numeric) ;;
    filters: [p_shipped_yn: "Yes"]
    #value_format: "D:H:MM"
    filters: [priority: "5"]
    html: {{ value | divided_by: 86400 | floor }}:{{ value | modulo:86400 | divided_by: 3600 | floor }}:{{ value | modulo:86400 | modulo:3600 | divided_by:60 | floor }};;
  }
  #FC to SHIP END

  # measure: p_actuals_units_percnt {
  #   group_label: "WH Capacity"
  #   label: "Actual Ordered Units Percnt"
  #   type: number
  #   value_format: "0.00\%"
  #   sql: safe_divide(${dm_fulfillment_wm9_orders_details.p_order_quantity},${dm_wh_ecom_order_forecast_data.p_forecast_unit})*100;;
  # }

  #Process Date END

 }
