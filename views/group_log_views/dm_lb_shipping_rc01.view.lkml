view: dm_lb_shipping_rc01 {
  sql_table_name: `chb-prod-stage-oracle.prod_supply_chain.ref_union_shipping_rc01_agg_actvities_reference`
    ;;

  dimension: activity_name {
    type: string
    sql: ${TABLE}.ACTIVITY_NAME ;;
  }

  dimension: brand_name {
    type: string
    sql: ${TABLE}.BRAND_NAME ;;
  }

  dimension: bu_code {
    type: string
    sql: ${TABLE}.BU_CODE ;;
  }

  dimension: bu_desc_icsb {
    type: string
    sql: ${TABLE}.BU_DESC_ICSB ;;
  }

  dimension: charge_country {
    type: string
    sql: ${TABLE}.CHARGE_COUNTRY ;;
  }

  dimension: charge_currency {
    type: string
    sql: ${TABLE}.CHARGE_CURRENCY ;;
  }

  dimension: charge_rate {
    type: string
    sql: ${TABLE}.CHARGE_RATE ;;
  }

  dimension: column_name {
    type: string
    sql: ${TABLE}.COLUMN_NAME ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.COUNTRY ;;
  }

  dimension: e_com_y_n {
    type: string
    sql: ${TABLE}.E_COM_Y_N ;;
  }

  dimension: facility_name {
    type: string
    sql: ${TABLE}.FACILITY_NAME ;;
  }

  dimension: icsb_service_name {
    type: string
    sql: ${TABLE}.ICSB_SERVICE_NAME ;;
  }

  dimension: inbound_outbound {
    type: string
    sql: ${TABLE}.INBOUND_OUTBOUND ;;
  }

  dimension: legal_entity {
    type: string
    sql: ${TABLE}.LEGAL_ENTITY ;;
  }

  dimension: location {
    type: string
    sql: ${TABLE}.location ;;
  }

  dimension: manual_y_n {
    type: string
    sql: ${TABLE}.MANUAL_Y_N ;;
  }

  dimension: mapped_bu_brand {
    type: string
    sql: ${TABLE}.MAPPED_BU_BRAND ;;
  }

  dimension: period_from {
    type: string
    sql: ${TABLE}.period_from ;;
  }

  dimension: period_to {
    type: string
    sql: ${TABLE}.period_to ;;
  }

  dimension: quantity {
    type: string
    sql: ${TABLE}.QUANTITY ;;
  }

  dimension: receiptkey {
    type: string
    sql: ${TABLE}.RECEIPTKEY ;;
  }

  dimension: service_type {
    type: string
    sql: ${TABLE}.SERVICE_TYPE ;;
  }

  dimension: storerkey {
    type: string
    sql: ${TABLE}.STORERKEY ;;
  }

  dimension: total_amount {
    type: string
    sql: ${TABLE}.Total_Amount ;;
  }

  measure: count {
    type: count
    drill_fields: [activity_name, column_name, icsb_service_name, facility_name, brand_name]
  }
}
