
view: dm_dsp_forecasted_orders {

derived_table: {
  sql_trigger_value: SELECT FLOOR(((TIMESTAMP_DIFF(CURRENT_TIMESTAMP(),'1970-01-01 00:00:00',SECOND)) - 60*60*5)/(60*60*24)) ;;
    sql:
     select a.country,  a.service_type, a.vertical, a.brand ,a.date,  a.forecasted_orders, b.provider, b.coverage  from `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dim_dsp_share`  b,
`chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dim_order_summary_forecast` a
where a.country = b.country
and a.service_type = b.service_type

;;
      }


  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: concat(${date},'-',${brand},'-',${country},'-',${provider},'-',${service_type}) ;;
  }
  dimension: date {
    type: date
  }
  dimension: brand {}
  dimension: country {}
  dimension: provider {
    sql: upper(case when ${TABLE}.provider ="In House" then "FAREYE" else ${TABLE}.provider end) ;;
  }
  dimension: service_type {}
  dimension: forecasted_orders {
    type: number
  }
  dimension: coverage {
    type: number
  }

  measure: dsp_fcast {
    type: sum
    sql: ${forecasted_orders} * ${coverage}/100   ;;
  }




}
