view: dm_lb_period_from_to {

   # this view is to get the unique start and end period of the logistics book

  derived_table: {
    sql_trigger_value: SELECT FLOOR((TIMESTAMP_DIFF(CURRENT_TIMESTAMP(),'1970-01-01 00:00:00',SECOND)) / (6*60*60)) ;;
    sql:
        SELECT distinct
           period_from,
           --CAST( PARSE_TIMESTAMP('%d %m %Y', period_from) AS DATE) date_from,
           --FORMAT_DATE("%Y-%b-%d",CAST( PARSE_TIMESTAMP('%d %m %Y', period_from) AS DATE)) date_month_from,
           FORMAT_DATE("%Y-%b",CAST( PARSE_TIMESTAMP('%d %m %Y', period_from) AS DATE)) year_month_from,
           period_to,
           --CAST( PARSE_TIMESTAMP('%d %m %Y', period_to) AS DATE) date_to,
           --FORMAT_DATE("%Y-%b-%d",CAST( PARSE_TIMESTAMP('%d %m %Y', period_to) AS DATE)) date_month_to,
           FORMAT_DATE("%Y-%b",CAST( PARSE_TIMESTAMP('%d %m %Y', period_to) AS DATE)) year_month_to
        FROM `chb-prod-stage-oracle.prod_supply_chain.ref_union_distribution_b2b_actvities_reference`
        ;;
  }

  dimension: pk {
    primary_key: yes
    type: string
    hidden: yes
    sql: concat(${period_from},${period_to});;
  }

  dimension: period_from {
    label: "Period From"
    type: string
  }

  dimension: year_month_from {
    label: "Start Date"
    type: string
  }

  dimension: period_to {
    label: "Period To"
    type: string
  }

  dimension: year_month_to {
    label: "End Date"
    type: string
  }

  dimension: billing_period{
    label: "Billing Period"
    type: string
    sql: concat(${year_month_to}," (",${period_from}," To ",${period_to},")") ;;
  }


}
