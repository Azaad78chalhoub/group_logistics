view: dm_fulfillment_wm9_orders_details {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dm_fulfillment_wm9_orders_details`
    ;;

  #extension: required

  dimension: pk {
    primary_key: yes
    type: string
    hidden: yes
    sql: concat(${order_key},${order_line_number}) ;;
  }

  dimension: country {
    hidden: yes
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: facility_name {
    hidden: yes
    type: string
    sql: ${TABLE}.facility_name ;;
  }

  dimension: orders_details {
    hidden: yes
    sql: ${TABLE}.orders_details ;;
  }

  # dimension: sku {
  #   hidden: no
  #   sql: ${TABLE}.sku ;;
  # }

  dimension: warehouse_id {
    hidden: yes
    type: string
    sql: ${TABLE}.warehouse_id ;;
  }


#}

#view: dm_fulfillment_wm9_orders_details__orders_details {

  dimension_group: add_date {
    hidden: yes
    #label: "Ship Complete"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.orders_details.add_date AS TIMESTAMP);;
  }

  dimension: adjusted_qty {
    hidden: yes
    type: number
    sql: ${TABLE}.orders_details.adjusted_qty ;;
  }

  dimension: open_qty {
    hidden: yes
    type: number
    sql: ${TABLE}.orders_details.open_qty ;;
  }

  dimension: order_key {
    hidden: yes
    type: string
    sql: ${TABLE}.orders_details.order_key ;;
  }

  dimension: order_line_number {
    hidden: yes
    type: string
    sql: ${TABLE}.orders_details.order_line_number ;;
  }

  dimension: original_qty {
    hidden: no
    type: number
    sql: ${TABLE}.orders_details.original_qty ;;
  }

  # dimension: pack_key {
  #   type: string
  #   sql: ${TABLE}.orders_details.pack_key ;;
  # }

  dimension: qty_allocated {
    hidden: yes
    type: number
    sql: ${TABLE}.orders_details.qty_allocated ;;
  }

  dimension: qty_picked {
    hidden: yes
    type: number
    sql: ${TABLE}.orders_details.qty_picked ;;
  }

  dimension: qty_preallocated {
    hidden: yes
    type: number
    sql: ${TABLE}.orders_details.qty_preallocated ;;
  }

  dimension: shipped_qty {
    hidden: no
    type: number
    sql: ${TABLE}.orders_details.shipped_qty ;;
  }

  # dimension: sku {
  #   type: string
  #   sql: ${TABLE}.sku ;;
  # }

  dimension: status {
    hidden: yes
    type: string
    sql: ${TABLE}.orders_details.status;;
  }

  dimension: new_status {
    hidden: yes
    type: number
    sql: cast(${TABLE}.orders_details.status as int64);;
  }

  dimension: storerkey {
    label: "Storer Key"
    hidden: no
    type: string
    sql: ${TABLE}.orders_details.storerkey ;;
  }

  dimension: unit_price {
    hidden: yes
    type: number
    sql: ${TABLE}.orders_details.unit_price ;;
  }

  dimension: uom {
    hidden: yes
    type: string
    sql: ${TABLE}.orders_details.uom ;;
  }

  dimension: whseid {
    hidden: yes
    type: string
    sql: ${TABLE}.orders_details.whseid ;;
  }
#}

#view: dm_fulfillment_wm9_orders_details__sku {
  dimension: brand_name_pre {
    type: string
    hidden: yes
    #sql:  ${TABLE}.sku.brand_name;;
    sql:
    case
      when ${TABLE}.sku.brand_name = 'L OCCITANE' then 'LOCCITANE'
      when ${TABLE}.sku.brand_name ='PENHALIGON S' then "PENHALIGON'S"
      else ${TABLE}.sku.brand_name
    end;;
  }

  dimension: brand_name {
    type: string
    #sql:  ${TABLE}.sku.brand_name;;
    sql:
    case
      when ${facility_name} ='QLC-Qatar' then
        case
          when ${storerkey} = '1000181' then 'FACES'
          when ${storerkey} = '1000190' then 'LOCCITANE'
          else ${brand_name_pre}
        end
      when ${facility_name} ='REAL Emirates - DIP' then
        case
          when ${storerkey} = '1000222' then 'FACES'
          when ${storerkey} = '1000140' then 'FOL'
          when ${storerkey} = '2000145' then 'SAJ'
          when ${storerkey} = '1000216' then 'SEPHORA'
          when ${storerkey} ='NS12003' then 'MUSE'
          when ${storerkey} ='NS14003' then 'DOLCE & GABBANA'
          else ${brand_name_pre}
        end
      when ${facility_name} ='REAL KSA JEDDAH' then
        case
          when ${storerkey} = '2000509' then 'SAJ'
          when ${storerkey} = '1000224' then 'LOCCITANE'
          when ${storerkey} = '1000042' then 'FACES'
          when ${storerkey} = 'NS24008' then 'MUSE'
          when ${storerkey} ='1000217' then 'SEPHORA'
          else ${brand_name_pre}
        end
      when ${facility_name} ='REAL KSA RIYADH' then
        case
          when ${storerkey} = '1000047' then 'FACES'
          else ${brand_name_pre}
        end
      when ${facility_name} ='ASHRAF BGDC' then
        case
          when ${storerkey} = '1000229' then 'LOCCITANE'
          else ${brand_name_pre}
        end
      when ${facility_name} ='MAC' then
        case
          when ${storerkey} = '1000092' then 'FACES'
          else ${brand_name_pre}
        end
      else ${brand_name_pre}
    end;;
  }

  # dimension: brand_name {
  #   type: string
  #   #sql:  ${TABLE}.sku.brand_name;;
  #   sql:
  #   case
  #     when ${dm_fulfillment_wm9_orders.facility_name} ='QLC' then
  #       case
  #         when ${dm_fulfillment_wm9_orders.owner} = 'MACH TRADING FACES ECOMMERCE WH' then 'FACES'
  #         when ${dm_fulfillment_wm9_orders.owner} = 'QATAR LUXURY BEAUTY RETAIL' then 'LOCCITANE'
  #         else ${brand_name_pre}
  #       end
  #     when ${dm_fulfillment_wm9_orders.facility_name} ='DIP' then
  #       case
  #         when ${dm_fulfillment_wm9_orders.owner} = 'ALLIED FACES ECOMMERCE WH' then 'FACES'
  #         when ${dm_fulfillment_wm9_orders.owner} = 'ALLIED RETAIL FOL' then 'FOL'
  #         when ${dm_fulfillment_wm9_orders.owner} = 'SAJ YSL DISTRIBUTION ECOMMERCE WH' then 'SAJ'
  #         when ${dm_fulfillment_wm9_orders.owner} = 'SEPHORA EMIRATES RETAIL ECOMMERCE' then 'SEPHORA'
  #         when ${dm_fulfillment_wm9_orders.owner} ='MUSE' then 'MUSE'
  #         when ${dm_fulfillment_wm9_orders.owner} ='D&G FARFETCH WH' then 'DOLCE & GABBANA'
  #         else ${brand_name_pre}
  #       end
  #     when ${dm_fulfillment_wm9_orders.facility_name} ='JED' then
  #       case
  #         when ${dm_fulfillment_wm9_orders.owner} = 'AL JAMAL YSL DISTRIBUTION ECOMMERCE WH' then 'SAJ'
  #         when ${dm_fulfillment_wm9_orders.owner} = 'FAROUK RETAIL LOCCITANE ECOMMERCE' then 'LOCCITANE'
  #         when ${dm_fulfillment_wm9_orders.owner} = 'FAROUK TRADING FACES ECOMMERCE JEDDAH WH' then 'FACES'
  #         when ${dm_fulfillment_wm9_orders.owner} = 'MUSE JEDDAH NON MERCHANDISING' then 'MUSE'
  #         when ${dm_fulfillment_wm9_orders.owner} ='SEPHORA KSA ECOM WH' then 'SEPHORA'
  #         else ${brand_name_pre}
  #       end
  #     when ${dm_fulfillment_wm9_orders.facility_name} ='JED' then
  #       case
  #         when ${dm_fulfillment_wm9_orders.owner} = 'FAROUK TRADING FACES ECOMMERCE RIYADH WH' then 'FACES'
  #         else ${brand_name_pre}
  #       end
  #     when ${dm_fulfillment_wm9_orders.facility_name} ='BAH' then
  #       case
  #         when ${dm_fulfillment_wm9_orders.owner} = 'ASHRAF-BGDC LOCCITANE RETAIL ECOMMERCE WH' then 'LOCCITANE'
  #         else ${brand_name_pre}
  #       end
  #     when ${dm_fulfillment_wm9_orders.facility_name} ='MAC' then
  #       case
  #         when ${dm_fulfillment_wm9_orders.owner} = 'MAC FOR PROMOTING COMMERCIAL FACES ECOMMERCE ' then 'FACES'
  #         else ${brand_name_pre}
  #       end
  #     else ${brand_name_pre}
  #   end;;
  # }

  dimension: pack_key {
    hidden: yes
    type: string
    sql: ${TABLE}.sku.pack_key ;;
  }

  dimension: putaway_zone {
    hidden: yes
    type: string
    sql: ${TABLE}.sku.putaway_zone ;;
  }

  dimension: sku {
    type: string
    sql: ${TABLE}.sku.sku ;;
  }

  # dimension: storerkey {
  #   type: string
  #   sql: ${TABLE}.sku.storerkey ;;
  # }

  dimension: brand_item_category {
    hidden: yes
    type: string
    sql: ${TABLE}.sku.brand_item_category ;;
  }

  # dimension: whseid {
  #   type: string
  #   sql: ${TABLE}.sku.whseid ;;
  # }

  measure: count {
    label: "Line Count"
    group_label: "Order Count"
    type: count
    sql: ${order_line_number} ;;
  }

  measure: order_ship_quantity {
    label: "Total Shipped quantity"
    group_label: "Order Count"
    description: "No of Units Shipped/Packed"
    type: sum
    sql:  ${shipped_qty};;
    value_format_name: decimal_0
  }

  measure: order_order_quantity {
    label: "Total Order quantity"
    group_label: "Order Count"
    description: "No of Units Requested"
    type: sum
    sql:  ${original_qty};;
    value_format_name: decimal_0
  }

  measure: order_picked_quantity {
    label: "Total Picked quantity"
    group_label: "Order Count"
    description: "No of Units Picked"
    type: sum
    sql:  ${qty_picked};;
    value_format_name: decimal_0
  }

}
