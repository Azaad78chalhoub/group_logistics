include: "../group_log_views/dm_fulfillment_wm9_orders_details.view"

view: dm_fulfillment_wm9_orders_details_b2b {

  extends: [dm_fulfillment_wm9_orders_details]

  # dimension: is_line_fullfilled {
  #   label: "line Fulfilled Check"
  #   type: yesno
  #   sql: ${original_qty} = ${shipped_qty} ;;
  # }

  dimension: is_line_fullfilled {
    label: "line Fulfilled Check"
    type: yesno
    sql: ${original_qty} = ${shipped_qty}  ;;
  }

  measure: line_fulfilled_100_pcnt {
    label: "Count Of Line Fulfilled 100%"
    type: count
    sql: ${order_line_number};;
    filters: [is_line_fullfilled: "Yes"]
  }

  measure: line_fulfilled_percnt {
    label: "Line Fulfilled %"
    type: number
    value_format_name: percent_2
    sql: safe_divide(${order_ship_quantity},${order_order_quantity});;
  }

  measure: no_of_line_fulfilled {
    label: "No Of Line Fulfilled"
    type: count
    sql: ${order_line_number} ;;
    filters: [new_status: ">=68",adjusted_qty: "0"]
  }

}
