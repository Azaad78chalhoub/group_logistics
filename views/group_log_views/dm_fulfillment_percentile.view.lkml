# If necessary, uncomment the line below to include explore_source.

# include: "group_supply_chain.model.lkml"

view: dm_fulfillment_percentile {
  derived_table: {
    explore_source: dm_fulfillment_wm9_orders {
      column: warehouse_id {}
      column: order_id {}
      column: time_to_pick_time {}
      derived_column: order2pick_percentile {
        sql:
        case
          when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_pick_time)*100) <=95
          then "Yes" else "No"
        end;;
      }
      column: time_to_pack_time {}
      derived_column: pack2pick_percentile {
        sql:
        case
          when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_pack_time)*100) <=95
          then "Yes" else "No"
        end;;
      }
      column: time_to_ready_to_ship_time {}
      derived_column: pack2readytoship_percentile {
        sql:
        case
          when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_ready_to_ship_time)*100) <=95
          then "Yes" else "No"
        end;;
      }
      column: time_to_ship_time {}
      derived_column: readytoship2ship_percentile {
        sql:
        case
          when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY time_to_ship_time)*100) <=95
          then "Yes" else "No"
        end;;
      }
      column: total_manifest_time {}
      derived_column: click2manifest_percentile {
        sql:
        case
          when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY total_manifest_time)*100) <=95
          then "Yes" else "No"
        end;;
      }
      column: total_ship_time {}
      derived_column: click2ship_percentile {
        sql:
        case
          when ROUND(PERCENT_RANK() OVER(PARTITION BY warehouse_id ORDER BY total_ship_time)*100) <=95
          then "Yes" else "No"
        end;;
      }
      bind_all_filters: yes
    }
  }
  dimension: warehouse_id{}
  dimension: order_id {}
  dimension: time_to_pick_time {}
  dimension:order2pick_percentile{}
  dimension: time_to_pack_time {}
  dimension:pack2pick_percentile{}
  dimension: time_to_ready_to_ship_time {}
  dimension:pack2readytoship_percentile{}
  dimension: time_to_ship_time {}
  dimension:readytoship2ship_percentile{}
  dimension: total_manifest_time {}
  dimension:click2manifest_percentile{}
  dimension: total_ship_time {}
  dimension:click2ship_percentile{}

}
