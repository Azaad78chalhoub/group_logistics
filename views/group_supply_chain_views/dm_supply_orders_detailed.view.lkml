include: "period_over_period_gsc.view"
view: dm_supply_orders_detailed {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.dm_supply_orders_detailed`
    ;;

  extends: [period_over_period_gsc]

  dimension: _fivetran_deleted  {
    hidden:yes
    type: yesno
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: pop_no_tz {
    sql: ${TABLE}.written_timestamp ;;
  }

  dimension_group: _fivetran_synced {
    hidden:yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: activity {
    hidden:yes
    type: string
    sql: ${TABLE}.activity ;;
  }

  dimension: add_1 {
    hidden:yes
    type: string
    sql: ${TABLE}.add_1 ;;
  }

  dimension: add_2 {
    hidden:yes
    type: string
    sql: ${TABLE}.add_2 ;;
  }

  dimension: addinvc_gross_net {
    hidden:yes
    type: string
    sql: ${TABLE}.addinvc_gross_net ;;
  }

  dimension: addr_key {
    hidden:yes
    type: number
    sql: ${TABLE}.addr_key ;;
  }

  dimension: addr_type {
    hidden:yes
    type: string
    sql: ${TABLE}.addr_type ;;
  }

  dimension: age_of_skin {
    hidden:yes
    type: string
    sql: ${TABLE}.age_of_skin ;;
  }

  dimension: agent {
    hidden:yes
    type: string
    sql: ${TABLE}.agent ;;
  }

  dimension: alcohol_content_gross {
    hidden:yes
    type: string
    sql: ${TABLE}.alcohol_content_gross ;;
  }

  dimension: alcohol_content_net {
    hidden:yes
    type: string
    sql: ${TABLE}.alcohol_content_net ;;
  }


  dimension: arabic_desc {
    hidden:yes
    type: string
    sql: ${TABLE}.arabic_desc ;;
  }

  dimension: area {
    hidden:yes
    type: number
    sql: ${TABLE}.area ;;
  }

  dimension: area_name {
    hidden:yes
    type: string
    sql: ${TABLE}.area_name ;;
  }

  dimension: atrb_boy_girl {
    hidden:yes
    type: string
    sql: ${TABLE}.atrb_boy_girl ;;
  }


  dimension: attrb_made_of {
    hidden:yes
    type: string
    sql: ${TABLE}.attrb_made_of ;;
  }

  dimension: attrb_theme {
    hidden:yes
    type: string
    sql: ${TABLE}.attrb_theme ;;
  }

  dimension: auto_appr_dbt_memo_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.auto_appr_dbt_memo_ind ;;
  }

  dimension: auto_appr_invc_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.auto_appr_invc_ind ;;
  }

  dimension: back_order_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.back_order_ind ;;
  }

  dimension: backhaul_allowance {
    hidden:yes
    type: number
    sql: ${TABLE}.backhaul_allowance ;;
  }

  dimension: backhaul_type {
    hidden:yes
    type: string
    sql: ${TABLE}.backhaul_type ;;
  }

  dimension: backorder_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.backorder_ind ;;
  }

  dimension: barcode {
    hidden:yes
    type: string
    sql: ${TABLE}.barcode ;;
  }

  dimension: batch_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.batch_ind ;;
  }

  dimension: bill_to_id {
    hidden:yes
    type: string
    sql: ${TABLE}.bill_to_id ;;
  }

  dimension: bracket_costing_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.bracket_costing_ind ;;
  }






  dimension: buyer {
    hidden:yes
    type: number
    sql: ${TABLE}.buyer ;;
  }



  dimension: channel_id {
    hidden:yes
    type: number
    sql: ${TABLE}.channel_id ;;
  }

  dimension: channel_name {
    hidden:yes
    type: string
    sql: ${TABLE}.channel_name ;;
  }

  dimension: channel_type {
    hidden:yes
    type: string
    sql: ${TABLE}.channel_type ;;
  }



  dimension: class {
    hidden:yes
    type: number
    sql: ${TABLE}.class ;;
  }

  dimension: class_name {
    hidden:yes
    type: string
    sql: ${TABLE}.class_name ;;
  }

  dimension: clearing_zone_id {
    hidden:yes
    type: string
    sql: ${TABLE}.clearing_zone_id ;;
  }




  dimension: contact_email {
    hidden:yes
    type: string
    sql: ${TABLE}.contact_email ;;
  }

  dimension: contact_fax {
    hidden:yes
    type: string
    sql: ${TABLE}.contact_fax ;;
  }



  dimension: contact_pager {
    hidden:yes
    type: string
    sql: ${TABLE}.contact_pager ;;
  }

  dimension: contact_phone {
    hidden:yes
    type: string
    sql: ${TABLE}.contact_phone ;;
  }

  dimension: contact_telex {
    hidden:yes
    type: string
    sql: ${TABLE}.contact_telex ;;
  }


  dimension: cost_chg_amt_var {
    hidden:yes
    type: number
    sql: ${TABLE}.cost_chg_amt_var ;;
  }

  dimension: cost_chg_pct_var {
    hidden:yes
    type: number
    sql: ${TABLE}.cost_chg_pct_var ;;
  }





  dimension: country_of_manu {
    hidden:yes
    type: string
    sql: ${TABLE}.country_of_manu ;;
  }

  dimension_group: create_datetime {
    hidden:yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.create_datetime ;;
  }



   dimension: dbt_memo_code {
    hidden:yes
    type: string
    sql: ${TABLE}.dbt_memo_code ;;
  }

  dimension: default_item_lead_time {
    hidden:yes
    type: number
    sql: ${TABLE}.default_item_lead_time ;;
  }

  dimension: delivery_policy {
    hidden:yes
    type: string
    sql: ${TABLE}.delivery_policy ;;
  }

   dimension: deposit_code {
    hidden:yes
    type: string
    sql: ${TABLE}.deposit_code ;;
  }




  dimension: description {
    hidden:yes
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: dgr_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.dgr_ind ;;
  }

  dimension: diff_1 {
    hidden:yes
    type: string
    sql: ${TABLE}.diff_1 ;;
  }

  dimension: diff_2 {
    hidden:yes
    type: string
    sql: ${TABLE}.diff_2 ;;
  }






  dimension: dsd_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.dsd_ind ;;
  }

  dimension: duns_loc {
    hidden:yes
    type: string
    sql: ${TABLE}.duns_loc ;;
  }

  dimension: duns_number {
    hidden:yes
    type: string
    sql: ${TABLE}.duns_number ;;
  }


  dimension: edi_asn {
    hidden:yes
    type: string
    sql: ${TABLE}.edi_asn ;;
  }

  dimension: edi_channel_id {
    hidden:yes
    type: number
    sql: ${TABLE}.edi_channel_id ;;
  }

  dimension: edi_contract_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.edi_contract_ind ;;
  }

  dimension: edi_invc_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.edi_invc_ind ;;
  }

  dimension: edi_po_chg {
    hidden:yes
    type: string
    sql: ${TABLE}.edi_po_chg ;;
  }

  dimension: edi_po_confirm {
    hidden:yes
    type: string
    sql: ${TABLE}.edi_po_confirm ;;
  }



  dimension: edi_sales_rpt_freq {
    hidden:yes
    type: string
    sql: ${TABLE}.edi_sales_rpt_freq ;;
  }



  dimension: edi_supp_available_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.edi_supp_available_ind ;;
  }

  dimension: elect_mtk_clubs {
    hidden:yes
    type: string
    sql: ${TABLE}.elect_mtk_clubs ;;
  }




  dimension: final_dest_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.final_dest_ind ;;
  }

  dimension: fixed_tare_uom {
    hidden:yes
    type: string
    sql: ${TABLE}.fixed_tare_uom ;;
  }

  dimension: fixed_tare_value {
    hidden:yes
    type: number
    sql: ${TABLE}.fixed_tare_value ;;
  }



  dimension: food_stamp_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.food_stamp_ind ;;
  }



  dimension: freight_charge_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.freight_charge_ind ;;
  }


  dimension: full_pallet_item {
    hidden:yes
    type: string
    sql: ${TABLE}.full_pallet_item ;;
  }



  dimension: grey_mkt_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.grey_mkt_ind ;;
  }

  dimension: group_name {
    hidden:yes
    type: string
    sql: ${TABLE}.group_name ;;
  }

  dimension: group_no {
    hidden:yes
    type: number
    sql: ${TABLE}.group_no ;;
  }

  dimension: handling_pct {
    hidden:yes
    type: number
    sql: ${TABLE}.handling_pct ;;
  }

  dimension: ib_shelf_life {
    hidden:yes
    type: number
    sql: ${TABLE}.ib_shelf_life ;;
  }



  dimension: in_store_market_basket {
    hidden:yes
    type: string
    sql: ${TABLE}.in_store_market_basket ;;
  }



  dimension: inv_mgmt_lvl {
    hidden:yes
    type: string
    sql: ${TABLE}.inv_mgmt_lvl ;;
  }

  dimension: invc_pay_loc {
    hidden:yes
    type: string
    sql: ${TABLE}.invc_pay_loc ;;
  }

  dimension: invc_receive_loc {
    hidden:yes
    type: string
    sql: ${TABLE}.invc_receive_loc ;;
  }





  dimension: key_value_1 {
    hidden:yes
    type: string
    sql: ${TABLE}.key_value_1 ;;
  }

  dimension: key_value_2 {
    hidden:yes
    type: string
    sql: ${TABLE}.key_value_2 ;;
  }



  dimension: lang {
    hidden:yes
    type: number
    sql: ${TABLE}.lang ;;
  }



  dimension_group: last_update_datetime {
    hidden:yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_update_datetime ;;
  }

  dimension: last_update_id {
    hidden:yes
    type: string
    sql: ${TABLE}.last_update_id ;;
  }



  dimension_group: launch {
    hidden:yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.launch_date ;;
  }


  dimension: loc {
    hidden:yes
    type: number
    sql: ${TABLE}.loc ;;
  }


  dimension: location {
    hidden:yes
    type: number
    sql: ${TABLE}.location ;;
  }


  dimension: mall_name {
    hidden:yes
    type: string
    sql: ${TABLE}.mall_name ;;
  }

  dimension: manual_price_entry {
    hidden:yes
    type: string
    sql: ${TABLE}.manual_price_entry ;;
  }

  dimension: module {
    hidden:yes
    type: string
    sql: ${TABLE}.module ;;
  }

  dimension: natl_brand_comp_item {
    hidden:yes
    type: string
    sql: ${TABLE}.natl_brand_comp_item ;;
  }


  dimension_group: not_after {
    hidden:yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.not_after_date ;;
  }


  dimension: org_num {
    hidden:yes
    type: string
    sql: ${TABLE}.org_num ;;
  }

  dimension: org_prod_key {
    hidden:yes
    type: number
    sql: ${TABLE}.org_prod_key ;;
  }







  dimension: physical_wh {
    hidden:yes
    type: number
    sql: ${TABLE}.physical_wh ;;
  }




  dimension: post {
    hidden:yes
    type: string
    sql: ${TABLE}.post ;;
  }


  dimension: prepay_invc_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.prepay_invc_ind ;;
  }

  dimension: primary_addr_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.primary_addr_ind ;;
  }

  dimension: primary_vwh {
    hidden:yes
    type: number
    sql: ${TABLE}.primary_vwh ;;
  }



  dimension: prod_num {
    hidden:yes
    type: string
    sql: ${TABLE}.prod_num ;;
  }


  dimension: proportional_tare_pct {
    hidden:yes
    type: number
    sql: ${TABLE}.proportional_tare_pct ;;
  }


  dimension: qc_freq {
    hidden:yes
    type: number
    sql: ${TABLE}.qc_freq ;;
  }


  dimension: qc_pct {
    hidden:yes
    type: number
    sql: ${TABLE}.qc_pct ;;
  }


  dimension: rack_size {
    hidden:yes
    type: string
    sql: ${TABLE}.rack_size ;;
  }


  dimension: refundable_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.refundable_ind ;;
  }




  dimension: replen_approval_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.replen_approval_ind ;;
  }


  dimension: req_shelf_life_on_receipt {
    hidden:yes
    type: number
    sql: ${TABLE}.req_shelf_life_on_receipt ;;
  }

  dimension: req_shelf_life_on_selection {
    hidden:yes
    type: number
    sql: ${TABLE}.req_shelf_life_on_selection ;;
  }

  dimension: ret_allow_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.ret_allow_ind ;;
  }

  dimension: ret_auth_req {
    hidden:yes
    type: string
    sql: ${TABLE}.ret_auth_req ;;
  }

  dimension: ret_courier {
    hidden:yes
    type: string
    sql: ${TABLE}.ret_courier ;;
  }

  dimension: ret_min_dol_amt {
    hidden:yes
    type: number
    sql: ${TABLE}.ret_min_dol_amt ;;
  }

  dimension: return_policy {
    hidden:yes
    type: string
    sql: ${TABLE}.return_policy ;;
  }

  dimension: returnable_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.returnable_ind ;;
  }

  dimension: reward_eligible_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.reward_eligible_ind ;;
  }







  dimension: scale_aip_orders {
    hidden:yes
    type: string
    sql: ${TABLE}.scale_aip_orders ;;
  }



  dimension: selling_square_ft {
    hidden:yes
    type: number
    sql: ${TABLE}.selling_square_ft ;;
  }





  dimension: service_perf_req_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.service_perf_req_ind ;;
  }

  dimension: settlement_code {
    hidden:yes
    type: string
    sql: ${TABLE}.settlement_code ;;
  }






  dimension: state {
    hidden:yes
    type: string
    sql: ${TABLE}.state ;;
  }


  dimension: stop_sale_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.stop_sale_ind ;;
  }

  dimension: storage_location {
    hidden:yes
    type: string
    sql: ${TABLE}.storage_location ;;
  }

  dimension_group: store_close {
    hidden:yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_close_date ;;
  }



  dimension: store_format {
    hidden:yes
    type: number
    sql: ${TABLE}.store_format ;;
  }

  dimension: store_name10 {
    hidden:yes
    type: string
    sql: ${TABLE}.store_name10 ;;
  }

  dimension: store_name3 {
    hidden:yes
    type: string
    sql: ${TABLE}.store_name3 ;;
  }

  dimension_group: store_open {
    hidden:yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_open_date ;;
  }

  dimension: store_reorderable_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.store_reorderable_ind ;;
  }



  dimension: sup_name_secondary {
    hidden:yes
    type: string
    sql: ${TABLE}.sup_name_secondary ;;
  }

  dimension: sup_qty_level {
    hidden:yes
    type: string
    sql: ${TABLE}.sup_qty_level ;;
  }

  dimension: sup_status {
    hidden:yes
    type: string
    sql: ${TABLE}.sup_status ;;
  }


  dimension: supp_add_seq_no {
    hidden:yes
    type: number
    sql: ${TABLE}.supp_add_seq_no ;;
  }



  dimension: supplier_parent {
    hidden:yes
    type: number
    sql: ${TABLE}.supplier_parent ;;
  }





  dimension: total_square_ft {
    hidden:yes
    type: number
    sql: ${TABLE}.total_square_ft ;;
  }





  dimension: uda_16_attribute {
    hidden:yes
    type: string
    sql: ${TABLE}.uda_16_attribute ;;
  }






  dimension: vat_region {
    hidden:yes
    type: number
    sql: ${TABLE}.vat_region ;;
  }

  dimension: vc_freq {
    hidden:yes
    type: number
    sql: ${TABLE}.vc_freq ;;
  }

  dimension: vc_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.vc_ind ;;
  }

  dimension: vc_pct {
    hidden:yes
    type: number
    sql: ${TABLE}.vc_pct ;;
  }




  dimension: vmi_order_status {
    hidden:yes
    type: string
    sql: ${TABLE}.vmi_order_status ;;
  }


  dimension: wh_name_secondary {
    hidden:yes
    type: string
    sql: ${TABLE}.wh_name_secondary ;;
  }

  dimension: wic_ind {
    hidden:yes
    type: string
    sql: ${TABLE}.wic_ind ;;
  }

 #-------------------   Dimensions - PO starts --------------------------

  dimension: pk {
    type: number
    primary_key: yes
    hidden: yes
    sql: CONCAT(${order_no}, " - ", ${item}) ;;
  }


  dimension: comment_desc {

    hidden:no
    type: string
    sql: ${TABLE}.comment_desc ;;
  }
  dimension: contract_no {
    hidden:no
    type: number
    sql: ${TABLE}.contract_no ;;
  }

  dimension: conversion_rate_retail {
    hidden:no
    type: number
    sql: ${TABLE}.conversion_rate_retail ;;
  }

  dimension: cost_source {
    hidden:no
    type: string
    sql: ${TABLE}.cost_source ;;
  }

  dimension: currency_code {
    hidden:no
    type: string
    sql: ${TABLE}.currency_code ;;
  }

  dimension: cust_order {
    hidden:no
    type: string
    sql: ${TABLE}.cust_order ;;
  }
  dimension: delivery_supplier {
    hidden:no
    type: number
    sql: ${TABLE}.delivery_supplier ;;
  }
  dimension: dept {
    hidden:no
    type: number
    sql: ${TABLE}.dept ;;
  }

  dimension: discharge_port {
    hidden:no
    type: string
    sql: ${TABLE}.discharge_port ;;
  }

  dimension_group: earliest_ship {
    hidden:no
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.earliest_ship_date ;;
  }

  dimension: edi_po_ind {
    hidden:no
    type: string
    sql: ${TABLE}.edi_po_ind ;;
  }


  dimension: edi_sent_ind {
    hidden:no
    type: string
    sql: ${TABLE}.edi_sent_ind ;;
  }
  dimension_group: estimated_instock {
    hidden:no
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.estimated_instock_date ;;
  }
  dimension: exchange_rate {
    hidden:no
    type: number
    sql: ${TABLE}.exchange_rate ;;
  }

  dimension: factory {
    hidden:no
    type: string
    sql: ${TABLE}.factory ;;
  }
  dimension: fob_title_pass {
    hidden:no
    type: string
    sql: ${TABLE}.fob_title_pass ;;
  }

  dimension: fob_title_pass_desc {
    hidden:no
    type: string
    sql: ${TABLE}.fob_title_pass_desc ;;
  }

  dimension: fob_trans_res {
    hidden:no
    type: string
    sql: ${TABLE}.fob_trans_res ;;
  }

  dimension: fob_trans_res_desc {
    hidden:no
    type: string
    sql: ${TABLE}.fob_trans_res_desc ;;
  }


  dimension: freight_contract_no {
    hidden:no
    type: string
    sql: ${TABLE}.freight_contract_no ;;
  }

  dimension: freight_terms {
    hidden:no
    type: string
    sql: ${TABLE}.freight_terms ;;
  }

  dimension: from_currency_retail {
    hidden:no
    type: string
    sql: ${TABLE}.from_currency_retail ;;
  }

  dimension: import_country_id {
    hidden:no
    type: string
    sql: ${TABLE}.import_country_id ;;
  }

  dimension: import_id {
    hidden:no
    type: number
    sql: ${TABLE}.import_id ;;
  }

  dimension: import_order_ind {
    hidden:no
    type: string
    sql: ${TABLE}.import_order_ind ;;
  }

  dimension: import_type {
    hidden:no
    type: string
    sql: ${TABLE}.import_type ;;
  }

  dimension: include_on_order_ind {
    hidden:no
    type: string
    sql: ${TABLE}.include_on_order_ind ;;
  }
  dimension: lading_port {
    hidden:no
    type: string
    sql: ${TABLE}.lading_port ;;
  }
  dimension: last_grp_rounded_qty {
    hidden:no
    type: number
    sql: ${TABLE}.last_grp_rounded_qty ;;
  }

  dimension: last_received {
    hidden:no
    type: number
    sql: ${TABLE}.last_received ;;
  }

  dimension: last_rounded_qty {
    hidden:no
    type: number
    sql: ${TABLE}.last_rounded_qty ;;
  }

  dimension: last_sent_rev_no {
    hidden:no
    type: number
    sql: ${TABLE}.last_sent_rev_no ;;
  }

  dimension_group: latest_ship {
    hidden:no
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.latest_ship_date ;;
  }

  dimension: loc_type {
    hidden:no
    type: string
    sql: ${TABLE}.loc_type ;;
  }

  dimension: locs_currency {
    hidden:no
    type: string
    sql: ${TABLE}.locs_currency ;;
  }

  dimension: non_scale_ind {
    hidden:no
    type: string
    sql: ${TABLE}.non_scale_ind ;;
  }

  dimension_group: not_before {
    hidden:no
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.not_before_date ;;
  }

  dimension: order_no {
    hidden:no
    type: number
    sql: ${TABLE}.order_no;;
    value_format: "0"
  }

  dimension: order_type {
    hidden:no
    type: string
    sql: ${TABLE}.order_type ;;
  }

  dimension_group: orig_approval {
    hidden:no
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.orig_approval_date ;;
  }

  dimension: orig_approval_id {
    hidden:no
    type: string
    sql: ${TABLE}.orig_approval_id ;;
  }

  dimension: orig_ind {
    hidden:no
    type: number
    sql: ${TABLE}.orig_ind ;;
  }

  dimension: original_repl_qty {
    hidden:no
    type: number
    sql: ${TABLE}.original_repl_qty ;;
  }

  dimension_group: otb_eow {
    hidden:no
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.otb_eow_date ;;
  }



  dimension: partner1 {
    hidden:no
    type: string
    sql: ${TABLE}.partner1 ;;
  }

  dimension: partner2 {
    hidden:no
    type: string
    sql: ${TABLE}.partner2 ;;
  }

  dimension: partner3 {
    hidden:no
    type: string
    sql: ${TABLE}.partner3 ;;
  }

  dimension: partner_type_1 {
    hidden:no
    type: string
    sql: ${TABLE}.partner_type_1 ;;
  }

  dimension: partner_type_2 {
    hidden:no
    type: string
    sql: ${TABLE}.partner_type_2 ;;
  }

  dimension: partner_type_3 {
    hidden:no
    type: string
    sql: ${TABLE}.partner_type_3 ;;
  }

  dimension: payment_method {
    hidden:no
    type: string
    sql: ${TABLE}.payment_method ;;
  }

  dimension_group: close {
    hidden:no
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.close_date ;;
  }


  dimension: cancel_code {
    hidden:no
    type: string
    sql: ${TABLE}.cancel_code ;;
  }

  dimension_group: cancel {
    hidden:no
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.cancel_date ;;
  }

  dimension: cancel_id {
    hidden:no
    type: string
    sql: ${TABLE}.cancel_id ;;
  }



  dimension_group: app_datetime {
    hidden:no
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.app_datetime ;;
  }

  dimension_group: pickup {
    hidden:no
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.pickup_date ;;
  }

  dimension: pickup_loc {
    hidden:no
    type: string
    sql: ${TABLE}.pickup_loc ;;
  }

  dimension: pickup_no {
    hidden:no
    type: string
    sql: ${TABLE}.pickup_no ;;
  }

  dimension: po_ack_recvd_ind {
    hidden:no
    type: string
    sql: ${TABLE}.po_ack_recvd_ind ;;
  }

  dimension: po_type {
    hidden:no
    type: string
    sql: ${TABLE}.po_type ;;
  }
  dimension: pre_mark_ind {
    hidden:no
    type: string
    sql: ${TABLE}.pre_mark_ind ;;
  }


  dimension: promotion {
    hidden:no
    type: number
    sql: ${TABLE}.promotion ;;
  }
  dimension: purchase_type {
    hidden:no
    type: string
    sql: ${TABLE}.purchase_type ;;
  }
  dimension: qc_ind {
    hidden:no
    type: string
    sql: ${TABLE}.qc_ind ;;
  }

  dimension: qty_cancelled {
    hidden:no
    type: number
    sql: ${TABLE}.qty_cancelled ;;
  }

  dimension: qty_key_options {
    hidden:no
    type: string
    sql: ${TABLE}.qty_key_options ;;
  }

  dimension: qty_ordered {
    hidden:no
    type: number
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: qty_prescaled {
    hidden:no
    type: number
    sql: ${TABLE}.qty_prescaled ;;
  }

  dimension: qty_received {
    hidden:no
    type: number
    sql: ${TABLE}.qty_received ;;
  }
  dimension: reject_code {
    hidden:no
    type: string
    sql: ${TABLE}.reject_code ;;
  }
  dimension: routing_loc_id {
    hidden:no
    type: string
    sql: ${TABLE}.routing_loc_id ;;
  }
  dimension: ship_method {
    hidden:no
    type: string
    sql: ${TABLE}.ship_method ;;
  }

  dimension: ship_pay_method {
    hidden:no
    type: string
    sql: ${TABLE}.ship_pay_method ;;
  }
  dimension: split_ref_ordno {
    hidden:no
    type: number
    sql: ${TABLE}.split_ref_ordno ;;
  }


  dimension: status {
    hidden:no
    type: string
    sql: ${TABLE}.status ;;
  }
  dimension: terms {
    hidden:no
    type: string
    sql: ${TABLE}.terms ;;
  }
  dimension: triangulation_ind {
    hidden:no
    type: string
    sql: ${TABLE}.triangulation_ind ;;
  }

  dimension: tsf_po_link_no {
    hidden:no
    type: number
    sql: ${TABLE}.tsf_po_link_no ;;
  }
  dimension: unit_cost {
    hidden:no
    type: number
    sql: ${TABLE}.unit_cost ;;
  }

  dimension: unit_cost_in_local_curr {
    hidden:no
    type: number
    sql: ${TABLE}.unit_cost_in_local_curr ;;
  }

  dimension: unit_cost_init {
    hidden:no
    type: number
    sql: ${TABLE}.unit_cost_init ;;
  }

  dimension: unit_cost_usd {
    hidden:no
    type: number
    sql: ${TABLE}.unit_cost_usd ;;
  }

  dimension: unit_retail {
    hidden:no
    type: number
    sql: ${TABLE}.unit_retail ;;
  }

  dimension: unit_retail_usd {
    hidden:no
    type: number
    sql: ${TABLE}.unit_retail_usd ;;
  }

  dimension: vendor_order_no {
    hidden:no
    type: string
    sql: ${TABLE}.vendor_order_no ;;
  }


  dimension_group: written {
    hidden:no
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.written_timestamp;;
  }

  dimension: ltl_till_date {
    type: string
    view_label: "Like to Like Timeframe comparison"
    label: "LTL Timeframe"
    description: "For Like to like timeframe comparison"
    sql: CASE WHEN EXTRACT(MONTH FROM ${TABLE}.written_timestamp) > EXTRACT(MONTH FROM CURRENT_TIMESTAMP) THEN "NO"
              WHEN EXTRACT(MONTH FROM ${TABLE}.written_timestamp) < EXTRACT(MONTH FROM CURRENT_TIMESTAMP) THEN "YES"
              WHEN EXTRACT(MONTH FROM ${TABLE}.written_timestamp) = EXTRACT(MONTH FROM CURRENT_TIMESTAMP) AND
              EXTRACT(DAY FROM ${TABLE}.written_timestamp) >= EXTRACT(DAY FROM CURRENT_TIMESTAMP) THEN "NO"
              ELSE "YES" END;;
  }

#-------------------   Dimensions - BU Hierarchy starts --------------------------

  dimension: brand {
    type: string
    view_label: "BU Alternative Hierarchy"
    label: "Business Unit"
    description: "Brand Name from alternative hierarchy"
    sql: ${TABLE}.brand ;;
  }


  dimension: bu_code {
    type: string
    label: "BU Code"
    view_label: "BU Alternative Hierarchy"
    description: "BU code from alternative hierarchy"
    sql: ${TABLE}.bu_code ;;
  }

  dimension: bu_dep_code {
    type: string
    view_label: "BU Alternative Hierarchy"
    label: "BU Department Code"
    description: "BU Department Code from alternative hierarchy"
    sql: ${TABLE}.bu_dep_code ;;
  }

  dimension: bu_dep_desc {
    type: string
    view_label: "BU Alternative Hierarchy"
    label: "BU Department Description"
    description: "BU Departmetn Description from alternative hierarchy"
    sql: ${TABLE}.bu_dep_desc ;;
  }

  dimension: bu_desc {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "BU description from alternative hierarchy"
    label: "BU Description"
    sql: ${TABLE}.bu_desc ;;
  }

  dimension: country {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Country value from alternative hierarchy"
    label: "Country"
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: ownership {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Ownership from alternative hierarchy"
    sql: ${TABLE}.ownership ;;
  }

  dimension: store_code {
    type: string
    view_label: "BU Alternative Hierarchy"
    hidden: no
    description: "Store code from alternative hierarchy"
    sql: ${TABLE}.store_code ;;
  }

  dimension: sub_vertical {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Boat(sub_vertical) from alternative hierarchy"
    label: "Boat"
    sql: ${TABLE}.sub_vertical ;;
  }

  dimension: vertical {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Vertical from alternative hierarchy"
    label: "Vertical"
    sql: ${TABLE}.vertical ;;
  }


  dimension: city {
    type: string
    view_label: "BU Alternative Hierarchy"
    sql: ${TABLE}.city ;;
  }

  dimension: country_name {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Country Name used for Map Visualizations"
    label: "Country Name"
    map_layer_name: countries
    sql: CASE WHEN ${TABLE}.country ="UAE" then "AE"
          WHEN ${TABLE}.country ="KSA" then "SA"
          WHEN ${TABLE}.country ="BAH" then "BH"
          ELSE ${TABLE}.country END  ;;
    drill_fields: [city,store_code]
  }


  dimension: brand_for_access_filter {
    type: string
    hidden: yes
    view_label: "BU Alternative Hierarchy"
    label: "Business Unit"
    description: "Brand Name from alternative hierarchy"
     sql: case when ${brand} is null then 'x' else  ${brand} end  ;;
  }

  dimension: country_for_access_filter {
    type: string
    hidden: yes

    view_label: "BU Alternative Hierarchy"
    description: "Country value from alternative hierarchy"
    label: "Country"
    map_layer_name: countries
     sql: case when ${country} is null then 'x' else  ${country} end  ;;
  }

  dimension: vertical_for_access_filter {
    type: string
    hidden: yes

    view_label: "BU Alternative Hierarchy"
    description: "Vertical from alternative hierarchy"
    label: "Vertical"
    sql: case when ${vertical} is null then 'x' else ${vertical} end  ;;
  }



#-------------------   Dimensions - BU Hierarchy ends   --------------------------

# -------------------  Dimensions item_loc_traits ----------------------------------------

  dimension: alt_storage_location {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Season (Location)"
    description: "Season value specific for the location"
    sql: UPPER(${TABLE}.alt_storage_location) ;;
  }

  dimension: consignment_flag {
    hidden:yes
    type: string
    sql: ${TABLE}.consignment_flag ;;
  }

  dimension: consignment
  {
    type: yesno
    view_label: "Location Traits"
    label: "Is Consignment?"
    sql: ${consignment_flag} = "Y" ;;
  }

  dimension: alt_storeage_location_bucket {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Level Season Bucket"
    description: "SS/FW/Basic/Carry-Over etc"
    sql: CASE
    WHEN  REPLACE(${alt_storage_location}, ' ', '') IN ("SS21P", "SS21M", "SS21B") THEN "SS21"
    WHEN  REPLACE(${alt_storage_location}, ' ', '') IN ("FW21P", "FW21M", "FW21B") THEN "FW21"
    WHEN  REPLACE(${alt_storage_location}, ' ', '') IN ("SS22P", "SS22M", "SS22B") THEN "SS22"
    WHEN  REPLACE(${alt_storage_location}, ' ', '') IN ("FW22P", "FW22M", "FW22B") THEN "FW22"
    WHEN  ${alt_storage_location} LIKE "BASIC" THEN "Basic"
    WHEN  ${alt_storage_location} LIKE "REGULAR" THEN "Regular"
    WHEN  ${alt_storage_location} LIKE "%C" OR  ${alt_storage_location}  LIKE "%CO" THEN "Carry-over"
    WHEN  ${alt_storage_location} IS NULL THEN "No Season Info"
    ELSE "Previous Seasons"
    END;;
  }

  dimension: alt_storeage_location_bucket_2 {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Level Season Bucket 2"
    description: "Basic/Seasonal Classification"
    sql: CASE WHEN  ${alt_storage_location} LIKE "BASIC" THEN "Basic"
          ELSE "Seasonal"
          END;;
  }

  dimension: report_code {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Zone"
    description: "Report Code value"
    sql: ${TABLE}.report_code ;;
  }

  dimension: zone_3 {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Zone 3"
    description: "Zone 3 for Level shoes"
    sql: CASE WHEN ${report_code} LIKE "%ACC" Then "Accesories"
          WHEN ${report_code}="GIAROS" AND ${gender}="WOMEN" THEN "Direct Concession"
          ELSE "Multibrand"
          END;;
  }

  dimension: zone_category_sort {
    type: string
    hidden: yes
    view_label: "Location Traits"
    label: "Zone Category Sort"
    description: "Zone Category Sort for Level shoes"
    sql: CASE WHEN ${report_code} in ("CONTMP", "DESIGM", "DESIGW", "KIDS", "MENACC", "TRENDM", "TRENDW", "WMNACC", "KDACC") Then "1"
              WHEN ${report_code} in ("AXEARI", "CREPRO", "GIAROS", "SOLLOU", "STRFAR", "TORBUR", "VERSAC", "DOLCE") Then "2"
              WHEN ${report_code} in ("INTCON", "EXTCON") Then "3"
          ELSE "4"
          END;;
  }

  dimension: zone_category {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Zone Category"
    order_by_field: zone_category_sort
    description: "Zone Category for Level shoes"
    sql: CASE WHEN ${report_code} in ("CONTMP", "DESIGM", "DESIGW", "KIDS", "MENACC", "TRENDM", "TRENDW", "WMNACC", "KDACC") Then "Multibrand"
              WHEN ${report_code} in ("AXEARI", "CREPRO", "GIAROS", "SOLLOU", "STRFAR", "TORBUR", "VERSAC", "DOLCE") Then "Shop in Shop"
              WHEN ${report_code} in ("INTCON", "EXTCON") Then "Concessions"
          ELSE null
          END;;
  }



# -------------------  Dimensions item_loc_traits ends  ----------------------------------------

# -------------------  Dimensions Product starts  ----------------------------------------


  dimension: prod_brand {
    hidden:no
    view_label: "Products"
    label: "Brand"
    description: "Product Brand from RMS"
    type: string
    sql: ${TABLE}.prod_brand ;;
  }

  dimension: attrb_color {
    view_label: "Products"
    label: "Colour"
    description: "Colour attribute from Oracle RMS"
    type: string
    suggest_explore: dim_retail_prod_colour
    suggest_dimension: dim_retail_prod_colour.colour
    sql: ${TABLE}.attrb_color ;;
  }


  dimension: dept_name {
    type: string
    hidden: no
    label: "Department Name"
    view_label: "Products"
    description: "Department attribute from Oracle RMS"
    sql: ${TABLE}.dept_name ;;
  }

  dimension: dept_no {
    type: number
    hidden: yes
    sql: ${TABLE}.dept_no ;;
  }


  dimension: division {
    description: "Division attribute from Oracle RMS"
    view_label: "Products"
    type: string
    suggest_explore: dim_retail_prod_division
    suggest_dimension: dim_retail_prod_division.division
    sql: ${TABLE}.division ;;
  }

  dimension: division_no {
    type: number
    hidden: yes
    sql: ${TABLE}.division_no ;;
  }

  dimension: entity_type {
    type: string
    view_label: "Products"
    hidden: no
    sql: ${TABLE}.entity_type ;;
  }

  dimension: format {
    type: string
    hidden: yes
    sql: ${TABLE}.format ;;
  }

  dimension: gender {
    view_label: "Products"
    description: "Gender attribute from Oracle RMS"
    type: string
    suggest_explore: dim_retail_prod_gender
    suggest_dimension: dim_retail_prod_gender.gender
    sql: ${TABLE}.gender ;;
  }


  dimension: item {
    view_label: "Products"
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: item_desc {
    label:"Product Description"
    view_label: "Products"
    description: "Description from Oracle RMS"
    type: string
    hidden: no
    sql: ${TABLE}.item_desc ;;
  }

  dimension: item_style {
    view_label: "Products"
    label: "Style"
    description: "Style ID from Oracle RMS"
    type: string
    suggest_explore: fact_soh_sales_poc
    suggest_dimension: dim_retail_prod.item_style
    sql: ${TABLE}.item_style ;;
  }

  dimension: line {
    type: string
    view_label: "Products"
    hidden: no
    sql: ${TABLE}.line ;;
  }

  dimension: perishable_ind {
    type: string
    view_label: "Products"
    hidden: no
    sql: ${TABLE}.perishable_ind ;;
  }

  dimension: recurrence {
    type: string
    hidden: yes
    sql: ${TABLE}.recurrence ;;
  }

  dimension: sap_item_code {
    type: string
    hidden: yes
    sql: ${TABLE}.sap_item_code ;;
  }

  dimension: saso_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.saso_ind ;;
  }

  dimension: season_desc {
    view_label: "Products"
    description: "Season attribute from Oracle RMS"
    label: "Season"
    type: string
    suggest_explore: dim_retail_prod_season
    suggest_dimension: dim_retail_prod_season.season
    sql: ${TABLE}.season_desc ;;
  }

  dimension: sep_axis {
    type: string
    hidden: yes
    sql: ${TABLE}.sep_axis ;;
  }

  dimension: sep_category {
    hidden: yes
    sql: ${TABLE}.sep_category ;;
  }

  dimension: sep_market {
    type: string
    view_label: "Products"
    hidden: no
    label: "Market"
    sql: ${TABLE}.sep_market ;;
  }

  dimension: sep_nature {
    type: string
    hidden: yes
    sql: ${TABLE}.sep_nature ;;
  }

  dimension: sep_range {
    type: string
    hidden: yes
    sql: ${TABLE}.sep_range ;;
  }

  dimension: size_uda {
    view_label: "Products"
    description: "Size attribute from Oracle RMS"
    type: string
    label: "Size"
    suggest_explore: dim_retail_prod_size
    suggest_dimension: dim_retail_prod_size.size
    sql: ${TABLE}.size_uda ;;
  }

  dimension: standard_uom {
    type: string
    hidden: yes
    sql: ${TABLE}.standard_uom ;;
  }

  dimension: sub_line {
    type: string
    hidden: no
    view_label: "Products"
    sql: ${TABLE}.sub_line ;;
  }

  dimension: subclass {
    type: number
    hidden: yes
    sql: ${TABLE}.subclass ;;
  }

  dimension: subclass_name {
    view_label: "Products"
    label: "Sub Class"
    description: "Oracle RMS product subclass"
    type: string
    suggest_explore: dim_retail_prod_sub_class
    suggest_dimension: dim_retail_prod_sub_class.subclass_name
    sql: ${TABLE}.subclass_name ;;
  }

  dimension: sup_class {
    type: string
    hidden: yes
    sql: ${TABLE}.sup_class ;;
  }

  dimension: sup_subclass {
    type: string
    hidden: yes
    sql: ${TABLE}.sup_subclass ;;
  }

  dimension: taxo_class {
    view_label: "Products"
    label: "Taxonomy Class"
    description: "Taxonomy Class from Oracle RMS"
    type: string
    suggest_explore: dim_retail_prod_taxo_class
    suggest_dimension: dim_retail_prod_taxo_class.taxo_class
    sql: ${TABLE}.taxo_class ;;
  }

  dimension: taxo_subclass {
    view_label: "Products"
    label: "Taxonomy Subclass"
    description: "Taxonomy subclass attribute from RMS"
    type: string
    suggest_explore: dim_retail_prod_taxo_subclass
    suggest_dimension: dim_retail_prod_taxo_subclass.taxo_subclass
    sql: ${TABLE}.taxo_subclass ;;
  }

  dimension: uc_code {
    type: string
    hidden: yes
    sql: ${TABLE}.uc_code ;;
  }

  dimension: uda_zone {
    type: string
    hidden: yes
    sql: ${TABLE}.uda_zone ;;
  }

  dimension: usage_specificity {
    type: string
    hidden: yes
    sql: ${TABLE}.usage_specificity ;;
  }

  dimension: vpn {
    view_label: "Products"
    description: "Item VPN attribute from Oracle RMS"
    type: string
    label: "VPN"
    suggest_dimension: dim_retail_prod.item_vpn
    sql: ${TABLE}.vpn ;;
  }

# -------------------  Dimensions Product ends  ----------------------------------------

# -------------------  Dimensions Supplier starts  ----------------------------------------

  dimension: sup_name {
    type: string
    view_label: "Supplier"
    label: "Supplier Name"
    sql: ${TABLE}.sup_name ;;
  }

  dimension: contact_name {
    type: string
    view_label: "Supplier"

    sql: ${TABLE}.contact_name ;;
  }

  dimension: supplier {
    type: number
    view_label: "Supplier"
    sql: ${TABLE}.supplier ;;
  }

# -------------------  Dimensions Supplier ends  ----------------------------------------


# -------------------  Dimensions Locations starts  ----------------------------------------

 dimension: loc_code {
  type: number
  view_label: "Locations"
  label: "Location Code"
  sql: ${TABLE}.loc_code ;;
}

dimension: loc_name {
  type: string
  view_label: "Locations"
  label: "Location Name"
  sql: ${TABLE}.loc_name ;;
}

  dimension: chain {
    type: number
    hidden: yes
   view_label: "Locations"
    sql: ${TABLE}.chain ;;
  }

  dimension: chain_name {
    type: string
  view_label: "Locations"
    label: "Chain"
    sql: ${TABLE}.chain_name ;;
  }

  dimension: region {
    type: number
    hidden: yes
    view_label: "Locations"
    sql: ${TABLE}.region ;;
  }

  dimension: region_name {
    type: string
    view_label: "Locations"
    label: "Region"
    sql: ${TABLE}.region_name ;;
  }

  dimension: district {
    type: number
    hidden: yes
    view_label: "Locations"
    sql: ${TABLE}.district ;;
  }

  dimension: district_name {
    type: string
    view_label: "Locations"
    label: "District"
    sql: ${TABLE}.district_name ;;
  }

  dimension: stock_type {
    description: "Stock Type(Ecom/WH/Store)"
    hidden: no
    label: "Stock Type(Ecom/WH/Store)"
    group_label: "Locations"
    type: string
    sql: CASE WHEN ${TABLE}.loc_name LIKE "%ECOMMERCE%" THEN "Ecom" WHEN BYTE_LENGTH(${org_num})>6 THEN "Warehouse" ELSE "Store" END ;;
  }


# -------------------  Dimensions Locations ends  ----------------------------------------

# -------------------  Dimensions dm_soh_sales starts ----------------------------------------

  dimension: item_unit_cost {
    hidden: no
    label: "WAC Local"
    view_label: "Product Data in RMS"
    type: number
    sql: ${TABLE}.item_unit_cost ;;}

  dimension: item_unit_cost_usd {
      hidden: no
      label: "WAC USD"
      view_label: "Product Data in RMS"
      type: number
      sql: ${TABLE}.item_unit_cost_usd ;;
      }

  dimension: item_status {
        hidden: no
        label: "Status"
        view_label: "Product Data in RMS"
        type: string
        sql: ${TABLE}.item_status ;;

        }


# ------------------- Dimensions dm_soh_sales ends ----------------------------------------

# -------------------  Derived Dimensions starts ----------------------------------------

  dimension: revised_qty_received {
    hidden: no
    type: number
    sql: CASE WHEN ${qty_received} > ${qty_ordered} THEN ${qty_ordered} ELSE ${qty_received} END ;;
  }

# -------------------  Derived Dimensions ends ----------------------------------------


# -------------------  Measures starts ----------------------------------------


  measure: last_updated_date {
    type: date_time
    label: "Last refresh date"
    sql: MAX(${written_raw}) ;;
    convert_tz: no
  }




#correction made by LK SPD-292
  measure: qty_ordered_sum {
    label: "Quantity Ordered"
    type: sum
    sql: ${qty_ordered} ;;
  }


  measure: orig_qty_ordered_sum {
    label: "Original Quantity Ordered"
    type: sum
    sql: ${qty_prescaled} ;;
  }



  measure: cancelled_qty_sum {
    label: "Cancelled Quantity"
    type: sum
    sql: ${qty_cancelled} ;;
  }





#corection made by LK SPD-292
  measure: qty_received_sum {
    label: "Quantity Received"
    type: sum
    sql: ${qty_received} ;;
  }

  measure: supplier_fill_rate {
    label: "Quantity Fill Rate %"
    type: number
    value_format_name: "percent_2"
    sql: SAFE_DIVIDE(SUM(${revised_qty_received}),SUM(${qty_prescaled})) ;;
  }



  measure: supplier_fill_rate_value{
    label: "Value Fill Rate %"
    type: number
    value_format_name: "percent_2"
    sql: SAFE_DIVIDE(SUM(${revised_qty_received}* ${unit_cost_usd}),SUM(${qty_prescaled}* ${unit_cost_usd}));;
  }


  measure: qty_ordered_value {
    label: "Ordered Value USD"
    type: sum
    value_format_name: usd
    sql: ${qty_ordered}*${unit_cost_usd}  ;;
  }



  measure: qty_ordered_value_loc {
    label: "Ordered Value Local"
    type: sum
    value_format_name: decimal_2
    sql: ${qty_ordered}*${unit_cost_in_local_curr} ;;
  }



  measure: orig_qty_ordered_value_usd {
    label: "Original Ordered Value USD"
    type: sum
    value_format_name: usd
    sql: ${qty_prescaled} * ${unit_cost_usd} ;;
  }

  measure: orig_qty_ordered_value_loc {
    label: "Original Ordered Value Local"
    type: sum
    value_format_name: decimal_2
    sql: ${qty_prescaled} * ${unit_cost_in_local_curr} ;;
  }



  measure: qrt_received_value {
    label: "Received Value USD"
    type: sum
    value_format_name: usd
    sql: ${qty_received}* ${unit_cost_usd} ;;
  }




  measure: qrt_received_value_loc {
    label: "Received Value Local"
    type: sum
    value_format_name: decimal_2
    sql: ${qty_received}* ${unit_cost_in_local_curr} ;;
  }

  #Received cost local based on WAC
  measure: qrt_received_value_wac_loc {
    label: "Received Value Local WAC"
    type: sum
    value_format_name: decimal_2
    sql: ${qty_received}* ${item_unit_cost} ;;
  }

  measure: po_age {
    label: "PO Age"
    type: average
    sql: case
    when date_diff(CURRENT_DATE(), ${written_date}, week) > 0
      then date_diff(CURRENT_DATE(), ${written_date}, day) - (date_diff(CURRENT_DATE(), ${written_date}, week) * 2)
    else
      date_diff(CURRENT_DATE(), ${written_date}, day)
    end ;;
  }

  measure: delivery_status {
    hidden: yes
    label: "PO Delivery Status"
    type: string
    sql: CASE WHEN ${qty_received_sum} > 0 THEN "Delivered" ELSE "Pending for Delivery" END ;;
  }

  # --- SPD-355 using unit cost for ordered value ends..

  measure: distinct_orders {
    type: count_distinct
    label: "Distinct Purchase Orders"
    description: "Total number of distinct Purchase Orders placed"
    sql: ${order_no} ;;
  }




  # -------------------  Measures ends ----------------------------------------


  measure: count {
    hidden:no
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {

    fields: [
      group_name,
      sup_name,
      region_name,
      class_name,
      subclass_name,
      dept_name,
      area_name,
      loc_name,
      chain_name,
      channel_name,
      contact_name,
      mall_name,
      district_name
    ]
  }
}

view: dm_supply_orders_detailed__sep_category {

  dimension: item {
    hidden:yes
    type: string
    sql: ${TABLE}.ITEM ;;
  }

  dimension: sep_category {
    hidden:yes
    type: string
    sql: ${TABLE}.SEP_CATEGORY ;;
  }

  dimension: uda_value {
    hidden:yes
    type: number
    sql: ${TABLE}.UDA_VALUE ;;
  }
}
