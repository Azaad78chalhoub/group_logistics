include: "period_over_period_gsc.view"
view: carolina_herrera_sales_at_time_interval {
  derived_table: {
    sql: SELECT DISTINCT COALESCE(a.country,e.country) as country,
                        COALESCE(cast(a.store as string),cast(e.ilion_store_id as string)) as store,
                        COALESCE(a.store_code,e.store_code) as store_code,
                        a.idstorecashclose,
                        COALESCE(a.dateop,e.calendar_date) as dateop,
                        a.datetimeop,
                        a.ticketnumber,
                        a.idarticlesku,
                        a.sku_externalcode,
                        a.ref_code2,
                        a.ean13,
                        a.fullprice,
                        a.currency,
                        a.idhr,
                        a.idhrop,
                        a.aliasname,
                        a.business_line,
                        a.ticket_price,
                        a.arr_amount,
                        a.freight_amount,
                        a.idop,
                        a.record_type,
                        a.fullprice_usd,
                        a.ticket_price_usd,
                        a.arr_amount_usd,
                        a.freight_amount_usd,
                        a.bonus,
                        a.bonus_usd,
                        a.total,
                        a.totalnet,
                        a.total_usd,
                        a.totalnet_usd,
                        a.quantity,
                        (fullprice*quantity)-total as discount_amt_local,
                        (fullprice_usd*quantity)-total_usd as discount_amt_usd,
                        b.tran_type,
                        c.key_store_sum,
                        c.total_netsales_store AS store_avg_monthly_sales,
                        d.key_store_daily_sum,
                        d.total_netsales_store_daily as store_avg_daily_sales,
                        e.net_sales_in_lc,
                        e.net_sales_in_usd,
                        e.net_sales_in_lc_r1,
                        e.net_sales_in_usd_r1
          FROM `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_sales_at_time_interval` a
          --labeling a transaction as 'SALES', 'EXCHANGE' or 'RETURN'
          LEFT JOIN
            (SELECT     store_code,
                        dateop,ticketnumber,
                        sum(quantity) as total_quantity,
                        CASE WHEN sum(quantity) > 0 then 'SALES'
                            when sum(quantity) = 0 then 'EXCHANGE'
                            when sum(quantity) < 0 then 'RETURN' end as tran_type
            FROM `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_sales_at_time_interval`
            group by 1,2,3) b
            ON a.store_code = b.store_code AND
                a.dateop = b.dateop AND
                a.ticketnumber = b.ticketnumber

            --Joining monthly store sales to get the monthly category contribution
            LEFT JOIN
            (SELECT store_code,
                    CONCAT(EXTRACT(YEAR FROM dateop),' - ',EXTRACT(MONTH from dateop)) as dateop_ym,
                    concat(cast(store_code as string),":",CONCAT(EXTRACT(YEAR FROM dateop),' - ',EXTRACT(MONTH from dateop))) as key_store_sum,
                    sum(totalnet) as total_netsales_store
            FROM `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_sales_at_time_interval`
            group by 1,2,3) c
           ON a.store_code = c.store_code AND
               CONCAT(EXTRACT(YEAR FROM a.dateop),' - ',EXTRACT(MONTH from a.dateop)) = c.dateop_ym
            --joining daily store sales to get the daily category contribution
            LEFT JOIN
            ( SELECT store_code,
                     dateop as dateop,
                     concat(cast(store_code as string),"-",dateop) as key_store_daily_sum,
                     sum(totalnet) as total_netsales_store_daily
              FROM `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_sales_at_time_interval`
              group by 1,2,3) d
              ON a.store_code = d.store_code AND
                 a.dateop = d.dateop
          --budgets at daily level for all stores
          FULL OUTER JOIN
            (select sm.ilion_store_id,t.*,sls.country
             from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_daily_targets` t
             left join `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_store_master` sm
             on t.store_code = sm.ilion_store_code
          --to get country values for the missing budget values
          left join (select distinct country,
                                    store,
                                    store_code
                                    from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_sales_at_time_interval`) sls
          on t.store_code = sls.store_code)e
          ON a.store_code = e.store_code
          AND a.dateop = e.calendar_date
              ;;
  }

  extends: [period_over_period_gsc]
  dimension_group: pop_no_tz {
    sql: ${TABLE}.dateop ;;
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${store}+"-"+${datetimeop_date}+"-"+${ticketnumber}+"-"+${sku_externalcode} ;;
  }

  dimension: ltl_till_date {
    type: string
    view_label: "Like to Like Timeframe comparison"
    label: "LTL Timeframe"
    description: "For Like to like timeframe comparison"
    sql: CASE WHEN EXTRACT(MONTH FROM ${TABLE}.dateop) > EXTRACT(MONTH FROM CURRENT_TIMESTAMP) THEN "NO"
              WHEN EXTRACT(MONTH FROM ${TABLE}.dateop) < EXTRACT(MONTH FROM CURRENT_TIMESTAMP) THEN "YES"
              WHEN EXTRACT(MONTH FROM ${TABLE}.dateop) = EXTRACT(MONTH FROM CURRENT_TIMESTAMP) AND
              EXTRACT(DAY FROM ${TABLE}.dateop) >= EXTRACT(DAY FROM CURRENT_TIMESTAMP) THEN "NO"
              ELSE "YES" END;;
  }

#Location based dimensions
  dimension: store {
    type: number
    group_label: "Location"
    label: "Store Number"
    hidden: no
    sql: ${TABLE}.store ;;
  }

  dimension: store_code {
    type: string
    group_label: "Location"
    label: "Store Code Identifier"
    hidden: no
    drill_fields: [carolina_herrera_category_mapping.category]
    sql: ${TABLE}.store_code ;;
  }

  dimension: store_type {
    type: string
    group_label: "Location"
    label: "Store Type"
    hidden: no
    sql: case when ${TABLE}.store_code like 'C%' then 'CH' else 'PG' end ;;
  }

  dimension: idstorecashclose {
    type: number
    hidden: yes
    sql: ${TABLE}.idstorecashclose ;;
  }

  dimension: key_store_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.key_store_sum ;;
  }


  dimension: country {
    type: string
    map_layer_name: countries
    group_label: "Location"
    label: "Country Name"
    hidden: no
    drill_fields: [carolina_herrera_store_master.store_cd_name]
    sql: ${TABLE}.country ;;
  }

  dimension: market_rank {
    type: number
    group_label: "Location"
    label: "Market Rank"
    hidden: no
    sql: case when ${TABLE}.country = 'UNITED ARAB EMIRATES' then 1
              when ${TABLE}.country = 'SAUDI ARABIA' then 2
              when ${TABLE}.country = 'KUWAIT' then 3
              when ${TABLE}.country = 'QATAR' then 4
              when ${TABLE}.country = 'BAHRAIN' then 5
              when ${TABLE}.country = 'EGYPT' then 6
              when ${TABLE}.country = 'JORDAN' then 7
              else 0 end
              ;;
  }

  #Date dimensions
  dimension_group: business {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.dateop ;;
  }

  dimension: business_week_no{
    type: number
    group_label: "Sales"
    label: "Business Week No."
    description: "Week No. extracted from the business date"
    sql: EXTRACT(WEEK FROM (${TABLE}.dateop))+1 ;;
  }

  #Date dimensions
  dimension_group: transaction {
    type: time
    timeframes: [
      time,
      time_of_day,
      hour_of_day,
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: datetime
    sql: ${TABLE}.datetimeop ;;
  }

  dimension: year_month {
    type: string
    label: "Calendar Month"
    sql: CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.dateop) AS STRING), "-",
          case when length(CAST(EXTRACT(MONTH FROM ${TABLE}.dateop) AS STRING)) =1 then CONCAT("0",CAST(EXTRACT(MONTH FROM ${TABLE}.dateop) AS STRING))
          ELSE CAST(EXTRACT(MONTH FROM ${TABLE}.dateop) AS STRING) END);;
  }

  dimension_group: datetimeop {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.datetimeop ;;
  }

  dimension: ticketnumber {
    type: string
    label: "Invoice Number"
    hidden: no
    sql: ${TABLE}.ticketnumber ;;
  }


#Product related dimensions
  dimension: idarticlesku {
    type: number
    hidden: yes
    sql: ${TABLE}.idarticlesku ;;
  }

  dimension: sku_externalcode {
    type: string
    group_label: "Product"
    label: "SKU Code"
    sql: ${TABLE}.sku_externalcode
      ;;
  }

  dimension: sku_url {
    type: string
    group_label: "Product"
    label: "SKU URL"
    sql: case when ${sku_externalcode} like 'A%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/ATMP/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"/","1500/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"_01.jpg")
           when ${sku_externalcode} like '1%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2021/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"/","1500/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"_01.jpg")
           when ${sku_externalcode} like '0%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2020/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"/","1500/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"_01.jpg")
           when ${sku_externalcode} like '9%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2019/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"/","1500/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"_01.jpg")
           when ${sku_externalcode} like '8%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2018/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"/","1500/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"_01.jpg")
           when ${sku_externalcode} like '7%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2017/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"/","1500/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"_01.jpg")
           when ${sku_externalcode} like '6%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2016/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"/","1500/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"_01.jpg")
           when ${sku_externalcode} like '5%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2015/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"/","1500/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"_01.jpg")
           when ${sku_externalcode} like '4%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2014/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"/","1500/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"_01.jpg")
           when ${sku_externalcode} like '3%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2013/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"/","1500/",
                                                      SUBSTR(${sku_externalcode},1,LENGTH(${sku_externalcode})-2),"_01.jpg")
          else "https://vcunited.club/no-image-available-2-jpg/" end;;
  }

  dimension: sku_externalcode2 {
    type: string
    group_label: "Product"
    label: "SKU Code Image"
    sql: ${sku_url} ;;
    html: <img src="{{ value }}" width="300" height="300"/>  ;;
  }

  dimension: sku_size {
    type: string
    group_label: "Product"
    label: "SKU Size"
    drill_fields: [sku_externalcode]
    sql:SUBSTR(${TABLE}.sku_externalcode,14)   ;;
  }

  dimension: sku_externalcode_wo_size {
    type: string
    label: "SKU Code w/o size"
    sql: left(${TABLE}.sku_externalcode,13) ;;
  }

  dimension: sku_season {
    type: string
    group_label: "Product"
    label: "SKU Season"
    sql: LEFT(${TABLE}.sku_externalcode,2);;
  }

  dimension: ref_code2 {
    type: string
    group_label: "Product"
    label: "Refernce Code for SKU"
    sql: ${TABLE}.ref_code2 ;;
  }

  dimension: ean13 {
    type: string
    group_label: "Product"
    label: "EAN(13 digit code)"
    sql: ${TABLE}.ean13 ;;
  }

  dimension: quantity {
    type: number
    hidden: yes
    sql: ${TABLE}.quantity ;;
  }

  dimension: discount_amt_local {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amt_local ;;
  }

  dimension: discount_amt_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amt_usd ;;
  }

  dimension: fullprice {
    type: number
    hidden: yes
    sql: ${TABLE}.fullprice ;;
  }

  dimension: total {
    type: number
    hidden: yes
    sql: ${TABLE}.total ;;
  }

  dimension: store_avg_monthly_sales {
    type: number
    hidden: yes
    sql: ${TABLE}.store_avg_monthly_sales ;;
  }

  dimension: store_avg_daily_sales {
    type: number
    hidden: yes
    sql: ${TABLE}.store_avg_daily_sales ;;
  }

  dimension: totalnet {
    type: number
    hidden: yes
    sql: ${TABLE}.totalnet ;;
  }

  dimension: totalnet_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.totalnet_usd ;;
  }

  dimension: currency {
    type: string
    hidden: no
    label: "Local Currency Code"
    sql: ${TABLE}.currency ;;
  }

  dimension: idhr {
    type: number
    hidden: no
    label: "Staff code(idhr)"
    sql: ${TABLE}.idhr ;;
  }


  dimension: aliasname {
    type: string
    group_label: "Product"
    label: "Alias Name"
    sql: ${TABLE}.aliasname ;;
  }

  dimension: arr_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.arr_amount ;;
  }

  dimension: arr_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.arr_amount_usd ;;
  }

  dimension: bonus {
    type: number
    hidden: yes
    sql: ${TABLE}.bonus ;;
  }

  dimension: bonus_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.bonus_usd ;;
  }

  dimension: business_line {
    type: string
    group_label: "Product"
    label: "Business Line"
    sql: ${TABLE}.business_line ;;
  }

  dimension: freight_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.freight_amount ;;
  }

  dimension: freight_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.freight_amount_usd ;;
  }


  dimension: fullprice_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.fullprice_usd ;;
  }

  dimension: idhrop {
    type: number
    hidden: yes
    sql: ${TABLE}.idhrop ;;
  }

  dimension: fp_transaction_ind {
    type: number
    hidden: yes
    sql: case when abs(${total}) < abs(${fullprice}*${quantity}) then 0 else 1 end ;;
  }

  dimension: idop {
    type: string
    hidden: yes
    sql: ${TABLE}.idop ;;
  }

  dimension: record_type {
    type: string
    hidden: yes
    sql: ${TABLE}.record_type ;;
  }

  dimension: tran_type {
    type: string
    label: "Transaction Type"
    hidden: no
    sql: ${TABLE}.tran_type ;;
  }

  dimension: ticket_price {
    type: number
    hidden: yes
    sql: ${TABLE}.ticket_price ;;
  }

  dimension: ticket_price_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.ticket_price_usd ;;
  }


  dimension: total_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.total_usd ;;
  }

  dimension: net_sales_in_lc_r0 {
    type: number
    hidden: yes
    sql: ${TABLE}.net_sales_in_lc ;;
  }

  dimension: net_sales_in_usd_r0 {
    type: number
    hidden: yes
    sql: ${TABLE}.net_sales_in_usd ;;
  }

  dimension: net_sales_in_lc_r1 {
    type: number
    hidden: yes
    sql: ${TABLE}.net_sales_in_lc_r1 ;;
  }

  dimension: net_sales_in_usd_r1 {
    type: number
    hidden: yes
    sql: ${TABLE}.net_sales_in_usd_r1 ;;
  }
#Measure start from here

  measure: totalnet_sales_local {
    type: sum
    group_label: "Sales"
    label: "Total Net Sales in Local Currency"
    sql: ${totalnet} ;;
  }

  measure: budget_totalnet_sales_local {
    type: sum_distinct
    sql_distinct_key: CONCAT(${store_code},"-",${business_date}) ;;
    group_label: "Sales"
    label: "Budget Net Sales in Local Currency R0"
    sql: ${net_sales_in_lc_r0} ;;
  }

  measure: budget_totalnet_sales_local_r1 {
    type: sum_distinct
    sql_distinct_key: CONCAT(${store_code},"-",${business_date}) ;;
    group_label: "Sales"
    label: "Budget Net Sales in Local Currency R1"
    sql: ${net_sales_in_lc_r1} ;;
  }

  measure: totalnet_sales_local_storelevel {
    type: sum_distinct
    sql_distinct_key: ${TABLE}.key_store_sum ;;
    group_label: "Sales"
    label: "Total Net Sales in Local Currency at store level"
    sql: ${store_avg_monthly_sales} ;;
  }

  measure: totalnet_sales_local_storelevel_daily {
    type: sum_distinct
    sql_distinct_key: ${TABLE}.key_store_daily_sum ;;
    group_label: "Sales"
    label: "Total daily Net Sales in Local Currency at store level"
    sql: ${store_avg_daily_sales} ;;
  }


  measure: totalnet_sales_usd {
    type: sum
    hidden: no
    group_label: "Sales"
    label: "Total Net Sales in USD"
    sql: ${totalnet_usd} ;;
    value_format_name: usd
  }

  measure: budget_totalnet_sales_usd {
    type: sum_distinct
    sql_distinct_key: CONCAT(${store_code},"-",${business_date});;
    group_label: "Sales"
    label: "Budget Net Sales in USD R0"
    sql: ${net_sales_in_usd_r0} ;;
  }

  measure: budget_totalnet_sales_usd_r1 {
    type: sum_distinct
    sql_distinct_key: CONCAT(${store_code},"-",${business_date});;
    group_label: "Sales"
    label: "Budget Net Sales in USD R1"
    sql: ${net_sales_in_usd_r1} ;;
  }

  measure : quantity_sold {
    type: sum
    hidden: no
    group_label: "Sales"
    label: "Total Quantity sold"
    sql: ${quantity} ;;
  }

  measure: full_price_usd {
    type: number
    hidden: yes
    group_label: "Sales"
    label: "Full Price in USD"
    sql: ${fullprice_usd} ;;
    value_format_name: usd
  }

  measure : fullprice_local {
    type: sum
    hidden: yes
    group_label: "Sales"
    label: "Full Price in Local Currency"
    sql: ${TABLE}.fullprice ;;
  }

  measure: markdown_investment_usd {
    type: sum
    hidden: no
    group_label: "Sales"
    label: "Markdown Investment USD"
    sql:  ${discount_amt_usd};;
    value_format_name: usd
  }

  measure: markdown_depth_perc {
    type: number
    hidden: no
    group_label: "Sales"
    label: "Markdown Depth%"
    sql:  SUM(${discount_amt_usd})/NULLIF(SUM(${total_usd}+${discount_amt_usd}),0);;
    value_format_name: percent_2
  }

  measure: markdown_investment_local {
    type: sum
    hidden: no
    group_label: "Sales"
    label: "Markdown Investment local"
    sql: ${discount_amt_local};;
    value_format_name: decimal_2
  }

  measure: transactions_count_gross {
    type: count_distinct
    hidden: no
    group_label: "Transactions"
    label: "Gross Transaction Count"
    filters: {
      field: tran_type
      value: "SALES"
    }
    sql: ${ticketnumber} ;;
  }

  measure: transactions_count_return {
    type: count_distinct
    hidden: no
    group_label: "Transactions"
    label: "Gross Transaction Count return"
    filters: {
      field: tran_type
      value: "RETURN"
    }
    sql: ${ticketnumber} ;;
  }

  measure: transactions_count_xchg {
    type: count_distinct
    hidden: no
    group_label: "Transactions"
    label: "Gross Transaction Count Exchange"
    filters: {
      field: tran_type
      value: "EXCHANGE"
    }
    sql: ${ticketnumber} ;;
  }

  measure: transaction_count_net {
    type: number
    hidden: no
    group_label: "Transactions"
    label: "Net Transactions (Returns adjusted)"
    sql: (${transactions_count_gross}-(${transactions_count_return})) ;;
  }

  measure: VPT_local {
    type: number
    hidden: no
    group_label: "Transactions"
    label: "Value Per transaction local"
    sql: SAFE_DIVIDE(${totalnet_sales_local},${transaction_count_net}) ;;
    value_format_name: decimal_0
  }

  measure: VPT_USD {
    type: number
    hidden: no
    group_label: "Transactions"
    label: "Value Per transaction USD"
    sql: SAFE_DIVIDE(${totalnet_sales_usd},${transaction_count_net}) ;;
    value_format_name: usd_0
  }

  measure: UPT {
    type: number
    hidden: no
    group_label: "Transactions"
    label: "Units Per transaction"
    sql: SAFE_DIVIDE(${quantity_sold},${transaction_count_net}) ;;
    value_format_name: decimal_2
  }

  measure: transactions_count_gross_fp {
    type: count_distinct
    hidden: yes
    group_label: "Transactions"
    label: "Gross Transaction Count FP"
    filters: {
      field: record_type
      value: "SALE"
    }
    filters: {
      field: fp_transaction_ind
      value: "1"
    }
    sql: ${ticketnumber} ;;
  }

  measure: transactions_count_return_fp {
    type: count_distinct
    hidden: yes
    group_label: "Transactions"
    label: "Gross Transaction Count return FP"
    filters: {
      field: record_type
      value: "RETURN"
    }
    filters: {
      field: fp_transaction_ind
      value: "1"
    }
    sql: ${ticketnumber} ;;
  }

  measure: full_price_transactions {
    type: number
    hidden: yes
    group_label: "Transactions"
    label: "Full Price Transactions"
    sql: (${transactions_count_gross_fp}-(2*${transactions_count_return_fp})) ;;
    value_format_name: decimal_0
  }

  measure: gross_margin_local {
    type: sum
    hidden: no
    group_label: "Sales"
    label: "Gross Margin Local"
    sql: ${totalnet}*0.40 ;;
    value_format_name: decimal_2
  }

  measure: gross_margin_usd {
    type: sum
    hidden: no
    group_label: "Sales"
    label: "Gross Margin USD"
    sql: ${totalnet_usd}*0.40 ;;
    value_format_name: usd
  }

  measure: bonus_local {
    type: sum
    hidden: no
    label: "Bonus Amount in Local Currency"
    sql: ${bonus} ;;
  }

  measure: bonus_amt_usd {
    type: sum
    hidden: no
    label: "Bonus Amount in Local Currency"
    sql: ${bonus_usd} ;;
  }

  measure: total_sales {
    type: sum
    group_label: "Sales"
    label: "Gross Sales Value Local"
    hidden: no
    sql: ${total} ;;
    value_format_name: decimal_2
  }

  measure: total_sales_usd {
    type: sum
    group_label: "Sales"
    label: "Gross Sales Value USD"
    hidden: no
    sql: ${total_usd} ;;
    value_format_name: usd
  }


  measure: count {
    type: count
    drill_fields: [aliasname]
  }
}
