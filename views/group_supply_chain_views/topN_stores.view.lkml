view: topN_stores {
  derived_table: {
    sql:
  SELECT DISTINCT bk_storeid,
                  STORE_RANK
  FROM
  (
   SELECT brand,
   bk_storeid,
   RANK() OVER(PARTITION BY brand ORDER BY Netsales DESC ) AS STORE_RANK
   FROM
   (
    SELECT brand,
    sales.bk_storeid,
    store_name,
    SUM(amountusd_beforetax) AS Netsales
    FROM
      `chb-prod-supplychain-data.prod_supply_chain.factretailsales` sales
       LEFT JOIN
          `chb-prod-supplychain-data.prod_shared_dimensions.dim_bu_cdl` dims
        ON
          cast(sales.bk_productid as string)=dims.prod_num
          AND CAST(sales.bk_storeid AS STRING)=dims.org_num

      LEFT JOIN
        `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` diml
         ON CAST(sales.bk_storeid AS STRING)=CAST(diml.store as STRING)

      -- joining to get the right BU for the item
        LEFT JOIN
          `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` bu
        ON
          dims.business_unit_final=bu.bu_code

      WHERE atr_trandate  >= IFNULL(DATE({% date_start timeframe %}),CURRENT_DATE()) AND atr_trandate <=   IFNULL(DATE({% date_end timeframe %}),CURRENT_DATE())
      GROUP BY 1,2,3
      ORDER BY 1,3 DESC))


      ;;

  }


  filter: timeframe {
    type: date
    label: "Time Frame for TOP N"
    default_value: "after 2019/12/01"
  }

  dimension: store {
    type: number
    hidden: yes
    label: "Store ID"
    sql: ${TABLE}.bk_storeid;;

  }

  dimension:  rank{
    type: number
    hidden: no
    label: "Store Rank"
    sql: ${TABLE}.STORE_RANK;;
  }

  }
