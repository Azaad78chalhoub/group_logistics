view: carolina_herrera_consolidated_tickets_at_time_interval {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_consolidated_tickets_at_time_interval`
    ;;

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension_group: dateop {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.dateop ;;
  }

  dimension_group: datetimeop {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.datetimeop ;;
  }

  dimension: idpersonclient {
    type: number
    sql: ${TABLE}.idpersonclient ;;
  }

  dimension: idstorecashclose {
    type: number
    sql: ${TABLE}.idstorecashclose ;;
  }

  dimension: items {
    type: string
    sql: ${TABLE}.items ;;
  }

  dimension: nettotal {
    type: number
    sql: ${TABLE}.nettotal ;;
  }

  dimension: nettotal_usd {
    type: number
    sql: ${TABLE}.nettotal_usd ;;
  }

  dimension: record_type {
    type: string
    sql: ${TABLE}.record_type ;;
  }

  dimension: store {
    type: number
    sql: ${TABLE}.store ;;
  }

  dimension: store_code {
    type: string
    sql: ${TABLE}.store_code ;;
  }

  dimension: taxes {
    type: number
    sql: ${TABLE}.taxes ;;
  }

  dimension: taxes_usd {
    type: number
    sql: ${TABLE}.taxes_usd ;;
  }

  dimension: ticketnumber {
    type: string
    sql: ${TABLE}.ticketnumber ;;
  }

  dimension: total {
    type: number
    sql: ${TABLE}.total ;;
  }

  dimension: total_usd {
    type: number
    sql: ${TABLE}.total_usd ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
