view: dm_soh_sales_inv {
  label:"Cost and Retail"
  derived_table: {
    sql_trigger_value:  SELECT CURRENT_DATE() ;;
    sql: SELECT
        stock_date,
        prod_num,
        org_num,
        item,
        loc_code,
        loc_name,
        city,
        country_desc,
        district_name
        unit_cost,
        unit_cost_usd,
        unit_retail_local,
        unit_retail_usd
        FROM `chb-prod-supplychain-data.prod_supply_chain.dm_soh_sales`;;
  }


  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${stock_date}, " - ", ${prod_num}, " - " ${org_num}) ;;
  }

  dimension: stock_date {
    hidden: no
    sql: ${TABLE}.stock_date ;;
  }

  dimension: City {
    hidden: no
    sql: ${TABLE}.city ;;
  }
  dimension: Country_desc{
    hidden: no
    sql: ${TABLE}.country_desc ;;
  }

  dimension: District_name{
    hidden: no
    sql: ${TABLE}.district_name ;;
  }


  dimension: loc_code {
    hidden: yes
    sql: ${TABLE}.loc_code ;;
  }


  dimension: loc_name {
    hidden: no
    sql: ${TABLE}.loc_name ;;
  }

  dimension: item {
    hidden: no
    sql: ${TABLE}.item ;;
  }

  dimension: prod_num {
    hidden: no
    sql: ${TABLE}.prod_num ;;
  }

  dimension: org_num {
    hidden: no
    sql: ${TABLE}.org_num ;;
  }

  dimension: unit_cost {
    hidden: yes
    sql: ${TABLE}.unit_cost ;;
  }

  dimension: unit_cost_usd {
    hidden: no
    type: number
    sql: ${TABLE}.unit_cost_usd ;;
    value_format: "$0"
  }

  dimension: unit_retail_local {
    hidden: yes
    sql: ${TABLE}.unit_retail_local ;;
  }

  dimension: unit_retail_usd {
    hidden: no
    sql: ${TABLE}.unit_retail_usd ;;
    value_format: "$0"
  }





  # measure: st_unit_cost {
  #   label: "Unit Cost (WAC Local)"
  #   type: sum
  #   hidden: yes
  #   description: "Aggregate of Unit Cost Local"
  #   sql: ${TABLE}.unit_cost ;;
  # }

  # measure: st_unit_cost_usd {
  #   label: "Unit Cost (WAC USD)"
  #   type: sum
  #   hidden: yes
  #   description: "Aggregate of Unit Cost USD"
  #   sql: ${TABLE}.unit_cost_usd ;;
  # }

  # measure: st_unit_retail_local {
  #   label: "Retail Selling Price Local"
  #   type: sum
  #   hidden: yes
  #   description: "Aggregate of Retail Price Local"
  #   sql: ${TABLE}.unit_retail_local ;;
  # }

  # measure: st_unit_retail_usd {
  #   label: "Retail Selling Price USD"
  #   type: sum
  #   hidden: yes
  #   description: "Aggregate of Retail Price USD"
  #   sql: ${TABLE}.unit_retail_usd ;;
  # }

}
