view: baan_sales {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.baan_sales`
    ;;
  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(${sales_invoice_id},${item},${ingestion_time},${file_name},${gross_sales}) ;;
  }

  dimension: barcode {
    type: string
    sql: ${TABLE}.barcode ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: business_unit {
    type: string
    sql: ${TABLE}.business_unit ;;
  }

  dimension: color_uda {
    type: string
    sql: ${TABLE}.color_uda ;;
  }

  dimension: cost_price {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.cost_price AS NUMERIC) ;;
  }
  measure: cost_price_measure {
    label: "Cost Price"
    type: sum
    sql: ${cost_price} ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: discount_value {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.discount_value AS NUMERIC) ;;
  }
  measure: discount_value_measure {
    label: "Discount Value"
    type: sum
    sql: ${discount_value} ;;
  }

  dimension: division_description {
    type: string
    sql: ${TABLE}.division_description ;;
  }

  dimension: file_name {
    hidden: yes
    type: string
    sql: ${TABLE}.file_name ;;
  }

  dimension: gross_sales {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.gross_sales AS NUMERIC) ;;
  }
  measure: gross_sales_measure  {
    label: "Gross Sales"
    type: sum
    sql: ${gross_sales} ;;
  }

  dimension: ingestion_time {
    hidden: yes
    type: string
    sql: ${TABLE}.ingestion_time;;
  }

  dimension_group: invoice {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(PARSE_DATE('%d-%m-%Y', ${TABLE}.invoice_date) AS TIMESTAMP);;
  }

  dimension: item {
    type: string
    label: "VPN"
    sql: ${TABLE}.item ;;
  }

  dimension: item_description {
    type: string
    sql: ${TABLE}.item_description ;;
  }

  dimension: prod__category {
    type: string
    sql: ${TABLE}.prod__category ;;
  }

  dimension: product_division {
    type: string
    sql: ${TABLE}.product_division ;;
  }

  dimension: qty_sold {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.qty_sold AS NUMERIC);;
  }
  measure: qty_sold_measure  {
    label: "Quantity Sold"
    type: sum
    sql: ${qty_sold} ;;
  }

  dimension: sales_invoice_id {
    type: string
    sql: ${TABLE}.sales_invoice_id ;;
  }

  dimension: season {
    type: string
    sql: ${TABLE}.season ;;
  }

  dimension: size {
    type: string
    sql: ${TABLE}.size ;;
  }

  dimension: store {
    type: string
    sql: ${TABLE}.store ;;
  }

  dimension: store_name {
    type: string
    sql: ${TABLE}.store_name ;;
  }

  dimension: taxonomy_1 {
    type: string
    sql: ${TABLE}.taxonomy_1 ;;
  }

  dimension: taxonomy_1_1 {
    type: string
    sql: ${TABLE}.taxonomy_1_1 ;;
  }

  dimension: unit_price {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.unit_price AS NUMERIC) ;;
  }
  measure: unit_price_measure  {
    label: "Unit Price"
    type: sum
    sql: ${unit_price} ;;
  }

  dimension: vpn {
    hidden: yes
    label: "VPN"
    type: string
    sql: ${TABLE}.vpn ;;
  }

}
