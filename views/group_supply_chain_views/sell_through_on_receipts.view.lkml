include: "period_over_period_gsc.view"
view: sell_through_on_receipts {

  derived_table: {
    sql_trigger_value: SELECT CURRENT_DATE() ;;
    sql:SELECT
        date, location, item, brand,
        SUM(qty_received) AS qty_received,
        SUM(qty_sold) AS qty_sold,
        SUM(quantity_sold_fp) AS quantity_sold_fp,
        SUM(quantity_sold_md) as quantity_sold_md
        FROM (
            SELECT receipt_date AS date, location, item,
              SUM(qty_received) AS qty_received,
              0 AS qty_sold,
              0 AS quantity_sold_fp,
              0 AS quantity_sold_md
            FROM
              `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dm_receipts_detailed_all`
            WHERE
              CAST(receipt_date as DATE) > DATE({% date_start receipt_season %}) AND CAST(receipt_date as DATE) <=  IFNULL(DATE({% date_end receipt_season %}),CAST(current_date() AS DATE))
            GROUP BY
              1, 2, 3
            UNION ALL
            SELECT
              stock_date AS date,
              org_num AS location,
              prod_num AS item,
              inv_soh_qty AS qty_received,
              0 AS qty_sold,
              0 AS quantity_sold_fp,
              0 AS quantity_sold_md
            FROM
              `chb-prod-supplychain-data.prod_supply_chain.dm_soh_sales`
            WHERE
              CAST(stock_date as TIMESTAMP) = {% date_start receipt_season %}
            UNION ALL
            SELECT
              atr_trandate AS date,
              CAST(bk_storeid AS STRING) AS location,
              bk_productid AS item,
              0 AS qty_received,
              mea_quantity AS qty_sold,
              CASE WHEN (discountamt_usd IS NULL OR discountamt_usd = 0) THEN mea_quantity ELSE 0 END as quantity_sold_fp,
              CASE WHEN discountamt_usd <> 0 THEN mea_quantity ELSE 0 END as quantity_sold_md
            FROM
              `chb-prod-supplychain-data.prod_supply_chain.factretailsales`
            WHERE
              CAST(atr_trandate as DATE) >= DATE({% date_start sales_season %}) AND CAST(atr_trandate as DATE) <= IFNULL(DATE({% date_end sales_season %}),current_date()))
            LEFT JOIN
          `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy`
        ON
          location=store_code
      GROUP BY
        1, 2, 3, 4;;
  }

  extends: [period_over_period_gsc]

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${date}, " - ", ${location}, " - " ${item}) ;;
  }

  dimension_group: receipt {
    type: time
    timeframes: [
      raw,
      date,
      day_of_month,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension_group: pop_no_tz {
    sql: ${date} ;;
  }

  filter: receipt_season {
    type: date
    label: "Receipt Season"
    default_value: "after 2019/01/01"
  }

  filter: sales_season {
    type: date
    label: "Sales Season"
    default_value: "after 2019/01/01"
  }

  dimension: date {
    type: date
    hidden: yes
  }

  dimension: location {
    type: string
    hidden: no
  }

  dimension: item {
    type: string
    hidden: no
  }

  dimension: brand {
    type: string
    hidden: no
  }

  dimension: qty_received {
    hidden: yes
    sql: ${TABLE}.qty_received ;;
  }

  dimension: qty_sold {
    hidden: yes
    sql: ${TABLE}.qty_sold ;;
  }

  dimension: quantity_sold_fp {
    hidden: yes
    sql: ${TABLE}.quantity_sold_fp ;;
  }



  measure: st_qty_received {
    label: "Total Stock"
    description: "Aggregate of Total Quantity Received and Opening Stock"
    type: sum
    sql:  ${qty_received};;
  }

  measure: st_qty_sold {
    label: " Quantity Sold"
    description: "Aggregate of Quantity Sold"
    type: sum
    sql: ${qty_sold} ;;
  }

  measure: st_quantity_sold_fp {
    label: "Quantity Sold on FP"
    description: "Aggregate of Quantity Sold on FP"
    type: sum
    sql: ${quantity_sold_fp};;
  }

  measure: st_sold_cost_val {
    label: "Sales Value (on WAC Local)"
    type: sum
    value_format_name: decimal_0
    description: "Aggregate of Sales Value (Qty Sold * WAC)"
    sql: ${qty_sold} * ${dm_soh_sales_wac_rsp.unit_cost} ;;
  }

  measure: st_received_cost_val {
    type: sum
    label: "Received Value (on WAC Local)"
    value_format_name: decimal_0
    description: "Aggregate of Received Value ((Qty Received + Opening Stock) * WAC)"
    sql: ${qty_received} * ${dm_soh_sales_wac_rsp.unit_cost} ;;
  }

  measure: st_sold_cost_val_fp {
    label: "Cost Value on FP(on WAC Local)"
    type: sum
    value_format_name: decimal_0
    description: "Aggregate of Cost Value on FP items (Qty Sold on FP * WAC)"
    sql: ${quantity_sold_fp} * ${dm_soh_sales_wac_rsp.unit_cost} ;;
  }

  measure: st_sold_retail_val {
    label: "Sales Value (on RSP Local)"
    type: sum
    value_format_name: decimal_0
    description: "Aggregate of Sales Value (Qty Sold * RSP)"
    sql: ${qty_sold} * ${dm_soh_sales_wac_rsp.unit_retail_local} ;;
  }

  measure: st_received_retail_val {
    label: "Received Retail Value (on RSP Local)"
    type: sum
    value_format_name: decimal_0
    description: "Aggregate of Received Retail Value (Qty Received * RSP)"
    sql: ${qty_received} * ${dm_soh_sales_wac_rsp.unit_retail_local} ;;
  }

}


view: receipt_running_sums {
  derived_table: {
    explore_source: sell_through_on_receipts {
      column: date {}
      column: brand {}
      column: location {}
      column: item {}
      column: qty_received {}
      column: qty_sold {}
      derived_column: runing_qty_sold {
        sql: SUM(qty_sold) OVER (PARTITION BY location, item ORDER BY date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) ;;
      }
      derived_column: runing_qty_sold {
        sql: SUM(qty_received) OVER (PARTITION BY location, item ORDER BY date ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) ;;
      }
    }
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${date}, " - ", ${location}, " - " ${item}) ;;
  }

  dimension: date {
    hidden: yes
    label: "Good Receipt Notes Date"
    type: date
  }
  dimension: brand {
    hidden: yes
    label: "Good Receipt Notes Brand"
  }
  dimension: location {
    hidden: yes
    label: "Good Receipt Notes Location"
  }
  dimension: item {
    hidden: yes
    label: "Good Receipt Notes Item"
  }
  dimension: qty_received {
    hidden: yes
    label: "Good Receipt Notes Quantity Received"
    type: number
  }
  dimension: qty_sold {
    hidden: yes
    label: "Good Receipt Notes  Quantity Sold"
    type: number
  }

  dimension: running_qty_sold {
    hidden: yes
    sql: ${TABLE}.running_qty_sold ;;
  }
  dimension: running_qty_received {
    hidden: yes
    sql: ${TABLE}.running_qty_received ;;
  }

  measure: st_running_qty_sold {
    label: "Running Quantity Sold"
    type: sum
    sql: ${running_qty_sold} ;;
  }

  measure: st_running_qty_received {
    label: "Running Quantity Received"
    type: sum
    sql: ${running_qty_received} ;;
  }

  measure: st_running_sales_value {
    label: "Running Sales Value (on WAC Local)"
    type: sum
    description: "Aggregate of Running Sals Value (Runinng Total Qty Sold * WAC)"
    sql: ${running_qty_sold} * ${dm_soh_sales_wac_rsp.unit_cost}  ;;
  }
  measure: st_running_received_vale {
    label: "Running Received Value (on WAC Local)"
    type: sum
    description: "Aggregate of Running Received Value (Runinng Total Qty Received * WAC)"
    sql: ${running_qty_received} * ${dm_soh_sales_wac_rsp.unit_cost}  ;;
  }

}
