view: sell_through_chosen_analysis_period_pdt {

  derived_table: {
    sql_trigger_value:
    SELECT
        DATE_DIFF(MAX(stock_date), MIN(stock_date), DAY) + 1 as chosen_number_of_days,
        FROM base
        WHERE {% condition sell_through_prototype.prototype_date_1 %} TIMESTAMP(stock_date) {% endcondition %}
    ;;
    sql:  SELECT
        DATE_DIFF(MAX(stock_date), MIN(stock_date), DAY) + 1 as chosen_number_of_days,
        FROM base
        WHERE {% condition sell_through_prototype.prototype_date_1 %} TIMESTAMP(stock_date) {% endcondition %} ;;
  }

    dimension: chosed_number_of_days {
      type: number
      sql: ${TABLE}.chosen_number_of_days ;;
    }




}
