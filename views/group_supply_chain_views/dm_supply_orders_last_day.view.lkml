view: dm_supply_orders_last_day {

  derived_table: {
    sql_trigger_value: SELECT FLOOR(((TIMESTAMP_DIFF(CURRENT_TIMESTAMP(),'1970-01-01 00:00:00',SECOND)) - 60*60*5)/(60*60*24)) ;;

  sql:
select * from (SELECT
    status,
    stock_date,
    org_num,
    prod_num,
    rank() over(partition by org_num, prod_num order by stock_date desc) rank
    FROM chb-prod-supplychain-data.prod_supply_chain.dm_soh_sales
    WHERE stock_date > DATE_SUB(CURRENT_DATE(), INTERVAL 2 DAY) )
    where rank =1
   ;;
}


dimension: org_num {
  type: string
  hidden: yes

  sql: ${TABLE}.org_num ;;
}

dimension: prod_num {
  type: string
  hidden: yes
  label: "Product ID"
  sql: ${TABLE}.prod_num ;;
}



dimension: status {
  type: string
  label: "Latest Status"
  view_label: "Product Data in RMS"
  hidden: no
  sql: ${TABLE}.status ;;
}


}
