view: item_hts {
  view_label: "Products"
  derived_table: {
    sql: select * from chb-prod-stage-oracle.prod_supply_chain.item_hts
    where _fivetran_deleted = false;;
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${item}, "-", ${import_country_id}, "-", ${origin_country_id}) ;;
  }


  dimension: _fivetran_deleted {
    type: yesno
    hidden: yes
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: clearing_zone_id {
    type: string
    hidden: yes
    sql: ${TABLE}.clearing_zone_id ;;
  }

  dimension_group: create_datetime {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.create_datetime ;;
  }

  dimension_group: effect_from {
    type: time
    hidden: yes
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.effect_from ;;
  }

  dimension_group: effect_to {
    type: time
    hidden: yes
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.effect_to ;;
  }

  dimension: hts {
    type: string
    label: "HTS"
    sql: ${TABLE}.hts ;;
  }

  dimension: import_country_id {
    type: string
    hidden: no
    label: "Import Country ID"
    sql: ${TABLE}.import_country_id ;;
  }

  dimension: item {
    type: string
    hidden: yes
    label: "Item Code"
    sql: ${TABLE}.item ;;
  }

  dimension_group: last_update_datetime {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_update_datetime ;;
  }

  dimension: last_update_id {
    type: string
    hidden: yes
    sql: ${TABLE}.last_update_id ;;
  }

  dimension: origin_country_id {
    type: string
    hidden: no
    label: "Origin Country ID"
    sql: ${TABLE}.origin_country_id ;;
  }

  dimension: status {
    type: string
    hidden: yes
    sql: ${TABLE}.status ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: []
  }
}
