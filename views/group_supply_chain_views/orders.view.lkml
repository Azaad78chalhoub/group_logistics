view: orders {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.orders`
    ;;

  dimension: agent {
    type: string
    sql: ${TABLE}.agent ;;
  }

  dimension_group: app_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.app_datetime ;;
  }

  dimension: backhaul_allowance {
    type: number
    sql: ${TABLE}.backhaul_allowance ;;
  }

  dimension: backhaul_type {
    type: string
    sql: ${TABLE}.backhaul_type ;;
  }

  dimension: bill_to_id {
    type: string
    sql: ${TABLE}.bill_to_id ;;
  }

  dimension: buyer {
    type: number
    sql: ${TABLE}.buyer ;;
  }

  dimension: cancel_code {
    type: string
    sql: ${TABLE}.cancel_code ;;
  }

  dimension_group: cancel {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.cancel_date ;;
  }

  dimension: cancel_id {
    type: string
    sql: ${TABLE}.cancel_id ;;
  }

  dimension: clearing_zone_id {
    type: string
    sql: ${TABLE}.clearing_zone_id ;;
  }

  dimension_group: close {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.close_date ;;
  }

  dimension: comment_desc {
    type: string
    sql: ${TABLE}.comment_desc ;;
  }

  dimension: consignment_flag {
    type: string
    sql: ${TABLE}.consignment_flag ;;
  }

  dimension: contract_no {
    type: number
    sql: ${TABLE}.contract_no ;;
  }

  dimension: conversion_rate_retail {
    type: number
    sql: ${TABLE}.conversion_rate_retail ;;
  }

  dimension: cost_source {
    type: string
    sql: ${TABLE}.cost_source ;;
  }

  dimension: currency_code {
    type: string
    sql: ${TABLE}.currency_code ;;
  }

  dimension: cust_order {
    type: string
    sql: ${TABLE}.cust_order ;;
  }

  dimension: delivery_supplier {
    type: number
    sql: ${TABLE}.delivery_supplier ;;
  }

  dimension: dept {
    type: number
    sql: ${TABLE}.dept ;;
  }

  dimension: discharge_port {
    type: string
    sql: ${TABLE}.discharge_port ;;
  }

  dimension_group: earliest_ship {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.earliest_ship_date ;;
  }

  dimension: edi_po_ind {
    type: string
    sql: ${TABLE}.edi_po_ind ;;
  }

  dimension: edi_sent_ind {
    type: string
    sql: ${TABLE}.edi_sent_ind ;;
  }

  dimension_group: estimated_instock {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.estimated_instock_date ;;
  }

  dimension: exchange_rate {
    type: number
    sql: ${TABLE}.exchange_rate ;;
  }

  dimension: factory {
    type: string
    sql: ${TABLE}.factory ;;
  }

  dimension: fob_title_pass {
    type: string
    sql: ${TABLE}.fob_title_pass ;;
  }

  dimension: fob_title_pass_desc {
    type: string
    sql: ${TABLE}.fob_title_pass_desc ;;
  }

  dimension: fob_trans_res {
    type: string
    sql: ${TABLE}.fob_trans_res ;;
  }

  dimension: fob_trans_res_desc {
    type: string
    sql: ${TABLE}.fob_trans_res_desc ;;
  }

  dimension: freight_contract_no {
    type: string
    sql: ${TABLE}.freight_contract_no ;;
  }

  dimension: freight_terms {
    type: string
    sql: ${TABLE}.freight_terms ;;
  }

  dimension: from_currency_retail {
    type: string
    sql: ${TABLE}.from_currency_retail ;;
  }

  dimension: import_country_id {
    type: string
    sql: ${TABLE}.import_country_id ;;
  }

  dimension: import_id {
    type: number
    sql: ${TABLE}.import_id ;;
  }

  dimension: import_order_ind {
    type: string
    sql: ${TABLE}.import_order_ind ;;
  }

  dimension: import_type {
    type: string
    sql: ${TABLE}.import_type ;;
  }

  dimension: include_on_order_ind {
    type: string
    sql: ${TABLE}.include_on_order_ind ;;
  }

  dimension: item {
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: lading_port {
    type: string
    sql: ${TABLE}.lading_port ;;
  }

  dimension: last_grp_rounded_qty {
    type: number
    sql: ${TABLE}.last_grp_rounded_qty ;;
  }

  dimension: last_received {
    type: number
    sql: ${TABLE}.last_received ;;
  }

  dimension: last_rounded_qty {
    type: number
    sql: ${TABLE}.last_rounded_qty ;;
  }

  dimension: last_sent_rev_no {
    type: number
    sql: ${TABLE}.last_sent_rev_no ;;
  }

  dimension_group: latest_ship {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.latest_ship_date ;;
  }

  dimension: loc_type {
    type: string
    sql: ${TABLE}.loc_type ;;
  }

  dimension: location {
    type: number
    label: "Location Number"
    sql: ${TABLE}.location ;;
  }




  dimension: locs_currency {
    type: string
    sql: ${TABLE}.locs_currency ;;
  }

  dimension: non_scale_ind {
    type: string
    sql: ${TABLE}.non_scale_ind ;;
  }

  dimension_group: not_after {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.not_after_date ;;
  }

  dimension_group: not_before {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.not_before_date ;;
  }

  dimension: order_no {
    type: number
    primary_key: no
    sql: ${TABLE}.order_no ;;
  }

  dimension: pk {
    type: number
    primary_key: yes
    sql: CONCAT(${order_no}, " - ", ${item}) ;;
  }

  dimension: order_type {
    type: string
    sql: ${TABLE}.order_type ;;
  }

  dimension_group: orig_approval {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.orig_approval_date ;;
  }

  dimension: orig_approval_id {
    type: string
    sql: ${TABLE}.orig_approval_id ;;
  }

  dimension: orig_ind {
    type: number
    sql: ${TABLE}.orig_ind ;;
  }

  dimension: original_repl_qty {
    type: number
    sql: ${TABLE}.original_repl_qty ;;
  }

  dimension_group: otb_eow {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.otb_eow_date ;;
  }

  dimension: partner1 {
    type: string
    sql: ${TABLE}.partner1 ;;
  }

  dimension: partner2 {
    type: string
    sql: ${TABLE}.partner2 ;;
  }

  dimension: partner3 {
    type: string
    sql: ${TABLE}.partner3 ;;
  }

  dimension: partner_type_1 {
    type: string
    sql: ${TABLE}.partner_type_1 ;;
  }

  dimension: partner_type_2 {
    type: string
    sql: ${TABLE}.partner_type_2 ;;
  }

  dimension: partner_type_3 {
    type: string
    sql: ${TABLE}.partner_type_3 ;;
  }

  dimension: payment_method {
    type: string
    sql: ${TABLE}.payment_method ;;
  }

  dimension_group: pickup {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.pickup_date ;;
  }

  dimension: pickup_loc {
    type: string
    sql: ${TABLE}.pickup_loc ;;
  }

  dimension: pickup_no {
    type: string
    sql: ${TABLE}.pickup_no ;;
  }

  dimension: po_ack_recvd_ind {
    type: string
    sql: ${TABLE}.po_ack_recvd_ind ;;
  }

  dimension: po_type {
    type: string
    sql: ${TABLE}.po_type ;;
  }

  dimension: pre_mark_ind {
    type: string
    sql: ${TABLE}.pre_mark_ind ;;
  }

  dimension: promotion {
    type: number
    sql: ${TABLE}.promotion ;;
  }

  dimension: purchase_type {
    type: string
    sql: ${TABLE}.purchase_type ;;
  }

  dimension: qc_ind {
    type: string
    sql: ${TABLE}.qc_ind ;;
  }

  dimension: qty_cancelled {
    type: number
    sql: ${TABLE}.qty_cancelled ;;
  }

  dimension: qty_ordered {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: qty_prescaled {
    type: number
    sql: ${TABLE}.qty_prescaled ;;
  }

  dimension: qty_received {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_received ;;
  }

  dimension: revised_qty_received {
    type: number
    sql: CASE WHEN ${qty_received} > ${qty_ordered} THEN ${qty_ordered} ELSE ${qty_received} END ;;
  }

  dimension: reject_code {
    type: string
    sql: ${TABLE}.reject_code ;;
  }

  dimension: routing_loc_id {
    type: string
    sql: ${TABLE}.routing_loc_id ;;
  }

  dimension: ship_method {
    type: string
    sql: ${TABLE}.ship_method ;;
  }

  dimension: ship_pay_method {
    type: string
    sql: ${TABLE}.ship_pay_method ;;
  }

  dimension: split_ref_ordno {
    type: number
    sql: ${TABLE}.split_ref_ordno ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: supp_add_seq_no {
    type: number
    sql: ${TABLE}.supp_add_seq_no ;;
  }

  dimension: supplier {
    type: string
    sql: ${TABLE}.supplier ;;
  }

  dimension: terms {
    type: string
    sql: ${TABLE}.terms ;;
  }

  dimension: triangulation_ind {
    type: string
    sql: ${TABLE}.triangulation_ind ;;
  }

  dimension: tsf_po_link_no {
    type: number
    sql: ${TABLE}.tsf_po_link_no ;;
  }

  dimension: unit_cost {
    type: number
    sql: ${TABLE}.unit_cost ;;
  }

  dimension: unit_cost_init {
    type: number
    sql: ${TABLE}.unit_cost_init ;;
  }

  dimension: unit_cost_usd {
    type: number
    sql: ${TABLE}.unit_cost_usd ;;
  }
  # --- SPD-355 using unit cost for ordered value starts...
  dimension: unit_cost_in_local_curr {
    type: number
    sql: ${TABLE}.unit_cost_in_local_curr ;;
  }
  # --- SPD-355 using unit cost for ordered value ends

  dimension: unit_retail {
    type: number
    sql: ${TABLE}.unit_retail ;;
  }

  dimension: unit_retail_usd {
    type: number
    sql: ${TABLE}.unit_retail_usd ;;
  }

  dimension: vendor_order_no {
    type: string
    sql: ${TABLE}.vendor_order_no ;;
  }

  dimension_group: written {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.written_date ;;
  }


#   SPD-384 to add is closed now dimension starts
  dimension: is_closed {
    type: yesno
    label: "Is Closed Now?"
    view_label: "Retail Locations"
    sql: ${dim_distr_ret_loc.store_close_date} IS NOT NULL ;;
  }
 #   SPD-384 to add is closed now dimension ends

  measure: last_updated_date {
    type: date
    label: "Last refresh date"
    sql: MAX(${written_raw}) ;;
    convert_tz: no
  }

#correction made by LK SPD-292
  measure: qty_ordered_sum {
    label: "Quantity Ordered"
    type: sum
    sql: ${qty_ordered} ;;
  }


  measure: orig_qty_ordered_sum {
    label: "Original Quantity Ordered"
    type: sum
    sql: ${qty_prescaled} ;;
  }



  measure: cancelled_qty_sum {
    label: "Cancelled Quantity"
    type: sum
    sql: ${qty_cancelled} ;;
  }





#corection made by LK SPD-292
  measure: qty_received_sum {
    label: "Quantity Received"
    type: sum
    sql: ${qty_received} ;;
  }

  measure: supplier_fill_rate {
    label: "Quantity Fill Rate %"
    type: number
    value_format_name: "percent_2"
    sql: SAFE_DIVIDE(SUM(${revised_qty_received}),SUM(${qty_prescaled})) ;;
  }

#   measure: supplier_fill_rate_value{
#     label: "Value Fill Rate %"
#     type: number
#     value_format_name: "percent_2"
#     sql: SAFE_DIVIDE(SUM(${revised_qty_received}*${cost_facts.unit_cost_usd}),SUM(${qty_ordered}*${cost_facts.unit_cost_usd}));;
#   }


  measure: supplier_fill_rate_value{
    label: "Value Fill Rate %"
    type: number
    value_format_name: "percent_2"
    sql: SAFE_DIVIDE(SUM(${revised_qty_received}* ${unit_cost_usd}),SUM(${qty_prescaled}* ${unit_cost_usd}));;
  }

# --- SPD-355 using unit cost for ordered value starts...
#   measure: qty_ordered_value {
#     label: "Ordered Value USD"
#     type: sum
#     value_format_name: usd
#     sql: ${qty_ordered}*${cost_facts.unit_cost_usd} ;;
#   }

  measure: qty_ordered_value {
    label: "Ordered Value USD"
    type: sum
    value_format_name: usd
    sql: ${qty_ordered}*${unit_cost_usd}  ;;
  }


#   measure: qty_ordered_value_loc {
#     label: "Ordered Value Local"
#     type: sum
#     value_format_name: decimal_2
#     sql: ${qty_ordered}*${cost_facts.unit_cost} ;;
#   }
  measure: qty_ordered_value_loc {
    label: "Ordered Value Local"
    type: sum
    value_format_name: decimal_2
    sql: ${qty_ordered}*${unit_cost_in_local_curr} ;;
  }



  measure: orig_qty_ordered_value_usd {
    label: "Original Ordered Value USD"
    type: sum
    value_format_name: usd
    sql: ${qty_prescaled} * ${unit_cost_usd} ;;
  }

  measure: orig_qty_ordered_value_loc {
    label: "Original Ordered Value Local"
    type: sum
    value_format_name: decimal_2
    sql: ${qty_prescaled} * ${unit_cost_in_local_curr} ;;
  }



#   measure: qrt_received_value {
#     label: "Received Value USD"
#     type: sum
#     value_format_name: usd
#     sql: ${qty_received}*${cost_facts.unit_cost_usd} ;;
#   }

  measure: qrt_received_value {
    label: "Received Value USD"
    type: sum
    value_format_name: usd
    sql: ${qty_received}* ${unit_cost_usd} ;;
  }


#   measure: qrt_received_value_loc {
#     label: "Received Value Local"
#     type: sum
#     value_format_name: decimal_2
#     sql: ${qty_received}*${cost_facts.unit_cost} ;;
#   }

  measure: qrt_received_value_loc {
    label: "Received Value Local"
    type: sum
    value_format_name: decimal_2
    sql: ${qty_received}* ${unit_cost_in_local_curr} ;;
  }


   # --- SPD-355 using unit cost for ordered value ends..

  measure: distinct_orders {
    type: count_distinct
    label: "Distinct Purchase Orders"
    description: "Total number of distinct Purchase Orders placed"
    sql: ${order_no} ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }







}
