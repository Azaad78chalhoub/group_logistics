view: sell_through_sums {
  label: "Sales"
  derived_table: {
    sql:
  SELECT
  PARSE_DATE('%Y%m%d', CAST(bk_businessdate AS STRING)) AS date,
  bk_storeid,
  bk_productid AS item,
  SUM(mea_quantity) AS quantity_sold,
  SUM(CASE WHEN (discountamt_usd IS NULL OR discountamt_usd = 0) THEN mea_quantity ELSE 0 END) quantity_sold_fp,
  SUM(CASE WHEN (discountamt_usd > 0) THEN mea_quantity ELSE 0 END) quantity_sold_md
  FROM `chb-prod-supplychain-data.prod_supply_chain.factretailsales` sales
WHERE atr_trandate >= DATE({% parameter order_sums.first_order_written_date %}) AND atr_trandate <= DATE({% parameter sell_through_sums.last_transaction_date %})
  GROUP BY
  bk_businessdate,
  bk_storeid,
  item
  ;;
  }

  parameter: last_transaction_date {
    type: date
    default_value: "2020-03-30"
  }

  dimension: pk {
    type: string
    primary_key: yes
    sql: CONCAT(${business_date}, "-", ${item}, "-", ${bk_storeid}) ;;
  }

  dimension_group: business{
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: TIMESTAMP(${TABLE}.date) ;;
  }

  dimension: date {
    type: date
    primary_key: no
    hidden: yes
    sql: ${TABLE}.date ;;
  }


  dimension: item {
    type: string
    primary_key: no
    hidden: yes
    sql: ${TABLE}.item ;;
  }

  dimension: bk_storeid {
    type: string
    primary_key: no
    hidden: yes
    sql: ${TABLE}.bk_storeid ;;
  }

  dimension: quantity_sold {
    type: number
    primary_key: no
    hidden: yes
    sql: ${TABLE}.quantity_sold ;;
  }

  dimension: quantity_sold_fp {
    type: number
    primary_key: no
    hidden: yes
    sql: ${TABLE}.quantity_sold_fp ;;
  }

  dimension: quantity_sold_md {
    type: number
    primary_key: no
    hidden: yes
    sql: ${TABLE}.quantity_sold_md ;;
  }

  measure: total_quantity_sold {
    type: sum
    sql: ${quantity_sold} ;;
  }

  measure: total_quantity_sold_md {
    type: sum
    label: "Total Quantity Sold on MD"
    sql: ${quantity_sold_md} ;;
  }

  measure: total_quantity_sold_fp {
    type: sum
    label: "Total Quantity Sold on FP"
    sql: ${quantity_sold_fp} ;;
  }




}
