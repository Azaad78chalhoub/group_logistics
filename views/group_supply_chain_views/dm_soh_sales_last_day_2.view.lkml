view: dm_soh_sales_last_day_2 {
  label: "Sales and Stock Information"
  derived_table: {
    explore_source: fact_soh_sales_poc {
      column: stock_date {}
      column: org_num {}
      column: prod_num {}
      column: unit_cost_usd {}
      column: unit_cost {}
      column: inv_soh_qty {}
      column: sales_qty {}
      derived_column: maximum_stock_date {
        sql: LAST_VALUE(stock_date) OVER (ORDER BY stock_date  ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) ;;
      }
      derived_column: running_sales_qty {
        sql: CASE WHEN stock_date = LAST_VALUE(stock_date) OVER (ORDER BY stock_date  ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                  THEN
                  SUM(sales_qty) OVER (PARTITION BY org_num,prod_num ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
                  ELSE NULL
                  END;;
      }
      #BR-69fix unit cost usd interchanged
      derived_column: last_day_unit_cost_usd {
        sql: CASE WHEN stock_date = LAST_VALUE(stock_date) OVER (ORDER BY stock_date  ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                  THEN
                  LAST_VALUE(unit_cost_usd) OVER (PARTITION BY org_num,prod_num ORDER BY stock_date  ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                          ELSE NULL
                  END;;
      }
      derived_column: last_day_unit_cost{
        sql: CASE WHEN stock_date = LAST_VALUE(stock_date) OVER (ORDER BY stock_date  ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                  THEN
                  LAST_VALUE(unit_cost) OVER (PARTITION BY org_num,prod_num ORDER BY stock_date  ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                        ELSE NULL
                  END;;
      }
      #BR-69fix unit cost usd interchanged
      derived_column: last_day_inv_soh_qty {
        sql: CASE WHEN stock_date = LAST_VALUE(stock_date) OVER (ORDER BY stock_date  ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                  THEN
                  LAST_VALUE(inv_soh_qty) OVER (PARTITION BY org_num,prod_num ORDER BY stock_date ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                          ELSE NULL
                  END;;
      }
      bind_all_filters: yes
    }
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${stock_date}, " - ", ${org_num}, " - ", ${prod_num}) ;;
  }

  dimension: stock_date {
    type: date
    hidden: yes
  }

  dimension: maximum_stock_date {
    type: date
    hidden: yes
  }


  dimension: max_stock_date {
    hidden:yes
    type: date
    sql: ${maximum_stock_date} ;;
  }

  dimension: is_last_date {
    type: yesno
    hidden: yes
    sql: ${stock_date}=${max_stock_date} ;;
  }

  dimension: org_num {
    type: string
    hidden: yes
  }

  dimension: prod_num {
    type: string
    hidden: yes
  }

  dimension: last_day_unit_cost_usd {
    type: number
    hidden: yes
  }

  dimension: inv_soh_qty {
    type: number
    hidden: yes
  }

  dimension: running_sales_qty {
    type: number
    hidden: no
  }

  dimension: last_day_unit_cost {
    type: number
    hidden: yes
  }

  dimension: last_day_inv_soh_qty {
    type: number
    hidden: no
  }

  measure: total_quantity_sold {
    type: sum
    label: "Total Quantity Sold NEW"
    value_format_name: decimal_0
    hidden: yes
    sql: ${running_sales_qty};;
  }

  measure: sum_running_sales_qty {
    type: number
    label: "Sell Through Rate on Stock % New w. filter"
    value_format_name: percent_2
    sql: SAFE_DIVIDE(SUM(${running_sales_qty}),SUM(${running_sales_qty}+${last_day_inv_soh_qty}));;
  }



  measure: last_day_stock_quantity_new {
    type: sum
    hidden: no
    group_label: "Stock Metrics NEW"
    label: "Last Day Closing SOH Quantity New w. filter"
    value_format_name: decimal_0
    sql: ${last_day_inv_soh_qty};;
  }

  measure: last_day_stock_value_2 {
    type: sum
    hidden: no
    group_label: "Stock Metrics NEW"
    label: "Last Day Closing SOH Value New w. filter"
    value_format_name: decimal_0
    sql: ${last_day_inv_soh_qty}*${last_day_unit_cost};;
  }

  measure: last_day_stock_value_usd_2 {
    type: sum
    hidden: no
    group_label: "Stock Metrics NEW"
    label: "Last Day Closing SOH Value USD New w. filter"
    value_format_name: usd
    sql: ${last_day_inv_soh_qty}*${last_day_unit_cost_usd};;
  }

}
