view: fact_soh {
  sql_table_name: `chb-prod-stage-oracle.prod_supply_chain.fact_soh`
    ;;

  dimension_group: first_received {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.first_received ;;
  }

  dimension_group: first_sold {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.first_sold ;;
  }

  dimension: inv_soh_qty {
    type: number
    sql: ${TABLE}.inv_soh_qty ;;
  }

  dimension: org_num {
    type: string
    sql: ${TABLE}.org_num ;;
  }

  dimension: prod_num {
    type: string
    sql: ${TABLE}.prod_num ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension_group: stock {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.stock_date ;;
  }

  dimension: unit_cost {
    type: number
    sql: ${TABLE}.unit_cost ;;
  }

  dimension: unit_cost_usd {
    type: number
    sql: ${TABLE}.unit_cost_usd ;;
  }

  dimension: unit_retail_usd {
    type: number
    sql: ${TABLE}.unit_retail_usd ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
