view: new_provision_last_date {
  derived_table: {
    explore_source: dm_age_provision {
      column: last_date {}

    }
  }

  dimension: last_date {
    type: date
    hidden: yes
  }

}
