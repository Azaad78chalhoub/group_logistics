


view: dm_distr_sales {

  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.dm_distr_sales`

    ;;


  dimension: accepted_by {
    type: string
    hidden: yes
    sql: ${TABLE}.accepted_by ;;
  }

  dimension: from_currency {
    type:string
    hidden: no
    label: "Invoice Currency"
    sql: ${TABLE}.from_currency;;
  }

  dimension: to_currency {
    type:string
    hidden: yes
    sql: ${TABLE}.to_currency;;
  }

  dimension: conversion_rate {
    type:string
    hidden: yes
    sql: ${TABLE}.CONVERSION_RATE;;
  }

  dimension: conversion_type {
    type:string
    hidden: yes
    sql: "Corporate";;
  }

  dimension: accepted_quantity {
    type: string
    hidden: yes
    sql: ${TABLE}.accepted_quantity ;;
  }

  dimension_group: actual_arrival {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.actual_arrival_date AS TIMESTAMP) ;;
  }

  dimension_group: actual_fulfillment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.actual_fulfillment_date AS TIMESTAMP) ;;
  }

  dimension_group: actual_shipment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.actual_shipment_date AS TIMESTAMP) ;;
  }


  dimension: attribute1 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute1 ;;
  }

  dimension: attribute13 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute13 ;;
  }

  dimension: class_name {
    type: string
    view_label: "Product"
    label: "Class"
    sql: ${TABLE}.class_name ;;
  }

  dimension: subclass_name {
    type: string
    view_label: "Product"
    label: "Sub Class"
    sql: ${TABLE}.subclass_name ;;
  }

  dimension: brand {
    type: string
    view_label: "Product"
    sql: ${TABLE}.brand ;;
  }

  dimension: dept_name {
    type: string
    view_label: "Product"
    label: "Department"
    sql: ${TABLE}.dept_name ;;
  }

  dimension: dept_name_type {
    type: string
    view_label: "Product"
    label: "Department Type"
    sql: case when ${dept_name} = "PLV" and ${subclass_name} LIKE "%TESTER%" then "PLV - TESTERS"
    when ${dept_name} = "PLV" and ${subclass_name} NOT LIKE "%TESTER%" then "PLV - Others"
              when ${dept_name} = "GWP" then "GWP"
              when  CAST(${TABLE}.unit_selling_price_usd AS FLOAT64) = 0 and ( ${dept_name} !="PLV" or  ${dept_name} != "GWP")  then "FOC"
              else "Sellable Goods" end
              ;;
  }



  dimension: item {
    type: string
    view_label: "Product"
    label: "Item"
    sql: ${TABLE}.item ;;
  }



  dimension: item_description {
    type: string
    view_label: "Product"
    sql: ${TABLE}.item_desc ;;
  }

# SPD-205 Change RMS Location Name to Location Name and place it under location section

  dimension: loc_name {
    type: string
    label: "Location Name"
    view_label: "RMS Locations"
    sql: ${TABLE}.loc_name ;;
  }

  dimension: city {
    type: string
    label: "City"
    sql: ${TABLE}.city ;;
  }

  dimension: address {
    type: string
    label: "Warehouse Address"
    view_label: "RMS Locations"
    sql: ${TABLE}.add_1 ;;
  }


  dimension: state {
    type: string
    label: "State"
    view_label: "RMS Locations"
    sql: ${TABLE}.state ;;
  }


  dimension: country {
    type: string
    label: "Country"
    view_label: "RMS Locations"
    sql: ${TABLE}.country_desc ;;
  }

  dimension: region_name {
    type: string
    label: "Region"
    view_label: "RMS Locations"
    sql: ${TABLE}.region_name ;;
  }

  dimension: district {
    type: string
    view_label: "RMS Locations"
    label: "District"
    sql: ${TABLE}.district_name ;;
  }

  dimension: attribute2 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute2 ;;
  }

  dimension: attribute9 {
    type: string
    hidden: yes
    sql: ${TABLE}.attribute9 ;;
  }

  dimension: booked_flag {
    type: string
    hidden: yes
    sql: ${TABLE}.booked_flag ;;
  }

  dimension: cancelled_flag {
    type: string
    hidden: yes
    sql: ${TABLE}.cancelled_flag ;;
  }

  dimension: cancelled_quantity {
    type: number
    hidden: no
    sql: CAST(${TABLE}.cancelled_quantity AS FLOAT64) ;;
  }

  dimension: cancelled_quantity2 {
    type: string
    hidden: yes
    sql: ${TABLE}.cancelled_quantity2 ;;
  }

  dimension: charge_periodicity_code {
    type: string
    hidden: yes
    sql: ${TABLE}.charge_periodicity_code ;;
  }

  dimension: component_number {
    type: string
    hidden: yes
    sql: ${TABLE}.component_number ;;
  }


  dimension: invoice_number {
    type: string
    sql: ${TABLE}.invoice_number ;;
  }

# SPD-205 All ID Columns to be hidden from Explore
  dimension: created_by {
    type: string
    hidden: yes
    label: "Creator ID"
    sql: ${TABLE}.created_by ;;
  }

#   SPD-205 changes to change the creation date to Sales Order Date
dimension_group: creation {
    label: " Line Creation"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.creation_date AS TIMESTAMP) ;;
  }

  #  BR85 fix
  dimension_group: ordered {
    label: " Sales Order"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.ordered_date AS TIMESTAMP) ;;
  }



  measure: last_updated_date {
    type: date_time
    label: "Last refresh date"
    sql: MAX(${creation_raw}) ;;
    convert_tz: no
  }

  dimension_group: invoice {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.invoice_date AS TIMESTAMP)  ;;
  }

  dimension: cust_po_number {
    type: string
    label: "Customer PO Number"
    sql: ${TABLE}.cust_po_number ;;
  }

  dimension: vpn {
    type: string
    view_label: "Product"
    sql: ${TABLE}.vpn ;;
  }

# SPD-205 All ID Columns to be hidden from Explore
  dimension: deliver_to_org_id {
    type: string
    hidden:  yes
    sql: ${TABLE}.deliver_to_org_id ;;
  }

  dimension: flow_status_code {
    type: string
    sql: ${TABLE}.flow_status_code ;;
  }



  dimension: fob_point_code {
    type: string
    hidden: yes
    sql: ${TABLE}.fob_point_code ;;
  }

  dimension: freight_terms_code {
    type: string
    hidden: yes
    sql: ${TABLE}.freight_terms_code ;;
  }

  dimension: fulfilled_flag {
    type: string
    hidden: yes
    sql: ${TABLE}.fulfilled_flag ;;
  }

  dimension: fulfilled_quantity {
    type: string
    hidden: yes
    sql: ${TABLE}.fulfilled_quantity ;;
  }

  dimension: fulfilled_quantity2 {
    type: string
    hidden: yes
    sql: ${TABLE}.fulfilled_quantity2 ;;
  }

  dimension_group: fulfillment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.fulfillment_date AS TIMESTAMP) ;;
  }

# SPD-205 All ID Columns to be hidden from Explore
  dimension: header_id {
    type: string
    hidden:  yes
    sql: ${TABLE}.header_id ;;
  }

  dimension: inventory_item_id {
    type: string
    hidden: yes
    sql: ${TABLE}.inventory_item_id ;;
  }

  dimension: invoice_interface_status_code {
    type: string
    hidden: yes
    sql: ${TABLE}.invoice_interface_status_code ;;
  }

# SPD-205 All ID Columns to be hidden from Explore
  dimension: invoice_to_org_id {
    type: string
    hidden:  yes
    sql: ${TABLE}.invoice_to_org_id ;;
  }

  dimension: invoiced_quantity {
    type: string
    hidden: yes
    sql: CAST(${TABLE}.invoiced_quantity AS FLOAT64) ;;
  }

  dimension: item_type_code {
    type: string
    hidden: yes
    sql: ${TABLE}.item_type_code ;;
  }

  dimension_group: last_ack {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.last_ack_date AS TIMESTAMP) ;;
  }

# SPD-205 All Last Updated Columns to be hidden from Explore starts..
  dimension_group: last_update {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.last_update_date AS TIMESTAMP) ;;
  }

  dimension: last_update_login {
    type: string
    hidden: yes
    sql: ${TABLE}.last_update_login ;;
  }

  dimension: last_updated_by {
    type: string
    hidden: yes
    sql: ${TABLE}.last_updated_by ;;
  }
# SPD-205 All Last Updated Columns to be hidden from Explore ends

  dimension: line_category_code {
    type: string
    sql: ${TABLE}.line_category_code ;;
  }

# SPD-205 All ID Columns to be hidden from Explore

  dimension: line_id {
    type: string
    hidden: no
#     primary_key: yes
    sql: ${TABLE}.line_id ;;
  }

  dimension: id {
    type: string
    hidden: no
    primary_key: yes
    sql: concat(${line_id}, "-", case when  ${invoice_number} is null then "" else ${invoice_number} end );;
  }

  dimension: line_number {
    type: string
    sql: ${TABLE}.line_number ;;
  }

# SPD-205 All ID Columns to be hidden from Explore
  dimension: line_type_id {
    hidden: yes
    type: string
    sql: ${TABLE}.line_type_id ;;
  }

  dimension: open_flag {
    type: string
    sql: ${TABLE}.open_flag ;;
  }

  dimension: option_number {
    type: string
    hidden: yes
    sql: ${TABLE}.option_number ;;
  }

  dimension: order_category_code {
    type: string
    sql: ${TABLE}.order_category_code ;;
  }

  dimension: order_number {
    type: number
    sql: ${TABLE}.order_number ;;
  }

  dimension: order_quantity_uom {
    type: string
    hidden: yes
    sql: ${TABLE}.order_quantity_uom ;;
  }

  dimension: ordered_item {
    type: string
    sql: ${TABLE}.ordered_item ;;
  }

  dimension: ordered_quantity {
    type: string
    hidden:  yes
    sql: ${TABLE}.ordered_quantity ;;
  }

  dimension: ordered_quantity2 {
    type: string
    hidden:  yes
    sql: ${TABLE}.ordered_quantity2 ;;
  }

  dimension: ordered_quantity_uom2 {
    type: string
    hidden: yes
    sql: ${TABLE}.ordered_quantity_uom2 ;;
  }
# SPD-205 All ID Columns to be hidden from Explore
  dimension: org_id {
    type: string
    hidden: yes
    sql: ${TABLE}.org_id ;;
  }

  dimension: pricing_quantity {
    type: number
    hidden: yes
    sql: CAST(${TABLE}.pricing_quantity AS FLOAT64) ;;
  }

  dimension: tran_type {
    type: string
    label: "Transaction Type Name"
    sql: ${TABLE}.tran_type_description;;
  }
# SPD-205 Change RMS Location ID to Location Code and place it under RMS location section
  dimension: loc_code {
    type: string
    label: "Location Code"
    view_label: "RMS Locations"
    sql: ${TABLE}.loc_code;;
  }
# SPD-205 - Bill to SIte ID to be renamed to Bill to Site Number
  dimension: bill_to_site_id {
    label: "bill_to_site_id"
    hidden: yes
    type: string
    sql: ${TABLE}.bill_to_site_id;;


  }


#   SPD-217 Bill to SIte name and SHip to SIte Name

  dimension: bill_to_site_number {
    label: "Bill to Site Number"
    hidden: no
    type: string
    sql: ${TABLE}.bill_to_site_number;;
  }

  dimension: bill_to_site_name {
    label: "Bill to Site Name"
    hidden: no
    type: string
    sql: ${TABLE}.bill_to_site_name;;
  }

  dimension: ship_to_site_number {
    label: "Ship to Site Number"
    hidden: no
    type: string
    sql: ${TABLE}.ship_to_site_number;;
  }

  dimension: ship_to_site_name {
    label: "Ship to Site Name"
    hidden: no
    type: string
    sql: ${TABLE}.ship_to_site_name;;
  }


 #SPD-205 - Sold to Org Name  to be renamed to Customer Name
  dimension: sold_to_org_name {
    label: "Customer Name"
    type: string
    sql: ${TABLE}.sold_to_org_name;;
  }

# SPD-219 ${tran_type} LIKE "%DSO" changed to ${tran_type} LIKE "%DSO%"
  dimension: drop_shipment{
    type: yesno
    label: "Is Dropshipment Order?"
    sql: (${tran_type} LIKE "%DSO%" OR ${tran_type} LIKE  "%Drop%") AND ${tran_type} NOT LIKE  "%Drop of%" AND ${tran_type} NOT LIKE  "%Dropoff%"
    AND ${tran_type} NOT LIKE  "%Dropof%" AND ${tran_type} NOT  LIKE  "%Drop Of%"  AND ${tran_type} NOT  LIKE  "%DropOf%";;
  }

# SPD-218 Returns Indicator

  dimension: is_returns {
    type: yesno
    label: "Is Return Order?"
    sql: upper(${line_category_code})="RETURN";;
  }

  # SPD-218 Returns Indicator


  dimension: pricing_quantity_uom {
    type: string
    hidden: yes
    sql: ${TABLE}.pricing_quantity_uom ;;
  }

  dimension_group: promise {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.promise_date AS TIMESTAMP) ;;
  }

  dimension_group: request {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.request_date AS TIMESTAMP) ;;
  }

  dimension: return_reason_code {
    type: string
    sql: ${TABLE}.return_reason_code ;;
  }

  dimension_group: schedule_ship {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.schedule_ship_date AS TIMESTAMP) ;;
  }

# SPD-205 All ID Columns to be hidden from Explore starts.
  dimension: ship_from_org_id {
    hidden: yes
    type: string
    sql: ${TABLE}.ship_from_org_id ;;
  }

 dimension: ship_to_contact_id {
    type: string
    hidden: yes
    sql: ${TABLE}.ship_to_contact_id ;;
  }

  dimension: ship_to_org_id {
    type: string
    hidden: yes
    sql: ${TABLE}.ship_to_org_id ;;
  }

# SPD-205 All ID Columns to be hidden from Explore ends

  dimension: shipment_number {
    type: string
    sql: ${TABLE}.shipment_number ;;
  }

  dimension: shippable_flag {
    type: string
    sql: ${TABLE}.shippable_flag ;;
  }

  dimension: shipped_quantity {
    type: number
    hidden: yes
    sql: CAST(${TABLE}.shipped_quantity AS FLOAT64) ;;
  }

  dimension: shipped_quantity2 {
    type: string
    hidden: yes
    sql: ${TABLE}.shipped_quantity2 ;;
  }

  dimension: shipping_interfaced_flag {
    type: string
    sql: ${TABLE}.shipping_interfaced_flag ;;
  }

  dimension: shipping_quantity {
    type: string
    sql: ${TABLE}.shipping_quantity ;;
  }

  dimension: shipping_quantity2 {
    type: string
    hidden: yes
    sql: ${TABLE}.shipping_quantity2 ;;
  }

  dimension: shipping_quantity_uom {
    type: string
    hidden: yes
    sql: ${TABLE}.shipping_quantity_uom ;;
  }

  dimension: shipping_quantity_uom2 {
    type: string
    hidden: yes
    sql: ${TABLE}.shipping_quantity_uom2 ;;
  }

# SPD-205 All ID Columns to be hidden from Explore starts

  dimension: sold_from_org_id {
    type: string
    hidden: yes
    sql: ${TABLE}.sold_from_org_id ;;
  }

  dimension: sold_to_org_id {
    type: string
    hidden: yes
    sql: ${TABLE}.sold_to_org_id ;;
  }

# SPD-205 All ID Columns to be hidden from Explore ends

  dimension: source_type_code {
    type: string
    sql: ${TABLE}.source_type_code ;;
  }


  dimension: top_model_line_id {
    type: string
    hidden: yes
    sql: ${TABLE}.top_model_line_id ;;
  }

#   SPD-215 hide the dimension
  dimension: unit_cost {
    type: number
    hidden: yes
    sql: CAST(${TABLE}.unit_cost AS FLOAT64) ;;
  }


  dimension: unit_cost_usd {
    type: number
    hidden: no
    sql: CAST(${TABLE}.unit_cost_usd AS FLOAT64) ;;
  }


# SPD-222 hide unit list price
  dimension: unit_list_price {
    hidden: yes
    type: string
    sql: ${TABLE}.unit_list_price ;;
  }

  dimension: unit_list_price_usd {
    hidden: yes
    type: string
    sql: ${TABLE}.unit_list_price_usd ;;
  }

  dimension: unit_selling_price {
    type: string
    sql: CAST(${TABLE}.unit_selling_price AS FLOAT64) ;;
  }



  dimension: unit_selling_price_usd {
    type: string
    hidden: no
    sql: CAST(${TABLE}.unit_selling_price_usd AS FLOAT64) ;;
  }

#   SPD-369 changes to cancelled amount starts
  dimension: unit_selling_price_order {
    type: string
    hidden: yes
    sql: ${TABLE}.unit_selling_price_order  ;;
  }


  dimension: unit_selling_price_order_usd {
    type: string
    hidden: yes
    sql: ${TABLE}.unit_selling_price_usd_order  ;;
  }

  dimension: store_delivery_date {
    type: date
    sql: ${TABLE}.store_delivery_date ;;
  }

  dimension: delivered_on_time {
    type: number
    sql: ${TABLE}.delivered_on_time ;;
  }

  measure: number_of_orders {
    type: count_distinct
    label: "Number of non-returned Invoices"
    filters: [is_returns: "No"]
    sql: ${invoice_number} ;;
  }

  measure: number_of_invoices {
    type: count_distinct
    label: "Number of Invoices"
    sql: ${invoice_number} ;;
  }

  measure: count_of_orders_on_time {
    #group_label: "Not delivered on Time"
    description: "Count of total orders with delivery on time."
    type: count_distinct
    sql: ${invoice_number} ;;
    hidden: yes
    filters: [ delivered_on_time: "1"]
  }

  measure: count_of_orders_missed {
    #group_label: "Not delivered on Time"
    description: "Count of total orders with delivery missed."
    type: count_distinct
    sql: ${invoice_number} ;;
    hidden: yes
    filters: [ delivered_on_time: "0"]
  }

  measure: share_of_orders_on_time {
    type: number
    value_format: "#.0%;(#.0%)"
    hidden: yes
    sql: SAFE_DIVIDE(${count_of_orders_on_time}, ${number_of_orders}) ;;
  }


  measure: delivery_time {
    type: average
    label: "Delivery Time"
    hidden: yes
    sql: DATE_DIFF(DATE(${store_delivery_date}), DATE(${creation_date}), DAY) ;;
  }


  #   SPD-369 changes to cancelled amount ends



  measure: revenue_net_local {
    type: sum
    label: "Net Invoiced Revenue Local"
    description: "Net Invoiced Revenue in Local Currency"
    value_format_name: decimal_2
#     filters: {
#       field: flow_status_code
#       value: "CLOSED"
#     }
    sql: ${unit_selling_price}*${invoiced_quantity} ;;
  }


#   SPD-330 replacing unit cost with unit selling price starts..

  measure: shipped_revenue_net_local {
    type: sum
    hidden:  yes
    value_format_name: decimal_2
#     sql: ${unit_cost}*${shipped_quantity} ;;
    sql: ${unit_selling_price}*${shipped_quantity} ;;
  }

  #   SPD-330 replacing unit cost with unit selling price ends

  measure: invoiced_revenue_net_local {
    type: sum
    hidden:  yes
    value_format_name: decimal_2
    sql: ${unit_selling_price}*${invoiced_quantity} ;;
  }

#   SPD-330 replacing unit cost with unit selling price starts..
#   SPD-369 changes to cancelled amount starts
  measure: cancelled_revenue_net_local {
    label: "Cancelled Amount Local"
    description: "Cancelled Amount in Local Currency"
    type: sum
    hidden:  yes
    value_format_name: decimal_2

#     sql: ${unit_cost}*${cancelled_quantity} ;;
    sql: ${unit_selling_price_order}*${cancelled_quantity} ;;

  }
  #   SPD-369 changes to cancelled amount ends
    #   SPD-330 replacing unit cost with unit selling price ends


dimension: WAC_USD_from_cost_facts {
  type: number
  hidden: yes
  sql: ${cost_facts.unit_cost_usd} ;;
}
  measure: revenue_net_usd {
    type: sum
     label: "Net Invoiced Revenue USD"
    description: "Net Invoiced Revenue in USD"
    value_format_name: usd
#     filters: {
#       field: flow_status_code
#       value: "CLOSED"
#     }
    sql: ${unit_selling_price_usd}*${invoiced_quantity} ;;
  }

  measure: revenue_before_disc_usd {
    type: sum
    group_label: "Discounts"
    label: "Revenue Before Discount USD"
    description: "Revenue Before Discount in USD"
    value_format_name: usd
    sql: ${unit_list_price_usd}*${invoiced_quantity} ;;
  }

  measure: discount_usd {
    type: number
    group_label: "Discounts"
    label: "Discount USD"
    description: "Revenue Before Discount USD minus Net Invoiced USD"
    value_format_name: usd
    sql:  ${revenue_before_disc_usd} - ${revenue_net_usd} ;;
  }

  measure: discount_percentage_usd {
    type: number
    group_label: "Discounts"
    label: "Discount %"
    description: "Discount %"
    value_format_name: percent_2
    sql: safe_divide(${discount_usd},${revenue_before_disc_usd}) ;;
  }

  measure: cost_usd {
    type: sum
    group_label: "Margin"
    label: "Cost USD"
    description: "Cost in USD"
    value_format_name: usd
    sql: ${unit_cost_usd}*${invoiced_quantity} ;;
  }

  measure: Margin_usd {
    type: number
    group_label: "Margin"
    label: "Margin USD"
    description: "Net Revenue in USD minus Cost USD"
    value_format_name: usd
    sql: ${revenue_net_usd} - ${cost_usd} ;;
  }

  measure: margin_percentage {
    type: number
    group_label: "Margin"
    label: "Margin %"
    description: "Margin %"
    value_format_name: percent_2
    sql: safe_divide(${Margin_usd},${revenue_net_usd}) ;;
  }

  dimension: is_foc_item {
    type: yesno
    label: "Is FOC Item?"
    description: "Items with zero selling price"
    sql: CAST(${TABLE}.unit_selling_price_usd AS FLOAT64) = 0;;
  }

  dimension: returned_quantity {
    type: string
    hidden: yes
    sql: CASE WHEN upper(${line_category_code}) = "RETURN" then CAST(${TABLE}.ordered_quantity AS INT64) else 0 end ;;
  }

  dimension: invoiced_quantity_not_returned {
    type: string
    hidden: yes
    sql: CASE WHEN upper(${line_category_code}) != "RETURN" then CAST(${TABLE}.invoiced_quantity AS INT64) else 0 end ;;
  }

  measure: return_quantity_sum {
    type: sum
    hidden:  no
    group_label: "Returns"
    label: "Returned Quantity"
    description: "Returned Quantity"
    sql: ${returned_quantity};;
  }

  measure: return_value_usd {
    type: sum
    group_label: "Returns"
    label: "Returned Value USD"
    description: "Returns in USD"
    value_format_name: usd
    sql: ${unit_selling_price_usd}*${returned_quantity} ;;
  }

  measure: invoiced_quantity_not_return_sum {
    type: sum
    hidden:  no
    group_label: "Returns"
    label: "Total Invoiced Quantity"
    description: "Invoiced Quantity Before Returns"
    sql: ${invoiced_quantity_not_returned};;
  }

  measure: PLV_TESTERS_invoiced_quantity_not_return_sum {
    type: sum
    hidden:  no
    group_label: "Returns"
    label: "PLV_TESTER Total Invoiced Quantity"
    description: "Invoiced Quantity Before Returns"
    sql: ${invoiced_quantity_not_returned};;
    filters: [dept_name_type: "PLV - TESTERS"]
  }

  measure: GWP_invoiced_quantity_not_return_sum {
    type: sum
    hidden:  no
    group_label: "Returns"
    label: "GWP Total Invoiced Quantity"
    description: "Invoiced Quantity Before Returns"
    sql: ${invoiced_quantity_not_returned};;
    filters: [dept_name_type: "GWP"]
  }


  measure: FOC_invoiced_quantity_not_return_sum {
    type: sum
    hidden:  no
    group_label: "Returns"
    label: "FOC Total Invoiced Quantity"
    description: "Invoiced Quantity Before Returns"
    sql: ${invoiced_quantity_not_returned};;
    filters: [dept_name_type: "FOC"]
  }

  measure: Sellable_Goods_invoiced_quantity_not_return_sum {
    type: sum
    hidden:  no
    group_label: "Returns"
    label: "Sellable Goods Total Invoiced Quantity"
    description: "Invoiced Quantity Before Returns"
    sql: ${invoiced_quantity_not_returned};;
    filters: [dept_name_type: "Sellable Goods"]
  }


  measure: PLV_Others_invoiced_quantity_not_return_sum {
    type: sum
    hidden:  no
    group_label: "Returns"
    label: "PLV Others Total Invoiced Quantity"
    description: "Invoiced Quantity Before Returns"
    sql: ${invoiced_quantity_not_returned};;
    filters: [dept_name_type: "PLV - Others"]
  }
  measure: PLV_Testers_return_quantity_sum {
    type: sum
    hidden:  no
    group_label: "Returns"
    label: "PLV - Testers Returned Quantity"
    description: "Returned Quantity"
    sql: ${returned_quantity};;
    filters: [dept_name_type: "PLV - TESTERS"]
  }

  measure: GWP_return_quantity_sum {
    type: sum
    hidden:  no
    group_label: "Returns"
    label: "GWP Returned Quantity"
    description: "Returned Quantity"
    sql: ${returned_quantity};;
    filters: [dept_name_type: "GWP"]
  }

  measure: PLV_Others_return_quantity_sum {
    type: sum
    hidden:  no
    group_label: "Returns"
    label: "PLV Others Returned Quantity"
    description: "Returned Quantity"
    sql: ${returned_quantity};;
    filters: [dept_name_type: "PLV - Others"]
  }

  measure: Sellable_Goods_return_quantity_sum {
    type: sum
    hidden:  no
    group_label: "Returns"
    label: "Sellable Goods Returned Quantity"
    description: "Returned Quantity"
    sql: ${returned_quantity};;
    filters: [dept_name_type: "Sellable Goods"]
  }


  measure: FOC_Testers_return_quantity_sum {
    type: sum
    hidden:  no
    group_label: "Returns"
    label: "FOC Returned Quantity"
    description: "Returned Quantity"
    sql: ${returned_quantity};;
    filters: [dept_name_type: "FOC"]
  }

  measure: revenue_not_returned_usd {
    type: sum
    group_label: "Returns"
    label: "Total Invoiced Revenue in USD"
    description: "Revenue Before Returns"
    value_format_name: usd
    sql: ${unit_selling_price_usd}*${invoiced_quantity_not_returned} ;;
  }

  measure: revenue_before_returns_usd {
    type: number
    group_label: "Returns"
    label: "Revenue Before Returns USD"
    description: "Revenue before returns in USD"
    value_format_name: usd
    sql: ${return_value_usd} + ${revenue_not_returned_usd} ;;
  }

  measure: return_rate {
    type: number
    group_label: "Returns"
    label: "Returns %"
    description: "Returns % on value"
    value_format_name: percent_2
    sql: safe_divide(${return_value_usd},${revenue_before_returns_usd}) ;;
  }

  measure: invoiced_quantity_sum {
    type: sum
    hidden:  no
    label: "Net Invoiced Quantity"
    description: "Net Invoiced Quantity"
    sql: ${invoiced_quantity} ;;
  }


 measure: invoiced_quantity_sum_plv {
    type: sum
    hidden:  no
    label: "Net Invoiced Quantity for PLV items"
    description: "Net Invoiced Quantity-PLV"
    sql: case when ${dept_name}="PLV" then ${invoiced_quantity} else 0 end ;;
  }

  measure: ordered_quantity_sum {
    type: sum
    hidden:  yes
    label: "Ordered Quantity"
    description: "Quantity Ordered"
    sql: ${ordered_quantity} ;;
  }


  measure: pricing_quantity_sum {
    type: sum
    hidden:  yes
    label: "Pricing Quantity"
    sql: ${pricing_quantity} ;;
  }
  measure: shipped_revenue_net_usd {
    type: sum
    hidden:  yes
    value_format_name: usd

    sql: ${unit_selling_price_usd}*${shipped_quantity} ;;
  }

  measure: invoiced_revenue_net_usd {
    type: sum
    hidden:  yes
    value_format_name: usd
    sql: ${unit_selling_price_usd}*${invoiced_quantity} ;;
  }

#   SPD-369 changes to cancelled amount starts
  measure: cancelled_revenue_net_usd {
    label: "Cancelled Amount USD"
    description: "Cancelled Amount in USD"
    type: sum
    hidden:  yes
    value_format_name: usd
#     SPD- 255 filter not required
#     filters: {
#       field: flow_status_code
#       value: "INVOICED"
#     }
    # sql: ${unit_selling_price_order_usd}*${cancelled_quantity};;
    sql: ${unit_selling_price_order_usd}*${cancelled_quantity};;
  }
  #   SPD-369 changes to cancelled amount ends

# SPD-205 Distinct Sales Order Count
  measure: order_count {
    label: "Sales Order Count"
    description: "Count of distinct Sales Orders"
    type: count_distinct
    hidden:  no
    value_format_name: decimal_0
    sql: ${order_number} ;;
  }




  measure: count {
    type: count
    hidden:yes
    drill_fields: []
  }



  # SPD -55 Start

  measure: invoiced_quantity_m{
    type: sum
    label: "Invoiced Quantity-Not Returned"
    description: "Invoiced Quantity Excluding Returned Units"

    hidden: yes
    sql: CASE WHEN upper(${line_category_code}) != "RETURN" then CAST(${TABLE}.invoiced_quantity AS INT64) else 0 end;;
  }

  measure: ordered_quantity_m {
    type: sum
    label: "Ordered Quantity Not Returned "
    description: "Ordered Quantity Excluding Returned Units"

    hidden: yes
    sql: CASE WHEN upper(${line_category_code}) != "RETURN" then CAST(${TABLE}.ordered_quantity AS INT64) else 0 end;;
  }

  measure: allocated_quantity {
    type: sum
    hidden: yes
    filters: {
      field: flow_status_code
      value: "ALLOCATED"
    }
    sql: CAST(${ordered_quantity} AS INT64);;
  }

  measure: picked_quantity {
    type: sum
    hidden: yes
    filters: {
      field: flow_status_code
      value: "PICKED"
    }
    sql: CAST(${ordered_quantity} AS INT64);;
  }

  measure: awaiting_shipping_quantity {
    type: sum
    hidden: yes
    filters: {
      field: flow_status_code
      value: "AWAITING_SHIPPING"
    }
    sql: CAST(${ordered_quantity} AS INT64);;
  }
  measure: in_process_quantity {
    type: sum
    description: "Quantity Allocated, Picked or Awaiting Shipping"

    hidden: yes
    sql:case when upper(${flow_status_code}) = "ALLOCATED" or upper(${flow_status_code}) = "PICKED" or upper(${flow_status_code}) = "AWAITING_SHIPPING" then CAST(${TABLE}.ordered_quantity AS INT64) else 0 end ;;
  }

  measure: cancelled_quantity_m {
    label: "Cancelled Quantity"
    description: "Cancelled Quantity"

    type: sum
    hidden: yes
    sql: CAST(${TABLE}.cancelled_quantity AS FLOAT64) ;;
  }

  measure: fill_rate {
    label: "Fill Rate by Quantity"
    description: "Fill Rate by Quantity indicates the percentage of units that are fulfilled. Calculated as the sum of invoiced and in process units divided by the ordered units"
    type: number
    hidden: yes
    sql:SAFE_DIVIDE((${invoiced_quantity_m}+${in_process_quantity}),(${ordered_quantity_m})) ;;
    value_format_name: percent_2
  }

#   SPD-55 End

# SPD - 293 & 294 Start
  # measure: shippped_quantity{
  #   label: "Shipped Quantity"
  #   description: "Quantity Shipped"
  #   type: sum
  #   sql:case when upper(${flow_status_code}) = "SHIPPED" or upper(${flow_status_code}) = "SHIPPPED" then CAST(${TABLE}.ordered_quantity AS INT64) else 0 end  ;;
  # }

  measure: shipped_quantity_sum{
    label: "Shipped Quantity"
    description: "Quantity Shipped"
    type: sum
    hidden: yes
    sql_distinct_key: ${id} ;;
    sql: ${shipped_quantity} ;;
  }

  # measure: shippped_quantity_new{
  #   label: "Shipped Quantity_new"
  #   description: "Quantity Shipped"
  #   type: number
  #   # sql_distinct_key: ${id} ;;
  #   sql: SUM(${shipped_quantity}) OVER (partition by ${order_number} , ${invoice_number} ) ;;
  # }




  measure: fulfilled_quantities{
    label: "Fullfilled Quantity"
    description: "Quantity Fulfilled"

    type: sum
    hidden: yes
    sql: case when upper(${flow_status_code}) = "FULFILLED" then CAST(${TABLE}.ordered_quantity AS INT64) else 0 end;;
  }
# SPD - 293 & 294 End

# SPD - 298 Start

  dimension: inProcessQuantity {
    type: number
    hidden: yes
    sql:case when upper(${flow_status_code}) = "ALLOCATED" or upper(${flow_status_code}) = "PICKED" or upper(${flow_status_code}) = "AWAITING_SHIPPING" then CAST(${TABLE}.ordered_quantity AS INT64) else 0 end ;;
  }


#   SPD-330 replacing unit cost with unit selling price starts..
  measure: in_process_revenue_net_usd{
    type: sum
    value_format_name: usd
    hidden: yes
    description: "Net Revenue in Process USD"



    sql: ${unit_selling_price_usd}*${inProcessQuantity};;
  }
  #   SPD-330 replacing unit cost with unit selling price ends

  dimension: invoiced_quantity_noreturn {
    type: number
    hidden: yes
    sql:CASE WHEN upper(${line_category_code}) != "RETURN" then CAST(${TABLE}.invoiced_quantity AS INT64) else 0 end;;
  }

#   SPD-330 replacing unit cost with unit selling price starts..

  measure: invoice_revenue_net_usd {
    type: sum
    label: "Invoiced Revenue - Not Returned"
    description: "Exludes the units returned"
    value_format_name: usd
    hidden: yes

    sql: ${unit_selling_price_usd}*${invoiced_quantity_noreturn} ;;
  }


  dimension: ordered_revenue_noreturn {
    type: number
    hidden: yes
    description: "Ordered Quantity -Exludes the units returned"
    sql: CASE WHEN upper(${line_category_code}) != "RETURN" then CAST(${TABLE}.ordered_quantity AS INT64) else 0 end;;
  }


#   SPD-330 replacing unit cost with unit selling price starts..

# https://chalhoub-data.atlassian.net/browse/BR-91
  measure: ordered_revenue_net_usd {
    type: sum
    value_format_name: usd
    hidden: yes
    description: "Ordered Revenue -Exludes the units returned"

    sql: coalesce(${unit_selling_price_order_usd}*${ordered_revenue_noreturn},0) ;;
  }



  measure: fill_rate_by_value {
    label: "Fill Rate by Value"
    type: number
    description: "Fill Rate by Value indicates the percentage of revenue that is fulfilled. Calculated as the sum of invoiced and in process revenue divided by the ordered revenue"
    value_format_name: percent_2
    hidden: yes
    sql: SAFE_DIVIDE((${invoice_revenue_net_usd}+${in_process_revenue_net_usd}),${ordered_revenue_net_usd});;
  }
  #   SPD - 298 End
}
