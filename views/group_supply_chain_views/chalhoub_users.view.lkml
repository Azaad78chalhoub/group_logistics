view: chalhoub_users {
  sql_table_name: prod_shared_dimensions.Users_Profiles_Test ;;

  dimension: brand_list {
    type: string
    sql: ${TABLE}.Brand_List ;;
  }

  dimension: country_list {
    type: string
    sql: ${TABLE}.Country_List ;;
  }

  dimension: user_email {
    type: string
    sql: ${TABLE}.User_Email ;;
  }

  dimension: user_first_name {
    type: string
    sql: ${TABLE}.User_FirstName ;;
  }

  dimension: user_last_name {
    type: string
    sql: ${TABLE}.User_LastName ;;
  }

  measure: count {
    type: count
    drill_fields: [user_last_name, user_first_name]
  }
}
