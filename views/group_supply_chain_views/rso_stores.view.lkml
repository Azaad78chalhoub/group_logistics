view: rso_stores {
  label: "RSO Stores"
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.rso_stores`
    ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: active {
    type: string
    sql: ${TABLE}.active ;;
  }

  dimension: bucode {
    type: string
    sql: ${TABLE}.bucode ;;
  }

  dimension: countryid {
    type: number
    value_format_name: id
    sql: ${TABLE}.countryid ;;
  }

  dimension: rmscode {
    type: string
    sql: ${TABLE}.rmscode ;;
  }

  dimension: subvertical {
    type: string
    sql: ${TABLE}.subvertical ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.vertical ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
