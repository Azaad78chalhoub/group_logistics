# If necessary, uncomment the line below to include explore_source.
# include: "group_supply_chain.model.lkml"

view: sell_through_report {
  derived_table: {
    explore_source: order_sums {
      column: total_quantity_ordered {}
      column: total_quantity_received {}
      column: total_quantity_sold {}
      column: order_date {}
      column: location {}
      column: report_code { field: dim_item_loc_traits.report_code }
      filters: {
        field: order_sums.first_order_written_date
        value: "2019/01/01"
      }
      filters: {
        field: order_sums.last_transaction_date
        value: "2020/05/01"
      }
    }
  }
#
#   dimension: pk {
#     primary_key: yes
#     hidden: yes
#     sql:  CONCAT(${order_date}," - ", ${report_code}) ;;
#   }


  dimension: total_quantity_ordered {
    label: "Order Purchase Total Quantity Ordered"
    type: number
  }

  dimension: location {
    type: string
    sql: CAST(${TABLE}.location AS STRING) ;;
  }

  dimension: total_quantity_received {
    label: "Order Purchase Total Quantity Received"
    type: number
  }
  dimension: total_quantity_sold {
    label: "Order Purchase Total Quantity Sold"
    type: number
  }
  dimension: order_date {
    label: "Business Date"
    type: date
    primary_key: yes
  }
  dimension: report_code {
    label: "Zone"
    description: "Report Code value"
  }

  measure: running_total_qty_sold {
    type:  running_total
    value_format_name: decimal_0
    label: "Running Total Quantity Sold"
    sql: SUM(${total_quantity_sold});;
  }

}
