# If necessary, uncomment the line below to include explore_source.

# include: "group_supply_chain.model.lkml"

view: po_delivered_status {
  derived_table: {
    explore_source: dm_supply_orders_detailed {
      column: order_no {}
      column: qty_received_sum {}
      bind_all_filters: yes
    }

  }

  dimension: order_no {
    label: "Orders Order No"
    hidden: yes
    value_format: "0"
    primary_key: yes
    type: number
  }
  dimension: qty_received_sum {
    label: "Orders Quantity Received"
    hidden: yes
    type: number
  }
  dimension: po_delivered_status {
    description: "calculated at the overall order level"
    label: "PO Delivery Status"
    view_label: "PO Delivery Status"
    type: string
    sql: case when ${qty_received_sum} > 0 then 'Delivered' else 'Not Delivered' end  ;;
  }



}
