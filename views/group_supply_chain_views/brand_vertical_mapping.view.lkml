view: brand_vertical_mapping {
  derived_table: {
    sql_trigger_value: SELECT CURDATE() ;;
    sql:
    SELECT
      DISTINCT brand,
      vertical
    FROM
      `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy`
    UNION ALL
    SELECT
      "MUSE"AS brand,
      "MUSE"AS vertical
    UNION ALL
    SELECT
      "WEAR THAT"AS brand,
      "WEAR THAT"AS vertical
    UNION ALL
    SELECT
      "ROGER AND GALLET"AS brand,
      "ROGER AND GALLET"AS vertical
    UNION ALL
    SELECT
      "B8TA"AS brand,
      "B8TA"AS vertical
    UNION ALL
    SELECT
      "Elemis"AS brand,
      "Elemis"AS vertical;;
  }

  dimension: pk {
    primary_key: yes
    type: string
    hidden: yes
    sql: CONCAT(${bu_brand}, " - ", ${vertical}) ;;
  }

  dimension: bu_brand {
    type: string
    label: "Business Unit"
    description: "Brand name from alternative hierarchy"
    group_label: "BU Alternative Hierarchy"
    sql: ${TABLE}.brand ;;
  }


  dimension: vertical {
    type: string
    label: "Vertical"
    group_label: "BU Alternative Hierarchy"
    sql: ${TABLE}.vertical ;;
  }

}
