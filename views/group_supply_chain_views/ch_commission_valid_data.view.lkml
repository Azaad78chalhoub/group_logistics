view: ch_commission_valid_data {
  label: "Comission"
  derived_table: {
    sql_trigger_value: SELECT CURRENT_DATE ;;
    sql:
       select distinct store,
      commission,
      country,
      dept,
      class,
      subclass from (SELECT
      store,
      commission,
      country,
      dept,
      class,
      subclass,
      effective_date,
      supplier_cost_flag,

      rank () OVER (PARTITION BY country, dept, class, subclass,store  ORDER BY effective_date desc ,commission desc) rank
    FROM
      `chb-prod-supplychain-data.prod_shared_dimensions.dim_ch_supp_loc_commission` where supplier_cost_flag = "NO"  and commission <> 0 ) where rank = 1  ;;
  }

dimension: pk {
  primary_key: yes
  sql: concat(coalesce(${store},0),coalesce(${country},'0'),coalesce(${dept},0),coalesce(${class},0),coalesce(${subclass},0)) ;;
  }

  dimension: store {
    type: number
    hidden: yes
  }

  dimension: commission {
    type: number
    hidden: yes
  }

  dimension: country {
    type: string
    hidden: yes
  }

  dimension: dept {
    type: number
    hidden: yes
  }

  dimension: class {
    type: number
    hidden: yes
  }

  dimension: subclass {
    type: number
    hidden: yes
  }

  dimension: supplier_cost_flag {
    type: string
    hidden: yes
  }


  dimension: effective_date {
    type: date
    hidden: yes
  }

  dimension: rank {
    type: number
    hidden: yes
  }

}
