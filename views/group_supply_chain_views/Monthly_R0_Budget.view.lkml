# If necessary, uncomment the line below to include explore_source.

# include: "group_supply_chain.model.lkml"
# If necessary, uncomment the line below to include explore_source.

# include: "group_supply_chain.model.lkml"

view: Monthly_R0_Budget {
  derived_table: {
    explore_source: finance_budgets {
      column: store_code {}
      column: bu_code {}
      column: month {}
      column: sales_target_aed {}
      column: sales_target_usd {}
      column: cogs_target_aed {}
      column: cogs_target_usd {}
      column: gm_target_aed {}
      column: gm_target_usd {}
    }
  }
  dimension: pk {
    sql: concat(${bu_code},${store_code},${month}) ;;
    primary_key: yes
    hidden: yes
  }
  dimension: store_code {
    label: "finance budgets Store Code"
    hidden: yes
  }
  dimension: bu_code {
    label: "finance budgets Bu Code"
    hidden: yes
  }
  dimension: month {
    label: "finance budgets Month"
    type: number
    hidden: yes
  }
  dimension: sales_target_aed {
    label: "R0 Sales Budget AED"
    value_format: "#,##0.00"
    type: number
    hidden: yes
  }
  dimension: sales_target_usd {
    label: "R0 Sales Budget USD"
    value_format: "$#,##0.00"
    type: number
    hidden: yes
  }
  measure: saless_target_aed {
    label: "R0 Sales Budget AED"
    value_format: "#,##0.00"
    type: sum
    sql: ${sales_target_aed} ;;
  }
  measure: saless_target_usd {
    label: "R0 Sales Budget USD"
    value_format: "$#,##0.00"
    type: sum
    sql: ${sales_target_usd} ;;
  }
  dimension: cogs_target_aed {
    label: "R0 COGS Budget AED"
    value_format: "#,##0.00"
    type: number
    hidden: yes
  }
  dimension: cogs_target_usd {
    label: "R0 COGS Budget USD"
    value_format: "$#,##0.00"
    type: number
    hidden: yes
  }
  dimension: gm_target_aed {
    label: "R0 Gross Margin Budget AED"
    value_format: "#,##0.00"
    type: number
    hidden: yes
  }
  dimension: gm_target_usd {
    label: "R0 Gross Margin Budget USD"
    value_format: "$#,##0.00"
    type: number
    hidden: yes
  }
  measure: gmm_target_aed {
    label: "R0 Gross Margin Budget AED"
    value_format: "#,##0.00"
    type: sum
    sql: ${gm_target_aed} ;;
  }
  measure: gmm_target_usd {
    label: "R0 Gross Margin Budget USD"
    value_format: "$#,##0.00"
    type: sum
    sql: ${gm_target_usd} ;;
  }
}
