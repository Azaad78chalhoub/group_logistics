view: sell_through_prototype {
  view_label: "Sell Through Base View"
  # sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.dm_sell_through`
    # ;;

  derived_table: {
    sql:
      WITH base as (
      SELECT
        stock_date,
        org_num,
        prod_num,
        qty_ordered,
        qty_received,
        sales_qty,
        inv_soh_qty,
        ARRAY_REVERSE(ARRAY_AGG(sales_qty) OVER(PARTITION BY
                                                                        org_num,
                                                                        prod_num
                                                      ORDER BY stock_date
                                                    ))[SAFE_OFFSET(
              {% parameter number_of_periods_back %} *
                                {% if comparison_period._parameter_value == "DAY"%}
                                    1
                                {% elsif comparison_period._parameter_value == "WEEK"%}
                                    7
                                {% elsif comparison_period._parameter_value == "MONTH"%}
                                    30
                                {% elsif comparison_period._parameter_value == "QUARTER"%}
                                    92
                                {% elsif comparison_period._parameter_value == "YEAR"%}
                                    365
                                {% endif %}

                )] as sales_qty_previous_period,

      FROM
        `chb-prod-supplychain-data.prod_supply_chain.dm_sell_through`
      WHERE {% condition prototype_date_1 %} TIMESTAMP(stock_date) {% endcondition %}
          OR
          {% condition prototype_date_1 %} TIMESTAMP(DATE_ADD(stock_date, INTERVAL
                                                    {% parameter number_of_periods_back %}
                                                    {% parameter comparison_period %})) {% endcondition %}


      )



      SELECT
              *

      FROM base

      WHERE {% condition prototype_date_1 %} TIMESTAMP(base.stock_date) {% endcondition %}
      ;;
  }


  # distinct_prod_orgs as (
  #     SELECT org_num, prod_num, stock_date
  #     FROM (SELECT distinct org_num, prod_num
  #     FROM base)
  #     CROSS JOIN (SELECT distinct stock_date FROM base)
  #     ),


  #     tmp as (
  #     SELECT distinct_prod_orgs.stock_date,
  #             distinct_prod_orgs.org_num,
  #             distinct_prod_orgs.prod_num,
  #             qty_ordered,
  #             qty_received,
  #             sales_qty,
  #             inv_soh_qty,

  #             ARRAY_REVERSE(ARRAY_AGG(sales_qty) OVER(PARTITION BY
  #                                                                       distinct_prod_orgs.org_num,
  #                                                                       distinct_prod_orgs.prod_num
  #                                                     ORDER BY distinct_prod_orgs.stock_date
  #                                                   ))[SAFE_OFFSET(
  #             {% parameter number_of_periods_back %} *
  #                               {% if comparison_period._parameter_value == "DAY"%}
  #                                   1
  #                               {% elsif comparison_period._parameter_value == "WEEK"%}
  #                                   7
  #                               {% elsif comparison_period._parameter_value == "MONTH"%}
  #                                   30
  #                               {% elsif comparison_period._parameter_value == "QUARTER"%}
  #                                   92
  #                               {% elsif comparison_period._parameter_value == "YEAR"%}
  #                                   365
  #                               {% endif %}

  #               )] as sales_qty_previous_period,


  #     FROM distinct_prod_orgs
  #     LEFT JOIN base
  #       ON distinct_prod_orgs.stock_date = base.stock_date
  #       AND distinct_prod_orgs.org_num = base.org_num
  #       AND distinct_prod_orgs.prod_num = base.prod_num

  #     )








  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${date_date}, ${location}, ${product}) ;;
  }

  dimension_group: date {
    type: time
    # hidden: yes
    description: "%m/%d/%E4Y"
    timeframes: [
      raw,
      date,
      week,
      week_of_year,
      month,
      quarter,
      quarter_of_year,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.stock_date ;;
  }

  dimension: date_2 {
    label: "Period"
    type: string
    sql: {% if comparison_period._parameter_value == "DAY"%}
        ${date_date}
         {% elsif comparison_period._parameter_value == "WEEK"%}
        ${date_week}
         {% elsif comparison_period._parameter_value == "MONTH"%}
        ${date_month}
         {% elsif comparison_period._parameter_value == "QUARTER"%}
        CONCAT(${date_year}, "-", ${date_quarter_of_year})
                # CASE
                #   WHEN
                #     RIGHT(${date_quarter}, 2) = "01" THEN CONCAT(LEFT(${date_quarter}, 5), "Q1")
                #   WHEN
                #     RIGHT(${date_quarter}, 2) = "02" THEN CONCAT(LEFT(${date_quarter}, 5), "Q2")
                #   WHEN
                #     RIGHT(${date_quarter}, 2) = "03" THEN CONCAT(LEFT(${date_quarter}, 5), "Q3")
                #   WHEN
                #     RIGHT(${date_quarter}, 2) = "04" THEN CONCAT(LEFT(${date_quarter}, 5), "Q4")
                #   ELSE ${date_quarter}
                # END
         {% elsif comparison_period._parameter_value == "YEAR"%}
        ${date_year}
        {% endif %}
        ;;
  }

  dimension: comparable_date {
    type: string
    hidden: yes
    sql: DATE_SUB(
    {% if comparison_period._parameter_value == "DAY"%}
    DATE(${date_date}),
    {% elsif comparison_period._parameter_value == "WEEK" %}
    DATE(${date_week}),
     {% elsif comparison_period._parameter_value == "MONTH" %}
    DATE(CONCAT(${date_month}, "-01")),
     {% elsif comparison_period._parameter_value == "QUARTER" %}
    DATE(CONCAT(${date_quarter}, "-01")),
     {% elsif comparison_period._parameter_value == "YEAR" %}
    DATE(CONCAT(${date_year}, "-01-01")),
    {% endif %}

    INTERVAL {% parameter number_of_periods_back %}
    {% parameter comparison_period %}) ;;
  }

  dimension: comparable_date_2 {
    type: string
    label: "Comparable Period"
    sql:
      {% if comparison_period._parameter_value == "DAY"%}
          ${comparable_date}
      {% elsif comparison_period._parameter_value == "WEEK" %}
          CONCAT(LEFT(CAST(${comparable_date} as STRING), 5), EXTRACT(WEEK(SUNDAY) from ${comparable_date}), "w")
      {% elsif comparison_period._parameter_value == "MONTH" %}
          CONCAT(LEFT(CAST(${comparable_date} as STRING), 5), EXTRACT(MONTH from ${comparable_date}))
      {% elsif comparison_period._parameter_value == "QUARTER" %}
          CONCAT(LEFT(CAST(${comparable_date} as STRING), 5), "Q", EXTRACT(QUARTER from ${comparable_date}))
      {% elsif comparison_period._parameter_value == "YEAR" %}
          EXTRACT(YEAR from ${comparable_date})
      {% endif %}
    ;;
  }



  dimension: location {
    type: string
    sql: CAST(${TABLE}.org_num as STRING) ;;
  }

  dimension: product {
    type: string
    sql: ${TABLE}.prod_num ;;
  }

  dimension: inv_soh_qty {
    type: number
    sql: ${TABLE}.inv_soh_qty ;;
  }

  dimension: qty_sold {
    type: number
    # hidden: yes
    sql: ${TABLE}.sales_qty ;;
  }


  dimension: chosen_number_of_days {
    type: number
    sql: ${TABLE}.chosen_number_of_days ;;
  }

  dimension: number_of_following_rows {
    type: number
    sql: ${TABLE}.number_of_following_rows ;;
  }


  dimension: sales_qty_previous_period {
    type: number
    sql: ${TABLE}.sales_qty_previous_period ;;
  }


  # dimension: quantity_on_hand {
  #   type: number
  #   sql: ${TABLE}.inv_soh_qty ;;
  # }

  # dimension: stock_on_hand_ordered {
  #   type: number
  #   sql: ${TABLE}.Stock_On_Hand_Ordered ;;
  # }

  # dimension: bu {
  #   type: string
  #   sql: ${TABLE}.bu ;;
  # }

  measure: sales {
    type: sum
    drill_fields: []
    sql: ${qty_sold} ;;
  }

  measure: sales_previous_period {
    type: sum
    drill_fields: []
    sql: ${sales_qty_previous_period} ;;
  }



  filter: prototype_date_1 {
    type: date
    hidden: yes
    sql:  {% condition prototype_date_1 %} TIMESTAMP(${date_date}) {% endcondition %}

                                                    ;;
  }

# OR
#     {% condition prototype_date_1 %} TIMESTAMP(DATE_ADD(stock_date, INTERVAL
#                                                     {% parameter number_of_periods_back %}
#                                                     {% parameter comparison_period %})) {% endcondition %}

  parameter: comparison_period {
    type: unquoted
    hidden: yes
    allowed_value: {
      label: "Day"
      value: "DAY"
    }
    allowed_value: {
      label: "Week"
      value: "WEEK"
    }
    allowed_value: {
      label: "Month"
      value: "MONTH"
    }
    allowed_value: {
      label: "Quarter"
      value: "QUARTER"
    }
    allowed_value: {
      label: "Year"
      value: "YEAR"
    }
  }



  parameter: number_of_periods_back {
    type: number
    hidden: yes
  }


  measure: sell_through_received {
    type: number
    value_format: "0.00%"
    sql: SAFE_DIVIDE(${sales},
                (
                    ${sell_through_last_first_values.sales_over_period}

                   +

                  ${sell_through_last_first_values.soh_last_value}
                )
            )

                  ;;
  }

  measure: sell_through_received_previous_period {
    type: number
    value_format: "0.00%"
    sql: SAFE_DIVIDE(${sales_previous_period},
                (
                    ${sell_through_last_first_values.sales_over_period_previous_period}

                   +

                  ${sell_through_last_first_values.soh_last_value_previous_period}
                )
            )
                  ;;
  }

  measure: sell_through_ordered {
    type: number
    value_format: "0.00%"
    sql: SAFE_DIVIDE(${sales},
                (
                    ${sell_through_last_first_values.ordered_over_period}

                   +

                  ${sell_through_last_first_values.soh_first_value}
                )
            )

                  ;;
  }

  measure: sell_through_ordered_previous_period {
    type: number
    value_format: "0.00%"
    sql: SAFE_DIVIDE(${sales_previous_period},
                (
                    ${sell_through_last_first_values.ordered_over_period_previous_period}

                   +

                  ${sell_through_last_first_values.soh_first_value_previous_period}
                )
            )

                  ;;
  }


}
