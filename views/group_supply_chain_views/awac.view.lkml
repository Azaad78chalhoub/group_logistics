view: awac {
  derived_table: {
    explore_source: orders {
      bind_all_filters: yes
      column: org_num { field: cost_facts.org_num }
      column: prod_num { field: cost_facts.prod_num }
      column: unit_cost { field: cost_facts.unit_cost }
      column: unit_cost_usd { field: cost_facts.unit_cost_usd }
      derived_column: awac {
        sql: CASE WHEN unit_cost = 0 THEN NULL ELSE AVG(unit_cost) END ;;
      }
      derived_column: awac_usd {
        sql: CASE WHEN unit_cost_usd = 0 THEN NULL ELSE AVG(unit_cost_usd) END  ;;
      }
    }

  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql:  CONCAT(${org_num}, " - ", ${prod_num}) ;;
  }

  dimension: org_num {
    label: "Product Data in RMS Location ID"
    hidden: yes
  }
  dimension: prod_num {
    label: "Product Data in RMS Product ID"
    hidden: yes
  }
  dimension: unit_cost {
    label: "Product Data in RMS WAC Local"
    description: "Weighted Average Unit Cost (WAC) Local"
    hidden: yes
    value_format: "#,##0.00"
    type: number
  }
  dimension: unit_cost_usd {
    label: "Product Data in RMS WAC USD"
    description: "Weighted Average Unit Cost (WAC) USD"
    hidden: yes
    value_format: "$#,##0.00"
    type: number
  }
  dimension: awac {
    view_label: "Product Data in RMS"
    description: "Aproximate Weighted Average Cost"
    label: "AWAC"
    hidden: no
    value_format_name:  decimal_2
    type: number
  }
  dimension: awac_usd {
    view_label: "Product Data in RMS"
    description: "Aproximate Weighted Average Cost USD"
    label: "AWAC USD"
    hidden: no
    value_format_name: usd
    type: number
}

}
