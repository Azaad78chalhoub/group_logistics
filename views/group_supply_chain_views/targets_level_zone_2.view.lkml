view: targets_level_zone_2 {
  label: "Level Shoes Targets"
  sql_table_name: `chb-prod-stage-oracle.prod_supply_chain.targets_level_zone_3`
    ;;

  dimension: bu {
    type: string
    hidden: yes
    sql: ${TABLE}.bu ;;
  }


  dimension: location {
    type: string
    hidden: no
    label: "Location ID"
    sql: ${TABLE}.Location ;;
  }

  dimension: location_name {
    case: {
      when: {
        sql: ${location} = "8001" ;;
        label: "LEVEL SHOES - DUBAI MALL"
      }
      when: {
        sql: ${location} = "8003" ;;
        label: "LEVEL SHOES - ECOMMERCE"
      }
      when: {
        sql: ${location} = "8005" ;;
        label: "LEVEL SHOES - ECOMMERCE - FARFETCH"
      }
    }
  }

  dimension: pk {
    primary_key: yes
    type: string
    sql: CONCAT(${date_date}, " - ", ${location_name}, " - ",  ${zone}) ;;
  }

  dimension_group: date {
    type: time
    label: "Business"
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    hidden: no
    sql: ${TABLE}.date ;;
  }

  dimension: margin_loc_target {
    type: number
    hidden: yes
    sql: ${TABLE}.margin_loc_target ;;
  }

  dimension: net_sales_loc_target {
    type: number
    hidden: yes
    sql: ${TABLE}.net_sales_loc_target ;;
  }

  dimension: zone {
    type: string
    hidden: no
    sql: ${TABLE}.zone ;;
  }

  dimension: col_type {
    type: string
    hidden: yes
    sql: "sale";;
  }



#  measure: net_sales_amount_local_vs_target_new {
#    type: number
#    hidden: yes
#    view_label: "Level Shoes Targets"
#    value_format_name: percent_2
#    sql: SAFE_DIVIDE(${fact_retail_sales.total_net_sales_local},${net_sales_local_target_total}) ;;
#  }

  measure: net_sales_local_target {
    type: sum
    view_label: "Level Shoes Targets"
    value_format_name: decimal_2
    sql: ${net_sales_loc_target} ;;
  }

  measure: net_sales_local_target_total {
    hidden: yes
    type: sum
    view_label: "Level Shoes Targets"
    value_format_name: decimal_2
    sql: ${net_sales_loc_target} ;;
  }

#   measure: net_sales_local_target1 {
#     type: sum
#     view_label: "Level Shoes Targets"
#     label: "net sales local target new"
#     value_format_name: decimal_2
#     sql:${TABLE}.net_sales_loc_target  ;;
#   }

    measure: margin_local_target {
    type: sum
    view_label: "Level Shoes Targets"
    value_format_name: decimal_2
    sql: ${margin_loc_target} ;;
  }

  measure: margin_local_target_total {
    type: sum
    hidden: yes
    view_label: "Level Shoes Targets"
    value_format_name: decimal_2
    sql: ${margin_loc_target} ;;
  }



  measure: margin_local_vs_target {
    type: number
    view_label: "Level Shoes Targets"
    value_format_name: percent_2
    sql: (SAFE_DIVIDE((${fact_level_sales.total_net_sales_local}-${fact_level_sales.cogs_local}),(${margin_local_target_total})))-1 ;;
  }


  measure: net_sales_amount_local_vs_target {
    type: number
    view_label: "Level Shoes Targets"
    value_format_name: percent_2
    sql: IFNULL((SAFE_DIVIDE(SUM(1*${fact_level_sales.amountlocal_beforetax}),${net_sales_local_target_total})),0)-1 ;;
  }


  measure: count {
    type: count
    drill_fields: []
  }
}
