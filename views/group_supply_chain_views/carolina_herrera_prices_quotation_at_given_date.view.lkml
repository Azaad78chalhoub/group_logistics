view: carolina_herrera_prices_quotation_at_given_date {
  label: "Price"
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_prices_quotation_at_given_date`
    ;;

  dimension_group: app_from {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.app_from ;;
  }

  dimension_group: app_to {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.app_to ;;
  }

  dimension: country {
    type: string
    hidden: yes
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: currency {
    type: string
    hidden: yes
    sql: ${TABLE}.currency ;;
  }

  dimension: ean13 {
    type: string
    hidden: yes
    sql: ${TABLE}.ean13 ;;
  }

  dimension: price {
    type: number
    sql: ${TABLE}.price ;;
  }

  dimension: seasonprice {
    type: number
    sql: ${TABLE}.seasonprice ;;
  }

  dimension: sku_code {
    type: string
    hidden: yes
    sql: ${TABLE}.sku_code ;;
  }

  dimension: store {
    type: number
    hidden: yes
    sql: ${TABLE}.store ;;
  }

  dimension: tag {
    type: string
    sql: ${TABLE}.tag ;;
  }

  dimension: tariff_code {
    type: string
    sql: ${TABLE}.tariff_code ;;
  }

  dimension: taxband {
    type: number
    sql: ${TABLE}.taxband ;;
  }

  dimension: taxes {
    type: number
    sql: ${TABLE}.taxes ;;
  }

  dimension: taxname {
    type: string
    sql: ${TABLE}.taxname ;;
  }

  measure: priceinlc {
    type: sum
    group_label: "Price"
    label: "Price in LC"
    sql:${TABLE}.price ;;
  }

  measure: count {
    type: count
    drill_fields: [taxname]
  }
}
