view: retail_transaction_classification {
  derived_table: {
    sql: select *,
        case when quantity_sold >= 0 then 'SALES'
             when quantity_sold < 0 then 'RETURN'
        ELSE 'MISC' END AS transaction_type
FROM (
SELECT
  CAST(fact_retail_sales.bk_storeid as STRING)  AS location_number,
  fact_retail_sales.atr_cginvoiceno as invoice_number,
  --CAST(TIMESTAMP(PARSE_DATE('%Y%m%d', CAST(fact_retail_sales.bk_businessdate AS STRING)))  AS DATE) AS business_date,
  fact_retail_sales.bk_businessdate AS business_date,
  COALESCE(SUM(fact_retail_sales.mea_quantity ), 0) AS quantity_sold
FROM `chb-prod-supplychain-data.prod_supply_chain.factretailsales`  AS fact_retail_sales
LEFT JOIN `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product`  AS dim_retail_product
      ON fact_retail_sales.bk_productid = dim_retail_product.item
LEFT JOIN `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy`  AS dim_alternate_bu_hierarchy
      ON CAST(fact_retail_sales.bk_storeid AS STRING) = dim_alternate_bu_hierarchy.store_code

WHERE (dim_alternate_bu_hierarchy.vertical = 'MANAGED COMPANIES')

GROUP BY 1,2,3
ORDER BY 3 DESC)
 ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: location_number {
    type: string
    hidden: yes
    sql: ${TABLE}.location_number ;;
  }

  dimension: invoice_number {
    type: string
    hidden: yes
    sql: ${TABLE}.invoice_number ;;
  }

  dimension: business_date {
    type: date
    hidden: yes
    datatype: date
    sql: ${TABLE}.business_date ;;
  }

  dimension: quantity_sold {
    type: number
    hidden: yes
    sql: ${TABLE}.quantity_sold ;;
  }

  dimension: transaction_type {
    hidden: yes
    type: string
    sql: ${TABLE}.transaction_type ;;
  }

  set: detail {
    fields: [location_number, invoice_number, business_date, quantity_sold, transaction_type]
  }
}
