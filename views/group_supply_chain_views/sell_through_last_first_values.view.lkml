view: sell_through_last_first_values {
label: "Sell Through Last Values"
    derived_table: {
    sql:
        WITH
        base as (
        SELECT



                -- DATE_TRUNC(stock_date, {% parameter sell_through_prototype.comparison_period %}) as truncated_date,
                org_num,
                prod_num,
                ARRAY_AGG(struct(stock_date, sales_qty, inv_soh_qty, qty_ordered) ORDER BY stock_date) as metrics





              FROM
                `chb-prod-supplychain-data.prod_supply_chain.dm_sell_through` stp

              WHERE {% condition sell_through_prototype.prototype_date_1 %} TIMESTAMP(stp.stock_date) {% endcondition %}

              OR

              {% condition sell_through_prototype.prototype_date_1 %}
                         TIMESTAMP(DATE_ADD(stock_date, INTERVAL
                                                    {% parameter sell_through_prototype.number_of_periods_back %}
                                                    {% parameter sell_through_prototype.comparison_period %}))
                    {% endcondition %}
              GROUP BY org_num, prod_num

        )


        SELECT
          org_num,
          prod_num,
          ARRAY_REVERSE(metrics)[SAFE_ORDINAL(1)].inv_soh_qty as last_value_soh,
          metrics[SAFE_ORDINAL(1)].inv_soh_qty as first_value_soh_previous_period,
          (SELECT sum(CASE WHEN  stock_date >= DATE({% date_start sell_through_prototype.prototype_date_1 %})
                  THEN

                    COALESCE(sales_qty, 0)

                  ELSE
                      0
                  END

          ) from UNNEST(metrics)) as sales_over_period,

          (SELECT sum(CASE WHEN  stock_date < DATE_SUB(DATE({% date_end sell_through_prototype.prototype_date_1 %}), INTERVAL
                                                                        {% parameter sell_through_prototype.number_of_periods_back %}
                                                                        {% parameter sell_through_prototype.comparison_period %}
                                                                              )
                  THEN

                    sales_qty

                  ELSE
                      0
                  END
                    ) from UNNEST(metrics)) as sales_over_period_previous_period,

            (SELECT sum(CASE WHEN  stock_date >= DATE({% date_start sell_through_prototype.prototype_date_1 %})
                  THEN

                    qty_ordered

                  ELSE
                      0
                  END
                    ) from UNNEST(metrics)) as ordered_over_period,

              (SELECT sum(CASE WHEN  stock_date < DATE_SUB(DATE({% date_end sell_through_prototype.prototype_date_1 %}), INTERVAL
                                                                        {% parameter sell_through_prototype.number_of_periods_back %}
                                                                        {% parameter sell_through_prototype.comparison_period %}
                                                                              )
                  THEN

                    qty_ordered

                  ELSE
                      0
                  END
                    ) from UNNEST(metrics)) as ordered_over_period_previous_period,

          ARRAY_REVERSE(metrics)[SAFE_ORDINAL(DATE_DIFF(DATE({% date_end sell_through_prototype.prototype_date_1 %}),
                                                          DATE({% date_start sell_through_prototype.prototype_date_1 %}), DAY))].inv_soh_qty as first_value_soh,
          metrics[SAFE_ORDINAL(DATE_DIFF(DATE({% date_end sell_through_prototype.prototype_date_1 %}),
            DATE({% date_start sell_through_prototype.prototype_date_1 %}), DAY))].inv_soh_qty as last_value_soh_previous_period,


        FROM base










    ;;
  }


# {% parameter sell_through_prototype.number_of_periods_back %} *
#                                 {% if sell_through_prototype.comparison_period._parameter_value == "DAY"%}
#                                     1
#                                 {% elsif sell_through_prototype.comparison_period._parameter_value == "WEEK"%}
#                                     7
#                                 {% elsif sell_through_prototype.comparison_period._parameter_value == "MONTH"%}
#                                     30
#                                 {% elsif sell_through_prototype.comparison_period._parameter_value == "QUARTER"%}
#                                     92
#                                 {% elsif sell_through_prototype.comparison_period._parameter_value == "YEAR"%}
#                                     365
#                                 {% endif %}





# -- LAST VALUE SOH

#                 LAST_VALUE(inv_soh_qty) OVER (PARTITION BY

#                                                                   org_num,
#                                                                   prod_num
#                                                 ORDER BY stock_date
#                                                 ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as last_val_soh,

#         -- FIRST VALUE SOH PREVIOUS PERIOD

#                 FIRST_VALUE(inv_soh_qty) OVER (PARTITION BY

#                                                                   org_num,
#                                                                   prod_num
#                                                 ORDER BY stock_date
#                                                 ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as first_value_soh_previous_period,

#         -- SALES OVER PERIOD

#                 sum(CASE WHEN  stock_date >= DATE({% date_start sell_through_prototype.prototype_date_1 %})
#                   THEN

#                     COALESCE(sales_qty, 0)

#                   ELSE
#                       0
#                   END
#                     ) OVER (PARTITION BY org_num,
#                                           prod_num
#                                                       ORDER BY stock_date

#               ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
#               )

#                       as sales_over_period,

#         -- SALES OVER PERIOD Previous Period

#                 sum(CASE WHEN  stock_date < DATE_SUB(DATE({% date_end sell_through_prototype.prototype_date_1 %}), INTERVAL
#                                                                         {% parameter sell_through_prototype.number_of_periods_back %}
#                                                                         {% parameter sell_through_prototype.comparison_period %}
#                                                                               )
#                   THEN

#                     sales_qty

#                   ELSE
#                       0
#                   END
#                     ) OVER (PARTITION BY org_num,
#                                           prod_num
#                                                       ORDER BY stock_date

#               ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
#               )

#                       as sales_over_period_previous_period,



#           -- ORDERED OVER PERIOD

#                 sum(CASE WHEN  stock_date >= DATE({% date_start sell_through_prototype.prototype_date_1 %})
#                   THEN

#                     qty_ordered

#                   ELSE
#                       0
#                   END
#                     ) OVER (PARTITION BY org_num,
#                                           prod_num
#                                                       ORDER BY stock_date

#               ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
#               )

#                       as ordered_over_period,

#           -- ORDERED OVER PERIOD Previous Period

#                 sum(CASE WHEN  stock_date < DATE_SUB(DATE({% date_end sell_through_prototype.prototype_date_1 %}), INTERVAL
#                                                                         {% parameter sell_through_prototype.number_of_periods_back %}
#                                                                         {% parameter sell_through_prototype.comparison_period %}
#                                                                               )
#                   THEN

#                     qty_ordered

#                   ELSE
#                       0
#                   END
#                     ) OVER (PARTITION BY org_num,
#                                           prod_num
#                                                       ORDER BY stock_date

#               ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
#               )

#                       as ordered_over_period_previous_period,


#           -- Additional service fields

#               DATE_DIFF( {% date_end sell_through_prototype.prototype_date_1 %},
#                     {% date_start sell_through_prototype.prototype_date_1 %}, DAY) as chosen_number_of_days,

#                 ROW_NUMBER() OVER (PARTITION BY org_num, prod_num
#                                           ORDER BY stock_date) as row_num_over_key,

#                                               DATE_DIFF( {% date_end sell_through_prototype.prototype_date_1 %},
#                     {% date_start sell_through_prototype.prototype_date_1 %}, DAY)

#                                                   -

#                                                   ROW_NUMBER() OVER (PARTITION BY org_num, prod_num
#                                           ORDER BY stock_date) as number_of_following_rows,
#                 ROW_NUMBER() OVER (PARTITION BY org_num, prod_num
#                                           ORDER BY stock_date DESC) as row_num_over_key_desc,










        # SELECT
        # DISTINCT

        # --truncated_date,

        # base.org_num,
        # base.prod_num,
        # base.last_val_soh as last_value_soh,
        # base.first_value_soh_previous_period,
        # base.sales_over_period,
        # base.sales_over_period_previous_period,
        # base2.inv_soh_qty as last_value_soh_previous_period,
        # base3.inv_soh_qty as first_value_soh,
        # base.ordered_over_period,
        # base.ordered_over_period_previous_period
        # from base
        #     JOIN (SELECT * FROM base WHERE row_num_over_key = chosen_number_of_days


        #     ) base2 -- Number of following rows = 0 is exactly where last_value_soh_previous_period
        #                                                                       -- and joining only on org and prod to propagate that exact value to all rows
        #           ON base.org_num = base2.org_num
        #           and base.prod_num = base2.prod_num

        #                                                                     -- row_num = number of periods * comparison period + 1
        #                                                                     -- is exactly where first value of Current period located
        #     JOIN (SELECT * FROM base WHERE row_num_over_key_desc = chosen_number_of_days
        #     ) base3
        #         ON base.org_num = base3.org_num
        #           and base.prod_num = base3.prod_num

        # WHERE
        #       {% condition sell_through_prototype.prototype_date_1 %} TIMESTAMP(base.stock_date) {% endcondition %}













  dimension: pk {
    primary_key: yes
    type: string
    hidden: yes
    sql:

    CONCAT( ${location},${product})

    ;;
  }
# ${date},

  dimension: location {
    type: string
    hidden: yes
    sql: CAST(${TABLE}.org_num as STRING) ;;
  }

  dimension: product {
    type: string
    hidden: yes
    sql: ${TABLE}.prod_num ;;
  }

  # dimension: date {
  #   type: date
  #   sql: ${TABLE}.truncated_date ;;
  # }


  # Current period metrics
  measure: soh_last_value {
    type: sum_distinct
    sql: ${TABLE}.last_value_soh ;;
  }

  measure: soh_first_value {
    type: sum_distinct
    sql: ${TABLE}.first_value_soh ;;
  }

  measure: sales_over_period {
    type: sum_distinct
    # type: sum
    sql: ${TABLE}.sales_over_period ;;
  }

  measure: ordered_over_period {
    type: sum_distinct
    sql: ${TABLE}.ordered_over_period ;;
  }

  # # Previous period metrics
  measure: soh_last_value_previous_period{
    type: sum_distinct
    sql: ${TABLE}.last_value_soh_previous_period ;;
  }

  measure: soh_first_value_previous_period {
    type: sum_distinct
    sql: ${TABLE}.first_value_soh_previous_period ;;
  }

  measure: sales_over_period_previous_period {
    type: sum_distinct
    sql: ${TABLE}.sales_over_period_previous_period ;;
  }

  measure: ordered_over_period_previous_period {
    type: sum_distinct
    sql: ${TABLE}.ordered_over_period_previous_period ;;
  }

}
