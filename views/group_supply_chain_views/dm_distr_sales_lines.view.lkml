# If necessary, uncomment the line below to include explore_source.

# include: "group_supply_chain.model.lkml"

view: dm_distr_sales_lines {
  view_label: "Sales Order Lines"
  derived_table: {
    explore_source: dm_distr_sales {
      column: ordered_quantity_sum {}
      column: ordered_revenue_net_usd {}
      column: cancelled_quantity_m {}
      column: cancelled_revenue_net_usd {}
      column: cancelled_revenue_net_local{}
      column: shipped_quantity_sum {}
      column: order_number {}
      column: invoice_number {}
      column: line_id {}
      derived_column: rank {sql: row_number() OVER (partition by order_number ,line_id order by coalesce(invoice_number,'0')) ;;}
      bind_all_filters: yes
    }

  }

  dimension: id {
    type: string
    hidden: yes
    primary_key: yes
    sql: concat(${line_id}, "-", case when  ${invoice_number} is null then "" else ${invoice_number} end );;
  }

  dimension: ordered_quantity_sum {
    label: "Distribution Sales Ordered Quantity"
    description: "Quantity Ordered"
    hidden: yes
    type: number
  }
  dimension: ordered_revenue_net_usd {
    label: "Distribution Sales Ordered Revenue Net USD"
    description: "Ordered Revenue -Exludes the units returned"
    value_format: "$#,##0.00"
    hidden: yes
    type: number
  }
  dimension: cancelled_quantity_m {
    label: "Distribution Sales Cancelled Quantity"
    description: "Cancelled Quantity"
    hidden: yes

    type: number
  }
  dimension: cancelled_revenue_net_usd {
    label: "Distribution Sales Cancelled Amount USD"
    description: "Cancelled Amount in USD"
    value_format: "$#,##0.00"
    hidden: yes

    type: number
  }

  dimension: cancelled_revenue_net_local {
    hidden: yes
    type: number
  }
  dimension: shipped_quantity_sum {
    label: "Distribution Sales Shipped Quantity"
    description: "Quantity Shipped"
    hidden: yes

    type: number
  }
  dimension: order_number {
    label: "Distribution Sales Order Number"
    hidden: yes

    type: number
  }
  dimension: invoice_number {
    label: "Distribution Sales Invoice Number"
    hidden: yes

  }
  dimension: line_id {
    label: "Distribution Sales Line ID"
    hidden: yes

  }

  dimension: is_valid {
    type: yesno
    hidden: yes

    sql: ${rank} = 1 ;;
  }

  dimension: rank {}


  measure: shipped_quantity   {
    type: sum
    filters: [is_valid: "yes"]
    sql:  ${shipped_quantity_sum}   ;;
  }

  measure: ordered_quantity   {
    type: sum
    filters: [is_valid: "yes"]
    sql:  ${ordered_quantity_sum}   ;;
  }

  measure: ordered_amount_usd   {
    type: sum
    filters: [is_valid: "yes"]
    sql:  ${ordered_revenue_net_usd}   ;;
  }

  measure: cancelled_quantity   {
    type: sum
    filters: [is_valid: "yes"]
    sql:  ${cancelled_quantity_m}   ;;
  }


  measure: cancelled_amount_usd   {
    type: sum
    filters: [is_valid: "yes"]
    sql:  ${cancelled_revenue_net_usd}   ;;
  }

  measure: cancelled_amount_local   {
    type: sum
    filters: [is_valid: "yes"]
    sql:  ${cancelled_revenue_net_local}   ;;
  }


}
