view: dim_ebs_org_units {
  sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_ebs_org_units`
    ;;

  dimension: _fivetran_deleted {
    type: yesno
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: attribute1 {
    type: string
    sql: ${TABLE}.attribute1 ;;
  }

  dimension: attribute10 {
    type: string
    sql: ${TABLE}.attribute10 ;;
  }

  dimension: attribute11 {
    type: string
    sql: ${TABLE}.attribute11 ;;
  }

  dimension: attribute12 {
    type: string
    sql: ${TABLE}.attribute12 ;;
  }

  dimension: attribute13 {
    type: string
    sql: ${TABLE}.attribute13 ;;
  }

  dimension: attribute14 {
    type: string
    sql: ${TABLE}.attribute14 ;;
  }

  dimension: attribute15 {
    type: string
    sql: ${TABLE}.attribute15 ;;
  }

  dimension: attribute16 {
    type: string
    sql: ${TABLE}.attribute16 ;;
  }

  dimension: attribute17 {
    type: string
    sql: ${TABLE}.attribute17 ;;
  }

  dimension: attribute18 {
    type: string
    sql: ${TABLE}.attribute18 ;;
  }

  dimension: attribute19 {
    type: string
    sql: ${TABLE}.attribute19 ;;
  }

  dimension: attribute2 {
    type: string
    sql: ${TABLE}.attribute2 ;;
  }

  dimension: attribute20 {
    type: string
    sql: ${TABLE}.attribute20 ;;
  }

  dimension: attribute21 {
    type: string
    sql: ${TABLE}.attribute21 ;;
  }

  dimension: attribute22 {
    type: string
    sql: ${TABLE}.attribute22 ;;
  }

  dimension: attribute23 {
    type: string
    sql: ${TABLE}.attribute23 ;;
  }

  dimension: attribute24 {
    type: string
    sql: ${TABLE}.attribute24 ;;
  }

  dimension: attribute25 {
    type: string
    sql: ${TABLE}.attribute25 ;;
  }

  dimension: attribute26 {
    type: string
    sql: ${TABLE}.attribute26 ;;
  }

  dimension: attribute27 {
    type: string
    sql: ${TABLE}.attribute27 ;;
  }

  dimension: attribute28 {
    type: string
    sql: ${TABLE}.attribute28 ;;
  }

  dimension: attribute29 {
    type: string
    sql: ${TABLE}.attribute29 ;;
  }

  dimension: attribute3 {
    type: string
    sql: ${TABLE}.attribute3 ;;
  }

  dimension: attribute30 {
    type: string
    sql: ${TABLE}.attribute30 ;;
  }

  dimension: attribute4 {
    type: string
    sql: ${TABLE}.attribute4 ;;
  }

  dimension: attribute5 {
    type: string
    sql: ${TABLE}.attribute5 ;;
  }

  dimension: attribute6 {
    type: string
    sql: ${TABLE}.attribute6 ;;
  }

  dimension: attribute7 {
    type: string
    sql: ${TABLE}.attribute7 ;;
  }

  dimension: attribute8 {
    type: string
    sql: ${TABLE}.attribute8 ;;
  }

  dimension: attribute9 {
    type: string
    sql: ${TABLE}.attribute9 ;;
  }

  dimension: attribute_category {
    type: string
    sql: ${TABLE}.attribute_category ;;
  }

  dimension: business_group_id {
    type: number
    sql: ${TABLE}.business_group_id ;;
  }

  dimension: comments {
    type: string
    sql: ${TABLE}.comments ;;
  }

  dimension: cost_allocation_keyflex_id {
    type: number
    sql: ${TABLE}.cost_allocation_keyflex_id ;;
  }

  dimension: created_by {
    type: number
    sql: ${TABLE}.created_by ;;
  }

  dimension_group: creation {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.creation_date AS TIMESTAMP) ;;
  }

  dimension_group: date_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.date_from AS TIMESTAMP) ;;
  }

  dimension_group: date_to {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.date_to AS TIMESTAMP) ;;
  }

  dimension: internal_address_line {
    type: string
    sql: ${TABLE}.internal_address_line ;;
  }

  dimension: internal_external_flag {
    type: string
    sql: ${TABLE}.internal_external_flag ;;
  }

  dimension_group: last_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.last_update_date AS TIMESTAMP) ;;
  }

  dimension: last_update_login {
    type: number
    sql: ${TABLE}.last_update_login ;;
  }

  dimension: last_updated_by {
    type: number
    sql: ${TABLE}.last_updated_by ;;
  }

  dimension: location_id {
    type: number
    sql: ${TABLE}.location_id ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: object_version_number {
    type: number
    sql: ${TABLE}.object_version_number ;;
  }

  dimension: organization_id {
    type: number
    primary_key: yes
    sql: ${TABLE}.organization_id ;;
  }

  dimension: party_id {
    type: number
    sql: ${TABLE}.party_id ;;
  }

  dimension: program_application_id {
    type: number
    sql: ${TABLE}.program_application_id ;;
  }

  dimension: program_id {
    type: number
    sql: ${TABLE}.program_id ;;
  }

  dimension_group: program_update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.program_update_date AS TIMESTAMP) ;;
  }

  dimension: request_id {
    type: number
    sql: ${TABLE}.request_id ;;
  }

  dimension: soft_coding_keyflex_id {
    type: number
    sql: ${TABLE}.soft_coding_keyflex_id ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  measure: count {
    type: count
    drill_fields: [name]
  }
}
