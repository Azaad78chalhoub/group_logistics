view: budgetsretail {

  # added table below to get the targets in AED

  derived_table: {
    sql_trigger_value: SELECT FLOOR((TIMESTAMP_DIFF(CURRENT_TIMESTAMP(),'1970-01-01 00:00:00',SECOND)) / (6*60*60)) ;;
    sql:
    SELECT targets.*,
    budget_usd*conversion_rate AS budget_local,
    budget_usd_r1*conversion_rate AS budget_local_r1,
    budget_usd_r2*conversion_rate AS budget_local_r2,
    budget_usd_r2_alt*conversion_rate AS budget_local_r2_alt,
    budget_usd_r3*conversion_rate AS budget_local_r3,
    cogs_usd*conversion_rate AS cogs_local,
    target_margin*conversion_rate AS target_margin_local,
    target_margin_r1*conversion_rate AS target_margin_r1_local,
    target_margin_r2*conversion_rate AS target_margin_r2_local,
    target_margin_r3*conversion_rate AS target_margin_r3_local,
    target_usd*conversion_rate AS target_local,
    target_usd_r1*conversion_rate AS target_local_r1,
    target_usd_r2*conversion_rate AS target_local_r2
    FROM
    (SELECT targets.*,rates.conversion_rate
    FROM `chb-prod-supplychain-data.prod_supply_chain.budgets_target` targets
    LEFT JOIN
    `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` loc
    ON cast(targets.bk_store as string) = cast(loc.store as string)
    LEFT JOIN
    `chb-prod-supplychain-data.prod_shared_dimensions.dim_conversion_daily_rates` rates
    ON targets.business_date=DATE(rates.conversion_date)
    AND from_currency="USD"
    AND to_currency=loc.currency_code
    AND conversion_type="1001") targets;;
  }


  label: "Retail Targets"
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    hidden: yes
    sql: ${TABLE}.id ;;
  }

  dimension: _fivetran_deleted {
    type: yesno
    hidden: yes
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: from_currency {
    type: string
    hidden: yes
    sql: "USD";;
  }

  dimension: to_currency {
    type: string
    hidden: yes
    sql: "AED";;
  }

  dimension: conversion_type {
    type: string
    hidden: yes
    sql: "1001";;
  }

  dimension: bk_store {
    type: string
    hidden: yes
    sql: ${TABLE}.bk_store ;;
  }

  dimension: bu_code {
    type: string
    hidden: yes
    sql: ${TABLE}.bu_code ;;
  }

  dimension: budget_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.budget_usd ;;
  }


  dimension: budget_local {
    type: number
    hidden: yes
    sql: ${TABLE}.budget_local ;;
  }

  dimension: budget_usd_r1 {
    type: number
    hidden: yes
    sql: ${TABLE}.budget_usd_r1 ;;
  }

  dimension: budget_usd_r2 {
    type: number
    hidden: yes
    sql: ${TABLE}.budget_usd_r2 ;;
  }

  dimension: budget_usd_r2_alt {
    type: number
    hidden: yes
    sql: ${TABLE}.budget_usd_r2_alt ;;
  }

  dimension: budget_usd_r3 {
    type: number
    hidden: yes
    sql: ${TABLE}.budget_usd_r3 ;;
  }

  dimension_group: business {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    hidden: yes
    sql: ${TABLE}.business_date ;;
  }

  dimension: cogs_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.cogs_usd ;;
  }

  dimension: month {
    type: string
    hidden: yes
    sql: ${TABLE}.month ;;
  }

  dimension: target_margin {
    type: number
    hidden: yes
    sql: ${TABLE}.target_margin ;;
  }

  dimension: target_margin_r1 {
    type: number
    hidden: yes
    sql: ${TABLE}.target_margin_r1 ;;
  }

  dimension: target_margin_r2 {
    type: number
    hidden: yes
    sql: ${TABLE}.target_margin_r2 ;;
  }

  dimension: target_margin_r3 {
    type: number
    hidden: yes
    sql: ${TABLE}.target_margin_r3 ;;
  }

  dimension: target_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.target_usd ;;
  }

  dimension: target_usd_r1 {
    type: number
    hidden: yes
    sql: ${TABLE}.target_usd_r1 ;;
  }

  dimension: target_usd_r2 {
    type: number
    hidden: yes
    sql: ${TABLE}.target_usd_r2 ;;
  }


#   #################################
#   ######       MEASURES       #####
#   #################################

  measure: budget_r0_usd{
    type: sum
    label: "Budget R0 (USD)"
    description: "Budget R0 submitted phased a day level by finance shown in USD"
    value_format_name: usd
    sql: ${TABLE}.budget_usd ;;
  }

  measure: budget_r1_usd{
    type: sum
    label: "Budget R1 (USD)"
    description: "Budget R1 submitted phased at a day level by finance shown in USD"
    value_format_name: usd
    sql: ${TABLE}.budget_usd_r1 ;;
  }

  measure: budget_r2_usd{
    type: sum
    label: "Budget R2 (USD)"
    description: "This a daily phased budget by Finance; it can be used at a day, location, business unit level "
    value_format_name: usd
    sql: ${TABLE}.budget_usd_r2 ;;
  }

  measure: budget_r2_usd_alt{
    type: sum
    label: "Budget R2 (USD) - Including Concessions"
    value_format_name: usd
    sql: ${TABLE}.budget_usd_r2_alt ;;
  }

  measure: budget_r0_local{
    type: sum
    value_format_name: decimal_2
    label: "Budget R0 (local)"
    sql: ${budget_local};;
  }

  measure: budget_r1_local{
    type: sum
    label: "Budget R1 (local)"
    value_format_name: decimal_2
    sql: ${TABLE}.budget_local_r1 ;;
  }

  measure: budget_r2_local{
    type: sum
    label: "Budget R2 (local)"
    value_format_name: decimal_2
    sql: ${TABLE}.budget_local_r2 ;;
  }

  measure: budget_r2_local_alt{
    type: sum
    label: "Budget R2 (local) - Including Concessions"
    value_format_name: decimal_2
    sql: ${TABLE}.budget_local_r2_alt ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [id]
  }
}
