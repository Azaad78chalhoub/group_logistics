view: sell_through_prototype_sales_previous_period {

  # sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.dm_sell_through`
  #   ;;

  derived_table: {
    sql:
        SELECT
        TIMESTAMP(DATE_ADD(stock_date, INTERVAL
                                                    {% parameter sell_through_prototype.number_of_periods_back %}
                                                    {% parameter sell_through_prototype.comparison_period %})) as stock_date,
        org_num,
        prod_num,
        qty_ordered,
        qty_received,
        sales_qty,
        inv_soh_qty
        FROM
        `chb-prod-supplychain-data.prod_supply_chain.dm_sell_through`
        WHERE {% condition sell_through_prototype.prototype_date_1 %} TIMESTAMP(DATE_ADD(stock_date, INTERVAL
                                                    {% parameter sell_through_prototype.number_of_periods_back %}
                                                    {% parameter sell_through_prototype.comparison_period %})) {% endcondition %}
    ;;
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${date_date}, ${location}, ${product}) ;;
  }

  dimension_group: date {
    type: time
    hidden: yes
    description: "%m/%d/%E4Y"
    timeframes: [
      raw,
      date,
      week,
      week_of_year,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    # sql: TIMESTAMP(DATE_ADD(${TABLE}.stock_date, INTERVAL
    #                                         {% parameter sell_through_prototype.number_of_periods_back %}
    #                                         {% parameter sell_through_prototype.comparison_period %}))  ;;
    sql: TIMESTAMP(${TABLE}.stock_date) ;;
  }

  dimension: location {
    type: string
    hidden: yes
    sql: CAST(${TABLE}.org_num as STRING) ;;
  }

  dimension: product {
    type: string
    hidden: yes
    sql: ${TABLE}.prod_num ;;
  }

  dimension: qty_sold {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_qty ;;
  }

  # dimension: bu {
  #   type: string
  #   sql: ${TABLE}.bu ;;
  # }

  measure: sales_previous_period {
    type: sum
    sql: ${qty_sold};;

      # CASE
      #   WHEN
      #     {% condition sell_through_prototype.prototype_date_1 %} DATETIME_ADD(TIMESTAMP(${sell_through_prototype.date_date}), INTERVAL 7 DAY) {% endcondition %}
      #   THEN ${qty_sold}
      #   END ;;
  }



}
