view: dm_soh_in_transfer {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.dm_soh_in_transfer`
    ;;

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql: CONCAT(${item}, " - ", ${loc}) ;;
  }

  dimension: date_mc {
    type: date
    label:"Transfer create date"
    sql: ${TABLE}.date_mc ;;
  }

  measure: last_updated_date {
    type: date
    label: "Last refresh date"
    sql: MAX(${date_mc}) ;;
    convert_tz: no
  }

  dimension: area_name {
    type: string
    group_label: "Locations"
    label: "Area Name"
    sql: ${TABLE}.area_name ;;
  }

  dimension: atrb_boy_girl {
    type: string
    hidden: yes
    sql: ${TABLE}.atrb_boy_girl ;;
  }

  dimension: attrb_color {
    type: string
    group_label: "Products"
    label: "Color"
    sql: ${TABLE}.attrb_color ;;
  }

  dimension: attrb_made_of {
    type: string
    group_label: "Products"
    label: "Material"
    sql: ${TABLE}.attrb_made_of ;;
  }

  dimension: barcode {
    type: string
    group_label: "Products"
    label: "Barcode"
    sql: ${TABLE}.barcode ;;
  }

  dimension: brand {
    type: string
    group_label: "Products"
    label: "Brand"
    sql: ${TABLE}.brand ;;
  }

  dimension: business_unit {
    type: string
    sql: ${TABLE}.business_unit ;;
  }

  dimension: cancelled_qty_from {
    type: number
    hidden: yes
    sql: ${TABLE}.cancelled_qty_from ;;
  }

  dimension: cancelled_qty_to {
    type: number
    hidden: yes
    sql: ${TABLE}.cancelled_qty_to ;;
  }

  dimension: chain_name {
    type: string
    group_label: "Products"
    label: "Material"
    sql: ${TABLE}.chain_name ;;
  }

  dimension: channel_name {
    type: string
    group_label: "Locations"
    label: "Chain"
    sql: ${TABLE}.channel_name ;;
  }

  dimension: city {
    type: string
    hidden: yes
    sql: ${TABLE}.city ;;
  }

  dimension: class {
    type: number
    group_label: "Products"
    label: "Class ID"
    sql: ${TABLE}.class ;;
  }

  dimension: class_name {
    type: string
    group_label: "Products"
    label: "Class Name"
    sql: ${TABLE}.class_name ;;
  }

  dimension: cogs_last182_value_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.cogs_last182_value_sum ;;
  }
  measure: total_cost_of_goods_sold_182 {
    type: sum
 #   hidden: yes
    sql: ${cogs_last182_value_sum} ;;
    value_format_name: usd
  }
  measure: total_cost_of_goods_sold_90 {
    type: sum
  #  hidden: yes
    sql: ${cogs_last90_value_sum};;
    value_format_name: usd
  }
  measure: total_cost_of_goods_sold_30 {
    type: sum
 #   hidden: yes
    sql: ${cogs_last30_value_sum};;
    value_format_name: usd
  }
  measure: total_cost_of_goods_sold_365 {
    type: sum
 #   hidden: yes
    sql: ${cogs_last365_value_sum};;
    value_format_name: usd
  }
  dimension: cogs_last30_value_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.cogs_last30_value_sum ;;
  }

  dimension: cogs_last365_value_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.cogs_last365_value_sum ;;
  }

  dimension: cogs_last90_value_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.cogs_last90_value_sum ;;
  }

  dimension: country_desc {
    type: string
    group_label: "Locations"
    label: "Country Name"
    description: "Country Name i.e. United Arab Emirates"
    sql: ${TABLE}.country_desc ;;
  }

  dimension: country_id {
    type: string
    group_label: "Locations"
    description: "Country ID i.e. AE for United Arab Emirates"
    sql: ${TABLE}.country_id ;;
  }

  dimension: currency_code {
    type: string
    group_label: "Products"
    label: "Currency"
    sql: ${TABLE}.currency_code ;;
  }

  dimension: dept_name {
    type: string
    group_label: "Locations"
    label: "Department Name"
    sql: ${TABLE}.dept_name ;;
  }

  dimension: dept_no {
    type: number
    group_label: "Locations"
    label: "Department No."
    sql: ${TABLE}.dept_no ;;
  }

  dimension: dgr_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.dgr_ind ;;
  }

  dimension: district_name {
    type: string
    group_label: "Locations"
    sql: ${TABLE}.district_name ;;
  }

  dimension: distro_qty_from {
    type: number
    hidden: yes
    sql: ${TABLE}.distro_qty_from ;;
  }

  dimension: distro_qty_to {
    type: number
    hidden: yes
    sql: ${TABLE}.distro_qty_to ;;
  }

  dimension: division {
    type: string
    group_label: "Products"
    sql: ${TABLE}.division ;;
  }

  dimension: division_no {
    type: number
    group_label: "Products"
    sql: ${TABLE}.division_no ;;
  }

  dimension: entity_type {
    type: string
    hidden: yes
    sql: ${TABLE}.entity_type ;;
  }

  dimension: fill_qty_from {
    type: number
    hidden: yes
    sql: ${TABLE}.fill_qty_from ;;
  }

  dimension: fill_qty_to {
    type: number
    hidden: yes
    sql: ${TABLE}.fill_qty_to ;;
  }

  dimension: gender {
    type: string
    group_label: "Products"
    sql: ${TABLE}.gender ;;
  }

  dimension: grey_mkt_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.grey_mkt_ind ;;
  }

 dimension: item {
    type: string
  group_label: "Products"
  label: "Product ID"
    sql: ${TABLE}.item ;;
  }

  dimension: item_desc {
    type: string
    group_label: "Products"
    label: "Product Description"
    sql: ${TABLE}.item_desc ;;
  }

  dimension: item_style {
    type: string
    group_label: "Products"
    label: "Product Style"
    sql: ${TABLE}.item_style ;;
  }

  dimension: line {
    type: string
    group_label: "Products"
    sql: ${TABLE}.line ;;
  }

  dimension: loc {
    type: number
    group_label: "Locations"
    label: "Location Number"
    sql: ${TABLE}.loc;;
    value_format: "0"
  }

  dimension: loc_name {
    type: string
    group_label: "Locations"
    label: "Location Name"
    sql: ${TABLE}.loc_name ;;
  }

  dimension: mall_name {
    type: string
    group_label: "Locations"
    label: "Mall Name"
    sql: ${TABLE}.mall_name ;;
  }

  dimension: module {
    type: string
    hidden: yes
    sql: ${TABLE}.module ;;
  }

  dimension: physical_wh {
    type: number
    hidden: yes
    sql: ${TABLE}.physical_wh ;;
  }

  dimension: received_qty_from {
    type: number
    hidden: yes
    sql: ${TABLE}.received_qty_from ;;
  }

  dimension: received_qty_to {
    type: number
    hidden: yes
    sql: ${TABLE}.received_qty_to ;;
  }

measure: total_received_qty_to {
  label: "Received qty to location"
  type: sum
  sql: ${received_qty_to} ;;
}
  measure: total_received_qty_from {
    label: "Received qty from location"
    type: sum
    sql: ${received_qty_from} ;;
  }

  dimension: reconciled_qty_from {
    type: number
    hidden: yes
    sql: ${TABLE}.reconciled_qty_from ;;
  }

  dimension: reconciled_qty_to {
    type: number
    hidden: yes
    sql: ${TABLE}.reconciled_qty_to ;;
  }

  dimension: recurrence {
    type: string
    hidden: yes
    sql: ${TABLE}.recurrence ;;
  }

  dimension: region_name {
    type: string
    group_label: "Locations"
    description: "Region Name coming from Oracle RMS"
    sql: ${TABLE}.region_name ;;
  }

  dimension: sales_last182_qty_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_last182_qty_sum ;;
  }

  dimension: sales_last182_value_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_last182_value_sum ;;
  }

  dimension: sales_last30_qty_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_last30_qty_sum ;;
  }

  dimension: sales_last30_value_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_last30_value_sum ;;
  }

  dimension: sales_last365_qty_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_last365_qty_sum ;;
  }

  dimension: sales_last365_value_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_last365_value_sum ;;
  }

  dimension: sales_last90_qty_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_last90_qty_sum ;;
  }

  dimension: sales_last90_value_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_last90_value_sum ;;
  }

  dimension: sap_item_code {
    type: string
    hidden: yes
    sql: ${TABLE}.sap_item_code ;;
  }

  dimension: season_desc {
    type: string
    group_label: "Products"
    description: "Season attribute from Oracle RMS"
    label: "Season"
    sql: ${TABLE}.season_desc ;;
  }

  dimension: selected_qty_from {
    type: number
    hidden: yes
    sql: ${TABLE}.selected_qty_from ;;
  }

  dimension: selected_qty_to {
    type: number
    hidden: yes
    sql: ${TABLE}.selected_qty_to ;;
  }

  dimension: sep_category {
    hidden: yes
    sql: ${TABLE}.sep_category ;;
  }

  dimension: ship_qty_from {
    type: number
    hidden: yes
    sql: ${TABLE}.ship_qty_from ;;
  }

  dimension: ship_qty_to {
    type: number
    hidden: yes
    sql: ${TABLE}.ship_qty_to ;;
  }

  measure: total_ship_qty_to {
    type: sum
    label: "Shipped qty to location"
    sql: ${ship_qty_to} ;;
  }
  measure: total_ship_qty_from {
    type: sum
    label: "Shipped qty from location"
    sql: ${ship_qty_from} ;;
  }

  dimension: size_uda {
    type: string
    hidden: yes
    sql: ${TABLE}.size_uda ;;
  }

  dimension: standard_uom {
    type: string
    hidden: yes
    sql: ${TABLE}.standard_uom ;;
  }

  dimension: state {
    type: string
    hidden: yes
    sql: ${TABLE}.state ;;
  }

  dimension: stock_on_hand {
    type: number
    hidden: yes
    sql: ${TABLE}.stock_on_hand ;;
  }
  measure: total_stock_on_hand {
    type: sum
    hidden: yes
    sql: ${stock_on_hand} ;;
    value_format_name: usd
  }
  dimension: store_format {
    type: number
    hidden: yes
    sql: ${TABLE}.store_format ;;
  }

  dimension: sub_line {
    type: string
    group_label: "Products"
    sql: ${TABLE}.sub_line ;;
  }

  dimension: subclass {
    type: number
    group_label: "Products"
    label: "Sub Class No"
    description: "Oracle RMS product subclass no"
    sql: ${TABLE}.subclass ;;
  }

  dimension: subclass_name {
    type: string
    group_label: "Products"
    label: "Sub Class Name"
    description: "Oracle RMS product subclass name"
    sql: ${TABLE}.subclass_name ;;
  }

  dimension: sup_class {
    type: string
    hidden: yes
    sql: ${TABLE}.sup_class ;;
  }

  dimension: sup_subclass {
    type: string
    hidden: yes
    sql: ${TABLE}.sup_subclass ;;
  }

  dimension: taxo_class {
    type: string
    group_label: "Products"
    label: "Taxonomy Class"
    description: "Taxonomy Class from Oracle RMS"
    sql: ${TABLE}.taxo_class ;;
  }

  dimension: taxo_subclass {
    type: string
    group_label: "Products"
    label: "Taxonomy Sub Class"
    description: "Taxonomy Sub Class from Oracle RMS"
    sql: ${TABLE}.taxo_subclass ;;
  }

  dimension: tsf_qty_from {
    type: number
    sql: ${TABLE}.tsf_qty_from ;;
  }

  dimension: tsf_qty_to {
    type: number
    sql: ${TABLE}.tsf_qty_to ;;
  }
  measure: total_tsf_qty_from {
    type: sum
    label: "Transferred qty from location"
    sql: ${tsf_qty_from} ;;
  }

  measure: total_tsf_qty_to {
    type: sum
    label: "Transferred qty to location"
    sql: ${tsf_qty_to} ;;
  }

  dimension: uda_16_attribute {
    type: string
    hidden: yes
    sql: ${TABLE}.uda_16_attribute ;;
  }

  dimension: uda_zone {
    type: string
    hidden: yes
    sql: ${TABLE}.uda_zone ;;
  }

  dimension: unit_cost_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.unit_cost_usd ;;
  }
measure: unit_cost_usd_sum {
  type: sum
  label: "Unit cost USD"
  sql: ${unit_cost_usd} ;;
  value_format: "$#,##0.00"
}
  dimension: usage_specificity {
    type: string
    hidden: yes
    sql: ${TABLE}.usage_specificity ;;
  }

  dimension: vpn {
    type: string
    label: "VPN"
    group_label: "Products"
    sql: ${TABLE}.vpn ;;
  }

  measure: last_day_stock_value_without_tsf {
    type: sum
    hidden: no
    group_label: "Stock Metrics"
    label: "Last Day closing SOH USD Value"
    value_format_name: usd
    sql: ${unit_cost_usd} * ${stock_on_hand};;
  }

  measure: last_day_stock_value_with_tsf {
    type: number
    hidden: no
    group_label: "Stock Metrics"
    label: "Last Day closing SOH with transfers USD Value"
    value_format_name: usd
    sql: sum(${unit_cost_usd} * ifnull(${stock_on_hand},0))+sum(${unit_cost_usd}*ifnull(${ship_qty_to},0))-sum(${unit_cost_usd}*ifnull(${received_qty_to},0));;
  }
  measure: stock_cover_1MTHS {
    type: number
    hidden: no
    label: "Stock Cover with transfers on 1 month"
    description: "Stock Cover on 1 months historical sales"
    group_label: "Stock Cover with transfers"
    sql: 1.0* ${last_day_stock_value_with_tsf}/NULLIF((${total_cost_of_goods_sold_30}),0);;
    value_format_name: decimal_1
  }
  measure: stock_cover_3MTHS {
    type: number
    hidden: no
    label: "Stock Cover with transfers on 3 months"
    description: "Stock Cover on 3 months historical sales"
    group_label: "Stock Cover with transfers"
    sql: 1.0* ${last_day_stock_value_with_tsf}/NULLIF((${total_cost_of_goods_sold_90}),0);;
    value_format_name: decimal_1
  }
  measure: stock_cover_6MTHS {
    type: number
    hidden: no
    label: "Stock Cover with transfers on 6 months"
    description: "Stock Cover on 6 months historical sales"
    group_label: "Stock Cover with transfers"
    sql: 1.0* ${last_day_stock_value_with_tsf}/NULLIF((${total_cost_of_goods_sold_182}),0);;
    value_format_name: decimal_1
  }
  measure: stock_cover_1YEAR {
    type: number
    hidden: no
    label: "Stock Cover with transfers on 1 year"
    description: "Stock Cover on 1 year historical sales"
    group_label: "Stock Cover with transfers"
    sql: 1.0* ${last_day_stock_value_with_tsf}/NULLIF((${total_cost_of_goods_sold_365}),0);;
    value_format_name: decimal_1
  }
}
