view: targets_level_brand {
  derived_table: {
    sql:
with subtable AS(
  SELECT
    * EXCEPT(str_target, type)
    ,null as str_target_fp
    ,str_target as str_target_md
  FROM
    `chb-prod-stage-oracle.prod_supply_chain.targets_level_brand`
  WHERE
    type ="MD"
 UNION ALL
   SELECT
    * EXCEPT(str_target, type)
    ,str_target as str_target_fp
    ,null as str_target_md
  FROM
    `chb-prod-stage-oracle.prod_supply_chain.targets_level_brand`
  WHERE
    type ="FP"
 )
 SELECT
 year
 ,weeknum
 ,brand
 ,zone
 ,bu_code
 ,season
 ,avg(str_target_fp) as str_target_fp
 ,avg(str_target_md) as str_target_md
 FROM subtable
 GROUP BY
   year
 ,weeknum
 ,brand
 ,zone
 ,bu_code
 ,season
;;
  }

  dimension: pk {
    primary_key: yes
    type: string
    hidden: yes
    sql: CONCAT(${year}, " - "${weeknum}, " - "${bu_code}, " - "${brand}, " - "${zone}, " - "${loc_season}) ;;
  }

  dimension: brand {
    type: string
    hidden: yes
    sql: ${TABLE}.brand ;;
  }

  dimension: loc_season {
    type: string
    hidden: yes
    sql: ${TABLE}.season ;;
  }

  dimension: col_type {
    type: string
    hidden: yes
    sql: "sale";;
  }

  dimension: bu_code {
    type: string
    sql: ${TABLE}.bu_code ;;
  }

  dimension: str_target_fp {
    type: number
    hidden: yes
    sql: ${TABLE}.str_target_fp ;;
  }

  dimension: str_target_md {
    type: number
    hidden: yes
    sql: ${TABLE}.str_target_md ;;
  }


  dimension: weeknum {
    type: number
    hidden: yes
    sql: ${TABLE}.weeknum ;;
  }

  dimension: year {
    type: number
    hidden: yes
    sql: ${TABLE}.year ;;
  }

  dimension: zone {
    type: string
    hidden: yes
    sql: ${TABLE}.zone ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: []
  }
}
