view: sales_grading {
  sql_table_name: chb-prod-supplychain-data.prod_shared_dimensions.dim_sales_grading ;;

  dimension: accum_item {
    type: number
    hidden: yes
    sql: ${TABLE}.accum_item ;;
  }

  dimension: accum_store {
    type: number
    hidden: yes
    sql: ${TABLE}.accum_store ;;
  }

  dimension: bk_productid {
    type: string
    hidden: yes
    sql: ${TABLE}.bk_productid ;;
  }

  dimension: bk_storeid {
    type: number
    hidden: yes
    value_format_name: id
    sql: ${TABLE}.bk_storeid ;;
  }

  dimension: mea_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.mea_amountusd ;;
  }

  dimension: month {
    type: number
    hidden: yes
    sql: ${TABLE}.month ;;
  }

  dimension: pareto_grade {
    type: string
    view_label: "Sales and Stock Information"
    group_label: "Products"
    sql: COALESCE(${TABLE}.pareto_grade,'Z') ;;
  }

  dimension: accum_share {
    type: number
    hidden: yes
    sql: ${TABLE}.accum_share ;;
  }

  dimension: share {
    type: number
    hidden: yes
    sql: ${TABLE}.share ;;
  }

  dimension: year {
    type: number
    hidden: yes
    sql: ${TABLE}.year ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: []
  }
}
