view: pg_product_grading {
  derived_table: {
    sql: WITH TOT_MONTHS_NON_NV AS (select sku_externalcode, min(dateop) AS first_transaction,DATE_DIFF(CURRENT_DATE(),min(dateop),MONTH) as history
      from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_sales_at_time_interval` sales
      where sku_externalcode like 'AAP%'
      group by 1
      having history != 0 ),

      NOV_SALES AS (
      select  sku_externalcode, sum(totalnet_usd) as total_sales
      from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_sales_at_time_interval` sales
      where sku_externalcode like IFNULL({% parameter Season %}, "11P%")
      group by 1
      ),

      NON_NV_12MNT_SALES AS (
      select  sku_externalcode,MIN(sales.dateop), sum(totalnet_usd) as total_sales
      from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_sales_at_time_interval` sales
      where sku_externalcode in (
      select distinct sku_externalcode
      from TOT_MONTHS_NON_NV
      where history >= 12
      ) and sales.dateop >= DATE_SUB(CURRENT_DATE(), INTERVAL 365 DAY)
      group by 1
      ),

      NON_NV_no12MNT_SALES AS (
      select  sales.sku_externalcode,history,MIN(sales.dateop), sum(totalnet_usd) as sales, ((sum(totalnet_usd)/history)*(12-history)+sum(totalnet_usd)) as total_sales
      from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_sales_at_time_interval` sales
      left join (select distinct sku_externalcode,history
                from TOT_MONTHS_NON_NV) b
            on  sales.sku_externalcode = b.sku_externalcode
      where sales.sku_externalcode in (
      select distinct sku_externalcode
      from TOT_MONTHS_NON_NV
      where history < 12
      ) and sales.dateop >= DATE_SUB(CURRENT_DATE(), INTERVAL 365 DAY)
      group by 1,2
      ),

      UNION_DATA AS (
      select distinct * from (
      select a.sku_externalcode,a.total_sales,"Novelty" as product_category
      from NOV_SALES a
      UNION ALL
      select b.sku_externalcode,b.total_sales,"Non-Novelty" as product_category
      from NON_NV_12MNT_SALES b
      UNION ALL
      select c.sku_externalcode,c.total_sales,"Non-Novelty" as product_category
      from NON_NV_no12MNT_SALES c )),

      RUNNING_SALES AS (
      SELECT distinct sku_externalcode,product_category,total_sales,
             SUM(total_sales) OVER(PARTITION BY product_category ORDER BY total_sales desc) AS cum_sales
             FROM UNION_DATA
             order by 2,3 desc)

      select a.*,b.grand_total,(a.cum_sales/b.grand_total)*100 as sales_contribution
            from RUNNING_SALES a
      left join (select product_category,max(cum_sales) as grand_total from RUNNING_SALES group by 1) b
      on a.product_category = b.product_category
 ;;
  }

  parameter: Season {
    label: "Season- Product grading"
    type: string
    allowed_value: {
      label: "SS21"
      value: "11P%"
    }
    allowed_value: {
      label: "FW21"
      value: "12P%"
    }
    allowed_value: {
      label: "SS22"
      value: "21P%"
    }
    allowed_value: {
      label: "FW22"
      value: "22P%"
    }

    default_value: "11P%"
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: sku_externalcode {
    type: string
    hidden: yes
    sql: ${TABLE}.sku_externalcode ;;
  }

  dimension: product_category {
    type: string
    hidden: yes
    sql: ${TABLE}.product_category ;;
  }

  dimension: total_sales {
    type: number
    hidden: yes
    sql: ${TABLE}.total_sales ;;
  }

  dimension: cum_sales {
    type: number
    hidden: yes
    sql: ${TABLE}.cum_sales ;;
  }

  dimension: grand_total {
    type: number
    hidden: yes
    sql: ${TABLE}.grand_total ;;
  }

  dimension: sales_contribution {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_contribution ;;
  }

  dimension: product_grade {
    type: string
    sql:  case when ${TABLE}.sales_contribution <= 70 then 'A Grade'
              when  ${TABLE}.sales_contribution >70 and ${TABLE}.sales_contribution <=90 then 'B Grade'
              when ${TABLE}.sales_contribution > 90 and ${TABLE}.sales_contribution <=100 and ${TABLE}.total_sales !=0 then 'C Grade'
              else 'Z Grade' end;;

    }

    set: detail {
      fields: [
        sku_externalcode,
        product_category,
        total_sales,
        cum_sales,
        grand_total,
        sales_contribution
      ]
    }
  }
