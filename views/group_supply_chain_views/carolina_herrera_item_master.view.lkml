view: carolina_herrera_item_master {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_item_master`
    ;;

  dimension: alias {
    type: string
    sql: ${TABLE}.alias ;;
  }

  dimension: bl {
    type: string
    sql: ${TABLE}.bl ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: ean13 {
    type: string
    sql: ${TABLE}.ean13 ;;
  }

  dimension: family {
    type: string
    sql: ${TABLE}.family ;;
  }

  dimension: group {
    type: string
    sql: ${TABLE}.`group` ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: sizes {
    type: string
    sql: ${TABLE}.sizes ;;
  }

  dimension: sku_code {
    type: string
    sql: ${TABLE}.sku_code ;;
  }

  dimension: store {
    type: number
    sql: ${TABLE}.store ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  measure: count {
    type: count
    drill_fields: [name]
  }
}
