include: "period_over_period_gsc.view"
view: carolina_herrera_theoretical_stock {
  derived_table:{
      sql:  select
                  case when stocktbl.stockdate is null then sales.dateop else stocktbl.stockdate end as stockdate,
                  case when stocktbl.country is null then sales.country else stocktbl.country end as country,
                  case when stocktbl.store is null then sales.store else stocktbl.store end as store,
                  case when stocktbl.store_code is null then sales.store_code else stocktbl.store_code end as store_code,
                  case when stocktbl.store_code is null then sales.store_code else stocktbl.store_code end as idstore,
                  case when stocktbl.idarticlesku is null then sales.idarticlesku else stocktbl.idarticlesku end as idarticlesku,
                  case when stocktbl.sku_externalcode is null then sales.sku_externalcode else stocktbl.sku_externalcode end as sku_externalcode,
                  case when stocktbl.ref_aliasname is null then sales.aliasname else stocktbl.ref_aliasname end as ref_aliasname,
                  stock,
                  availablestock,
                  reservedstock,
                  transitstock,
                  tarestock
                  ,DATE_ADD(stockdate,INTERVAL -1 DAY) as updated_stockdate,
                  LEAD(availablestock)
                  OVER(PARTITION BY stocktbl.store_code,stocktbl.sku_externalcode ORDER BY stocktbl.stockdate) - (availablestock-IFNULL(quantity,0)) as received_stock,
                  case when availablestock >=1 then 1 else 0 end as is_trading_day,
                  sales.quantity,
                  sales.total_local,
                  sales.totalnet_local,
                  sales.total_usd,
                  sales.totalnet_usd
            from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_theoretical_stock` stocktbl
            FULL OUTER JOIN
            (select
                    store,
                    store_code,
                    dateop,
                    sku_externalcode,
                    country,
                    idarticlesku,
                    aliasname,
                    sum(quantity) as quantity,
                    sum(total) as total_local,
                    sum(total_usd) as total_usd,
                    sum(totalnet) as totalnet_local,
                    sum(totalnet_usd) as totalnet_usd
            FROM `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_sales_at_time_interval`
            group by 1,2,3,4,5,6,7) sales

            on cast(stocktbl.store as string) = cast(sales.store as string)
            and stocktbl.sku_externalcode = sales.sku_externalcode
            and stocktbl.stockdate = sales.dateop


            order by store,sku_externalcode,stockdate;;
    }

  extends: [period_over_period_gsc]

  dimension_group: pop_no_tz {
    sql: ${TABLE}.stockdate ;;
  }

  dimension: pk {
    type: string
    hidden: yes
    sql: ${store}+"-"+${sku_externalcode}+"-"+${stock_date} ;;

  }

  dimension: availablestock {
    type: number
    hidden: yes
    sql: ${TABLE}.availablestock ;;
  }


  dimension: out_of_stock_flag{
    type: number
    hidden: no
    sql: case when ${TABLE}.availablestock >0 then 0 else 1 end ;;
  }

  dimension: receivedstock {
    type: number
    hidden: yes
    sql: ${TABLE}.received_stock ;;
  }

  dimension: expected_latest_stockdate {
    type: date
    hidden: no
    sql: CURRENT_DATE() ;;
  }

  dimension: country {
    type: string
    hidden: no
    label: "Country Name"
    map_layer_name: countries
    drill_fields: [store_code]
    sql: ${TABLE}.country ;;
  }

  dimension: idarticlesku {
    type: number
    hidden: yes
    sql: ${TABLE}.idarticlesku ;;
  }

  dimension: store_type {
    type: string
    label: "Store Type"
    hidden: no
    sql: case when ${TABLE}.store_code like 'C%' then 'CH' else 'PG' end ;;
  }

  dimension: idstore {
    type: number
    hidden: yes
    sql: ${TABLE}.idstore ;;
  }

  dimension: ref_aliasname {
    type: string
    hidden: no
    label: "Alias Name"
    sql: ${TABLE}.ref_aliasname ;;
  }

  dimension: reservedstock {
    type: number
    sql: ${TABLE}.reservedstock ;;
  }

  dimension: sku_externalcode {
    type: string
    label: "SKU Code"
    sql: ${TABLE}.sku_externalcode ;;
  }

  dimension: sku_externalcode_wo_size {
    type: string
    label: "SKU Code w/o size"
    sql: left(${TABLE}.sku_externalcode,13) ;;
  }

  dimension: sku_size {
    type: string
    group_label: "Product"
    label: "SKU Size"
    drill_fields: [sku_externalcode]
    sql:SUBSTR(${TABLE}.sku_externalcode,14)   ;;
  }

  dimension: sku_url {
    type: string
    group_label: "Product"
    label: "SKU URL"
    sql: case when ${sku_externalcode} like 'A%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/ATMP/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"/","1500/"),
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"_01.jpg"))
           when ${sku_externalcode} like '1%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2021/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"/","1500/"),
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"_01.jpg"))
           when ${sku_externalcode} like '0%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2020/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"/","1500/"),
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"_01.jpg"))
           when ${sku_externalcode} like '9%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2019/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"/","1500/"),
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"_01.jpg"))
           when ${sku_externalcode} like '8%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2018/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"/","1500/"),
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"_01.jpg"))
           when ${sku_externalcode} like '7%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2017/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"/","1500/"),
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"_01.jpg"))
           when ${sku_externalcode} like '6%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2016/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"/","1500/"),
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"_01.jpg"))
           when ${sku_externalcode} like '5%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2015/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"/","1500/"),
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"_01.jpg"))
           when ${sku_externalcode} like '4%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2014/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"/","1500/"),
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"_01.jpg"))
           when ${sku_externalcode} like '3%' then CONCAT("https://cdn.chcarolinaherrera.com/wcscontent/photos/CH/2013/",
                                                      LEFT(${sku_externalcode},2),"/",SUBSTR(${sku_externalcode},3,2),"/",
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"/","1500/"),
                                                      CONCAT(SUBSTR(${sku_externalcode},1,13),"_01.jpg"))
          else "https://vcunited.club/no-image-available-2-jpg/" end;;
  }

  dimension: season {
    type: string
    group_label: "Product"
    label: "Season"
    sql:  case when ${sku_externalcode} like 'A%' then "Non-Seasonal"
          when ${sku_externalcode} like '12%' then "FW21"
          when ${sku_externalcode} like '11%' then "SS21"
          when ${sku_externalcode} like '01%' then "SS20"
          when ${sku_externalcode} like '02%' then "FW20"
          when ${sku_externalcode} like '91%' then "SS19"
          when ${sku_externalcode} like '92%' then "FW19"
          when ${sku_externalcode} like '81%' then "SS18"
          when ${sku_externalcode} like '82%' then "FW18"

          else " Pre FW18" end;;
  }

  dimension: sku_externalcode2 {
    type: string
    group_label: "Product"
    label: "SKU Code Image"
    sql: ${sku_url} ;;
    html: <img src="{{ value }}" width="200" height="200"/>  ;;
  }

  dimension: ref_code2 {
    type: string
    label: "Refcode2"
    hidden: no
    sql: SUBSTR(${TABLE}.sku_externalcode,3,4) ;;
  }

  dimension: total_stock {
    type: number
    sql: ${TABLE}.stock ;;
  }

  dimension_group: stock {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.stockdate ;;
  }

  dimension: ltl_till_date {
    type: string
    view_label: "Like to Like Timeframe comparison"
    label: "LTL Timeframe"
    description: "For Like to like timeframe comparison"
    sql: CASE WHEN EXTRACT(MONTH FROM ${TABLE}.updated_stockdate) > EXTRACT(MONTH FROM CURRENT_TIMESTAMP) THEN "NO"
              WHEN EXTRACT(MONTH FROM ${TABLE}.updated_stockdate) < EXTRACT(MONTH FROM CURRENT_TIMESTAMP) THEN "YES"
              WHEN EXTRACT(MONTH FROM ${TABLE}.updated_stockdate) = EXTRACT(MONTH FROM CURRENT_TIMESTAMP) AND
              EXTRACT(DAY FROM ${TABLE}.updated_stockdate) >= EXTRACT(DAY FROM CURRENT_TIMESTAMP) THEN "NO"
              ELSE "YES" END;;
  }


  dimension: store {
    type: number
    label: "Location Code"
    sql: ${TABLE}.store ;;
  }

  dimension: store_code {
    type: string
    label: "Location Code 2"
    sql: ${TABLE}.store_code ;;
  }

  dimension: tarestock {
    type: number
    sql: ${TABLE}.tarestock ;;
  }

  dimension: transitstock {
    type: number
    sql: ${TABLE}.transitstock ;;
  }

  dimension: is_trading_day {
    type: number
    sql: ${TABLE}.is_trading_day ;;
  }

  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }

  dimension: total_local {
    type: number
    sql: ${TABLE}.total_local ;;
  }

  dimension: totalnet_local {
    type: number
    sql: ${TABLE}.totalnet_local ;;
  }

  dimension: total_usd {
    type: number
    sql: ${TABLE}.total_usd ;;
  }

  dimension: totalnet_usd {
    type: number
    sql: ${TABLE}.totalnet_usd ;;
  }
  dimension: last_day_of_month {
    hidden: no
    type: yesno
    sql: EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1 ;;
  }

  dimension: latest_date {
    hidden: no
    type: yesno
    sql: ${stock_date}=${expected_latest_stockdate} ;;
  }


  dimension: day_of_week {
    hidden: no
    type: number
    sql: EXTRACT(DAYOFWEEK from  ${stock_date}) ;;
  }

#Total stocks
  measure: available_stock {
    type: sum
    group_label: "Stock information"
    label: "Available Stock"
    sql: ${availablestock} ;;
  }

  measure: received_stock {
    type: sum
    group_label: "Stock information"
    label: "Received Stock"
    sql: ${receivedstock} ;;
  }

  measure: avg_available_stock {
    type: number
    group_label: "Stock information"
    label: "Avg. Available Stock"
    sql: SAFE_DIVIDE(SUM(${availablestock}),SUM(${is_trading_day})) ;;
  }

  measure: transit_stock {
    type: sum
    group_label: "Stock information"
    label: "In Transit Stock"
    sql: ${transitstock} ;;
  }

  measure: avg_transit_stock {
    type: average
    group_label: "Stock information"
    label: "Avg. In Transit Stock"
    sql: ${transitstock} ;;
  }
  measure: tare_stock {
    type: sum
    group_label: "Stock information"
    label: "Tare Stock"
    sql: ${tarestock} ;;
  }

  measure: avg_tare_stock {
    type: average
    group_label: "Stock information"
    label: "Avg. Tare Stock"
    sql: ${tarestock} ;;
  }

  measure: reserved_stock {
    type: sum
    group_label: "Stock information"
    label: "Reserved Stock"
    sql: ${reservedstock} ;;
  }

 measure: avg_reserved_stock {
    type: average
    group_label: "Stock information"
    label: "Avg. Reserved Stock"
    sql: ${reservedstock} ;;
  }

  measure: stock_total {
    type: sum
    group_label: "Stock information"
    label: "Total Stock"
    sql: ${total_stock} ;;
  }

#Total stocks - Last day closing
  measure: available_stock_latest {
    type: sum
    group_label: "Stock information"
    label: "Last Day Available Stock"
    filters: {
      field: latest_date
      value: "yes"
    }
    sql: ${availablestock} ;;
  }

  measure: transit_stock_latest {
    type: sum
    group_label: "Stock information"
    label: "Last Day In Transit Stock"
    filters: {
      field: latest_date
      value: "yes"
    }
    sql: ${transitstock} ;;
  }

  measure: tare_stock_latest {
    type: sum
    group_label: "Stock information"
    label: "Last Day Tare Stock"
    filters: {
      field: latest_date
      value: "yes"
    }
    sql: ${tarestock} ;;
  }

  measure: reserved_stock_latest {
    type: sum
    group_label: "Stock information"
    label: "Last Day Reserved Stock"
    filters: {
      field: latest_date
      value: "yes"
    }
    sql: ${reservedstock} ;;
  }

  measure: stock_total_latest {
    type: sum
    group_label: "Stock information"
    label: "Last Day Total Stock"
    filters: {
      field: latest_date
      value: "yes"
    }
    sql: ${total_stock} ;;
  }

#Total stocks - Weekly closing
  measure: available_stock_weekly {
    type: sum
    group_label: "Stock information"
    label: "Week Close Available Stock"
    filters: {
      field: day_of_week
      value: "7"
    }
    sql: ${availablestock} ;;
  }

  measure: transit_stock_weekly {
    type: sum
    group_label: "Stock information"
    label: "Week close In Transit Stock"
    filters: {
      field: day_of_week
      value: "7"
    }
    sql: ${transitstock} ;;
  }

  measure: tare_stock_weekly {
    type: sum
    group_label: "Stock information"
    label: "Week close Tare Stock"
    filters: {
      field: day_of_week
      value: "7"
    }
    sql: ${tarestock} ;;
  }

  measure: reserved_stock_weekly {
    type: sum
    group_label: "Stock information"
    label: "Week Close Reserved Stock"
    filters: {
      field: day_of_week
      value: "7"
    }
    sql: ${reservedstock} ;;
  }

  measure: stock_total_Weekly {
    type: sum
    group_label: "Stock information"
    label: "Week Close Total Stock"
    filters: {
      field: day_of_week
      value: "7"
    }
    sql: ${total_stock} ;;
  }

#Total stocks - Weekly closing
  measure: available_stock_monthly {
    type: sum
    group_label: "Stock information"
    label: "Month Close Available Stock"
    filters: {
      field: last_day_of_month
      value: "yes"
    }
    sql: ${availablestock} ;;
  }

  measure: transit_stock_monthly {
    type: sum
    group_label: "Stock information"
    label: "Month close In Transit Stock"
    filters: {
       field: last_day_of_month
      value: "yes"
    }
    sql: ${transitstock} ;;
  }

  measure: tare_stock_monthly {
    type: sum
    group_label: "Stock information"
    label: "Month close Tare Stock"
    filters: {
       field: last_day_of_month
      value: "yes"
    }
    sql: ${tarestock} ;;
  }

  measure: reserved_stock_monthly {
    type: sum
    group_label: "Stock information"
    label: "Month Close Reserved Stock"
    filters: {
      field: last_day_of_month
      value: "yes"
    }
    sql: ${reservedstock} ;;
  }

  measure: stock_total_Monthly {
    type: sum
    group_label: "Stock information"
    label: "Month Close Total Stock"
    filters: {
       field: last_day_of_month
      value: "yes"
    }
    sql: ${total_stock} ;;
  }


  measure: quantity_sold {
    type: sum
    group_label: "Stock information"
    label: "Quantity Sold"
    description: "Total Sold"
    sql: ${quantity};;
  }

  measure: totalnet_local_value {
    type: sum
    group_label: "Stock information"
    label: "Total Net Sales Local"
    description: "Total Sold value in Local Currency"
    sql: ${totalnet_local};;
  }


  measure: totalnet_usd_value {
    type: sum
    group_label: "Stock information"
    label: "Total Net Sales USD"
    description: "Total Sold value in USD"
    sql: ${totalnet_usd};;
    value_format_name: usd_0
  }

  measure: total_trading_days {
    type: sum
    group_label: "Stock information"
    label: "Total trading days"
    description: "Total days in a period when the stock was available in store"
    sql: ${is_trading_day};;
  }

  measure: rate_of_sales {
    type: number
    group_label: "Stock information"
    label: "Rate of Sales"
    description: "Per week sales Quantity for selected period"
    sql: SAFE_DIVIDE(SUM(${quantity}),SUM(${is_trading_day}))*7 ;;
    value_format_name: decimal_2
  }

  measure: distinct_skus{
    type: count_distinct
    group_label: "Stock information"
    label: "Distinct SKU count"
    description: "Distinct SKUs carried by a store"
    sql: ${sku_externalcode} ;;
  }

  measure: distinct_skus_oos{
    type: count_distinct
    group_label: "Stock information"
    label: "Total SKU count - out of stock"
    description: "Count of SKUs that were out of stock"
    filters: {
      field: availablestock
      value: "0"
    }
    sql: ${sku_externalcode} ;;
  }

  #--------Weighted OSA for CH--------------#

  measure: count_skus_grade_a{
    type: count
    group_label: "Out of Stock info"
    hidden: no
    filters: [ch_product_grading.product_grade : "A Grade" ]
    sql: ${sku_externalcode} ;;
  }

  measure: count_skus_oos_grade_a{
    type: count
    hidden: no
    group_label: "Out of Stock info"
    filters:  [availablestock: "0", ch_product_grading.product_grade : "A Grade" ]
    sql: ${sku_externalcode} ;;
  }

measure: count_skus_grade_b{
  type: count
  group_label: "Out of Stock info"
  hidden: no
  filters: [ch_product_grading.product_grade : "B Grade" ]
  sql: ${sku_externalcode} ;;
}

measure: count_skus_oos_grade_b{
  type: count
  hidden: no
  group_label: "Out of Stock info"
  filters:  [availablestock: "0", ch_product_grading.product_grade : "B Grade" ]
sql: ${sku_externalcode} ;;
}

measure: count_skus_grade_c{
  type: count
  group_label: "Out of Stock info"
  hidden: no
  filters: [ch_product_grading.product_grade : "C Grade" ]
  sql: ${sku_externalcode} ;;
}

measure: count_skus_oos_grade_c{
  type: count
  hidden: no
  group_label: "Out of Stock info"
  filters:  [availablestock: "0", ch_product_grading.product_grade : "C Grade" ]
sql: ${sku_externalcode} ;;
}


  measure: on_shelf_availability{
    type: number
    group_label: "Out of Stock info"
    label: "On-shelf availability"
    description: "Count of SKUs that were in stock/Total stocks held by store"
    sql: 1-(${distinct_skus_oos}/${distinct_skus}) ;;
    value_format_name: percent_1
  }

  measure: weighted_on_shelf_availability{
    type: number
    group_label: "Out of Stock info"
   hidden: no

    sql: 0.7*IFNULL((1-SAFE_DIVIDE(${count_skus_oos_grade_a},${count_skus_grade_a})),0) + 0.2*IFNULL((1-SAFE_DIVIDE(${count_skus_oos_grade_b},${count_skus_grade_b})),0)+0.1*IFNULL((1-SAFE_DIVIDE(${count_skus_oos_grade_c},${count_skus_grade_c})),0) ;;
    value_format_name: percent_1
  }

  #--------Weighted OSA for CH ends here--------------#

  #-----Weighted OSA for PG------------------#

  measure: pg_count_skus_grade_a{
    type: count
    group_label: "Out of Stock info"
    hidden: no
    filters: [pg_product_grading.product_grade : "A Grade" ]
    sql: ${sku_externalcode} ;;
  }

  measure: pg_count_skus_oos_grade_a{
    type: count
    hidden: no
    group_label: "Out of Stock info"
    filters:  [availablestock: "0", pg_product_grading.product_grade : "A Grade" ]
    sql: ${sku_externalcode} ;;
  }

  measure: pg_count_skus_grade_b{
    type: count
    group_label: "Out of Stock info"
    hidden: no
    filters: [pg_product_grading.product_grade : "B Grade" ]
    sql: ${sku_externalcode} ;;
  }

  measure: pg_count_skus_oos_grade_b{
    type: count
    hidden: no
    group_label: "Out of Stock info"
    filters:  [availablestock: "0", pg_product_grading.product_grade : "B Grade" ]
    sql: ${sku_externalcode} ;;
  }

  measure: pg_count_skus_grade_c{
    type: count
    group_label: "Out of Stock info"
    hidden: no
    filters: [pg_product_grading.product_grade : "C Grade" ]
    sql: ${sku_externalcode} ;;
  }

  measure: pg_count_skus_oos_grade_c{
    type: count
    hidden: no
    group_label: "Out of Stock info"
    filters:  [availablestock: "0", pg_product_grading.product_grade : "C Grade" ]
    sql: ${sku_externalcode} ;;
  }

  measure: pg_count_skus_grade_z{
    type: count
    group_label: "Out of Stock info"
    hidden: no
    filters: [pg_product_grading.product_grade : "Z Grade" ]
    sql: ${sku_externalcode} ;;
  }

  measure: pg_count_skus_oos_grade_z{
    type: count
    hidden: no
    group_label: "Out of Stock info"
    filters:  [availablestock: "0", pg_product_grading.product_grade : "Z Grade" ]
    sql: ${sku_externalcode} ;;
  }


  measure: pg_weighted_on_shelf_availability{
    type: number
    group_label: "Out of Stock info"
    hidden: no

    sql: 0.7*IFNULL((1-SAFE_DIVIDE(${pg_count_skus_oos_grade_a},${pg_count_skus_grade_a})),0) + 0.2*IFNULL((1-SAFE_DIVIDE(${pg_count_skus_oos_grade_b},${pg_count_skus_grade_b})),0)+0.1*IFNULL((1-SAFE_DIVIDE(${pg_count_skus_oos_grade_c},${pg_count_skus_grade_c})),0) ;;
    value_format_name: percent_1
  }

  #------PG OSA ends here---------


  measure: total_days_oos{
    type: sum
    group_label: "Stock information"
    label: "Total out of stock days"
    description: "Number of days for which the item was out of stock in a period"
    sql: ${out_of_stock_flag} ;;
    value_format_name: decimal_0
  }


  measure: sales_to_avg_stock {
    type: number
    group_label: "Stock information"
    label: "Sales to inventory ratio"
    sql: SAFE_DIVIDE(SAFE_DIVIDE(SUM(${quantity}),SUM(${is_trading_day})),${avg_available_stock}) ;;
    value_format_name: decimal_2
  }

  measure: total_days {
    type: count_distinct
    hidden: no
    label: "total days in period"
    sql: ${TABLE}.stockdate ;;
  }

  measure: total_days_available {
    type: count_distinct
    hidden: no
    label: "total available days in period"
    filters: {
      field: "availablestock"
      value: ">0"
    }
    sql: ${TABLE}.stockdate ;;
  }

  measure: total_stores {
    type: count_distinct
    hidden: no
    label: "total stores"
    sql: ${TABLE}.store ;;
  }


  measure: total_stores_available {
    type: count_distinct
    hidden: no
    label: "total stores unavailable on all days"
    filters: {
      field: "availablestock"
      value: "0"
    }
    sql: ${TABLE}.store ;;
  }


  #Sell through measures
  dimension: item_store_code {
    type: string
    hidden: yes
    sql: ${TABLE}.store_code  + '-' + ${TABLE}.sku_externalcode + '-' + ${TABLE}.stockdate ;;
  }

  measure: opening_stock {
    type: sum_distinct
    hidden: no
    group_label: "Sell through measures"
    label: "opening stock"
    sql_distinct_key: ${store_code} || ${sku_externalcode} ;;
    sql: ${sellthrough_ch_pg.opening_stock};;
  }

  measure: closing_stock {
    type: sum_distinct
    hidden: no
    group_label: "Sell through measures"
    label: "closing stock"
    sql_distinct_key: ${store_code} || ${sku_externalcode};;
    sql: ${sellthrough_ch_pg.closing_stock};;
  }

  measure: sellthrogh_qty {
    type: sum_distinct
    hidden: no
    group_label: "Sell through measures"
    label: "Quantity"
    sql_distinct_key: ${store_code} || ${sku_externalcode};;
    sql: ${sellthrough_ch_pg.total_quantity};;
  }

  measure: sellthrogh_qty_fp {
    type: sum_distinct
    hidden: no
    group_label: "Sell through measures"
    label: "Quantity FP"
    sql_distinct_key: ${store_code} || ${sku_externalcode};;
    sql: ${sellthrough_ch_pg.total_quantity_fp};;
  }

  measure: sellthrogh_qty_md {
    type: sum_distinct
    hidden: no
    group_label: "Sell through measures"
    label: "Quantity MD"
    sql_distinct_key: ${store_code} || ${sku_externalcode};;
    sql: ${sellthrough_ch_pg.total_quantity_md};;
  }

  measure: sellthrogh_totalnet_fp {
    type: sum_distinct
    hidden: no
    group_label: "Sell through measures"
    label: "Net Sales LC FP"
    sql_distinct_key: ${store_code} || ${sku_externalcode};;
    sql: ${sellthrough_ch_pg.totalnet_fp};;
  }

  measure: sellthrogh_totalnet_md {
    type: sum_distinct
    hidden: no
    group_label: "Sell through measures"
    label: "Net Sales LC MD"
    sql_distinct_key: ${store_code} || ${sku_externalcode};;
    sql: ${sellthrough_ch_pg.totalnet_md};;
  }

  measure: sellthrogh_totalnet_usd_fp {
    type: sum_distinct
    hidden: no
    group_label: "Sell through measures"
    label: "Net Sales USD FP"
    sql_distinct_key: ${store_code} || ${sku_externalcode};;
    sql: ${sellthrough_ch_pg.totalnet_usd_fp};;
  }

  measure: sellthrogh_totalnet_usd_md {
    type: sum_distinct
    hidden: no
    group_label: "Sell through measures"
    label: "Net Sales USD MD"
    sql_distinct_key: ${store_code} || ${sku_externalcode};;
    sql: ${sellthrough_ch_pg.totalnet_usd_md};;
  }


  measure: sellthrogh {
    type: number
    hidden: no
    group_label: "Sell through measures"
    label: "Sell Through Rate"
    sql: SAFE_DIVIDE(${sellthrogh_qty},(${sellthrogh_qty}+${closing_stock}));;
    value_format_name: percent_1
  }

  measure: sellthrogh_fp {
    type: number
    hidden: no
    group_label: "Sell through measures"
    label: "Sell Through Rate Full Price"
    sql: SAFE_DIVIDE(${sellthrogh_qty_fp},(${sellthrogh_qty}+${closing_stock}));;
    value_format_name: percent_1
  }

  measure: sellthrogh_md {
    type: number
    hidden: no
    group_label: "Sell through measures"
    label: "Sell Through Rate Markdown"
    sql: SAFE_DIVIDE(${sellthrogh_qty_md},(${sellthrogh_qty}+${closing_stock}));;
    value_format_name: percent_1
  }



  measure: count {
    type: count
    drill_fields: [ref_aliasname]
  }
}
