view: sell_through_on_stock {
  derived_table: {
    explore_source: fact_soh_sales_poc {
      column: last_date {}
      bind_all_filters: yes
    }
  }

dimension: last_date {
  type: date
}

}
