view: inv_adj {
  sql_table_name: `chb-prod-stage-oracle.prod_oracle_rms.inv_adj`
    ;;

  dimension_group: adj {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.adj_date ;;
  }

  dimension: adj_qty {
    type: number
    sql: ${TABLE}.adj_qty ;;
  }

  dimension: adj_weight {
    type: string
    sql: ${TABLE}.adj_weight ;;
  }

  dimension: adj_weight_uom {
    type: string
    sql: ${TABLE}.adj_weight_uom ;;
  }

  dimension: inv_status {
    type: number
    sql: ${TABLE}.inv_status ;;
  }

  dimension: item {
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: loc_type {
    type: string
    sql: ${TABLE}.loc_type ;;
  }

  dimension: location {
    type: number
    sql: ${TABLE}.location ;;
  }

  dimension: prev_qty {
    type: number
    sql: ${TABLE}.prev_qty ;;
  }

  dimension: reason {
    type: number
    sql:${TABLE}.reason ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }


}
