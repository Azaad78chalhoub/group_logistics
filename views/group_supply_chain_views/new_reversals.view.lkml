view: new_reversals {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.reversals`
    ;;



#   dimension: country{
#     group_label: "Locations"
#     description: "Country value from Oracle i.e. United Arab Emirates"
#     type: string
#     suggest_explore: dim_retail_loc_country_desc
#     suggest_dimension: dim_retail_loc_country_desc.country_desc
#     sql: ${TABLE}.dim_alternate_bu_hierarchy.country ;;
#   }
#
#   dimension: vertical{
#     group_label: "Locations"
#     label: "Vertical"
#     description: "Vertical attribute in Oracle RMS"
#     type: string
#     suggest_explore: dim_retail_loc_vertical
#     suggest_dimension: dim_retail_loc_vertical.vertical
#     sql: ${TABLE}.dim_alternate_bu_hierarchy.vertical ;;
#   }
#
#   dimension: business_unit{
#     group_label: "Locations"
#     description: "Business unit attribute in Oracle RMS"
#     type: string
#     suggest_explore: dim_retail_loc_bu
#     suggest_dimension: dim_retail_loc_bu.business_unit
#     sql: ${TABLE}.dim_alternate_bu_hierarchy.brand  ;;
#   }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }
  measure: last_updated_date {
    type: date
    label: "Last refresh date"
    sql: MAX(${date_raw}) ;;
    convert_tz: no
  }


  dimension: business_unit {
    type: string
    view_label: "BU Alternative Hierarchy"
    label: "Business Unit"
    description: "Brand Name from alternative hierarchy"
    sql: ${TABLE}.dim_alternate_bu_hierarchy.brand ;;
  }

  dimension: business_unit_access_filter {
    type: string
    hidden: yes
    sql: CASE when ${business_unit} is null then 'x' else ${business_unit} end  ;;
  }

  dimension: bu_code {
    type: string
    label: "BU Code"
    view_label: "BU Alternative Hierarchy"
    description: "BU code from alternative hierarchy"
    sql: ${TABLE}.dim_alternate_bu_hierarchy.bu_code ;;
  }

  dimension: bu_dep_code {
    type: string
    view_label: "BU Alternative Hierarchy"
    label: "BU Department Code"
    description: "BU Department Code from alternative hierarchy"
    sql: ${TABLE}.dim_alternate_bu_hierarchy.bu_dep_code ;;
  }

  dimension: bu_dep_desc {
    type: string
    view_label: "BU Alternative Hierarchy"
    label: "BU Department Description"
    description: "BU Departmetn Description from alternative hierarchy"
    sql: ${TABLE}.dim_alternate_bu_hierarchy.bu_dep_desc ;;
  }

  dimension: bu_desc {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "BU description from alternative hierarchy"
    label: "BU Description"
    sql: ${TABLE}.dim_alternate_bu_hierarchy.bu_desc ;;
  }

  dimension: country {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Country value from alternative hierarchy"
    label: "Country"
    map_layer_name: countries
    sql: ${TABLE}.dim_alternate_bu_hierarchy.country ;;
  }

  dimension: country_access_filter {
    type: string
    hidden: yes
    sql: CASE when ${country} is null then 'x' else ${country} end  ;;
  }


  dimension: ownership {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Ownership from alternative hierarchy"
    sql: ${TABLE}.dim_alternate_bu_hierarchy.ownership ;;
  }

  dimension: store_code {
    type: string
    view_label: "BU Alternative Hierarchy"
    hidden: no
    description: "Store code from alternative hierarchy"
    sql: ${TABLE}.dim_alternate_bu_hierarchy.store_code ;;
  }

  dimension: sub_vertical {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Boat(sub_vertical) from alternative hierarchy"
    label: "Boat"
    sql: ${TABLE}.dim_alternate_bu_hierarchy.sub_vertical ;;
  }

  dimension: vertical {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Vertical from alternative hierarchy"
    label: "Vertical"
    sql: ${TABLE}.dim_alternate_bu_hierarchy.vertical ;;
  }

  dimension: vertical_access_filter {
    type: string
    hidden: yes
    sql: CASE when ${vertical} is null then 'x' else ${vertical} end  ;;
  }


  dimension: city {
    type: string
    view_label: "BU Alternative Hierarchy"
    sql: ${TABLE}.dim_alternate_bu_hierarchy.city ;;
  }

  dimension: country_name {
    type: string
    view_label: "BU Alternative Hierarchy"
    description: "Country ID used for Map Visualizations"
    label: "Country ID"
    map_layer_name: countries
    sql: CASE WHEN ${country} ="UAE" then "AE"
          WHEN  ${country} ="KSA" then "SA"
          WHEN  ${country} ="BAH" then "BH"
          ELSE  ${country} END  ;;
    drill_fields: [city,store_code]
  }




  dimension: district {
    type: number
    view_label: "Retail Locations"
    label: "Disrict"
    sql: ${TABLE}.dim_distr_ret_loc.district ;;
  }

  dimension: district_name {
    type: string
    view_label: "Retail Locations"
    label: "District Name"
    sql: ${TABLE}.dim_distr_ret_loc.district_name ;;
  }

  dimension: loc_code {
    type: number
    view_label: "Retail Locations"
    label: "Location Code"
    hidden: yes
    sql: ${TABLE}.dim_distr_ret_loc.loc_code ;;
  }

  dimension: loc_name {
    type: string
    view_label: "Retail Locations"
    label: "Location Name"
    sql: ${TABLE}.dim_distr_ret_loc.loc_name ;;
  }

  dimension: region {
    type: number
    view_label: "Retail Locations"
    label: "Region"
    sql: ${TABLE}.dim_distr_ret_loc.region ;;
  }

  dimension: region_name {
    type: string
    view_label: "Retail Locations"
    label: "Region Name"
    sql: ${TABLE}.dim_distr_ret_loc.region_name ;;
  }

  dimension_group: store_close {
    type: time
    view_label: "Retail Locations"
    label: "Store Close"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.dim_distr_ret_loc.store_close_date ;;
  }

  dimension: chain {
    type: number
    view_label: "Retail Locations"
    label: "Chain"
    sql: ${TABLE}.dim_distr_ret_loc.chain ;;
  }

  dimension: chain_name {
    type: string
    view_label: "Retail Locations"
    label: "Chain Name"
    sql: ${TABLE}.dim_distr_ret_loc.chain_name ;;
  }

  dimension: channel_id {
    type: number
    view_label: "Retail Locations"
    label: "Channel ID"
    sql: ${TABLE}.dim_distr_ret_loc.channel_id ;;
  }

  dimension: channel_name {
    type: string
    view_label: "Retail Locations"
    label: "Channel Name"
    sql: ${TABLE}.dim_distr_ret_loc.channel_name ;;
  }

  dimension: channel_type {
    type: string
    view_label: "Retail Locations"
    label: "Channel Type"
    sql: ${TABLE}.dim_distr_ret_loc.channel_type ;;
  }


  dimension: brand {
    hidden: no
    view_label: "Products"
    description: "Brand attribute from Oracle RMS"
    type: string
    suggest_explore: dim_retail_prod_brand
    suggest_dimension: dim_retail_prod_brand.brand
    sql: ${TABLE}.dim_retail_product.brand ;;
  }

  dimension: class {
    type: number
    hidden: yes
    sql: ${TABLE}.dim_retail_product.class ;;
  }

  dimension: class_name {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.class_name ;;
  }

  dimension: is_concession {
    type: yesno
    hidden: yes
    view_label: "Products"
    sql: UPPER(${class_name}) LIKE '%CONCESSION%' ;;
  }

  dimension: country_of_manu {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.country_of_manu ;;
  }

  dimension: dept_name {
    type: string
    hidden: no
    label: "Department Name"
    view_label: "Products"
    description: "Department attribute from Oracle RMS"
    sql: ${TABLE}.dim_retail_product.dept_name ;;
  }

  dimension: dept_no {
    type: number
    hidden: yes
    sql: ${TABLE}.dim_retail_product.dept_no ;;
  }

  dimension: dgr_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.dgr_ind ;;
  }

  dimension: diff_1 {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.diff_1 ;;
  }

  dimension: diff_2 {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.diff_2 ;;
  }

  dimension: division {
    description: "Division attribute from Oracle RMS"
    view_label: "Products"
    type: string
    suggest_explore: dim_retail_prod_division
    suggest_dimension: dim_retail_prod_division.division
    sql: ${TABLE}.dim_retail_product.division ;;
  }

  dimension: division_no {
    type: number
    hidden: yes
    sql: ${TABLE}.dim_retail_product.division_no ;;
  }

  dimension: entity_type {
    type: string
    view_label: "Products"
    hidden: no
    sql: ${TABLE}.dim_retail_product.entity_type ;;
  }

  dimension: format {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.format ;;
  }

  dimension: gender {
    view_label: "Products"
    description: "Gender attribute from Oracle RMS"
    type: string
    suggest_explore: dim_retail_prod_gender
    suggest_dimension: dim_retail_prod_gender.gender
    sql: ${TABLE}.dim_retail_product.gender ;;
  }

  dimension: grey_mkt_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.grey_mkt_ind ;;
  }

  dimension: group_name {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.group_name ;;
  }

  dimension: group_no {
    type: number
    hidden: yes
    sql: ${TABLE}.dim_retail_product.group_no ;;
  }

  dimension: item {
    view_label: "Products"
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.item ;;
  }

  dimension: item_desc {
    label:"Product Description"
    view_label: "Products"
    description: "Description from Oracle RMS"
    type: string
    hidden: no
    sql: ${TABLE}.dim_retail_product.item_desc ;;
  }

  dimension: item_style {
    view_label: "Products"
    label: "Style"
    description: "Style ID from Oracle RMS"
    type: string
    suggest_explore: fact_soh_sales_poc
    suggest_dimension: dim_retail_product.item_style
    sql: ${TABLE}.dim_retail_product.item_style ;;
  }

  dimension: line {
    type: string
    view_label: "Products"
    hidden: no
    sql: ${TABLE}.dim_retail_product.line ;;
  }

  dimension: isconcession {
    type: yesno
    view_label: "Products"
    hidden: no
    sql: ${line} like '%CONCESSION%';;
  }
  dimension: perishable_ind {
    type: string
    view_label: "Products"
    hidden: no
    sql: ${TABLE}.dim_retail_product.perishable_ind ;;
  }

  dimension: recurrence {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.recurrence ;;
  }

  dimension: sap_item_code {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.sap_item_code ;;
  }

  dimension: saso_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.saso_ind ;;
  }

  dimension: season_desc {
    view_label: "Products"
    description: "Season attribute from Oracle RMS"
    label: "Season"
    type: string
    suggest_explore: dim_retail_prod_season
    suggest_dimension: dim_retail_prod_season.season
    sql: ${TABLE}.dim_retail_product.season_desc ;;
  }

  dimension: sep_axis {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.sep_axis ;;
  }

  # dimension: sep_category {
  #   hidden: yes
  #   sql: ${TABLE}.dim_retail_product.sep_category ;;
  # }

  # dimension: sep_market {
  #   type: string
  #   view_label: "Products"
  #   hidden: no
  #   label: "Market"
  #   sql: ${TABLE}.dim_retail_product.sep_market ;;
  # }

  # dimension: sep_nature {
  #   type: string
  #   hidden: yes
  #   sql: ${TABLE}.dim_retail_product.sep_nature ;;
  # }

  # dimension: sep_range {
  #   type: string
  #   hidden: yes
  #   sql: ${TABLE}.dim_retail_product.sep_range ;;
  # }

  dimension: size_uda {
    view_label: "Products"
    description: "Size attribute from Oracle RMS"
    type: string
    label: "Size"
    suggest_explore: dim_retail_prod_size
    suggest_dimension: dim_retail_prod_size.size
    sql: ${TABLE}.dim_retail_product.size_uda ;;
  }

  dimension: standard_uom {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.standard_uom ;;
  }

  dimension: sub_line {
    type: string
    hidden: no
    view_label: "Products"
    sql: ${TABLE}.dim_retail_product.sub_line ;;
  }

  dimension: subclass {
    type: number
    hidden: yes
    sql: ${TABLE}.dim_retail_product.subclass ;;
  }

  dimension: subclass_name {
    view_label: "Products"
    label: "Sub Class"
    description: "Oracle RMS product subclass"
    type: string
    suggest_explore: dim_retail_prod_sub_class
    suggest_dimension: dim_retail_prod_sub_class.subclass_name
    sql: ${TABLE}.dim_retail_product.subclass_name ;;
  }

  dimension: sup_class {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.sup_class ;;
  }

  dimension: sup_subclass {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.sup_subclass ;;
  }

  dimension: taxo_class {
    view_label: "Products"
    label: "Taxonomy Class"
    description: "Taxonomy Class from Oracle RMS"
    type: string
    suggest_explore: dim_retail_prod_taxo_class
    suggest_dimension: dim_retail_prod_taxo_class.taxo_class
    sql: ${TABLE}.dim_retail_product.taxo_class ;;
  }

  dimension: taxo_subclass {
    view_label: "Products"
    label: "Taxonomy Subclass"
    description: "Taxonomy subclass attribute from RMS"
    type: string
    suggest_explore: dim_retail_prod_taxo_subclass
    suggest_dimension: dim_retail_prod_taxo_subclass.taxo_subclass
    sql: ${TABLE}.dim_retail_product.taxo_subclass ;;
  }

  dimension: uc_code {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.uc_code ;;
  }

  dimension: uda_zone {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.uda_zone ;;
  }

  dimension: usage_specificity {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.usage_specificity ;;
  }

  dimension: vpn {
    view_label: "Products"
    description: "Item VPN attribute from Oracle RMS"
    type: string
    label: "VPN"
    suggest_dimension: dim_retail_product.item_vpn
    sql: ${TABLE}.dim_retail_product.vpn ;;
  }



  # dimension_group: dim_retail_loc_store_close {
  #   type: time
  #   timeframes: [
  #     raw,
  #     time,
  #     date,
  #     week,
  #     month,
  #     quarter,
  #     year
  #   ]
  #   sql: ${TABLE}.dim_retail_loc_store_close_date ;;
  # }

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql: CONCAT(${date_date}, " - ", ${prod_num}, " - ", ${org_num}) ;;
  }

  dimension: dim_retail_product_item_desc {
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.item_desc ;;
  }

  dimension: prod_num {
    description: "Product number from Oracle RMS"
    hidden: no
    type: string
    label: "Product ID"
    suggest_dimension: dim_retail_product.item
    view_label: "Products"
    sql: ${TABLE}.prod_num ;;
  }

  dimension: org_num {
    description: "Location number from Oracle RMS"
    hidden: no
    label: "Location Number"

    view_label: "Retail Locations"
    suggest_explore: dim_retail_loc_store
    suggest_dimension: dim_retail_loc_store.store
    type: string
    sql: ${TABLE}.org_num ;;
  }

  dimension: provisions_local_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.provisions_local_amount ;;
  }

  dimension: provisions_local_amount_lm {
    type: number
    hidden: yes
    sql: ${TABLE}.provisions_local_amount_lm ;;
  }

  dimension: provisions_usd_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.provisions_usd_amount ;;
  }

  dimension: provisions_usd_amount_lm {
    type: number
    hidden: yes
    sql: ${TABLE}.provisions_usd_amount_lm ;;
  }

 measure: provision_usd_current_month {
   type: sum
   value_format_name: usd
   label: "Provision Amount Current Month USD"
   sql:  ${provisions_usd_amount};;
 }

  measure: provision_usd_last_month {
    type: sum
    value_format_name: usd
    label: "Provision Amount Last Month USD"
    sql:  ${provisions_usd_amount_lm};;
  }

  measure: reversal_usd {
    type: number
    label: "Reversal Amount USD"
    description: "Reversal is the difference between the Provision amount of current month and the previous month."
    value_format_name: usd
    hidden: no
    sql: CASE WHEN SUM(${provisions_usd_amount})-SUM(${provisions_usd_amount_lm}) >= 0 THEN 0 ELSE ABS(SUM(${provisions_usd_amount})-SUM(${provisions_usd_amount_lm})) END ;;
  }

  measure: reversal_local {
    type: number
    label: "Reversal Amount Local"
    description: "Reversal is the difference between the Provision amount of current month and the previous month."
    value_format_name: decimal_2
    hidden: no
    sql: CASE WHEN SUM(${provisions_local_amount})-SUM(${provisions_local_amount_lm}) >= 0 THEN 0 ELSE ABS(SUM(${provisions_local_amount})-SUM(${provisions_local_amount_lm})) END;;
  }


  measure: count {
    type: count
    drill_fields: []
  }
}
