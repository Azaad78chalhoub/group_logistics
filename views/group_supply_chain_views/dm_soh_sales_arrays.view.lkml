view: dm_soh_sales_arrays {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.dm_soh_sales_arrays`
    ;;

  dimension: arabic_desc {
    type: string
    label: "Arabic Description"
    sql: ${TABLE}.arabic_desc ;;
  }
  dimension: area {
    type: number
    label: "Area Code"
    sql: ${TABLE}.area ;;
  }

  parameter: bu_name {
      type: number

    allowed_value: {
      label: "ACCOUNTING"
      value: "1"
    }

    allowed_value: {
      label: "ADMINISTRATION"
      value: "2"
    }

    allowed_value: {
      label: "AL HEBA"
      value: "3"
    }

    allowed_value: {
      label: "AL JAMAL"
      value: "4"
    }

    allowed_value: {
      label: "ALICE & OLIVIA"
      value: "5"
    }

    allowed_value: {
      label: "ALICE PIZZA"
      value: "6"
    }

    allowed_value: {
      label: "AMOUROUD"
      value: "7"
    }

    allowed_value: {
      label: "ATELIER COLOGNE"
      value: "8"
    }

    allowed_value: {
      label: "AUBAINE"
      value: "9"
    }

    allowed_value: {
      label: "B2B"
      value: "10"
    }

    allowed_value: {
      label: "B2B MANAGEMENT"
      value: "11"
    }

    allowed_value: {
      label: "B8TA"
      value: "12"
    }

    allowed_value: {
      label: "BEAUTY DISTRIBUTION EGY"
      value: "13"
    }

    allowed_value: {
      label: "BEAUTY DISTRIBUTION KSA"
      value: "14"
    }

    allowed_value: {
      label: "BEAUTY DISTRIBUTION KWT QAT & BAH"
      value: "15"
    }

    allowed_value: {
      label: "BEAUTY DISTRIBUTION UAE"
      value: "16"
    }

    allowed_value: {
      label: "BEAUTY FRANCHISE & DISTRIBUTION 1 MANAGEMENT"
      value: "17"
    }

    allowed_value: {
      label: "BEAUTY FRANCHISE & DISTRIBUTION 2 MANAGEMENT"
      value: "18"
    }

    allowed_value: {
      label: "BEAUTY MANAGEMENT DISCONTINUED"
      value: "19"
    }

    allowed_value: {
      label: "BEAUTY MULTIBRAND MANAGEMENT"
      value: "20"
    }

    allowed_value: {
      label: "BEAUTY NATION"
      value: "21"
    }

    allowed_value: {
      label: "BENEFIT"
      value: "22"
    }

    allowed_value: {
      label: "BERLUTI"
      value: "23"
    }

    allowed_value: {
      label: "BOBBI BROWN"
      value: "24"
    }

    allowed_value: {
      label: "BY KILIAN"
      value: "25"
    }

    allowed_value: {
      label: "CAROLINA HERRERA"
      value: "26"
    }

    allowed_value: {
      label: "CATERING"
      value: "27"
    }

    allowed_value: {
      label: "CELINE"
      value: "28"
    }

    allowed_value: {
      label: "CENTER OF EXCELLENCE"
      value: "29"
    }

    allowed_value: {
      label: "CERRUTI"
      value: "30"
    }

    allowed_value: {
      label: "CGL"
      value: "31"
    }

    allowed_value: {
      label: "CHANEL"
      value: "32"
    }

    allowed_value: {
      label: "CHAUMET"
      value: "33"
    }

    allowed_value: {
      label: "CHRISTIAN DIOR"
      value: "34"
    }

    allowed_value: {
      label: "CHRISTIAN LOUBOUTIN"
      value: "35"
    }

    allowed_value: {
      label: "CHRISTOFLE"
      value: "36"
    }

    allowed_value: {
      label: "CONCIERGE"
      value: "37"
    }

    allowed_value: {
      label: "COTY"
      value: "38"
    }

    allowed_value: {
      label: "COUNTRY MANAGEMENT"
      value: "39"
    }

    allowed_value: {
      label: "COUNTRY MANAGEMENT KSA"
      value: "40"
    }

    allowed_value: {
      label: "COURCELLES"
      value: "41"
    }

    allowed_value: {
      label: "CPU"
      value: "42"
    }

    allowed_value: {
      label: "CX & STRATEGIC MARKETING"
      value: "43"
    }

    allowed_value: {
      label: "DATA & ANALYTICS"
      value: "44"
    }

    allowed_value: {
      label: "DESIGN"
      value: "45"
    }

    allowed_value: {
      label: "DIGITAL TECHNOLOGY"
      value: "46"
    }

    allowed_value: {
      label: "DISCONTINUED"
      value: "47"
    }

    allowed_value: {
      label: "DOLCE & GABBANA"
      value: "48"
    }

    allowed_value: {
      label: "DSQUARED2"
      value: "49"
    }

    allowed_value: {
      label: "DSQUARED2 & PAUL SMITH"
      value: "50"
    }

    allowed_value: {
      label: "DUTY FREE"
      value: "51"
    }

    allowed_value: {
      label: "DUVAL"
      value: "52"
    }

    allowed_value: {
      label: "DYLAN'S CANDY BAR"
      value: "53"
    }

    allowed_value: {
      label: "ELEMIS"
      value: "54"
    }

    allowed_value: {
      label: "ELIZABETH ARDEN"
      value: "55"
    }

    allowed_value: {
      label: "ELLA AFRIQUE"
      value: "56"
    }

    allowed_value: {
      label: "ERBORIAN"
      value: "57"
    }

    allowed_value: {
      label: "ESTEE LAUDER"
      value: "58"
    }

    allowed_value: {
      label: "ESTEE LAUDER MANAGEMENT"
      value: "59"
    }

    allowed_value: {
      label: "EUC"
      value: "60"
    }

    allowed_value: {
      label: "EX NIHILO"
      value: "61"
    }

    allowed_value: {
      label: "FAAST MANAGEMENT"
      value: "62"
    }

    allowed_value: {
      label: "FACES"
      value: "63"
    }

    allowed_value: {
      label: "FACILITY MAINTENANCE"
      value: "64"
    }

    allowed_value: {
      label: "FARFETCH"
      value: "65"
    }

    allowed_value: {
      label: "FASHION & ACCESSORIES 1 MANAGEMENT"
      value: "66"
    }

    allowed_value: {
      label: "FASHION & ACCESSORIES 2 MANAGEMENT"
      value: "67"
    }

    allowed_value: {
      label: "FENDI"
      value: "68"
    }

    allowed_value: {
      label: "FILA"
      value: "69"
    }

    allowed_value: {
      label: "FINANCE"
      value: "70"
    }

    allowed_value: {
      label: "FINANCE SERVICES"
      value: "71"
    }

    allowed_value: {
      label: "FRAGRANCES"
      value: "72"
    }

    allowed_value: {
      label: "FREDERIC MALLE"
      value: "73"
    }

    allowed_value: {
      label: "FURLA"
      value: "74"
    }

    allowed_value: {
      label: "GEOX"
      value: "75"
    }

    allowed_value: {
      label: "GHAWALI"
      value: "76"
    }

    allowed_value: {
      label: "GIVENCHY"
      value: "77"
    }

    allowed_value: {
      label: "GREENHOUSE & IBTIKAR - KSA"
      value: "78"
    }

    allowed_value: {
      label: "GREENHOUSE & IBTIKAR - UAE"
      value: "79"
    }

    allowed_value: {
      label: "GROUP MANAGEMENT"
      value: "80"
    }

    allowed_value: {
      label: "GROUP TREASURY"
      value: "81"
    }

    allowed_value: {
      label: "HACKETT"
      value: "82"
    }

    allowed_value: {
      label: "HAVAS"
      value: "83"
    }

    allowed_value: {
      label: "HOURGLASS"
      value: "84"
    }

    allowed_value: {
      label: "HR SERVICES"
      value: "85"
    }

    allowed_value: {
      label: "HUGO BOSS"
      value: "86"
    }

    allowed_value: {
      label: "IBTIKAR"
      value: "87"
    }

    allowed_value: {
      label: "IL GUFO"
      value: "88"
    }

    allowed_value: {
      label: "INNOVATION"
      value: "89"
    }

    allowed_value: {
      label: "INVESTMENT"
      value: "90"
    }

    allowed_value: {
      label: "INVESTMENT PORTFOLIO"
      value: "91"
    }

    allowed_value: {
      label: "JV MANAGEMENT"
      value: "92"
    }

    allowed_value: {
      label: "KARL LAGERFELD"
      value: "93"
    }

    allowed_value: {
      label: "KENZO"
      value: "94"
    }

    allowed_value: {
      label: "LA MER"
      value: "95"
    }

    allowed_value: {
      label: "LACOSTE"
      value: "96"
    }

    allowed_value: {
      label: "LANCASTER"
      value: "97"
    }

    allowed_value: {
      label: "LEARNING & DEVELOPMENT"
      value: "98"
    }

    allowed_value: {
      label: "LEGAL"
      value: "99"
    }

    allowed_value: {
      label: "LES BENJAMINS"
      value: "100"
    }

    allowed_value: {
      label: "LEVEL SHOES"
      value: "101"
    }

    allowed_value: {
      label: "LOCCITANE"
      value: "102"
    }

    allowed_value: {
      label: "LOEWE"
      value: "103"
    }

    allowed_value: {
      label: "LOGISTICS"
      value: "104"
    }

    allowed_value: {
      label: "LONGCHAMP"
      value: "105"
    }

    allowed_value: {
      label: "LOUIS VUITTON"
      value: "106"
    }

    allowed_value: {
      label: "MAINTENANCE"
      value: "107"
    }

    allowed_value: {
      label: "MAKE UP FOR EVER"
      value: "108"
    }

    allowed_value: {
      label: "MANAGED COMPANIES CUSTOMER"
      value: "109"
    }

    allowed_value: {
      label: "MANAGED COMPANIES ECOMMERCE"
      value: "110"
    }

    allowed_value: {
      label: "MANAGED COMPANIES MANAGEMENT"
      value: "111"
    }

    allowed_value: {
      label: "MANAGEMENT"
      value: "112"
    }

    allowed_value: {
      label: "MARC JACOBS"
      value: "113"
    }

    allowed_value: {
      label: "MAX MARA"
      value: "114"
    }

    allowed_value: {
      label: "MICHAEL KORS"
      value: "115"
    }

    allowed_value: {
      label: "MODERN MEDIA"
      value: "116"
    }

    allowed_value: {
      label: "MOLTON BROWN"
      value: "117"
    }

    allowed_value: {
      label: "MUFE MANAGEMENT"
      value: "118"
    }

    allowed_value: {
      label: "MULBERRY"
      value: "119"
    }

    allowed_value: {
      label: "MULTIBRAND"
      value: "120"
    }

    allowed_value: {
      label: "MULTIBRAND ACCESSORIES"
      value: "121"
    }

    allowed_value: {
      label: "MULTIBRAND MANAGEMENT"
      value: "122"
    }

    allowed_value: {
      label: "NARS"
      value: "123"
    }

    allowed_value: {
      label: "NEW MARKETS"
      value: "124"
    }

    allowed_value: {
      label: "OFF WHITE"
      value: "125"
    }

    allowed_value: {
      label: "OPERATIONS MANAGEMENT"
      value: "126"
    }

    allowed_value: {
      label: "OUT OF SCOPE"
      value: "127"
    }

    allowed_value: {
      label: "PAUL SMITH"
      value: "128"
    }

    allowed_value: {
      label: "PAULE KA"
      value: "129"
    }

    allowed_value: {
      label: "PCD"
      value: "130"
    }

    allowed_value: {
      label: "PENHALIGON'S"
      value: "131"
    }

    allowed_value: {
      label: "PEOPLE & CULTURE"
      value: "132"
    }

    allowed_value: {
      label: "PERFUMES LOEWE"
      value: "133"
    }

    allowed_value: {
      label: "PIXI"
      value: "134"
    }

    allowed_value: {
      label: "PRIVE REVAUX"
      value: "135"
    }

    allowed_value: {
      label: "PROCUREMENT"
      value: "136"
    }

    allowed_value: {
      label: "PROJECTS MANAGEMENT"
      value: "137"
    }

    allowed_value: {
      label: "PROP AND PROJ - MANAGEMENT"
      value: "138"
    }

    allowed_value: {
      label: "PUCCI"
      value: "139"
    }

    allowed_value: {
      label: "PUIG"
      value: "140"
    }

    allowed_value: {
      label: "PURIFICACION GARCIA"
      value: "141"
    }

    allowed_value: {
      label: "RADWA LVMH"
      value: "142"
    }

    allowed_value: {
      label: "RALPH LAUREN"
      value: "143"
    }

    allowed_value: {
      label: "RDBS"
      value: "144"
    }

    allowed_value: {
      label: "REAL ESTATE"
      value: "145"
    }

    allowed_value: {
      label: "REGIONAL COUNTRY MANAGEMENT"
      value: "146"
    }

    allowed_value: {
      label: "RENE CAOVILLA"
      value: "147"
    }

    allowed_value: {
      label: "RETAIL ACADEMY"
      value: "148"
    }

    allowed_value: {
      label: "RETAIL OPERATIONS"
      value: "149"
    }

    allowed_value: {
      label: "RITC"
      value: "150"
    }

    allowed_value: {
      label: "ROGER & GALLET"
      value: "151"
    }

    allowed_value: {
      label: "RPD"
      value: "152"
    }

    allowed_value: {
      label: "SAJ"
      value: "153"
    }

    allowed_value: {
      label: "SAKS"
      value: "154"
    }

    allowed_value: {
      label: "SALVATORE FERRAGAMO"
      value: "155"
    }

    allowed_value: {
      label: "SEPHORA"
      value: "156"
    }

    allowed_value: {
      label: "SERVICE OPERATIONS TECH"
      value: "157"
    }

    allowed_value: {
      label: "SGII MANAGEMENT"
      value: "158"
    }

    allowed_value: {
      label: "SMASHBOX"
      value: "159"
    }

    allowed_value: {
      label: "SOGEDIMO"
      value: "160"
    }

    allowed_value: {
      label: "ST DUPONT"
      value: "161"
    }

    allowed_value: {
      label: "STELLA MC CARTNEY"
      value: "162"
    }

    allowed_value: {
      label: "STOCK COUNT"
      value: "163"
    }

    allowed_value: {
      label: "STRATEGY, INTELLIGENCE & GROWTH"
      value: "164"
    }

    allowed_value: {
      label: "STRUCTURE"
      value: "165"
    }

    allowed_value: {
      label: "SUPPLY CHAIN MANAGEMENT"
      value: "166"
    }

    allowed_value: {
      label: "SUSTAINABILITY"
      value: "167"
    }

    allowed_value: {
      label: "SWAROVSKI"
      value: "168"
    }

    allowed_value: {
      label: "TAG HEUER"
      value: "169"
    }

    allowed_value: {
      label: "TALENT ACQUISITION"
      value: "170"
    }

    allowed_value: {
      label: "TANAGRA & ADV"
      value: "171"
    }

    allowed_value: {
      label: "TARZ"
      value: "172"
    }

    allowed_value: {
      label: "TECHNOLOGY OPERATIONS"
      value: "173"
    }

    allowed_value: {
      label: "THE BEAUTY MAKERS"
      value: "174"
    }

    allowed_value: {
      label: "THE DEAL"
      value: "175"
    }

    allowed_value: {
      label: "THE FASHION MAKERS"
      value: "176"
    }

    allowed_value: {
      label: "THE VISITOR"
      value: "177"
    }

    allowed_value: {
      label: "TOM FORD"
      value: "178"
    }

    allowed_value: {
      label: "TOO FACED"
      value: "179"
    }

    allowed_value: {
      label: "TORY BURCH"
      value: "180"
    }

    allowed_value: {
      label: "TRAVEL RETAIL"
      value: "181"
    }

    allowed_value: {
      label: "TRYANO"
      value: "182"
    }

    allowed_value: {
      label: "TUMI"
      value: "183"
    }

    allowed_value: {
      label: "URBAN DECAY"
      value: "184"
    }

    allowed_value: {
      label: "VAT COMPLIANCE"
      value: "185"
    }

    allowed_value: {
      label: "VERSACE"
      value: "186"
    }

    allowed_value: {
      label: "VILEBREQUIN"
      value: "187"
    }

    allowed_value: {
      label: "VILHELM"
      value: "188"
    }

    allowed_value: {
      label: "VISUAL MERCHANDISING"
      value: "189"
    }

    allowed_value: {
      label: "VMD"
      value: "190"
    }

    allowed_value: {
      label: "WATCHES & INSTRUMENTS"
      value: "191"
    }

    allowed_value: {
      label: "WHOLESALE & KIDS"
      value: "192"
    }

    allowed_value: {
      label: "WIDIAN"
      value: "193"
    }

    allowed_value: {
      label: "WOW BY WOJOOH"
      value: "194"
    }

    allowed_value: {
      label: "ZADIG & VOLTAIRE"
      value: "195"
    }
    }


  dimension: business_unit_hash {
    label_from_parameter: bu_name
    type: number
    sql: ${TABLE}.business_unit_hash ;;
  }


  dimension: area_name {
    type: string
    label: "Area Name"
    sql: ${TABLE}.area_name ;;
  }

  dimension: atr_homecurrency {
    type: string
    label: "Currency Code"
    sql: ${TABLE}.atr_homecurrency ;;
  }

  dimension: atrb_boy_girl {
    type: string
    label: "Gender"
    sql: ${TABLE}.atrb_boy_girl ;;
  }

  dimension: attrb_color {
    type: string
    label: "Colour"
    sql: ${TABLE}.attrb_color ;;
  }


  dimension: attrb_made_of {
    type: string
    label: "Made of"
    sql: ${TABLE}.attrb_made_of ;;
  }

  dimension: attrb_theme {
    type: string
    label: "Theme"
    sql: ${TABLE}.attrb_theme ;;
  }

  dimension: avg_last182days_cogs_amountusd {
    type: number
    sql: ${TABLE}.avg_last182days_cogs_amountusd ;;
  }

  dimension: avg_last30days_cogs_amountusd {
    type: number
    sql: ${TABLE}.avg_last30days_cogs_amountusd ;;
  }

  dimension: avg_last7days_cogs_amountusd {
    type: number
    sql: ${TABLE}.avg_last7days_cogs_amountusd ;;
  }

  dimension: avg_last90days_cogs_amountusd {
    type: number
    sql: ${TABLE}.avg_last90days_cogs_amountusd ;;
  }

  dimension: avg_lastyear_cogs_amountusd {
    type: number
    sql: ${TABLE}.avg_lastyear_cogs_amountusd ;;
  }

  dimension: barcode {
    type: string
    label: "Barcode"
    sql: ${TABLE}.barcode ;;
  }

  dimension: brand {
    type: string
    label: "Brand"
    sql: ${TABLE}.brand ;;
  }

  dimension: bu_dep_code {
    type: string
    label: "BU Department Code"
    sql: ${TABLE}.bu_dep_code ;;
  }

  dimension: bu_dep_desc {
    type: string
    label: "BU Department Description"
    sql: ${TABLE}.bu_dep_desc ;;
  }

  dimension: bu_desc {
    type: string
    label: "BU Description"
    sql: ${TABLE}.bu_desc ;;
  }

  dimension: business_unit {
    type: string
    label: "Business Unit"
    sql: ${TABLE}.business_unit ;;
  }

  dimension: business_unit_code {
    type: string
    label: "BU Code"
    sql: ${TABLE}.business_unit_code ;;
  }

  dimension: chain {
    type: number
    label: "Chain Code"
    sql: ${TABLE}.chain ;;
  }

  dimension: chain_name {
    type: string
    label: "Chain"
    sql: ${TABLE}.chain_name ;;
  }


  dimension: channel_id {
    type: number
    label: "Channel ID"
    sql: ${TABLE}.channel_id ;;
  }


  dimension: channel_name {
    type: string
    label: "Channel Name"
    sql: ${TABLE}.channel_name ;;
  }

  dimension: channel_type {
    type: string
    label: "Channel Type"
    sql: ${TABLE}.channel_type ;;
  }

  dimension: city {
    type: string
    label: "City"
    sql: ${TABLE}.city ;;
  }

  dimension: class {
    type: number
    label: "Class"
    sql: ${TABLE}.class ;;
  }

  dimension: class_name {
    type: string
    label: "Class Name"
    sql: ${TABLE}.class_name ;;
  }

  dimension: consignment_flag {
    type: string
    label: "Consignment Flag"
    sql: ${TABLE}.consignment_flag ;;
  }


  dimension: conversion_rate {
    type: number
    sql: ${TABLE}.CONVERSION_RATE ;;
  }

  dimension: country_desc {
    type: string
    label: "Country"
    sql: ${TABLE}.country_desc ;;
  }

  dimension: country_id {
    type: string
    label: "Country Code"
    sql: ${TABLE}.country_id ;;
  }

  dimension: country_code{
    group_label: "Locations"
    label: "Country Code"
    description: "Country code for location in Oracle RMS"
    type: string
    suggest_dimension: dim_retail_loc.country_code
    sql: CASE
        WHEN ${country_id} = "BH" THEN 'BAH'
        WHEN ${country_id} = "EG" THEN 'EGY'
        WHEN ${country_id} = "KW" THEN 'KWT'
        WHEN ${country_id} = "SA" THEN 'KSA'
        WHEN ${country_id} = "QA" THEN 'QAT'
        WHEN ${country_id} = "EF" THEN 'EGY'
        WHEN ${country_id} = "MA" THEN 'MAR'
        WHEN ${country_id} = "AE" THEN 'UAE'
        ELSE 'UAE'
        END ;;
  }

  dimension: country_of_manu {
    type: string
    label: "Country of Manufacturer"
    sql: ${TABLE}.country_of_manu ;;
  }

  dimension: day_of_week {
    type: number
    label: "Day of Week"
    sql: ${TABLE}.day_of_week ;;
  }

  dimension: dept_name {
    type: string
    label: "Department Name"
    sql: ${TABLE}.dept_name ;;
  }

  dimension: dept_no {
    type: number
    label: "Department No."
    sql: ${TABLE}.dept_no ;;
  }

  dimension: description {
    type: string
    label: "Region Descrption"
    sql: ${TABLE}.description ;;
  }


  dimension: dgr_ind {
    type: string
    sql: ${TABLE}.dgr_ind ;;
  }

  dimension: distr_sales_ind {
    type: number
    sql: ${TABLE}.distr_sales_ind ;;
  }

   dimension: district {
    type: number
    label: "District"
    sql: ${TABLE}.district ;;
  }

  dimension: district_name {
    type: string
    label: "District Name"
    sql: ${TABLE}.district_name ;;
  }

  dimension: division {
    type: string
    label: "Division"
    sql: ${TABLE}.division ;;
  }

  dimension: division_no {
    type: number
    label: "Division No."
    sql: ${TABLE}.division_no ;;
  }

  dimension: entity_type {
    type: string
    label: "Entity Type"
    sql: ${TABLE}.entity_type ;;
  }


  dimension_group: first_received {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.first_received ;;
  }

  dimension_group: first_sold {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.first_sold ;;
  }

dimension: gender {
  type: string
  label: "Gender 2"
  sql: ${TABLE}.gender ;;
}

dimension: grey_mkt_ind {
  type: string
  hidden: yes
  sql: ${TABLE}.grey_mkt_ind ;;
}

dimension: group_name {
  type: string
  label: "Group Name"
  sql: ${TABLE}.group_name ;;
}

dimension: group_no {
  type: number
  label: "Group No."
  sql: ${TABLE}.group_no ;;
}

dimension: inv_soh_qty {
  type: number
  label: "SOH Quantity"
  sql: ${TABLE}.inv_soh_qty ;;
}

dimension: item {
  type: string
  label: "Item No."
  sql: ${TABLE}.item ;;
}

dimension: item_desc {
  type: string
  label: "Item Description"
  sql: ${TABLE}.item_desc ;;
}

dimension: item_style {
  type: string
  label: "Item Style"
  sql: ${TABLE}.item_style ;;
}

  dimension: last182days_cogs_amountusd {
    type: number
    sql: ${TABLE}.last182days_cogs_amountusd ;;
  }

  dimension: last30days_cogs_amountusd {
    type: number
    sql: ${TABLE}.last30days_cogs_amountusd ;;
  }

  dimension: last7days_cogs_amountusd {
    type: number
    sql: ${TABLE}.last7days_cogs_amountusd ;;
  }

  dimension: last90days_cogs_amountusd {
    type: number
    sql: ${TABLE}.last90days_cogs_amountusd ;;
  }

  dimension_group: last_update_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_update_datetime ;;
  }

  dimension: lastyear_cogs_amountusd {
    type: number
    sql: ${TABLE}.lastyear_cogs_amountusd ;;
  }

    dimension: line {
      type: string
      label: "Line"
      sql: ${TABLE}.line ;;
    }

    dimension: loc_code {
      type: number
      label: "Location Code"
      sql: ${TABLE}.loc_code ;;
    }

    dimension: loc_name {
      type: string
      label: "Location Name"
      sql: ${TABLE}.loc_name ;;
    }

    dimension: loc_season {
      type: string
      label: "Location Season"
      sql: ${TABLE}.loc_season ;;
    }

    dimension: mall_name {
      type: string
      label: "Mall Name"
      sql: ${TABLE}.mall_name ;;
    }

   dimension: max_price_local_by_country_day {
    type: number
    label: "Max Price Local By Country"
    sql: ${TABLE}.max_price_local_by_country_day ;;
  }

  dimension: max_price_usd_by_country {
    type: number
    label: "Max Price USD By Country"
    sql: ${TABLE}.max_price_usd_by_country ;;
  }

  dimension: module {
    type: string
    label: "Module"
    sql: ${TABLE}.module ;;
  }

  dimension: month {
    type: number
    label: "Month"
    sql: ${TABLE}.month ;;
  }

  dimension: org_num {
    type: string
    hidden: yes
    sql: ${TABLE}.org_num ;;
  }

  dimension: ownership {
    type: string
    label: "Ownership"
    sql: ${TABLE}.ownership ;;
  }

  dimension: physical_wh {
    type: number
    label: "Physical Warehouse"
    sql: ${TABLE}.physical_wh ;;
  }

  dimension: previous_week_stock_qty {
    type: number
    label: "Previous Week Stock Quantity"
    sql: ${TABLE}.previous_week_stock_qty ;;
  }

  dimension: previous_week_unit_cost_usd {
    type: number
    label: "Previous Week Unit Cost USD"
    sql: ${TABLE}.previous_week_unit_cost_usd ;;
  }

  dimension: price_local {
    type: number
    label: "Price Local"
    sql: ${TABLE}.price_local ;;
  }

  dimension: prod_num {
    type: string
    label: "Prod No."
    sql: ${TABLE}.prod_num ;;
  }

  dimension: recurrence {
    type: string
    label: "Recurrence"
    sql: ${TABLE}.recurrence ;;
  }

   dimension: region {
    type: number
    label: "Region"
    sql: ${TABLE}.region ;;
  }

  dimension: region_name {
    type: string
    label: "Region"
    sql: ${TABLE}.region_name ;;
  }

  dimension: report_code {
    type: string
    label: "Report Code"
    sql: ${TABLE}.report_code ;;
  }

  dimension: sales_amountlocal_beforetax {
    type: number
    label: "Net Sales Amount Local"
    hidden: yes
    sql: ${TABLE}.sales_amountlocal_beforetax ;;
  }

  dimension: sales_amountusd_beforetax {
    type: number
    label: "Net Sales Amount Local"
    hidden: yes
    sql: ${TABLE}.sales_amountusd_beforetax ;;
  }

  dimension: sales_qty {
    type: number
    sql: ${TABLE}.sales_qty ;;
  }

  dimension: sap_item_code {
    type: string
    sql: ${TABLE}.sap_item_code ;;
  }

  dimension: season_desc {
    type: string
    label: "Season"
    sql: ${TABLE}.season_desc ;;
  }

  dimension: selling_square_ft {
    type: number
    label: "Selling Square Foot"
    sql: ${TABLE}.selling_square_ft ;;
  }

  dimension: sep_category {
    hidden: yes
    label: "SEP Category"
    sql: ${TABLE}.sep_category ;;
  }

  dimension: sep_market {
    type: string
    label: "SEP Market"
    sql: ${TABLE}.sep_market ;;
  }

  dimension: size_uda {
    type: string
    label: "Size UDA"
    sql: ${TABLE}.size_uda ;;
  }

  dimension: standard_uom {
    type: string
    label: "Standard UOM"
    sql: ${TABLE}.standard_uom ;;
  }

  dimension: state {
    type: string
    label: "State"
    sql: ${TABLE}.state ;;
  }

  dimension: status {
    type: string
    label: "Status"
    sql: ${TABLE}.status ;;
  }

  dimension_group: store_close {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_close_date ;;
  }

  dimension: store_code {
    type: string
    sql: ${TABLE}.store_code ;;
  }


  dimension: store_format {
    type: number
    label: "Store Code"
    sql: ${TABLE}.store_format ;;
  }

  dimension_group: store_open {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_open_date ;;
  }

  dimension: sub_line {
    type: string
    label: "Sub Line"
    sql: ${TABLE}.sub_line ;;
  }

  dimension: sub_vertical {
    type: string
    label: "Sub Vertical"
    sql: ${TABLE}.sub_vertical ;;
  }

  dimension: subclass {
    type: number
    label: "Sub Class"
    sql: ${TABLE}.subclass ;;
  }

  dimension: subclass_name {
    type: string
    label: "Subclass Name"
    sql: ${TABLE}.subclass_name ;;
  }

  dimension: sup_class {
    type: string
    label: "SUP Class"
    sql: ${TABLE}.sup_class ;;
  }

  dimension: sup_subclass {
    type: string
    label: "SUP Cubclass"
    sql: ${TABLE}.sup_subclass ;;
  }

  dimension: taxo_class {
    type: string
    label: "Taxonomy Class"
    sql: ${TABLE}.taxo_class ;;
  }

  dimension: taxo_subclass {
    type: string
    label: "Taxonomy Subclass"
    sql: ${TABLE}.taxo_subclass ;;
  }

  dimension: total_square_ft {
    type: number
    label: "Total Square Foot"
    sql: ${TABLE}.total_square_ft ;;
  }

  dimension: uda_16_attribute {
    type: string
    hidden: yes
    sql: ${TABLE}.uda_16_attribute ;;
  }

  dimension: uda_zone {
    type: string
    label: "Zone"
    sql: ${TABLE}.uda_zone ;;
  }

  dimension: unit_cost {
    type: number
    sql: ${TABLE}.unit_cost ;;
  }

  dimension: unit_cost_usd {
    type: number
    sql: ${TABLE}.unit_cost_usd ;;
  }

  dimension: unit_retail_local {
    type: number
    sql: ${TABLE}.unit_retail_local ;;
  }

  dimension: unit_retail_usd {
    type: number
    sql: ${TABLE}.unit_retail_usd ;;
  }

  dimension: unit_selling_price_usd {
    type: number
    sql: ${TABLE}.unit_selling_price_usd ;;
  }

  dimension: usage_specificity {
    type: string
    sql: ${TABLE}.usage_specificity ;;
  }

  dimension: vertical {
    type: string
    label: "Vertical"
    sql: ${TABLE}.vertical ;;
  }

  dimension: vpn {
    type: string
    label: "VPN"
    sql: ${TABLE}.vpn ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      area_name,
      loc_name,
      group_name,
      region_name,
      class_name,
      chain_name,
      channel_name,
      subclass_name,
      mall_name,
      district_name,
      dept_name
    ]
  }

}

view: stock_dates {

  dimension_group: stock {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: TIMESTAMP(${TABLE}) ;;
  }
}
