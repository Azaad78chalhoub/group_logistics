view: carolina_herrera_handbags_mapping {
  label: "Handbags Mapping"
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_handbags_mapping`
    ;;

  dimension: collection_name {
    type: string
    label: "Collection Name"
    drill_fields: [model]
    sql: ${TABLE}.collection_name ;;
  }

  dimension: identifier {
    type: string
    hidden: yes
    sql: ${TABLE}.identifier ;;
  }

  dimension: model {
    type: string
    label: "Model"
    drill_fields: [carolina_herrera_sales_at_time_interval.sku_externalcode]
    sql: ${TABLE}.model ;;
  }

  dimension: ref_code_wo_size {
    type: string
    label: "Refernce code w/o size"
    sql: ${TABLE}.ref_code_wo_size ;;
  }

  dimension: ref_code_wt_size {
    type: string
    label: "Refernce code"
    sql: ${TABLE}.ref_code_wt_size ;;
  }

  measure: count {
    type: count
    drill_fields: [collection_name]
  }
}
