include: "../_common/period_over_period.view"



view: order_sums {
  derived_table: {
    persist_for: "24 hours"
    sql:
        WITH calendar AS( SELECT
  *
  FROM
    UNNEST(GENERATE_DATE_ARRAY( DATE({% date_start season %}),IFNULL(DATE({% date_end sales_season %}),current_date()), INTERVAL 1 DAY)) AS date)
 , sales_sum AS(
   SELECT
  sales.bk_businessdate AS date,
  bk_storeid as location,
  bk_productid AS item,
  0 AS qty_ordered,
  0 AS qty_received,
  0 AS received_cost,
  SUM(mea_quantity) AS quantity_sold,
  SUM(CASE WHEN (discountamt_usd IS NULL OR discountamt_usd = 0) THEN mea_quantity ELSE 0 END) quantity_sold_fp,
  SUM(CASE WHEN (discountamt_usd <> 0) THEN mea_quantity ELSE 0 END) quantity_sold_md,
  SUM(IFNULL(av_cost_local * mea_quantity,0)) AS cogs
  FROM `chb-prod-supplychain-data.prod_supply_chain.factretailsales`  sales
WHERE
 atr_trandate >= DATE({% date_start sales_season %}) AND atr_trandate <= IFNULL(DATE({% date_end sales_season %}),current_date())
  GROUP BY
  bk_businessdate,
  location,
  item
 ),
 orders_sum AS(
 SELECT
  DATE(written_date) as date,
  location,
  orders.item,
  SUM(IFNULL(qty_ordered,0)) AS qty_ordered,
  SUM(IFNULL(qty_received,0)) AS qty_received,
  SUM(IFNULL(qty_received * unit_cost_usd,0)) AS received_cost,
  0 AS quantity_sold,
  0 AS quantity_sold_fp,
  0 AS quantity_sold_md,
  0 AS cogs
FROM
  `chb-prod-supplychain-data.prod_supply_chain.dm_supply_orders` orders
WHERE
written_date  >= {% date_start season %} AND written_date <=  IFNULL({% date_end season %},CAST(current_date() AS TIMESTAMP))
GROUP BY
  written_date,
  location,
  item
 ),
distincts as (SELECT distinct location,
item FROM(SELECT distinct location,
item from orders_sum
UNION ALL
SELECT distinct location,
item from sales_sum
WHERE CASE WHEN {% parameter parameter2 %} IS TRUE THEN
item IN (SELECT item FROM orders_sum)
ELSE item IS NOT NULL
END))
SELECT
cross_table.date,
cross_table.location,
cross_table.item,
SUM(IFNULL(orders_sum.qty_ordered,0)) AS qty_ordered,
SUM(SUM(IFNULL(orders_sum.qty_ordered,0))) OVER (PARTITION BY cross_table.location, cross_table.item ORDER BY cross_table.date ASC) AS running_qty_ordered,
SUM(IFNULL(orders_sum.qty_received,0)) AS qty_received,
SUM(SUM(IFNULL(orders_sum.qty_received,0))) OVER (PARTITION BY cross_table.location, cross_table.item ORDER BY cross_table.date ASC) AS running_qty_received,
SUM(IFNULL(orders_sum.received_cost,0)) AS received_cost,
SUM(SUM(IFNULL(orders_sum.received_cost,0))) OVER (PARTITION BY cross_table.location, cross_table.item ORDER BY cross_table.date ASC) AS running_received_cost,
SUM(IFNULL(sales_sum.quantity_sold,0)) AS quantity_sold,
SUM(SUM(IFNULL(sales_sum.quantity_sold,0))) OVER (PARTITION BY cross_table.location, cross_table.item ORDER BY cross_table.date ASC) AS running_qty_sold,
SUM(IFNULL(sales_sum.quantity_sold_fp,0)) AS quantity_sold_fp,
SUM(SUM(IFNULL(sales_sum.quantity_sold_fp,0))) OVER (PARTITION BY cross_table.location, cross_table.item ORDER BY cross_table.date ASC) AS running_qty_sold_fp,
SUM(IFNULL(sales_sum.quantity_sold_md,0)) AS quantity_sold_md,
SUM(SUM(IFNULL(sales_sum.quantity_sold_md,0))) OVER (PARTITION BY cross_table.location, cross_table.item ORDER BY cross_table.date ASC) AS running_qty_sold_md,
SUM(IFNULL(sales_sum.cogs,0)) AS cogs,
SUM(SUM(IFNULL(sales_sum.cogs,0))) OVER (PARTITION BY cross_table.location, cross_table.item ORDER BY cross_table.date ASC) AS running_cogs,
FROM(
SELECT
calendar.date as date,
location,
item FROM
calendar
CROSS JOIN distincts) cross_table
LEFT JOIN sales_sum ON
cross_table.date=sales_sum.date
AND cross_table.location=sales_sum.location
AND cross_table.item=sales_sum.item
LEFT JOIN orders_sum ON
cross_table.date=orders_sum.date
AND cross_table.location=orders_sum.location
AND cross_table.item=orders_sum.item
GROUP BY
date,
location,
item
    ;;
  }

  extends: [period_over_period]

  dimension_group: order {
    type: time
    timeframes: [
      raw,
      date,
      day_of_month,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }
  measure: last_updated_date {
    type: date
    label: "Last refresh date"
    sql: MAX(${order_raw}) ;;
    convert_tz: no
  }

filter: season {
  type: date
  label: "Purchase Season"
  default_value: "after 2019/03/01"
}

  parameter: parameter2 {
type: yesno
label: "Only Purchased Products"
  }


  filter: sales_season {
    type: date
    label: "Sales Season"
    default_value: "after 2019/10/01"
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${order_date}, "-", ${item}, "-",${location}) ;;
  }

  dimension: weekno {
    hidden: no
    type: number
    value_format: "0"
    label: "Week No."
    sql:CAST(EXTRACT(WEEK(MONDAY) FROM ${order_date} +1) AS INT64) ;;
  }

  dimension: yearno {
    hidden: no
    type: number
    value_format: "0"
    label: "Year No."
    sql:EXTRACT(YEAR FROM ${order_date})  ;;
  }


  dimension: item {
    type: string
    description: "Product number from Oracle RMS"
    label: "Item No"
    primary_key: no
    sql: CAST(${TABLE}.item AS STRING) ;;
  }


  dimension_group: pop_no_tz {
    sql: ${order_raw} ;;
  }

  dimension: location {
    type: number
    label: "Location Code"
    value_format: "0"
    primary_key: no
    sql: ${TABLE}.location ;;
  }

  dimension: running_qty_ordered {
    type: number
    sql: ${TABLE}.running_qty_ordered ;;
  }

#
#   dimension: running_qty_sold {
#     type: number
#     sql: ${TABLE}.running_qty_sold ;;
#   }


  dimension: qty_received {
    type: number
    primary_key: no
    hidden: yes
    sql: ${TABLE}.qty_received ;;
  }

  dimension: qty_ordered {
    type: number
    primary_key: no
    hidden: yes
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: quantity_sold {
    type: number
    primary_key: no
    hidden: yes
    sql: ${TABLE}.quantity_sold ;;
  }

  dimension: quantity_sold_fp {
    type: number
    primary_key: no
    hidden: yes
    sql: ${TABLE}.quantity_sold_fp ;;
  }

  dimension: cogs {
    type: number
    hidden: yes
    sql: ${TABLE}.cogs ;;
  }

  dimension: received_cost {
    type: number
    hidden: yes
    sql: ${TABLE}.received_cost ;;
  }

  dimension: running_qty_received {
    type: number
    hidden: yes
    sql: ${TABLE}.running_qty_received ;;
  }

  dimension: running_qty_sold {
    type: number
    hidden: yes
    sql: ${TABLE}.running_qty_sold ;;
  }

  dimension: running_qty_sold_fp {
    type: number
    hidden: yes
    sql: ${TABLE}.running_qty_sold_fp ;;
  }

  dimension: running_qty_sold_md {
    type: number
    hidden: yes
    sql: ${TABLE}.running_qty_sold_md ;;
  }

  dimension: running_cogs {
    type: number
    hidden: yes
    sql: ${TABLE}.running_cogs ;;
  }

  dimension: running_received_cost {
    type: number
    hidden: yes
    sql: ${TABLE}.running_received_cost ;;
  }

  dimension: quantity_sold_md {
    type: number
    primary_key: no
    hidden: yes
    sql: ${TABLE}.quantity_sold_md ;;
  }

    dimension: Category_basedon_division_TB {
    group_label: "Products"
    description: "Product categories for Tory Burch"
    label: "Category"
    type: string
    sql:CASE WHEN ${dim_retail_product.division} ="APPAREL" AND  ${dim_retail_product.sub_line} LIKE "%Basic%" Then "BASIC"
              WHEN ${dim_retail_product.division} ="APPAREL" AND  ${dim_retail_product.sub_line}  LIKE "%BASIC%" Then "BASIC"
              WHEN ${dim_retail_product.division} ="APPAREL" AND  ${dim_retail_product.sub_line}  LIKE "%FASHION%" Then "FASHION"
              WHEN ${dim_retail_product.division} ="APPAREL" Then "RTW"
              WHEN ${dim_retail_product.division} ="APPAREL COMPLEMENTS" AND  ${dim_retail_product.sub_line}  LIKE "%RC%" Then "BELTS"
              WHEN ${dim_retail_product.division} = "APPAREL COMPLEMENTS" THEN "ACC"
              WHEN ${dim_retail_product.division} = "COSTUME JEWELRY" THEN "ACC"
              WHEN ${dim_retail_product.division} = "JEWELRY" THEN "ACC"
              WHEN ${dim_retail_product.division} = "DECORATION" THEN "ACC"
              WHEN ${dim_retail_product.division} = "TOYS & GAMES" THEN "ACC"
              WHEN ${dim_retail_product.division} = "BATH & BEACHWEAR" Then "RTW"
              WHEN ${dim_retail_product.division} = "EYE WEAR" Then "EYE WEAR"
              WHEN ${dim_retail_product.division} = "FOOTWEAR" Then "SHOES"
              WHEN ${dim_retail_product.division} = "FRAGRANCES" Then "FRAGRANCES"
              WHEN ${dim_retail_product.division} = "AMBIENT SCENTS" Then "FRAGRANCES"
              WHEN ${dim_retail_product.division} = "GWP" Then "GWP"
              WHEN ${dim_retail_product.division} = "WATCHES" Then "WATCHES"
              WHEN ${dim_retail_product.division} = "LIGHT ELECTRONICS" Then "WATCHES"
              WHEN ${dim_retail_product.division} = "SERVICE ITEMS" Then "SERVICE ITEMS"
              WHEN ${dim_retail_product.division} = "PLV" Then "PLV"
              WHEN ${dim_retail_product.division} = "LEATHER GOODS & LUGGAGE" AND  ${dim_retail_product.taxo_class} LIKE "%BAG%" Then "HANDBAGS"
              WHEN ${dim_retail_product.division} = "LEATHER GOODS & LUGGAGE" AND  ${dim_retail_product.taxo_class} LIKE "%SMALL LEATHER GOODS%" Then "SLG"
              WHEN ${dim_retail_product.division} = "LEATHER GOODS & LUGGAGE" Then "HANDBAGS"
              WHEN ${dim_retail_product.division} = "MEDICARE" Then "MEDICARE"
              WHEN ${dim_retail_product.division}= "INTIMATE" Then "INTIMATE"
              WHEN ${dim_retail_product.division} = "CHARITY" Then "CHARITY"
              WHEN ${dim_retail_product.division} = "SERVICE MATERIAL" Then "SERVICE MATERIAL"
              END;; }
#######################################
#####        ZONE TARGETS         #####
#######################################

  measure: total_quantity_ordered {
    label: "Quantity ordered"
    type: sum
    description: "Aggregate quantity of units ordered"
    sql: ${qty_ordered} ;;
  }

  measure: total_quantity_received {
    label: "Quantity received"
    type: sum
    description: "Aggregate quantity of units received"
    sql: ${qty_received} ;;
  }

  measure: total_quantity_sold {
    label: "Quantity sold"
    description: "Aggregate quantity of units sold"
    type: sum
    sql: ${quantity_sold} ;;
  }

  measure: total_quantity_sold_on_fp {
    label: "Full Price quantity sold"
    description: "Aggregate quantity of units sold on Full Price"
    type: sum
    sql: ${quantity_sold_fp} ;;
  }

  measure: total_quantity_sold_on_md {
    label: "Markdown quantity sold"
    description: "Aggregate quantity of units sold on Discount"
    type: sum
    sql: ${quantity_sold_md} ;;
  }

  measure: running_sum {
    type: running_total
    hidden: yes
    sql: ${qty_ordered} ;;
  }

  measure: sell_through_rate_on_value {
    type: number
    hidden: yes
    value_format_name: percent_2
    label: "ST Rate % on Value"
    sql: SAFE_DIVIDE(SUM(${cogs}),SUM(${received_cost})) ;;
  }


  measure: sell_through_rate_on_received {
    type: number
    value_format_name: percent_2
    label: "ST Rate % on Received"
    description: "Aggregate quantity of units sold divided by the aggregate quantity of units received"
    sql: SAFE_DIVIDE(SUM(${quantity_sold}),SUM(${qty_received})) ;;
  }

  measure: sell_through_rate_on_purchase {
    type: number
    value_format_name: percent_2
    label: "ST Rate % on Purchased"
    description: "Aggregate quantity of units sold divided by the aggregate quantity of units ordered"
    sql: SAFE_DIVIDE(SUM(${quantity_sold}),SUM(${qty_ordered})) ;;
  }

  measure: sell_through_rate_fp_on_received {
    type: number
    value_format_name: percent_2
    label: "ST Rate % FP on Received"
    description: "Aggregate quantity of units sold at Full Price divided by the aggregate quantity of units received"
    sql: SAFE_DIVIDE(SUM(${quantity_sold_fp}),SUM(${qty_received})) ;;
  }

  measure: sell_through_rate_fp_on_purchase {
    type: number
    value_format_name: percent_2
    label: "ST Rate % FP on Purchased"
    description: "Aggregate quantity of units sold at Full Price divided by the aggregate quantity of units ordered"
    sql: SAFE_DIVIDE(SUM(${quantity_sold_fp}),SUM(${qty_ordered})) ;;
  }

  measure: sell_through_rate_md_on_received {
    type: number
    value_format_name: percent_2
    label: "ST Rate % MD on Received"
    description: "Aggregate quantity of units sold at discount divided by the aggregate quantity of units received"
    sql: SAFE_DIVIDE(SUM(${quantity_sold_md}),SUM(${qty_received})) ;;
  }

  measure: sell_through_rate_md_on_purchase {
    type: number
    value_format_name: percent_2
    label: "ST Rate % MD on Purchased"
    description: "Aggregate quantity of units sold at discount divided by the aggregate quantity of units ordered"
#     sql: IFNULL(SAFE_DIVIDE(SUM(${quantity_sold_md}),SUM(${qty_ordered})) );;
    sql: (SAFE_DIVIDE(SUM(${quantity_sold_md}),SUM(${qty_ordered})) );;

  }

  measure: running_sell_through_rate_on_received {
    type: number
    value_format_name: percent_2
    label: "Running ST Rate % on Received"
    description: "Aggregate quantity of units sold divided by the aggregate quantity of units received since the start of the sales and purchase season"
    view_label: "-- Period over Period"
    sql: IFNULL(SAFE_DIVIDE(
    SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR  ${order_date} =  ${period_1_end}  THEN ${running_qty_sold} ELSE 0 END)
    ,SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_received}
    ELSE 0 END)),0);;
  }

  measure: running_sell_through_rate_on_purchase {
    type: number
    value_format_name: percent_2
    label: "Running ST Rate % on Purchased"
    description: "Aggregate quantity of units sold divided by the aggregate quantity of units ordered since the start of the sales and purchase season"
    view_label: "-- Period over Period"
    sql: IFNULL(SAFE_DIVIDE(
    SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR  ${order_date} =  ${period_1_end}  THEN ${running_qty_sold} ELSE 0 END)
    ,SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_ordered}
    ELSE 0 END)),0);;
  }

  measure: runing_sell_through_rate_fp_on_received {
    type: number
    value_format_name: percent_2
    label: "Running ST Rate % FP on Received"
    description: "Aggregate quantity of units sold on Full Price divided by the aggregate quantity of units received since the start of the sales and purchase season"
    view_label: "-- Period over Period"
    sql: IFNULL(SAFE_DIVIDE(
    SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR  ${order_date} =  ${period_1_end}  THEN ${running_qty_sold_fp} ELSE 0 END)
    ,SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_received}
    ELSE 0 END)),0);;
  }

  measure: weekly_sell_through_rate_fp_on_received {
    type: number
    value_format_name: percent_2
    label: "Period ST Rate % FP on Received"
    view_label: "-- Period over Period"
    sql: SAFE_DIVIDE(
          SUM(${quantity_sold_fp})
          ,SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_received}
          ELSE 0 END));;
  }

  measure: weekly_sell_through_rate_md_on_received {
    type: number
    value_format_name: percent_2
    label: "Period ST Rate % MD on Received"
    view_label: "-- Period over Period"
    sql: SAFE_DIVIDE(
          SUM(${quantity_sold_md})
          ,SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_received}
          ELSE 0 END));;
  }

#######################################
#####        ZONE TARGETS         #####
#######################################

  measure: str_fp_on_received_target {
    type: average
    value_format_name: percent_2
    group_label: "Level Shoes ST Metrics"
    label: "Zone ST Rate % FP on Received Target"
    view_label: "-- Period over Period"
    sql: ${targets_level_zone.str_target_fp};;
  }

  measure: str_md_on_received_target {
    type: average
    value_format_name: percent_2
    group_label: "Level Shoes ST Metrics"
    label: "Zone ST Rate % MD on Received Target"
    view_label: "-- Period over Period"

    sql: ${targets_level_zone.str_target_md};;
  }

  measure: runing_sell_through_rate_fp_on_received_vs_target {
    type: number
    value_format_name: percent_2
    group_label: "Level Shoes ST Metrics"
    label: "ST Rate % FP on Received vs Target"
    view_label: "-- Period over Period"
    sql: SAFE_DIVIDE(
          SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_sold_fp}
          ELSE 0 END)
          ,SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_received}
          ELSE 0 END))-AVG(${targets_level_zone.str_target_fp});;
  }

  measure: runing_sell_through_rate_md_on_received_vs_target {
    type: number
    value_format_name: percent_2
    group_label: "Level Shoes ST Metrics"
    label: "ST Rate % MD on Received vs Target"
    view_label: "-- Period over Period"
    sql: IFNULL(SAFE_DIVIDE(
          SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_sold_md}
          ELSE 0 END)
          ,SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_received}
          ELSE 0 END))-AVG(${targets_level_zone.str_target_md}),0);;
  }

#######################################
#####       BRAND TARGETS         #####
#######################################

  measure: brand_str_fp_on_received_target {
    type: average
    value_format_name: percent_2
    group_label: "Level Shoes ST Metrics"
    label: "Brand ST Rate % FP on Received Target"
    description: "Brand ST Rate % for Full Price items divided by Brand ST Rate % for Full Price items target"
    view_label: "-- Period over Period"
    sql: ${targets_level_brand.str_target_fp};;
  }


  measure: brand_str_md_on_received_target {
    type: average
    value_format_name: percent_2
    group_label: "Level Shoes ST Metrics"
    label: "Brand ST Rate % MD on Received Target"
    description: "Brand ST Rate % for Discounted items divided by Brand ST Rate % for Discounted items target"
    view_label: "-- Period over Period"

    sql: ${targets_level_brand.str_target_md};;
  }

  measure: brand_runing_sell_through_rate_fp_on_received_vs_target {
    type: number
    value_format_name: percent_2
    group_label: "Level Shoes ST Metrics"
    label: "Brand ST Rate % FP on Received vs Target"
    description: "Brand Running ST Rate % for Full Price items divided by Brand ST Rate % for Full Price items target"
    view_label: "-- Period over Period"
    sql:    SAFE_DIVIDE(
          SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_sold_fp}
          ELSE 0 END)
          ,SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_received}
          ELSE 0 END))-
          AVG(${targets_level_brand.str_target_fp});;
  }

  measure: brand_runing_sell_through_rate_md_on_received_vs_target {
    type: number
    value_format_name: percent_2
    group_label: "Level Shoes ST Metrics"
    label: "Brand ST Rate % MD on Received vs Target"
    description: "Brand Running ST Rate % for Discounted items divided by Brand ST Rate % for Discounted items target"
    view_label: "-- Period over Period"
    sql: IFNULL(SAFE_DIVIDE(
          SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_sold_md}
          ELSE 0 END)
          ,SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_received}
          ELSE 0 END))-AVG(${targets_level_brand.str_target_md}),0);;
  }

#######################################
#####       RUNNING STR           #####
#######################################

  measure: running_sell_through_rate_fp_on_purchase {
    type: number
    value_format_name: percent_2
    label: "Running ST Rate % FP on Purchased"
    description: "Aggregate quantity of units sold on Full Price divided by the aggregate quantity of units ordered since the start of the sales and purchase season"
    view_label: "-- Period over Period"
    sql: IFNULL(SAFE_DIVIDE(
    SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR  ${order_date} =  ${period_1_end}  THEN ${running_qty_sold_fp} ELSE 0 END)
    ,SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_ordered}
    ELSE 0 END)),0);;
  }

  measure: running_sell_through_rate_md_on_received {
    type: number
    value_format_name: percent_2
    label: "Running ST Rate % MD on Received"
    description: "Aggregate quantity of units sold on discount divided by the aggregate quantity of units received since the start of the sales and purchase season"
    view_label: "-- Period over Period"
    sql: IFNULL(SAFE_DIVIDE(
    SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR  ${order_date} =  ${period_1_end}  THEN ${running_qty_sold_md} ELSE 0 END)
    ,SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_received}
    ELSE 0 END)),0);;
  }

  measure: running_sell_through_rate_md_on_purchase {
    type: number
    value_format_name: percent_2
    label: "Running ST Rate % MD on Purchased"
    description: "Aggregate quantity of units sold on discount divided by the aggregate quantity of units ordered since the start of the sales and purchase season"
    view_label: "-- Period over Period"
    sql: IFNULL(SAFE_DIVIDE(
    SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR  ${order_date} =  ${period_1_end}  THEN ${running_qty_sold_md} ELSE 0 END)
    ,SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_qty_ordered}
    ELSE 0 END)),0);;
  }

  measure: running_sell_through_rate_on_value {
    type: number
    value_format_name: percent_2
    label: "Running ST Rate % on Value"
    view_label: "-- Period over Period"
    hidden:  yes
    sql: IFNULL(SAFE_DIVIDE(
    SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR  ${order_date} =  ${period_1_end}  THEN ${running_cogs} ELSE 0 END)
    ,SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR ${order_date} =  ${period_1_end} THEN ${running_received_cost}
    ELSE 0 END)),0);;
  }

#######################################
#####       RUNNING QTY           #####
#######################################

  measure: sum_running_qty_ordered {
    type: number
    value_format_name: decimal_0
    label: "Running Quantity Ordered"
    description: "Aggregate quantity of units ordered since the start of the purchase season"
    view_label: "-- Period over Period"
    sql: SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR  ${order_date} =  ${period_1_end}  THEN ${running_qty_ordered} ELSE 0 END);;
  }

  measure: sum_running_qty_received {
    type: number
    value_format_name: decimal_0
    label: "Running Quantity Received"
    description: "Aggregate quantity of units received since the start of the purchase season"
    view_label: "-- Period over Period"
    sql: SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR  ${order_date} =  ${period_1_end}  THEN ${running_qty_received} ELSE 0 END);;
  }

  measure: sum_running_qty_sold {
    type: number
    value_format_name: decimal_0
    label: "Running Quantity Sold"
    description: "Aggregate quantity of units sold since the start of the sales season"
    view_label: "-- Period over Period"
    sql: SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR  ${order_date} =  ${period_1_end}  THEN ${running_qty_sold} ELSE 0 END);;
  }
  measure: sum_running_qty_sold_md {
    type: number
    value_format_name: decimal_0
    label: "Running Quantity Sold MD"
    description: "Aggregate quantity of units sold on Markdown since the start of the sales season"
    view_label: "-- Period over Period"
    sql: SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR  ${order_date} =  ${period_1_end}  THEN ${running_qty_sold_md} ELSE 0 END);;
  }

  measure: sum_running_qty_sold_fp {
    type: number
    value_format_name: decimal_0
    label: "Running Quantity Sold FP"
    description: "Aggregate quantity of units sold on Full Price since the start of the sales season"
    view_label: "-- Period over Period"
    sql: SUM(CASE WHEN ${order_date} = DATE_SUB(${period_2_end}, INTERVAL 1 DAY) OR  ${order_date} =  ${period_1_end}  THEN (${running_qty_sold}-IFNULL(${running_qty_sold_md},0)) ELSE 0 END);;
  }

#######################################
#####         NDT TEMPS           #####
#######################################


  measure: sum_qty_received {
    type: sum
    value_format_name: decimal_0
    label: "Total Quantity Received"
    sql: ${qty_received};;
  }

  measure: sum_cost_received {
    type: sum
    value_format_name: decimal_0
    label: "Total Cost Received"
    sql: ${received_cost};;
  }

}
