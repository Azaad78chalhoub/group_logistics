view: carolina_herrera_ytd_sales {
  derived_table: {
    sql: select store,
                sku_externalcode,
                sum(quantity) as total_quantity,
                sum(totalnet_usd) as total_netsales

      from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_sales_at_time_interval`
      where EXTRACT(YEAR from dateop ) = EXTRACT(YEAR FROM CURRENT_DATETIME())
      GROUP BY 1,2
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: store {
    type: number
    hidden: yes
    sql: ${TABLE}.store ;;
  }

  dimension: sku_externalcode {
    type: string
    hidden: yes
    sql: ${TABLE}.sku_externalcode ;;
  }

  dimension: total_quantity {
    type: number
    hidden: no
    label: "YTD quantity sold"
    sql: ${TABLE}.total_quantity ;;
  }

  dimension: ytd_sales_netsales {
    type: number
    label: "YTD Net sales USD"
    sql: ${TABLE}.total_netsales ;;
  }

  set: detail {
    fields: [store, sku_externalcode, total_quantity,ytd_sales_netsales]
  }
}
