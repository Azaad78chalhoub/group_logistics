view: udas {
  label: "User Defined Attributes"
  derived_table: {
    sql_trigger_value: SELECT CURDATE() ;;
    sql: SELECT * FROM(
SELECT
lov.*, val.uda_value_desc
FROM

  `chb-prod-supplychain-data.prod_shared_dimensions.dim_uda_item_lov` lov
  LEFT JOIN
    `chb-prod-supplychain-data.prod_shared_dimensions.dim_uda_values` val

ON
  val.uda_id= lov.uda_id
  AND
 val.uda_value= lov.uda_value
  )
  WHERE uda_value_desc LIKE "CARRY%OVER" ;;

  }

  dimension: item {
    type: string
    hidden: yes
    sql: ${TABLE}.item ;;
  }

  dimension: uda_id {
    type: string
    hidden: yes
    sql: ${TABLE}.uda_id ;;
  }

  dimension: uda_value {
    type: string
    hidden: yes
    sql: ${TABLE}.uda_value ;;
  }

  dimension: pk {
    type: string
    hidden: yes
    primary_key: yes
    sql: CONCAT(${item}, " - ", ${uda_id}, " - ", ${uda_value}) ;;
  }

  dimension: uda_value_desc {
    type: string
    label: "Carry Over Attribute"
    hidden: no
    sql: ${TABLE}.uda_value_desc ;;
  }

}
