view: inv_adj_reason {
  sql_table_name: `chb-prod-stage-oracle.prod_oracle_rms.inv_adj_reason`
    ;;

  dimension: cogs_ind {
    type: yesno
    sql: ${TABLE}.cogs_ind ;;
  }

  dimension: reason {
    type: number
    sql: ${TABLE}.reason ;;
  }

  dimension: reason_desc {
    type: string
    sql: ${TABLE}.reason_desc ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
