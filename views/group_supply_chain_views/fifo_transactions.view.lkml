view: fifo_transactions {
  derived_table: {
    sql: --Extract continuous dates from starting of receipt date(purchase season) till end of sales season to be used as timeline
      With calendar_date as (
            SELECT  *
            FROM
                UNNEST(GENERATE_DATE_ARRAY(CAST(IFNULL({% date_start season %},CURRENT_TIMESTAMP()) AS DATE),
                CAST(IFNULL(DATE({% date_end sales_season %}),CURRENT_DATE()) AS DATE), INTERVAL 1 DAY)) AS date
            ),

      subtable2 AS
        (
      -- selecting items and qty received in the season for the specific business unit
      SELECT
          orders.item,
          receipt.receipt_date,
          IFNULL(SUM(receipt.qty_received),0) AS total_received,
          IFNULL(SUM(receipt.qty_received*unit_cost_usd),0) AS value_received
        FROM
          `chb-prod-supplychain-data.prod_supply_chain.orders` orders
        LEFT JOIN
          `chb-prod-supplychain-data.prod_shared_dimensions.dim_bu_cdl` dims
        ON
          orders.item=dims.prod_num
          AND CAST(orders.location AS STRING)=dims.org_num
        --Joining the orders with receipt data to get the date of receipt against POs
        LEFT JOIN
        `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dm_receipts_detailed_all` receipt
        ON
        CAST(orders.order_no AS STRING) = CAST(receipt.order_no AS STRING)
        AND CAST(orders.item AS STRING) = CAST(receipt.item AS STRING)
        AND CAST(orders.location AS STRING)= CAST(receipt.location AS STRING)

        LEFT JOIN
        `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` diml
         ON CAST(orders.location AS STRING)=CAST(diml.store as STRING)

      -- joining to get the right BU for the item
        LEFT JOIN
          `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` bu
        ON
          dims.business_unit_final=bu.bu_code

        WHERE
          CAST(receipt.receipt_date as TIMESTAMP) >= IFNULL({% date_start season %},CURRENT_TIMESTAMP()) AND CAST(receipt.receipt_date AS TIMESTAMP) <=  IFNULL({% date_end season %},CURRENT_TIMESTAMP())
          AND orders.qty_received IS NOT NULL
          AND brand=IFNULL({% parameter bu_brand %}, "LACOSTE") AND diml.chain_name = 'Retail'

        GROUP BY
          1,2
        ),

      --Joining total received quantity and value across calendar date timeline established in step 1
      subtable3 as (
          Select a.item,
                  a.date,
                  IFNULL(b.total_received,0) as total_received,
                  IFNULL(b.value_received,0) as value_received
          from
          (
          select item,
                  date
          from
               (select distinct item from subtable2) a
          cross join
              (select b.date from calendar_date b)
          order by item,date) a
          left join subtable2 b
          on a.item = b.item
          and a.date = b.receipt_date

          ),

--Calculating the transfer quantity to the deal
      subtable4 as (
         SELECT a.*,
        CASE WHEN {% parameter deal_transfer %}= "no" THEN 0 ELSE IFNULL(b.transferred_qty,0) END AS transferred_qty,
        --To eliminate the stocks transferred to the deal
                    (IFNULL(a.total_received,0) - (CASE WHEN {% parameter deal_transfer %}= "no" THEN 0 ELSE IFNULL(b.transferred_qty,0) END)) as total_received_updated
          FROM
               subtable3 a
          LEFT JOIN
                (
                SELECT item,create_date,
                        IFNULL(sum(tsf_qty),0) as transferred_qty
                FROM  `chb-prod-supplychain-data.prod_supply_chain.dm_stock_transfers` transfer

                  LEFT JOIN
              `chb-prod-supplychain-data.prod_shared_dimensions.dim_bu_cdl` dims
              ON
             cast(transfer.item as string)=cast(dims.prod_num as string)
              AND CAST(transfer.from_loc AS STRING)=cast(dims.org_num as string)

               LEFT JOIN
                `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` bu
               ON
                  dims.business_unit_final=bu.bu_code
                LEFT JOIN
        `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` diml
         ON CAST(transfer.from_loc AS STRING)=CAST(diml.store as STRING)


                WHERE item in (select distinct item from  subtable2)
                and CAST(create_date AS TIMESTAMP) >= IFNULL({% date_start Create_date %},CURRENT_TIMESTAMP()) and CAST(create_date AS TIMESTAMP) <= IFNULL({% date_end Create_date %},CURRENT_TIMESTAMP())
               --and to_district_name LIKE '%FACTORY OUTLET%' and from_district_name like "%LACOSTE%"
              --and bu.brand = 'LACOSTE'
              and to_district_name LIKE '%FACTORY OUTLET%' and diml.chain_name = "Retail"
                group by 1,2) b

        ON a.item = b.item
        and a.date = b.create_date
      )

    select a.item,
          a.date as timeline_date,
          IFNULL(a.total_received,0) as item_date_total_received,
          IFNULL(a.value_received,0) as item_date_value_received,
          IFNULL(a.transferred_qty,0) as item_date_transferred_qty,
          --to filter and sum total received and value received at item, date level
          RANK() OVER(PARTITION BY a.item,a.date,a.total_received ORDER BY b.id ASC) AS rank_item_date,
          --Quantity : FIFO applied for both non-deal and deal
          case when IFNULL(b.running_quantity_non_deal,0) <= IFNULL(b.total_received,0) and (bu.brand != 'THE DEAL') then b.mea_quantity else 0 end as mea_quantity_non_deal,
          case when IFNULL(b.running_quantity_non_deal,0) <= IFNULL(b.total_received,0) and (bu.brand != 'THE DEAL') then b.amountusd_beforetax else 0 end as amountusd_beforetax_non_deal,
          case when IFNULL(b.running_quantity_non_deal,0) <= IFNULL(b.total_received,0) and (bu.brand != 'THE DEAL') then b.discountamt_usd else 0 end as discountamt_usd_non_deal,


          case when IFNULL(b.running_quantity_deal,0) <= IFNULL(b.Transferred_qty,0) AND (bu.brand = 'THE DEAL') then mea_quantity else 0 end as mea_quantity_deal,
          case when IFNULL(b.running_quantity_deal,0) <= IFNULL(b.Transferred_qty,0) AND ( bu.brand = 'THE DEAL') then b.amountusd_beforetax else 0 end as amountusd_beforetax_deal,
          case when IFNULL(b.running_quantity_deal,0) <= IFNULL(b.Transferred_qty,0) AND ( bu.brand = 'THE DEAL') then b.discountamt_usd else 0 end as discountamt_usd_deal,
          b.*
          FROM
              subtable4 a
          left join
            (
              select *
              from
              (
                SELECT
                sales.*,
                subtable2.avg_unit_cost,
                IFNULL(subtable2.total_received,0) as total_received,
                IFNULL(subtable2.value_received,0) as value_received,
                IFNULL(subtable2.Transferred_qty,0) as Transferred_qty,

                SUM(SUM(case when bu.brand != 'THE DEAL' then mea_quantity else 0 end))
                OVER(PARTITION BY bk_productid ORDER BY atr_trandate, id ASC) AS running_quantity_non_deal,

                SUM(SUM(case when bu.brand = 'THE DEAL' then mea_quantity else 0 end))
                OVER(PARTITION BY bk_productid ORDER BY atr_trandate, id ASC) AS running_quantity_deal,



                RANK() OVER(PARTITION BY bk_productid ORDER BY atr_trandate
                , id
                ASC) AS row_id

                FROM
                `chb-prod-supplychain-data.prod_supply_chain.factretailsales` sales
                --Getting total received and transferred qty at item level for FIFO for retail and The Deal respectively
                left join (
                            select item,
                                  SAFE_DIVIDE(SUM(value_received),SUM(total_received)) as avg_unit_cost,
                                  --Adjusted total received(-Deal transfer)
                                  IFNULL(SUM(total_received_updated),0) AS total_received,
                                  IFNULL(SUM(value_received),0) AS value_received,
                                  CASE WHEN {% parameter bu_brand %} = "THE DEAL" THEN IFNULL(SUM(total_received_updated),0)
                                      ELSE IFNULL(SUM(transferred_qty),0) END AS Transferred_qty
                                  from subtable4
                                  group by 1) subtable2
                on sales.bk_productid=subtable2.item

              --To get BU
              LEFT JOIN
              `chb-prod-supplychain-data.prod_shared_dimensions.dim_bu_cdl` dims
              ON
             cast(sales.bk_productid as string)=cast(dims.prod_num as string)
              AND CAST(sales.bk_storeid AS STRING)=cast(dims.org_num as string)

               LEFT JOIN
                `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` bu
                ON
                  dims.business_unit_final=bu.bu_code

                WHERE
                  atr_trandate  >= IFNULL(DATE({% date_start sales_season %}),CURRENT_DATE()) AND atr_trandate <=   IFNULL(DATE({% date_end sales_season %}),CURRENT_DATE()) and sales.mea_quantity > -100

                GROUP BY
                  id,
                  _fivetran_deleted,
                  _fivetran_synced,
                  amountlocal_beforetax,
                  amountusd_beforetax,
                  atr_action,
                  atr_activitytype,
                  atr_batchid,
                  atr_cginvoiceno,
                  atr_cginvoicenoline,
                  atr_createdate,
                  atr_day,
                  atr_documentdate,
                  atr_errordesc,
                  atr_homecurrency,
                  atr_itemseqno,
                  atr_itemstatus,
                  atr_membershipid,
                  atr_muse_id,
                  atr_promotionid,
                  atr_promotionid2,
                  atr_salesperson,
                  atr_stagestatus,
                  atr_status,
                  atr_storedayseqno,
                  atr_time,
                  atr_timezone,
                  atr_tranno,
                  atr_transeqno,
                  atr_trantype,
                  atr_updatedate,
                  av_cost_local,
                  av_cost_usd,
                  bk_brand,
                  bk_businessdate,
                  bk_customer,
                  bk_loyalty,
                  bk_loyaltyprogram,
                  bk_productid,
                  bk_salestype,
                  bk_storeid,
                  bk_top20,
                  creationdatetime,
                  crm_customer_id,
                  customer_golden_id,
                  discountamt_local,
                  discountamt_usd,
                  mea_amountlocal,
                  mea_amountusd,
                  mea_quantity,
                  pos_customer_id,
                  taxamt_local,
                  taxamt_usd,
                  sales_channel,
                  register,
                  atr_trandate,
                  CONVERSION_RATE,
                  consignment_flag,
                 unit_retail_usd,
                  total_received,
                  value_received,
                  avg_unit_cost,
                 -- transferred_qty,
                Transferred_qty,
                  unit_retail_local)
                --WHERE running_quantity_non_deal <= total_received and running_quantity_deal <= Transferred_qty
                ) b
            on a.item = b.bk_productid
            and a.date = b.atr_trandate
          LEFT JOIN
              `chb-prod-supplychain-data.prod_shared_dimensions.dim_bu_cdl` dims
              ON
             cast(b.bk_productid as string)=cast(dims.prod_num as string)
              AND CAST(b.bk_storeid AS STRING)=cast(dims.org_num as string)

               LEFT JOIN
                `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` bu
                ON
                  dims.business_unit_final=bu.bu_code
            order by 1,2;;
  }


  filter: sales_season {
    type: date
    label: "Sales Season"
    default_value: "after 2012/05/01"
  }


  filter: season {
    type: date
    label: "Purchase Season"
    default_value: "after 2016/12/01"
  }

  filter: Create_date {
    type: date
    label: "Transfer Window"
    default_value: "after 2017/07/01"
  }

  parameter: bu_brand {
    label: "Business Unit"
    type: string
    allowed_value: {
      label: "SWAROVSKI"
      value: "SWAROVSKI"
    }
    allowed_value: {
      label: "LACOSTE"
      value: "LACOSTE"
    }
    allowed_value: {
      label: "FILA"
      value: "FILA"
    }
    allowed_value: {
      label: "LEVEL SHOES"
      value: "LEVEL SHOES"
    }
    allowed_value: {
      label: "D&G"
      value: "D&G"
    }
    allowed_value: {
      label: "LOCCITANE"
      value: "LOCCITANE"
    }
    allowed_value: {
      label: "MICHAEL KORS"
      value: "MICHAEL KORS"
    }
    allowed_value: {
      label: "RALPH LAUREN"
      value: "RALPH LAUREN"
    }
    allowed_value: {
      label: "SAKS"
      value: "SAKS"
    }
    allowed_value: {
      label: "TRYANO"
      value: "TRYANO"
    }

 #   allowed_value: {
  #    label: "THE DEAL"
  #    value: "THE DEAL"
  #  }
    default_value: "LACOSTE"
  }

  parameter: deal_transfer {
    label: "Include Deal"
    type: string
    allowed_value: {
      label: "Yes"
      value: "yes"
    }

    allowed_value: {
      label: "No"
      value: "no"
    }

    default_value: "no"
  }

  dimension: amountusd_beforetax {
    hidden: yes
    type: number
    sql: ${TABLE}.amountusd_beforetax_non_deal ;;
  }

  dimension: mea_quantity {
    type: number
    hidden: yes
    sql: ${TABLE}.mea_quantity_non_deal ;;
  }

  dimension: discountamt_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.discountamt_usd_non_deal ;;
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${business_raw}, ${amountusd_beforetax}, ${item},${Timeline_date})} ;;
  }

  dimension: item {
    hidden: no
    label: "Item"
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: total_received {
    hidden: no
    type: number
    sql: ${TABLE}.item_date_total_received ;;
  }

  dimension: rank_item_date {
    type: number
    hidden: yes
    sql: ${TABLE}.rank_item_date;;
  }

  dimension: avg_unit_cost {
    type: number
    hidden: yes
    sql: ${TABLE}.avg_unit_cost;;
  }


  dimension: value_received_usd {
    hidden: no
    type: number
    sql: ${TABLE}.item_date_value_received;;
  }

  dimension: transferred_qty {
    hidden: no
    type: number
    sql: ${TABLE}.item_date_transferred_qty ;;
  }

  dimension: quantity_received {
    hidden: no
    type: number
    sql: CASE WHEN ${rank_item_date} = 1 THEN ${total_received} ELSE 0 END ;;
  }

  dimension: value_received {
    hidden: no
    type: number
    sql: CASE WHEN ${rank_item_date} = 1 THEN ${value_received_usd} ELSE 0 END ;;
  }

  dimension: deal_transferred_qty {
    hidden: no
    type: number
    sql: case when ${rank_item_date} = 1 then ${transferred_qty} ELSE 0 END ;;
  }

  dimension: row_id {
    type: number
    hidden: yes
    sql: ${TABLE}.row_id ;;
  }

  measure: total_received_updated{
    type: sum
    hidden: no
    label: "Total Received updated"
    filters: {
      field: row_id
      value: "1"
    }
    sql:${TABLE}.total_received ;;
  }

  dimension: av_cost_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.av_cost_usd ;;
  }


  dimension: count_markdown_items {
    type: number
    hidden: yes
    sql:  CASE WHEN ${discountamt_usd} <> 0 THEN ${mea_quantity} ELSE 0 END;;
  }

  dimension: count_markdown_items_non_deal {
    type: number
    hidden: yes
    sql:  CASE WHEN ${discountamt_usd} <> 0 THEN ${mea_quantity} ELSE 0 END;;
  }


  dimension: markdown_transaction {
    type: yesno
    hidden: no
    sql:  ${discountamt_usd} <> 0 ;;
  }

  dimension_group: business{
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: TIMESTAMP(PARSE_DATE('%Y%m%d', CAST(${TABLE}.bk_businessdate AS STRING))) ;;
  }

  dimension_group: Timeline{
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: TIMESTAMP(${TABLE}.timeline_date) ;;
  }

  dimension: bk_productid {
    type: string
    label: "Product ID"
    view_label: "Products"
    sql: ${TABLE}.bk_productid ;;
  }

  dimension: bk_storeid {
    type: string
    label: "Store ID"
    hidden: no
    sql: ${TABLE}.bk_storeid ;;
  }

##Deal vs Non Deal comparison metrics

  measure: deal_transfer_quantity {
    group_label: "Deal vs Non Deal comparison"
    label: "Deal Transfer Quantity"
    description: "Aggregate Quantity Transferred to The Deal"
    type: sum
    value_format_name: decimal_0
    sql: ${deal_transferred_qty} ;;
  }

  measure: total_quantity_sold_non_deal {
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Total Quantity Sold non deal"
    sql: ${TABLE}.mea_quantity_non_deal ;;
  }

  measure: total_quantity_sold_deal {
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Total Quantity Sold in deal"
    sql: ${TABLE}.mea_quantity_deal ;;
  }

  measure:  total_quantity_sold_non_deal_FP{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Total Quantity Sold non deal FP"
    filters: {
      field: markdown_transaction
      value: "no"
    }
    sql: ${TABLE}.mea_quantity_non_deal ;;

  }

  measure:  total_quantity_sold_deal_FP{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Total Quantity Sold deal FP"
    filters: {
      field: markdown_transaction
      value: "no"
    }
    sql: ${TABLE}.mea_quantity_deal ;;

  }

  measure:  total_quantity_sold_non_deal_MD{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Total Quantity Sold non deal MD"
    filters: {
      field: markdown_transaction
      value: "yes"
    }
    sql: ${TABLE}.mea_quantity_non_deal ;;

  }

  measure:  total_quantity_sold_deal_MD{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Total Quantity Sold deal MD"
    filters: {
      field: markdown_transaction
      value: "yes"
    }
    sql: ${TABLE}.mea_quantity_deal ;;

  }

  measure:  total_markdown_investment_non_deal{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Total Markdown Investment Non Deal"
    sql: ${TABLE}.discountamt_usd_non_deal ;;
    value_format_name: usd

  }

  measure:  total_markdown_investment_deal{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Total Markdown Investment Deal"
    sql: ${TABLE}.discountamt_usd_deal ;;
    value_format_name: usd

  }

  measure:  netsales_non_deal{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Net Sales non Deal"
    sql: ${TABLE}.amountusd_beforetax_non_deal ;;
    value_format_name: usd

  }

  measure:  netsales_deal{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Net Sales Deal"
    sql: ${TABLE}.amountusd_beforetax_deal ;;
    value_format_name: usd

  }

  measure:  netsales_non_deal_FP{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Net Sales non Deal FP"
    filters: {
      field: markdown_transaction
      value: "no"
    }
    sql: ${TABLE}.amountusd_beforetax_non_deal ;;
    value_format_name: usd

  }

  measure:  netsales_non_deal_MD{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Net Sales non Deal MD"
    filters: {
      field: markdown_transaction
      value: "yes"
    }
    sql: ${TABLE}.amountusd_beforetax_non_deal ;;
    value_format_name: usd

  }

  measure:  netsales_deal_FP{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Net Sales Deal FP"
    filters: {
      field: markdown_transaction
      value: "no"
    }
    sql: ${TABLE}.amountusd_beforetax_deal ;;
    value_format_name: usd

  }

  measure:  netsales_deal_MD{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "Net Sales Deal MD"
    filters: {
      field: markdown_transaction
      value: "yes"
    }
    sql: ${TABLE}.amountusd_beforetax_deal ;;
    value_format_name: usd

  }



  measure:  cogs_non_deal{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "COGS non Deal"
    sql: (${TABLE}.av_cost_usd)*${TABLE}.mea_quantity_non_deal) ;;
    value_format_name: usd

  }

  measure:  cogs_deal{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "COGS Deal"
    sql: (${TABLE}.av_cost_usd)*${TABLE}.mea_quantity_deal) ;;
    value_format_name: usd

  }

  measure:  cogs_non_deal_FP{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "COGS non Deal FP"
    filters: {
      field: markdown_transaction
      value: "no"
    }
    sql: (${TABLE}.av_cost_usd)*${TABLE}.mea_quantity_non_deal) ;;
    value_format_name: usd

  }

  measure:  cogs_non_deal_MD{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "COGS non Deal MD"
    filters: {
      field: markdown_transaction
      value: "yes"
    }
    sql:(${TABLE}.av_cost_usd)*${TABLE}.mea_quantity_non_deal) ;;
    value_format_name: usd

  }

  measure:  cogs_deal_FP{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "COGS Deal FP"
    filters: {
      field: markdown_transaction
      value: "no"
    }
    sql: (${TABLE}.av_cost_usd)*${TABLE}.mea_quantity_deal) ;;
    value_format_name: usd

  }

  measure:  cogs_deal_MD{
    type: sum
    group_label: "Deal vs Non Deal comparison"
    label: "COGS Deal MD"
    filters: {
      field: markdown_transaction
      value: "yes"
    }
    sql:(${TABLE}.av_cost_usd)*${TABLE}.mea_quantity_deal) ;;
    value_format_name: usd

  }

  measure: quantity_sold_below_purchase_value_deal {
    group_label: "Deal vs Non Deal comparison"
    label: "Quantity sold below purchase value in Deal"
    description: "Aggregate Quantity of Units Sold below purchase value in Deal"
    type: sum
    value_format_name: decimal_0
    sql: case when abs(${TABLE}.amountusd_beforetax_deal) < ABS(ABS(${avg_unit_cost})*${TABLE}.mea_quantity_deal)
      then ${TABLE}.mea_quantity_deal else 0 end ;;
  }


#------------Gross Margin---------------

  measure: gross_margin {
    type: number
    hidden: no
    label: "Gross Margin USD"
    group_label: "Gross Margin"
    description: "Net Sales (USD) minus Cost of Goods Sold (USD)"
    sql: ${net_sales_amount_usd} - ${cogs_usd} ;;
    value_format_name: usd
  }

  measure: gross_margin_share {
    type: number
    hidden: no
    label: "Gross Margin %"
    group_label: "Gross Margin"
    description: "Percentage of Gross Margin (USD) vs Net Sales (USD)"
    value_format_name: percent_2
    sql: SAFE_DIVIDE(${gross_margin},${net_sales_amount_usd}) ;;
  }


  measure: GrossMarginUSD_onMD{
    label: "Gross Margin USD on Markdown"
    description: "Net Sales (USD) minus Cost of Goods Sold (USD) of items sold at a discount"
    group_label: "Gross Margin"
    type: number
    value_format_name: usd
    sql: ${NetSalesUSD_onMD}-${COGSUSD_onMD}
      ;;
  }

  measure: GrossMarginUSD_onFP{
    label: "Gross Margin USD on Full Price"
    description: "Net Sales (USD) minus Cost of Goods Sold (USD) of items sold at full price"
    group_label: "Gross Margin"
    type: number
    value_format_name: usd
    sql: ${NetSalesUSD_onFP}-${COGSUSD_onFP}
      ;;
  }

  # measure: gross_margin_local {
  #   type: number
  #   hidden: no
  #   label: "Gross Margin Local"
  #   description: "Net Sales (local currency) minus Cost of Goods Sold (local currency)"
  #   group_label: "Gross Margin"
  #   sql: ${net_sales_amount_local} -${cogs_local} ;;
  #   value_format_name: decimal_0
  # }

#  SPD-257 changes ends
#----------------------------NET SALES CALCULATIONS -------------------------------
  measure: net_sales_amount_usd {
    description: "Gross Sales - Returns - Discounts, use with Customer ID for LTV"
    type: sum
    group_label: "Net Sales"
    value_format_name: usd
    sql: ${amountusd_beforetax} ;;
  }
  measure: NetSalesUSD_onFP{
    label: "Net Sales Amount USD on Full Price"
    description: "Net Sales (USD) made on items sold at full price (no discount)"
    group_label: "Net Sales"
    type: sum
    value_format_name: usd
    sql: CASE WHEN ${discountamt_usd} = 0 or  ${discountamt_usd} is NULL THEN (${amountusd_beforetax})
      ELSE null END ;;
  }
  measure: NetSalesUSD_onMD{
    label: "Net Sales Amount USD on Markdown"
    description: "Net Sales (USD) made on items sold at discount"
    group_label: "Net Sales"
    type: sum
    value_format_name: usd
    sql: CASE WHEN ${discountamt_usd} <> 0 THEN (${amountusd_beforetax})
      ELSE null END ;;
  }



#-------------Qunatity----------------
  measure: total_qty_received{
    group_label: "Quantity metrics"
    label: "Total Quantity Received"
    type: sum
    value_format_name: decimal_0
    sql: ${quantity_received}
      ;;
  }

  measure: total_quantity_sold {
    group_label: "Quantity metrics"
    type: sum
    label: "Total Quantity Sold"
    sql: ${mea_quantity} ;;
  }

  measure: quantity_sold {
    group_label: "Quantity metrics"
    description: "Aggregate Quantity of Units Sold"
    type: sum
    value_format_name: decimal_0
    sql: ${mea_quantity} ;;
  }

  measure: quantity_sold_below_purchase_value {
    group_label: "Quantity metrics"
    description: "Aggregate Quantity of Units Sold below purchase value"
    type: sum
    value_format_name: decimal_0
    sql: case when abs(${TABLE}.amountusd_beforetax_non_deal) < ABS(ABS(${avg_unit_cost})*${TABLE}.mea_quantity_non_deal)
      then ${mea_quantity} else 0 end ;;
  }


  measure: markdown_quantity_sold {
    group_label: "Quantity metrics"
    description: "Aggregate Quantity of Units Sold at Discount"
    type: sum
    value_format_name: decimal_0
    sql: ${count_markdown_items} ;;
  }

  measure: quantity_sold_onFP {
    group_label: "Quantity metrics"
    label: "Full Price Quantity Sold"
    description: "Aggregate Quantity of Units Sold on FP"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${discountamt_usd} = 0 THEN (${mea_quantity})
      ELSE null END ;;
  }

  #------------COGS--------------
  measure: cogs_usd {
    type: sum
    hidden: no
    group_label: "Cost of Goods Sold"
    label: "Cost of Goods Sold USD"
    description: "Average unit cost (in USD) multiplied by quantity sold"
    sql:ABS(${avg_unit_cost})*${mea_quantity} ;;
    value_format_name: usd
  }

  measure: COGSUSD_onFP{
    group_label: "Cost of Goods Sold"
    description: "Cost (USD) of items sold at full price (no discount)"
    label: "COGS USD on Full Price"
    type: sum
    value_format_name: usd
    sql: CASE WHEN ${discountamt_usd} = 0 or  ${discountamt_usd} is NULL THEN ABS(${avg_unit_cost})*${mea_quantity}
      ELSE null END ;;
  }
  measure: COGSUSD_onMD{
    group_label: "Cost of Goods Sold"
    description: "Cost (USD) of items sold at discount"
    label: "COGS USD on Markdown"
    type: sum
    value_format_name: usd
    sql: CASE WHEN ${discountamt_usd} <> 0 THEN ABS(${avg_unit_cost})*${mea_quantity}
      ELSE null END ;;
  }

  measure: total_value_received{
    label: "Total Value Received"
    type: sum
    value_format_name: usd
    sql: ${value_received}
      ;;
  }




}
