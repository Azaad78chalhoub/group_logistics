view: order_sums2 {
  derived_table: {
    sql:
      WITH sales_sum AS(
       SELECT
      "order" as type,
      sales.bk_businessdate AS date,
      bk_storeid as location,
      bk_productid AS item,
      0 AS qty_ordered,
      0 AS qty_received,
      0 AS received_cost,
      SUM(mea_quantity) AS quantity_sold,
      SUM(CASE WHEN (discountamt_usd IS NULL OR discountamt_usd = 0) THEN mea_quantity ELSE 0 END) quantity_sold_fp,
      SUM(CASE WHEN (discountamt_usd <> 0) THEN mea_quantity ELSE 0 END) quantity_sold_md,
      SUM(IFNULL(av_cost_local * mea_quantity,0)) AS cogs
      FROM `chb-prod-supplychain-data.prod_supply_chain.factretailsales`  sales
    WHERE
     atr_trandate >= DATE({% date_start sales_season %}) AND atr_trandate <= IFNULL(DATE({% date_end sales_season %}),current_date())
      GROUP BY
      type,
      date,
      location,
      item
     ),
     orders_sum AS(
     SELECT
      "order" as type,
      DATE(written_date) as date,
      location,
      orders.item,
      SUM(IFNULL(qty_ordered,0)) AS qty_ordered,
      SUM(IFNULL(qty_received,0)) AS qty_received,
      SUM(IFNULL(qty_received * unit_cost_usd,0)) AS received_cost,
      0 AS quantity_sold,
      0 AS quantity_sold_fp,
      0 AS quantity_sold_md,
      0 AS cogs
    FROM
      `chb-prod-supplychain-data.prod_supply_chain.orders` orders
    WHERE
    written_date  >= {% date_start season %} AND written_date <=  IFNULL({% date_end season %},CAST(current_date() AS TIMESTAMP))

     AND item
    {% if exclude_sold_before._parameter_value == "'exclude'" %}
     NOT IN (SELECT bk_productid AS item FROM `chb-prod-supplychain-data.prod_supply_chain.factretailsales`
    WHERE
     atr_trandate <= DATE({% date_start season %})
      )
    {% else %}
     IS NOT NULL
    {% endif %}
         AND item
    {% if exclude_bough_after._parameter_value == "'exclude'" %}
     NOT IN (SELECT item FROM
      `chb-prod-supplychain-data.prod_supply_chain.orders` orders
    WHERE
    written_date  <= {% date_start season %} OR
    written_date  >= {% date_end season %}
    )
    {% else %}
     IS NOT NULL
    {% endif %}
    GROUP BY
      type,
      written_date,
      location,
      item
     )
    SELECT * FROM orders_sum
    UNION ALL
    SELECT * FROM sales_sum
    WHERE item IN(SELECT
      item FROM orders_sum)
        ;;
  }

  parameter: exclude_sold_before {
    type: string
    allowed_value: {
      label: "Yes"
      value: "exclude"
    }
    allowed_value: {
      label: "No"
      value: "keep"
    }
  }

  parameter: exclude_bough_after {
    type: string
    label: "Exclude Bought Before and After"
    allowed_value: {
      label: "Yes"
      value: "exclude"
    }
    allowed_value: {
      label: "No"
      value: "keep"
    }
  }


  dimension_group: order {
    type: time
    timeframes: [
      raw,
      date,
      day_of_month,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }
  measure: last_updated_date {
    type: date
    label: "Last refresh date"
    sql: MAX(${order_raw}) ;;
    convert_tz: no
  }

  filter: season {
    type: date
    label: "Purchase Season"
    default_value: "after 2019/03/01"
  }

  parameter: parameter2 {
    type: yesno
    label: "Only Purchased Products"
  }


  filter: sales_season {
    type: date
    label: "Sales Season"
    default_value: "after 2019/10/01"
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${order_raw}, " - ", ${type}," - ",${location}, " - ", ${item} ) ;;
  }

  dimension: type {
    type: string
    hidden: no
    sql: ${TABLE}.type ;;
  }


  dimension: item {
    type: string
    description: "Product number from Oracle RMS"
    label: "Item No"
    primary_key: no
    sql: CAST(${TABLE}.item AS STRING) ;;
  }


  dimension_group: pop_no_tz {
    sql: ${order_raw} ;;
  }

  dimension: location {
    type: number
    label: "Location Code"
    value_format: "0"
    primary_key: no
    sql: ${TABLE}.location ;;
  }

  dimension: qty_received {
    type: number
    primary_key: no
    hidden: yes
    sql: ${TABLE}.qty_received ;;
  }

  dimension: qty_ordered {
    type: number
    primary_key: no
    hidden: yes
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: quantity_sold {
    type: number
    primary_key: no
    hidden: yes
    sql: ${TABLE}.quantity_sold ;;
  }

  dimension: quantity_sold_fp {
    type: number
    primary_key: no
    hidden: yes
    sql: ${TABLE}.quantity_sold_fp ;;
  }

  dimension: cogs {
    type: number
    hidden: yes
    sql: ${TABLE}.cogs ;;
  }

  dimension: received_cost {
    type: number
    hidden: yes
    sql: ${TABLE}.received_cost ;;
  }

  dimension: quantity_sold_md {
    type: number
    primary_key: no
    hidden: yes
    sql: ${TABLE}.quantity_sold_md ;;
  }

  dimension: app_received_cost {
    type: number
    hidden: yes
    sql: ${awac_str.awac_usd}*${qty_received} ;;
  }

  measure: total_quantity_ordered {
    label: "Quantity Ordered"
    type: sum
    description: "Aggregate quantity of units ordered"
    sql: ${qty_ordered} ;;
  }

  measure: total_quantity_received {
    label: "Quantity Received"
    type: sum
    description: "Aggregate quantity of units received"
    sql: ${qty_received} ;;
  }

  measure: total_quantity_sold {
    label: "Total Quantity Sold"
    description: "Aggregate quantity of units sold"
    type: sum
    sql: ${quantity_sold} ;;
  }

  measure: total_quantity_sold_on_fp {
    label: "Full Price quantity sold"
    description: "Aggregate quantity of units sold on Full Price"
    type: sum
    sql: ${quantity_sold_fp} ;;
  }

  measure: total_quantity_sold_on_md {
    label: "Markdown Quantity Sold"
    description: "Aggregate quantity of units sold on Discount"
    type: sum
    sql: ${quantity_sold_md} ;;
  }

  measure: sell_through_rate_on_ap_value {
    type: number
    hidden: no
    value_format_name: percent_2
    label: "ST Rate % on App. Value"
    sql: SAFE_DIVIDE(SUM(${awac_str.awac_usd}*${quantity_sold}),SUM(${awac_str.awac_usd}*${qty_received})) ;;
  }

  measure: sell_through_rate_on_received {
    type: number
    hidden: yes
    value_format_name: percent_2
    label: "ST Rate % on Received"
    description: "Aggregate quantity of units sold divided by the aggregate quantity of units received"
    sql: SAFE_DIVIDE(SUM(${quantity_sold}),SUM(${qty_received})) ;;
  }

  measure: sell_through_rate_on_purchase {
    type: number
    hidden: yes
    value_format_name: percent_2
    label: "ST Rate % on Purchased"
    description: "Aggregate quantity of units sold divided by the aggregate quantity of units ordered"
    sql: SAFE_DIVIDE(SUM(${quantity_sold}),SUM(${qty_ordered})) ;;
  }

  measure: sell_through_rate_fp_on_received {
    type: number
    hidden: yes
    value_format_name: percent_2
    label: "ST Rate % FP on Received"
    description: "Aggregate quantity of units sold at Full Price divided by the aggregate quantity of units received"
    sql: SAFE_DIVIDE(SUM(${quantity_sold_fp}),SUM(${qty_received})) ;;
  }

  measure: sell_through_rate_fp_on_purchase {
    type: number
    hidden: yes
    value_format_name: percent_2
    label: "ST Rate % FP on Purchased"
    description: "Aggregate quantity of units sold at Full Price divided by the aggregate quantity of units ordered"
    sql: SAFE_DIVIDE(SUM(${quantity_sold_fp}),SUM(${qty_ordered})) ;;
  }

  measure: sell_through_rate_md_on_received {
    type: number
    hidden: yes
    value_format_name: percent_2
    label: "ST Rate % MD on Received"
    description: "Aggregate quantity of units sold at discount divided by the aggregate quantity of units received"
    sql: SAFE_DIVIDE(SUM(${quantity_sold_md}),SUM(${qty_received})) ;;
  }

  measure: sell_through_rate_md_on_purchase {
    type: number
    hidden: yes
    value_format_name: percent_2
    label: "ST Rate % MD on Purchased"
    description: "Aggregate quantity of units sold at discount divided by the aggregate quantity of units ordered"
#     sql: IFNULL(SAFE_DIVIDE(SUM(${quantity_sold_md}),SUM(${qty_ordered})) );;
    sql: (SAFE_DIVIDE(SUM(${quantity_sold_md}),SUM(${qty_ordered})) );;

  }


#######################################
#####         NDT TEMPS           #####
#######################################


  measure: sum_qty_received {
    type: sum
    value_format_name: decimal_0
    label: "Total Quantity Received"
    sql: ${qty_received};;
  }

  measure: sum_cost_received {
    type: sum
    value_format_name: usd
    label: "Total Cost Received"
    sql: ${received_cost};;
  }

  measure: sum_app_cost_received {
    type: sum
    value_format_name: usd
    label: "Approximate Cost Received"
    sql: ${app_received_cost};;
  }

  measure: sum_cogs {
    type: sum
    value_format_name: usd
    label: "Aproximate COGS"
    sql: ${awac_str.awac_usd}*${quantity_sold};;
  }

  measure: sum_cogs_md {
    type: sum
    value_format_name: usd
    label: "Aproximate COGS MD"
    sql: ${awac_str.awac_usd}*${quantity_sold_md};;
  }

  measure: sum_cogs_fp {
    type: sum
    value_format_name: usd
    label: "Aproximate COGS FP"
    sql: ${awac_str.awac_usd}*${quantity_sold_fp};;
  }


}
